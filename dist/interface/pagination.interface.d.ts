export type SortOrder = 'ASC' | 'DESC';
export interface PaginationLinkResponse {
    first?: string;
    last?: string;
    prev?: string;
    next?: string;
}
export interface PaginationResponse {
    total: number;
    page: number;
    pages: number;
    links: PaginationLinkResponse;
}
export interface PaginationOptions {
    page: number;
    offset: number;
    limit: number;
    order: {
        [key: string]: SortOrder;
    };
}
