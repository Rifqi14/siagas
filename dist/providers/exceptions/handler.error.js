"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const http_code_enum_1 = require("../../types/http_code.enum");
const zod_1 = require("zod");
const response_1 = __importDefault(require("../response"));
const api_error_1 = __importDefault(require("./api.error"));
class ErrorHandler {
    handleError(error, response) {
        if (response && (error instanceof zod_1.ZodError || error instanceof api_error_1.default)) {
            this.handleResponseError(error, response);
        }
        else {
            this.handleUnresponseError(error, response);
        }
    }
    handleResponseError(error, response) {
        if (error instanceof zod_1.ZodError) {
            response_1.default.validation(error).create(response);
        }
        else {
            response_1.default
                .error(error, error.httpCode, error.message)
                .create(response);
        }
    }
    handleUnresponseError(error, response) {
        // console.log('Response : ', response);
        if (response) {
            response_1.default
                .error(error, http_code_enum_1.HttpCode.INTERNAL_SERVER_ERROR, error.message)
                .create(response);
        }
        console.log('Application encountered an untrusted error.');
        console.log(error);
    }
}
exports.default = new ErrorHandler();
