"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
class ApiError extends Error {
    constructor(args) {
        super(args.message);
        Object.setPrototypeOf(this, new.target.prototype);
        this.name = args.name || 'Error';
        this.message = args.message;
        this.httpCode = args.httpCode;
        Error.captureStackTrace(this);
    }
}
exports.default = ApiError;
