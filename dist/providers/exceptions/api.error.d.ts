interface ApiErrorArgs {
    name?: string;
    httpCode: number;
    message: string;
}
export default class ApiError extends Error {
    readonly name: string;
    readonly httpCode: number;
    readonly message: string;
    constructor(args: ApiErrorArgs);
}
export {};
