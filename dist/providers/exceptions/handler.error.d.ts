import { Response } from 'express';
import { ZodError } from 'zod';
import ApiError from './api.error';
declare class ErrorHandler {
    handleError(error: Error | ApiError | ZodError, response?: Response): void;
    private handleResponseError;
    private handleUnresponseError;
}
declare const _default: ErrorHandler;
export default _default;
