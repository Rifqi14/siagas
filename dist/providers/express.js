"use strict";
var __createBinding = (this && this.__createBinding) || (Object.create ? (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    var desc = Object.getOwnPropertyDescriptor(m, k);
    if (!desc || ("get" in desc ? !m.__esModule : desc.writable || desc.configurable)) {
      desc = { enumerable: true, get: function() { return m[k]; } };
    }
    Object.defineProperty(o, k2, desc);
}) : (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    o[k2] = m[k];
}));
var __setModuleDefault = (this && this.__setModuleDefault) || (Object.create ? (function(o, v) {
    Object.defineProperty(o, "default", { enumerable: true, value: v });
}) : function(o, v) {
    o["default"] = v;
});
var __importStar = (this && this.__importStar) || function (mod) {
    if (mod && mod.__esModule) return mod;
    var result = {};
    if (mod != null) for (var k in mod) if (k !== "default" && Object.prototype.hasOwnProperty.call(mod, k)) __createBinding(result, mod, k);
    __setModuleDefault(result, mod);
    return result;
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const bodyParser = __importStar(require("body-parser"));
const cors_1 = __importDefault(require("cors"));
const express_1 = __importDefault(require("express"));
const swagger_ui_express_1 = __importDefault(require("swagger-ui-express"));
const data_source_1 = require("../data-source");
const config_1 = __importDefault(require("./config"));
const handler_error_1 = __importDefault(require("./exceptions/handler.error"));
const routes_1 = __importDefault(require("./routes"));
const swagger_json_1 = __importDefault(require("../docs/swagger.json"));
const response_1 = __importDefault(require("./response"));
const api_error_1 = __importDefault(require("./exceptions/api.error"));
const http_code_enum_1 = require("../types/http_code.enum");
const seed_1 = require("../data/seed/seed");
const express_handlebars_1 = require("express-handlebars");
const path_1 = require("path");
class Express {
    constructor() {
        this.express = (0, express_1.default)();
        this.express.use(bodyParser.urlencoded({ extended: false }));
        this.express.use(bodyParser.json());
        this.express.use((0, cors_1.default)());
    }
    mountRoutes() {
        this.express = routes_1.default.init(this.express);
    }
    mountMiddleware() {
        this.express.use(express_1.default.static('public'));
        swagger_json_1.default.components.securitySchemes = {
            bearer: {
                type: 'http',
                scheme: 'bearer',
                bearerFormat: 'JWT',
            },
        };
        swagger_json_1.default.servers.unshift({
            url: config_1.default.load().app_url,
            description: 'Development environment',
        });
        this.express.use('/docs', swagger_ui_express_1.default.serve, swagger_ui_express_1.default.setup(swagger_json_1.default, {
            swaggerOptions: {
                docExpansion: 'none',
                persistAuthorization: true,
                tagsSorter: 'alpha',
            },
        }));
    }
    init() {
        const port = config_1.default.load().app_port;
        const host = config_1.default.load().app_url;
        this.mountMiddleware();
        this.mountRoutes();
        data_source_1.AppDataSource.initialize().then(async (ds) => {
            await (0, seed_1.seed)(ds);
        });
        this.express.use((err, req, res, next) => {
            handler_error_1.default.handleError(err, res);
        });
        this.express.engine('hbs', (0, express_handlebars_1.engine)({ extname: 'hbs' }));
        this.express.set('view engine', 'hbs');
        this.express.set('views', (0, path_1.join)(__dirname, '..', 'views'));
        this.express.use((req, res, next) => {
            response_1.default
                .error(new api_error_1.default({
                httpCode: http_code_enum_1.HttpCode.NOT_FOUND,
                message: 'Route not found',
            }), http_code_enum_1.HttpCode.NOT_FOUND, 'Route not found!')
                .create(res);
        });
        this.express.listen(parseInt(port), () => {
            console.log(`App started on ${host}`);
        });
    }
}
exports.default = new Express();
