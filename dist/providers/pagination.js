"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
class Pagination {
    constructor() {
        this.limit = 10;
        this.maxLimit = 100;
        this.offset = 0;
        this.page = 1;
        this.pages = 1;
        this.options = (page = 0, length = 0, order = [['created_at', 'DESC']]) => {
            if (page < 0)
                page = 1;
            if (length <= 0 || length > this.maxLimit)
                length = this.limit;
            const offset = (page - 1) * length;
            const sort = this.buildOrder(order);
            return {
                page,
                offset: offset < 0 ? 0 : offset,
                limit: length,
                order: sort
            };
        };
        this.buildOrder = (orders) => {
            let sort = {
                created_at: 'DESC'
            };
            orders.forEach(order => {
                var _a;
                sort = Object.assign(Object.assign({}, sort), { [order[0]]: (_a = order[1]) !== null && _a !== void 0 ? _a : 'DESC' });
            });
            return sort;
        };
        this.build = (url, page, length, total) => {
            let lastPage = 0;
            if (total > 0)
                lastPage = Math.ceil(total / length);
            const next = +page + +1;
            const links = {
                first: url.replace(/page=([^&]*)/, `page=1`),
                last: url.replace(/page=([^&]*)/, `page=${lastPage <= 0 ? 1 : lastPage}`),
                prev: page > 1 ? url.replace(/page=([^&]*)/, `page=${page - 1}`) : undefined,
                next: page < lastPage
                    ? url.replace(/page=([^&]*)/, `page=${next}`)
                    : undefined
            };
            return {
                total,
                page: +page,
                pages: lastPage,
                links
            };
        };
    }
}
exports.default = new Pagination();
