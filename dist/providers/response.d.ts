import { Response } from 'express';
import { IResponse } from '../interface/response.interface';
import { ZodError } from 'zod';
import ApiError from './exceptions/api.error';
declare class ResponseProvider<T, P> implements IResponse<T, P> {
    private response;
    success(data: T, code?: number, message?: string): this;
    pagination(data: T, pagination: P, code?: number, message?: string): this;
    error(error: Error | ApiError, code?: number, message?: string): this;
    private buildErrorValidationMessage;
    validation(error: ZodError<any>, code?: number, message?: string): this;
    create(res: Response<any, Record<string, any>>): void;
}
declare const _default: ResponseProvider<unknown, unknown>;
export default _default;
