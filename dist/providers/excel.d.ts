/// <reference types="node" />
import { AddWorksheetOptions, Column, Style } from 'exceljs';
import { Stream } from 'stream';
interface WriteArgs {
    filename: string;
    stream: Stream;
    sheetname: string;
    dateformat: string;
}
type AtLeast<T, K extends keyof T> = Partial<T> & Pick<T, K>;
export declare class Excel {
    private workbook;
    private activeWs;
    private rowHeader;
    setProperties: (creator?: string) => this;
    setWorksheet: (worksheet?: string, opts?: Partial<AddWorksheetOptions>) => this;
    setHeader: (header: Array<Partial<Column>>) => this;
    styleSelectedCell: (start: number, end: number) => this;
    styleHeaderCell: (style: Partial<Style>) => this;
    setStyleAll: (style: Partial<Style>) => this;
    addRow: (data: unknown[]) => this;
    private writeXlsx;
    write: (format: 'xlsx' | 'csv', args: AtLeast<WriteArgs, 'stream' | 'filename'>) => Promise<void>;
}
export {};
