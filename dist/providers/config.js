"use strict";
var __createBinding = (this && this.__createBinding) || (Object.create ? (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    var desc = Object.getOwnPropertyDescriptor(m, k);
    if (!desc || ("get" in desc ? !m.__esModule : desc.writable || desc.configurable)) {
      desc = { enumerable: true, get: function() { return m[k]; } };
    }
    Object.defineProperty(o, k2, desc);
}) : (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    o[k2] = m[k];
}));
var __setModuleDefault = (this && this.__setModuleDefault) || (Object.create ? (function(o, v) {
    Object.defineProperty(o, "default", { enumerable: true, value: v });
}) : function(o, v) {
    o["default"] = v;
});
var __importStar = (this && this.__importStar) || function (mod) {
    if (mod && mod.__esModule) return mod;
    var result = {};
    if (mod != null) for (var k in mod) if (k !== "default" && Object.prototype.hasOwnProperty.call(mod, k)) __createBinding(result, mod, k);
    __setModuleDefault(result, mod);
    return result;
};
Object.defineProperty(exports, "__esModule", { value: true });
const dotenv = __importStar(require("dotenv"));
const path = __importStar(require("path"));
class Config {
    static load() {
        dotenv.config({ path: path.join(__dirname, '../../.env') });
        const res = {
            app_name: process.env.APP_NAME || 'SIAGAS',
            app_url: process.env.APP_URL || 'http://localhost',
            app_port: process.env.APP_PORT || '3000',
            database_host: process.env.DATABASE_HOST || 'localhost',
            database_name: process.env.DATABASE_NAME || 'siagas',
            database_user: process.env.DATABASE_USER || 'siagas',
            database_password: process.env.DATABASE_PASSWORD || 'siagaspass',
            database_port: process.env.DATABASE_PORT || '5434',
            token_expired_in_day: parseInt(process.env.TOKEN_EXPIRED_IN_DAY || '1'),
            private_key_location: process.env.PRIVATE_KEY_LOCATION ||
                path.join(__dirname, '..', '..', 'public', 'file', 'private.key'),
            public_key_location: process.env.PUBLIC_KEY_LOCATION ||
                path.join(__dirname, '..', '..', 'public', 'file', 'public.key')
        };
        return res;
    }
    static init(_express) {
        _express.locals.app = this.load();
        return _express;
    }
}
exports.default = Config;
