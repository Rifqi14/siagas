declare class Hash {
    private salt;
    generate(value: string): string;
    compare(plain: string, hash: string): boolean;
}
export default Hash;
