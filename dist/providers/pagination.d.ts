import { PaginationOptions, PaginationResponse } from '../interface/pagination.interface';
declare class Pagination {
    limit: number;
    maxLimit: number;
    offset: number;
    page: number;
    pages: number;
    options: (page?: number, length?: number, order?: [[string, string]]) => PaginationOptions;
    private buildOrder;
    build: (url: string, page: number, length: number, total: number) => PaginationResponse;
}
declare const _default: Pagination;
export default _default;
