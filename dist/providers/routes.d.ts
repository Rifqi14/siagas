import { Application } from "express";
declare class Route {
    static init(express: Application): Application;
}
export default Route;
