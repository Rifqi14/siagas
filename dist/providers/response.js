"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
class ResponseProvider {
    constructor() {
        this.buildErrorValidationMessage = (error) => {
            const validation = error.issues.map(issue => {
                return {
                    object: issue.path[1].toString(),
                    message: issue.message
                };
            });
            return validation;
        };
    }
    success(data, code = 200, message = 'Ok!') {
        const response = {
            status: 'success',
            isSuccess: true,
            code: code,
            message: message,
            data: data
        };
        this.response = response;
        return this;
    }
    pagination(data, pagination, code = 200, message = 'Ok!') {
        const response = {
            status: 'success',
            isSuccess: true,
            code: code,
            message: message,
            data: data,
            pagination: pagination
        };
        this.response = response;
        return this;
    }
    error(error, code = 400, message = 'Error!') {
        const response = {
            status: 'success',
            isSuccess: true,
            code: code,
            message: message,
            error: error
        };
        this.response = response;
        return this;
    }
    validation(error, code = 422, message = 'Error validation!') {
        const response = {
            status: 'validation',
            isSuccess: false,
            code: code,
            message: message,
            error: this.buildErrorValidationMessage(error)
        };
        this.response = response;
        return this;
    }
    create(res) {
        res.status(this.response.code).send(this.response);
    }
}
exports.default = new ResponseProvider();
