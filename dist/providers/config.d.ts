import { Application } from 'express';
import { ConfigType } from '../types/config.types';
declare class Config {
    static load(): ConfigType;
    static init(_express: Application): Application;
}
export default Config;
