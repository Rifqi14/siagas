import { Application } from 'express';
declare class Express {
    express: Application;
    constructor();
    private mountRoutes;
    private mountMiddleware;
    init(): void;
}
declare const _default: Express;
export default _default;
