"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.Excel = void 0;
const exceljs_1 = require("exceljs");
const config_1 = __importDefault(require("./config"));
class Excel {
    constructor() {
        this.workbook = new exceljs_1.Workbook();
        this.setProperties = (creator = config_1.default.load().app_name) => {
            this.workbook.creator = creator;
            this.workbook.created = new Date();
            return this;
        };
        this.setWorksheet = (worksheet, opts) => {
            this.activeWs = this.workbook.addWorksheet(worksheet, opts);
            return this;
        };
        this.setHeader = (header) => {
            this.activeWs.columns = header;
            this.rowHeader = this.activeWs.rowCount;
            return this;
        };
        this.styleSelectedCell = (start, end) => {
            this.activeWs.getCell(start, end).style;
            return this;
        };
        this.styleHeaderCell = (style) => {
            var _a;
            (_a = this.activeWs
                .getRows(this.rowHeader, this.activeWs.columnCount)) === null || _a === void 0 ? void 0 : _a.forEach((row, index, rows) => {
                row.eachCell((cell, colNumber) => {
                    cell.style = style;
                });
            });
            return this;
        };
        this.setStyleAll = (style) => {
            this.activeWs.eachRow({ includeEmpty: false }, (row, rowNumber) => {
                row.eachCell({ includeEmpty: false }, (cell, colNumber) => {
                    cell.style = style;
                });
            });
            return this;
        };
        this.addRow = (data) => {
            this.activeWs.addRows(data);
            return this;
        };
        this.writeXlsx = async (args) => {
            await this.workbook.xlsx.write(args.stream, { filename: args.filename });
        };
        this.write = async (format, args) => {
            switch (format) {
                case 'xlsx':
                    await this.writeXlsx(args);
                    break;
                default:
                    await this.writeXlsx(args);
                    break;
            }
        };
    }
}
exports.Excel = Excel;
