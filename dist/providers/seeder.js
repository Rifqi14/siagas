"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.seedRole = void 0;
const data_source_1 = require("../data-source");
const Role_entity_1 = __importDefault(require("../entity/Role.entity"));
const role_json_1 = __importDefault(require("../data/seeder/role.json"));
const seedRole = () => {
    const repo = data_source_1.AppDataSource.getRepository(Role_entity_1.default);
    role_json_1.default.forEach(async (value) => {
        const role = await repo.findOneBy({ name: value.name });
        if (role) {
            return;
        }
        await repo.insert({ is_creator: value.is_creator, name: value.name }).then(val => {
            console.log(`Success seeder ${value.name} data`);
        }, () => {
            console.log(`Error seeder ${value.name} data`);
        });
    });
};
exports.seedRole = seedRole;
