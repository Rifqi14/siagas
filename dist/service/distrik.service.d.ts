import { Distrik } from "../entity/Distrik.entity";
import { CreateDistrikRequest, GetDistrikRequest } from "../schema/distrik.schema";
import { PaginationResponse, SuccessResponse } from "../types/response.type";
import { PaginationResponse as PaginationResponseInterface } from "../interface/pagination.interface";
export default class DistrikService {
    private repo;
    constructor();
    create(req: CreateDistrikRequest): Promise<SuccessResponse<Distrik> | Distrik>;
    detail(id: number): Promise<SuccessResponse<Distrik> | Distrik>;
    get(url: string | undefined, req: GetDistrikRequest): Promise<PaginationResponse<PaginationResponseInterface, Array<Distrik>> | {
        paging: PaginationResponseInterface;
        res: Array<Distrik>;
    }>;
    delete(id: number): Promise<SuccessResponse<string> | boolean>;
    update(id: number, req: CreateDistrikRequest): Promise<SuccessResponse<string> | boolean>;
}
