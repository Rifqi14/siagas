"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const tsoa_1 = require("tsoa");
const jwt_middleware_1 = require("../middleware/jwt.middleware");
const config_1 = __importDefault(require("../providers/config"));
const api_error_1 = __importDefault(require("../providers/exceptions/api.error"));
const hash_1 = __importDefault(require("../providers/hash"));
const role_repository_1 = __importDefault(require("../repository/role.repository"));
const user_repository_1 = __importDefault(require("../repository/user.repository"));
const http_code_enum_1 = require("../types/http_code.enum");
let AuthenticationService = class AuthenticationService {
    constructor() {
        this.repo = new user_repository_1.default();
    }
    async login(req) {
        const userPass = await this.repo
            .createQueryBuilder('u')
            .select('u.id', 'id')
            .addSelect('u.password')
            .where(`u.username = :username`, { username: req.username })
            .getOne();
        if (!userPass || !new hash_1.default().compare(req.password, userPass.password)) {
            throw new api_error_1.default({
                httpCode: http_code_enum_1.HttpCode.UNAUTHORIZED,
                message: 'Unauthorized!',
            });
        }
        const user = await this.repo.findOneBy({ username: req.username });
        if (!user) {
            throw new api_error_1.default({
                httpCode: http_code_enum_1.HttpCode.UNAUTHORIZED,
                message: 'User not found!',
            });
        }
        const role = await new role_repository_1.default().findOneByOrFail({
            id: user.role_id,
        });
        const token = (0, jwt_middleware_1.signJwt)({ user_id: user.id, username: user.username, role: role }, req.remember_me);
        const now = new Date();
        const expiredAt = now.setDate(now.getDate() + config_1.default.load().token_expired_in_day);
        const res = {
            token: token,
            role: role,
            user: user,
            token_expired_at: req.remember_me ? null : expiredAt,
        };
        return res;
    }
    async checkEmail(email) {
        const user = await this.repo.findOneBy({ email: email });
        if (!user) {
            return false;
        }
        return true;
    }
    async getMe() {
        throw new api_error_1.default({ httpCode: http_code_enum_1.HttpCode.OK, message: 'Success' });
    }
};
__decorate([
    (0, tsoa_1.Post)('/login'),
    __param(0, (0, tsoa_1.Body)()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Object]),
    __metadata("design:returntype", Promise)
], AuthenticationService.prototype, "login", null);
__decorate([
    (0, tsoa_1.Get)('/reserved_email'),
    __param(0, (0, tsoa_1.Query)()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [String]),
    __metadata("design:returntype", Promise)
], AuthenticationService.prototype, "checkEmail", null);
__decorate([
    (0, tsoa_1.Get)('/me'),
    (0, tsoa_1.Security)('bearer'),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", []),
    __metadata("design:returntype", Promise)
], AuthenticationService.prototype, "getMe", null);
AuthenticationService = __decorate([
    (0, tsoa_1.Route)('/authentication'),
    (0, tsoa_1.Tags)('Authentication'),
    __metadata("design:paramtypes", [])
], AuthenticationService);
exports.default = AuthenticationService;
