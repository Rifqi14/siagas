import Regions from '../entity/Region.entity';
import { CreateRegionRequest, GetRegionRequest } from '../schema/region.schema';
import { PaginationResponse, SuccessResponse } from '../types/response.type';
import { PaginationResponse as PaginationResponseInterface } from '../interface/pagination.interface';
export default class RegionService {
    private repo;
    constructor();
    create(req: CreateRegionRequest): Promise<SuccessResponse<Regions> | Regions>;
    detail(id: number): Promise<SuccessResponse<Regions> | Regions>;
    get(url: string | undefined, req: GetRegionRequest): Promise<PaginationResponse<PaginationResponseInterface, Array<Regions>> | {
        paging: PaginationResponseInterface;
        res: Array<Regions>;
    }>;
    delete(id: number): Promise<SuccessResponse<string> | boolean>;
    update(id: number, req: CreateRegionRequest): Promise<SuccessResponse<string> | boolean>;
}
