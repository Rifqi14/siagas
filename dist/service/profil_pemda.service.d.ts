/// <reference types="multer" />
import { ProfilPemda } from '../entity/ProfilePemda.entity';
import { PaginationResponse, SuccessResponse } from '../types/response.type';
import { ListProfilPemda, ProfilPemdaFilterDownload, ProfilPemdaParams } from '../schema/profil_pemda.schema';
import { PaginationResponse as PaginationResponseInterface } from '../interface/pagination.interface';
import { PaginationData } from '../schema/base.schema';
import { DownloadType } from '../types/lib';
export default class ProfilPemdaService {
    private repo;
    private fileService;
    private exportService;
    download(type: DownloadType, username: string | undefined, req: ProfilPemdaFilterDownload): Promise<string>;
    downloadProfil(id: number, type: DownloadType): Promise<string>;
    create(nama_pemda: string, nama_daerah?: string, opd_yang_menangani?: string, alamat_pemda?: string, email?: string, no_telpon?: string, nama_admin?: string, file?: Express.Multer.File, username?: string): Promise<SuccessResponse<ProfilPemda> | ProfilPemda>;
    detail(id: number): Promise<SuccessResponse<ProfilPemda> | ProfilPemda>;
    get(url: string | undefined, username: string | undefined, req: ProfilPemdaParams): Promise<PaginationResponse<PaginationResponseInterface, ListProfilPemda> | PaginationData<ListProfilPemda>>;
    delete(id: number): Promise<SuccessResponse<string> | boolean>;
    update(id: number, nama_pemda: string, nama_daerah?: string, opd_yang_menangani?: string, alamat_pemda?: string, email?: string, no_telpon?: string, nama_admin?: string, file?: Express.Multer.File, username?: string): Promise<SuccessResponse<string> | boolean>;
}
