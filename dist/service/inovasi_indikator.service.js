"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const api_error_1 = __importDefault(require("../providers/exceptions/api.error"));
const pagination_1 = __importDefault(require("../providers/pagination"));
const inovasi_indikator_repository_1 = __importDefault(require("../repository/inovasi_indikator.repository"));
const http_code_enum_1 = require("../types/http_code.enum");
const tsoa_1 = require("tsoa");
let InovasiIndikatorService = class InovasiIndikatorService {
    constructor() {
        this.repo = new inovasi_indikator_repository_1.default();
    }
    async get(inovasi_id, url = '', username = '', req) {
        const query = this.repo
            .createQueryBuilder(`ii`)
            .leftJoinAndSelect(`ii.indikator`, 'i')
            .leftJoinAndSelect(`ii.files`, 'f')
            .where(`ii.inovasi_id = :inovasi_id`, { inovasi_id });
        const total = await query.getCount();
        const { limit, offset, page, order } = pagination_1.default.options(req.page, req.limit);
        query.limit(limit).offset(offset);
        for (const key in order) {
            query.addOrderBy(`ii.${key}`, order[key]);
        }
        const res = await query.getMany();
        let paging = pagination_1.default.build(url, page, limit, total);
        return { paging, res };
    }
    async detail(inovasi_id, indikator_id) {
        const res = await this.repo.findOneBy({ inovasi_id, indikator_id });
        if (!res) {
            throw new api_error_1.default({
                httpCode: http_code_enum_1.HttpCode.NOT_FOUND,
                message: 'Data not found'
            });
        }
        return res;
    }
};
__decorate([
    (0, tsoa_1.Get)('/{inovasi_id}/indikator'),
    __param(1, (0, tsoa_1.Query)('url')),
    __param(1, (0, tsoa_1.Hidden)()),
    __param(2, (0, tsoa_1.Query)()),
    __param(2, (0, tsoa_1.Hidden)()),
    __param(3, (0, tsoa_1.Queries)()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [String, String, String, Object]),
    __metadata("design:returntype", Promise)
], InovasiIndikatorService.prototype, "get", null);
__decorate([
    (0, tsoa_1.Get)('/{inovasi_id}/indikator/{indikator_id}'),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Number, Number]),
    __metadata("design:returntype", Promise)
], InovasiIndikatorService.prototype, "detail", null);
InovasiIndikatorService = __decorate([
    (0, tsoa_1.Route)('/inovasi_pemerintah_daerah'),
    (0, tsoa_1.Tags)('Database Inovasi Daerah | Inovasi Daerah | Inovasi Indikator'),
    (0, tsoa_1.Security)('bearer')
], InovasiIndikatorService);
exports.default = InovasiIndikatorService;
