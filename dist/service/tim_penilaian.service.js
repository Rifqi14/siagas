"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const api_error_1 = __importDefault(require("../providers/exceptions/api.error"));
const tim_penilaian_repository_1 = __importDefault(require("../repository/tim_penilaian.repository"));
const http_code_enum_1 = require("../types/http_code.enum");
const tsoa_1 = require("tsoa");
const pagination_1 = __importDefault(require("../providers/pagination"));
const User_entity_1 = __importDefault(require("../entity/User.entity"));
let TimPenilaianService = class TimPenilaianService {
    constructor() {
        this.repo = new tim_penilaian_repository_1.default();
    }
    async create(req, username = '') {
        const create = this.repo.create(Object.assign(Object.assign({}, req), { created_by: username, updated_by: username }));
        await this.repo.insert(create);
        return create;
    }
    async detail(id) {
        const res = await this.repo.findOneBy({ id: id });
        if (!res) {
            throw new api_error_1.default({
                httpCode: http_code_enum_1.HttpCode.NOT_FOUND,
                message: 'Data not found'
            });
        }
        return res;
    }
    async get(url = '', username = '', req) {
        var _a;
        const query = this.repo
            .createQueryBuilder('tp')
            .where(`lower(tp.nama) like lower(:q)`, { q: `%${(_a = req.q) !== null && _a !== void 0 ? _a : ''}%` });
        const total = await query.getCount();
        const { limit, offset, page, order } = pagination_1.default.options(req.page, req.limit);
        const user = await this.repo.manager
            .getRepository(User_entity_1.default)
            .findOneByOrFail({ username });
        if (user && user.role && user.role.is_super_admin === 't') {
            query.andWhere(`tp.created_by = (:created_by)`, { created_by: username });
        }
        query.limit(limit).offset(offset);
        for (const key in order) {
            query.addOrderBy(`tp.${key}`, order[key]);
        }
        const res = await query.getMany();
        let paging = pagination_1.default.build(url, page, limit, total);
        return { paging, res };
    }
    async delete(id) {
        const { affected } = await this.repo.delete({ id });
        if (affected && affected > 0) {
            return true;
        }
        return false;
    }
    async update(id, req, username = '') {
        const update = this.repo.create(Object.assign(Object.assign({}, req), { updated_by: username }));
        const { affected } = await this.repo.update(id, update);
        if (!affected) {
            return false;
        }
        return true;
    }
};
__decorate([
    (0, tsoa_1.Post)(''),
    __param(0, (0, tsoa_1.Body)()),
    __param(1, (0, tsoa_1.Query)()),
    __param(1, (0, tsoa_1.Hidden)()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Object, String]),
    __metadata("design:returntype", Promise)
], TimPenilaianService.prototype, "create", null);
__decorate([
    (0, tsoa_1.Get)('/{id}'),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Number]),
    __metadata("design:returntype", Promise)
], TimPenilaianService.prototype, "detail", null);
__decorate([
    (0, tsoa_1.Get)(''),
    __param(0, (0, tsoa_1.Query)('url')),
    __param(0, (0, tsoa_1.Hidden)()),
    __param(1, (0, tsoa_1.Query)()),
    __param(1, (0, tsoa_1.Hidden)()),
    __param(2, (0, tsoa_1.Queries)()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [String, String, Object]),
    __metadata("design:returntype", Promise)
], TimPenilaianService.prototype, "get", null);
__decorate([
    (0, tsoa_1.Delete)('/{id}'),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Number]),
    __metadata("design:returntype", Promise)
], TimPenilaianService.prototype, "delete", null);
__decorate([
    (0, tsoa_1.Patch)('/{id}'),
    __param(1, (0, tsoa_1.Body)()),
    __param(2, (0, tsoa_1.Query)()),
    __param(2, (0, tsoa_1.Hidden)()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Number, Object, String]),
    __metadata("design:returntype", Promise)
], TimPenilaianService.prototype, "update", null);
TimPenilaianService = __decorate([
    (0, tsoa_1.Route)('/tim_penilaian'),
    (0, tsoa_1.Tags)('Master | Tim Penilaian'),
    (0, tsoa_1.Security)('bearer')
], TimPenilaianService);
exports.default = TimPenilaianService;
