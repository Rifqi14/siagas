/// <reference types="multer" />
import { ListPemdaIndikatorFiles, PemdaIndikatorFileParams } from '../schema/pemda_indikator_file.schema';
import { PaginationResponse, SuccessResponse } from '../types/response.type';
import { PaginationResponse as PaginationResponseInterface } from '../interface/pagination.interface';
import { PaginationData } from '../schema/base.schema';
import { PemdaIndikatorFile } from '../entity/PemdaIndikatorFile.entity';
export default class PemdaIndikatorFileService {
    private repo;
    get(pemda_id: string, indikator_id: string, url: string | undefined, username: string | undefined, req: PemdaIndikatorFileParams): Promise<PaginationResponse<PaginationResponseInterface, ListPemdaIndikatorFiles> | PaginationData<ListPemdaIndikatorFiles>>;
    detail(pemda_id: number, indikator_id: number, file_id: number): Promise<SuccessResponse<PemdaIndikatorFile> | PemdaIndikatorFile>;
    upload(pemda_id: string, indikator_id: string, nomor_dokumen: string, tanggal_dokumen: string, tentang: string, dokumen: Express.Multer.File, nama_dokumen?: string, username?: string): Promise<SuccessResponse<PemdaIndikatorFile> | PemdaIndikatorFile>;
    delete(pemda_id: number, indikator_id: number, file_id: number): Promise<SuccessResponse<string> | boolean>;
}
