import { HasilReviewParams, HasilReviewResponse } from '../schema/hasil_review_inovasi.schema';
import { PaginationResponse, SuccessResponse } from '../types/response.type';
import { PaginationResponse as PaginationResponseInterface } from '../interface/pagination.interface';
import { PaginationData } from '../schema/base.schema';
import { ReviewInovasiDaerah } from '../entity/ReviewInovasiDaerah.entity';
import { DownloadType } from '../types/lib';
export default class HasilReviewInovasiService {
    private repo;
    private exportService;
    delete(review_inovasi_id: number): Promise<SuccessResponse<string> | boolean>;
    detail(review_inovasi_id: number): Promise<SuccessResponse<HasilReviewResponse> | HasilReviewResponse>;
    get(url: string | undefined, username: string | undefined, req: HasilReviewParams): Promise<PaginationResponse<PaginationResponseInterface, HasilReviewResponse[]> | PaginationData<HasilReviewResponse[]>>;
    download(type: DownloadType, username: string | undefined, req: HasilReviewParams): Promise<string>;
    toResponse(data: ReviewInovasiDaerah): HasilReviewResponse;
}
