import { CreatePreviewInovasiDaerahRequest, GetPreviewInovasiRequest } from '../schema/preview_inovasi_daerah.schema';
import { PaginationResponse, SuccessResponse } from '../types/response.type';
import { PaginationResponse as PaginationResponseInterface } from '../interface/pagination.interface';
import { PreviewReviewInovasiDaerah } from '../entity/PreviewReviewInovasiDaerah.entity';
import { PaginationData } from '../schema/base.schema';
export default class PreviewReviewInovasiDaerahService {
    private repo;
    create(review_id: number, username: string | undefined, req: CreatePreviewInovasiDaerahRequest): Promise<SuccessResponse<PreviewReviewInovasiDaerah> | PreviewReviewInovasiDaerah>;
    detail(review_id: number, id: number): Promise<SuccessResponse<PreviewReviewInovasiDaerah> | PreviewReviewInovasiDaerah>;
    get(review_id: number, url: string | undefined, username: string | undefined, req: GetPreviewInovasiRequest): Promise<PaginationResponse<PaginationResponseInterface, PreviewReviewInovasiDaerah[]> | PaginationData<PreviewReviewInovasiDaerah[]>>;
    delete(review_id: number, id: number): Promise<SuccessResponse<string> | boolean>;
    update(review_id: number, id: number, username: string | undefined, req: CreatePreviewInovasiDaerahRequest): Promise<SuccessResponse<string> | boolean>;
}
