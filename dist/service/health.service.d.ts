import { IHealthService } from './interface/ihealth.service';
declare class HealthService implements IHealthService {
    health(): Promise<string>;
}
export default HealthService;
