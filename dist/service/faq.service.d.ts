import Faq from '../entity/Faq.entity';
import { PaginationResponse as PaginationResponseInterface } from '../interface/pagination.interface';
import { PaginationData } from '../schema/base.schema';
import { CreateFaqRequest } from '../schema/faq.schema';
import { GetSettingRequest } from '../schema/setting.schema';
import { PaginationResponse, SuccessResponse } from '../types/response.type';
export default class FaqService {
    private repo;
    create(req: CreateFaqRequest): Promise<SuccessResponse<Omit<Faq, ''>> | Faq>;
    detail(id: number): Promise<SuccessResponse<Omit<Faq, ''>> | Faq>;
    get(url: string | undefined, req: GetSettingRequest): Promise<PaginationResponse<PaginationResponseInterface, Omit<Faq, ''>[]> | PaginationData<Omit<Faq, ''>[]>>;
    delete(id: number): Promise<SuccessResponse<string> | boolean>;
    update(id: number, req: CreateFaqRequest): Promise<SuccessResponse<string> | boolean>;
}
