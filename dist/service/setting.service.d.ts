import { PaginationResponse, SuccessResponse } from '../types/response.type';
import { PaginationResponse as PaginationResponseInterface } from '../interface/pagination.interface';
import Setting from '../entity/Setting.entity';
import { CreateSettingRequest, GetSettingRequest } from '../schema/setting.schema';
import { PaginationData } from '../schema/base.schema';
export default class SettingService {
    private repo;
    create(req: CreateSettingRequest): Promise<SuccessResponse<Omit<Setting, ''>> | Setting>;
    detail(id: number): Promise<SuccessResponse<Omit<Setting, ''>> | Setting>;
    get(url: string | undefined, req: GetSettingRequest): Promise<PaginationResponse<PaginationResponseInterface, Omit<Setting, 'details'>[]> | PaginationData<Omit<Setting, 'details'>[]>>;
    delete(id: number): Promise<SuccessResponse<string> | boolean>;
    update(id: number, req: CreateSettingRequest): Promise<SuccessResponse<string> | boolean>;
}
