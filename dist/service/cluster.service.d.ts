import Cluster from '../entity/Cluster.entity';
import { PaginationData } from '../schema/base.schema';
import { CreateClusterRequest, GetClusterRequest } from '../schema/cluster.schema';
import { PaginationResponse, SuccessResponse } from '../types/response.type';
import { PaginationResponse as PaginationResponseInterface } from '../interface/pagination.interface';
import { CreateClusterDetailRequest, GetClusterDetailRequest } from '../schema/clusterDetail.schema';
import ClusterDetail from '../entity/ClusterDetail.entity';
export default class ClusterService {
    private mainRepo;
    private detailRepo;
    create(req: CreateClusterRequest): Promise<SuccessResponse<Omit<Cluster, 'details'>> | Cluster>;
    detail(id: number): Promise<SuccessResponse<Omit<Cluster, 'details'>> | Cluster>;
    get(url: string | undefined, req: GetClusterRequest): Promise<PaginationResponse<PaginationResponseInterface, Omit<Cluster, 'details'>[]> | PaginationData<Omit<Cluster, 'details'>[]>>;
    delete(id: number): Promise<SuccessResponse<string> | boolean>;
    update(id: number, req: CreateClusterRequest, username?: string): Promise<SuccessResponse<string> | boolean>;
    getDetails(id: number, url: string | undefined, req: GetClusterDetailRequest): Promise<PaginationResponse<PaginationResponseInterface, ClusterDetail[]> | PaginationData<ClusterDetail[]>>;
    createDetail(id: number, req: CreateClusterDetailRequest): Promise<SuccessResponse<ClusterDetail> | ClusterDetail>;
    getDetail(id: number, region_id: number): Promise<SuccessResponse<ClusterDetail> | ClusterDetail>;
    updateDetail(id: number, region_id: number, req: CreateClusterDetailRequest): Promise<SuccessResponse<string> | boolean>;
    deleteDetail(id: number, region_id: number): Promise<SuccessResponse<string> | boolean>;
}
