import { InovasiDitolakParams, InovasiDitolakResponse } from '../schema/inovasi_ditolak.schema';
import { PaginationResponse, SuccessResponse } from '../types/response.type';
import { PaginationResponse as PaginationResponseInterface } from '../interface/pagination.interface';
import { PaginationData } from '../schema/base.schema';
import { ReviewInovasiDaerah } from '../entity/ReviewInovasiDaerah.entity';
import { DownloadType } from '../types/lib';
export default class InovasiDitolakService {
    private repo;
    private exportService;
    setujui(review_inovasi_id: number, username?: string): Promise<SuccessResponse<string> | boolean>;
    detail(review_inovasi_id: number): Promise<SuccessResponse<InovasiDitolakResponse> | InovasiDitolakResponse>;
    get(url: string | undefined, username: string | undefined, req: InovasiDitolakParams): Promise<PaginationResponse<PaginationResponseInterface, InovasiDitolakResponse[]> | PaginationData<InovasiDitolakResponse[]>>;
    download(type: DownloadType, username: string | undefined, req: InovasiDitolakParams): Promise<string>;
    toResponse(data: ReviewInovasiDaerah): InovasiDitolakResponse;
}
