"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const tsoa_1 = require("tsoa");
const api_error_1 = __importDefault(require("../providers/exceptions/api.error"));
const cluster_repository_1 = __importDefault(require("../repository/cluster.repository"));
const cluster_detail_repository_1 = __importDefault(require("../repository/cluster_detail.repository"));
const http_code_enum_1 = require("../types/http_code.enum");
const pagination_1 = __importDefault(require("../providers/pagination"));
let ClusterService = class ClusterService {
    constructor() {
        this.mainRepo = new cluster_repository_1.default();
        this.detailRepo = new cluster_detail_repository_1.default();
    }
    async create(req) {
        const create = this.mainRepo.create({
            name: req.name
        });
        await this.mainRepo.insert(create);
        return create;
    }
    async detail(id) {
        const res = await this.mainRepo.findOneBy({ id: id });
        if (!res) {
            throw new api_error_1.default({
                httpCode: http_code_enum_1.HttpCode.NOT_FOUND,
                message: 'Data not found'
            });
        }
        return res;
    }
    async get(url = '', req) {
        var _a;
        const query = this.mainRepo
            .createQueryBuilder()
            .where(`lower(name) like lower(:q)`, { q: `%${(_a = req.q) !== null && _a !== void 0 ? _a : ''}%` });
        const total = await query.getCount();
        const { limit, offset, page, order } = pagination_1.default.options(req.page, req.limit);
        query.limit(limit).offset(offset);
        for (const key in order) {
            query.addOrderBy(key, order[key]);
        }
        const res = await query.getMany();
        let paging = pagination_1.default.build(url, page, limit, total);
        return { paging, res };
    }
    async delete(id) {
        const { affected } = await this.mainRepo.delete({ id: id });
        if (affected && affected > 0) {
            return true;
        }
        return false;
    }
    async update(id, req, username = '') {
        const update = this.mainRepo.create({
            name: req.name
        });
        const { affected } = await this.mainRepo.update(id, update);
        if (!affected) {
            return false;
        }
        return true;
    }
    async getDetails(id, url = '', req) {
        var _a, _b;
        const query = this.detailRepo
            .createQueryBuilder('d')
            .leftJoinAndSelect('d.region', 'r')
            .leftJoinAndSelect('d.cluster', 'c')
            .where(`lower(r.name) like lower(:q)`, { q: `%${(_a = req.q) !== null && _a !== void 0 ? _a : ''}%` })
            .orWhere(`lower(c.name) like lower(:q)`, { q: `%${(_b = req.q) !== null && _b !== void 0 ? _b : ''}%` });
        const total = await query.getCount();
        const { limit, offset, page, order } = pagination_1.default.options(req.page, req.limit);
        query.limit(limit).offset(offset);
        for (const key in order) {
            query.addOrderBy(`d.${key}`, order[key]);
        }
        const res = await query.getMany();
        let paging = pagination_1.default.build(url, page, limit, total);
        return { paging, res };
    }
    async createDetail(id, req) {
        const detail = this.detailRepo.create({
            cluster_id: id,
            region_id: req.region_id
        });
        await this.detailRepo.insert(detail);
        return detail;
    }
    async getDetail(id, region_id) {
        const detail = await this.detailRepo.findOneBy({
            cluster_id: id,
            region_id: region_id
        });
        if (!detail) {
            throw new api_error_1.default({
                httpCode: http_code_enum_1.HttpCode.NOT_FOUND,
                message: 'Data not found!'
            });
        }
        return detail;
    }
    async updateDetail(id, region_id, req) {
        const { affected } = await this.detailRepo.delete({
            cluster_id: id,
            region_id: region_id
        });
        if (!affected || affected <= 0) {
            throw new api_error_1.default({
                httpCode: http_code_enum_1.HttpCode.BAD_REQUEST,
                message: 'Error update data!'
            });
        }
        const detail = this.detailRepo.create({
            cluster_id: id,
            region_id: req.region_id
        });
        const { identifiers } = await this.detailRepo.insert(detail);
        if (!identifiers) {
            return false;
        }
        return true;
    }
    async deleteDetail(id, region_id) {
        const { affected } = await this.detailRepo.delete({
            cluster_id: id,
            region_id: region_id
        });
        if (affected && affected > 0) {
            return true;
        }
        return false;
    }
};
__decorate([
    (0, tsoa_1.Post)(''),
    __param(0, (0, tsoa_1.Body)()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Object]),
    __metadata("design:returntype", Promise)
], ClusterService.prototype, "create", null);
__decorate([
    (0, tsoa_1.Get)('/{id}'),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Number]),
    __metadata("design:returntype", Promise)
], ClusterService.prototype, "detail", null);
__decorate([
    (0, tsoa_1.Get)(''),
    __param(0, (0, tsoa_1.Query)('url')),
    __param(0, (0, tsoa_1.Hidden)()),
    __param(1, (0, tsoa_1.Queries)()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [String, Object]),
    __metadata("design:returntype", Promise)
], ClusterService.prototype, "get", null);
__decorate([
    (0, tsoa_1.Delete)('/{id}'),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Number]),
    __metadata("design:returntype", Promise)
], ClusterService.prototype, "delete", null);
__decorate([
    (0, tsoa_1.Patch)('/{id}'),
    __param(1, (0, tsoa_1.Body)()),
    __param(2, (0, tsoa_1.Query)()),
    __param(2, (0, tsoa_1.Hidden)()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Number, Object, String]),
    __metadata("design:returntype", Promise)
], ClusterService.prototype, "update", null);
__decorate([
    (0, tsoa_1.Get)('/{id}/detail'),
    __param(1, (0, tsoa_1.Query)('url')),
    __param(1, (0, tsoa_1.Hidden)()),
    __param(2, (0, tsoa_1.Queries)()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Number, String, Object]),
    __metadata("design:returntype", Promise)
], ClusterService.prototype, "getDetails", null);
__decorate([
    (0, tsoa_1.Post)('/{id}/detail'),
    __param(1, (0, tsoa_1.Body)()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Number, Object]),
    __metadata("design:returntype", Promise)
], ClusterService.prototype, "createDetail", null);
__decorate([
    (0, tsoa_1.Get)('/{id}/detail/{region_id}'),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Number, Number]),
    __metadata("design:returntype", Promise)
], ClusterService.prototype, "getDetail", null);
__decorate([
    (0, tsoa_1.Patch)('/{id}/detail/{region_id}'),
    __param(2, (0, tsoa_1.Body)()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Number, Number, Object]),
    __metadata("design:returntype", Promise)
], ClusterService.prototype, "updateDetail", null);
__decorate([
    (0, tsoa_1.Delete)('/{id}/detail/{region_id}'),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Number, Number]),
    __metadata("design:returntype", Promise)
], ClusterService.prototype, "deleteDetail", null);
ClusterService = __decorate([
    (0, tsoa_1.Route)('/cluster'),
    (0, tsoa_1.Tags)('Master | Daerah | Kluster'),
    (0, tsoa_1.Security)('bearer')
], ClusterService);
exports.default = ClusterService;
