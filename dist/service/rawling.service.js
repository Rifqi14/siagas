"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const LaporanBentukInovasi_entity_1 = require("../entity/LaporanBentukInovasi.entity");
const LaporanInisatorInovasi_entity_1 = require("../entity/LaporanInisatorInovasi.entity");
const LaporanJenisInovasi_entity_1 = require("../entity/LaporanJenisInovasi.entity");
const LaporanUrusanInovasi_entity_1 = require("../entity/LaporanUrusanInovasi.entity");
const ProfilePemda_entity_1 = require("../entity/ProfilePemda.entity");
const api_error_1 = __importDefault(require("../providers/exceptions/api.error"));
const rawling_repository_1 = __importDefault(require("../repository/rawling.repository"));
const http_code_enum_1 = require("../types/http_code.enum");
const tsoa_1 = require("tsoa");
let RawlingService = class RawlingService {
    constructor() {
        this.repo = new rawling_repository_1.default();
    }
    async rawling(req) {
        if (!req.pemda_id)
            throw new api_error_1.default({
                httpCode: http_code_enum_1.HttpCode.BAD_REQUEST,
                message: 'Silahkan pilih pemda terlebih dahulu'
            });
        const pemda = await this.repo.manager.findOneBy(ProfilePemda_entity_1.ProfilPemda, {
            id: req.pemda_id
        });
        if (!pemda)
            throw new api_error_1.default({
                httpCode: http_code_enum_1.HttpCode.NOT_FOUND,
                message: 'Pemda tidak ditemukan'
            });
        const jenis_inovasi = await this.repo.manager.find(LaporanJenisInovasi_entity_1.LaporanJenisInovasi, {
            where: { pemda_id: pemda.id }
        });
        const urusan_pemerintah = await this.repo.manager.find(LaporanUrusanInovasi_entity_1.LaporanUrusanInovasi, {
            where: { pemda_id: pemda.id }
        });
        const inisiator = await this.repo.manager.find(LaporanInisatorInovasi_entity_1.LaporanInisiatorInovasi, {
            where: { pemda_id: pemda.id }
        });
        const bentuk_inovasi = await this.repo.manager.find(LaporanBentukInovasi_entity_1.LaporanBentukInovasi, {
            where: { pemda_id: pemda.id }
        });
        return {
            jenis_inovasi,
            urusan_pemerintah,
            inisiator,
            bentuk_inovasi
        };
    }
};
__decorate([
    (0, tsoa_1.Get)(''),
    __param(0, (0, tsoa_1.Queries)()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Object]),
    __metadata("design:returntype", Promise)
], RawlingService.prototype, "rawling", null);
RawlingService = __decorate([
    (0, tsoa_1.Route)('/rawlog'),
    (0, tsoa_1.Tags)('Rawlog'),
    (0, tsoa_1.Security)('bearer')
], RawlingService);
exports.default = RawlingService;
