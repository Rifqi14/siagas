import IndicatorScale from '../entity/IndicatorScale.entity';
import { CreateIndicatorScaleRequest, GetIndicatorScaleRequest } from '../schema/indicator_scale.scheme';
import { PaginationResponse, SuccessResponse } from '../types/response.type';
import { PaginationResponse as PaginationResponseInterface } from '../interface/pagination.interface';
import { PaginationData } from '../schema/base.schema';
export default class IndicatorScaleService {
    private repo;
    private indicatorRepo;
    constructor();
    create(indikator_id: number, req: CreateIndicatorScaleRequest): Promise<SuccessResponse<IndicatorScale> | IndicatorScale>;
    detail(id: number): Promise<SuccessResponse<IndicatorScale> | IndicatorScale>;
    get(indikator_id: number, url: string | undefined, req: GetIndicatorScaleRequest): Promise<PaginationResponse<PaginationResponseInterface, IndicatorScale[]> | PaginationData<IndicatorScale[]>>;
    delete(id: number): Promise<SuccessResponse<string> | boolean>;
    update(id: number, req: CreateIndicatorScaleRequest): Promise<SuccessResponse<string> | boolean>;
}
