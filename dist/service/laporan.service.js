"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const GovernmentInnovation_entity_1 = __importDefault(require("../entity/GovernmentInnovation.entity"));
const GovernmentSector_1 = __importDefault(require("../entity/GovernmentSector"));
const LaporanIndeks_entity_1 = require("../entity/LaporanIndeks.entity");
const LaporanInisatorInovasi_entity_1 = require("../entity/LaporanInisatorInovasi.entity");
const LaporanUrusanInovasi_entity_1 = require("../entity/LaporanUrusanInovasi.entity");
const ProfilePemda_entity_1 = require("../entity/ProfilePemda.entity");
const ReviewInovasiDaerah_entity_1 = require("../entity/ReviewInovasiDaerah.entity");
const api_error_1 = __importDefault(require("../providers/exceptions/api.error"));
const pagination_1 = __importDefault(require("../providers/pagination"));
const laporan_repository_1 = __importDefault(require("../repository/laporan.repository"));
const laporan_schema_1 = require("../schema/laporan.schema");
const tsoa_1 = require("tsoa");
const typeorm_1 = require("typeorm");
const export_service_1 = __importDefault(require("./export.service"));
let LaporanService = class LaporanService {
    constructor() {
        this.repo = new laporan_repository_1.default();
        this.exportService = new export_service_1.default();
    }
    async laporanUrusanInovasi(url = '', req) {
        // const query = this.repo.manager
        // 	.getRepository(LaporanUrusanInovasi)
        // 	.createQueryBuilder()
        // 	.where(`lower(urusan_pemerintahan) ilike lower(:q)`, {
        // 		q: `%${req.q ?? ""}%`,
        // 	});
        // if (req.pemda_id) {
        // 	query.andWhere(`pemda_id = :pemda_id`, { pemda_id: req.pemda_id });
        // }
        const query = this.repo.manager
            .getRepository(GovernmentSector_1.default)
            .createQueryBuilder('gs')
            .select('gs.name', 'urusan_pemerintah')
            .addSelect(qb => {
            const subQ1 = qb
                .select(`count(rid.id)`, 'count')
                .from(ReviewInovasiDaerah_entity_1.ReviewInovasiDaerah, 'rid')
                .leftJoin(GovernmentInnovation_entity_1.default, 'gi2', 'gi2.id = rid.inovasi_id')
                .where('gi2.government_sector_id = gi.government_sector_id')
                .andWhere(`lower(rid.status) = 'accept'`);
            if (req.pemda_id)
                subQ1.andWhere(`gi2.pemda_id = :pemda_id`, { pemda_id: req.pemda_id });
            return subQ1;
        }, 'total_disetujui')
            .addSelect(qb => {
            const subQ2 = qb
                .select(`count(rid.id)`, 'count')
                .from(ReviewInovasiDaerah_entity_1.ReviewInovasiDaerah, 'rid')
                .leftJoin(GovernmentInnovation_entity_1.default, 'gi2', 'gi2.id = rid.inovasi_id')
                .where('gi2.government_sector_id = gi.government_sector_id')
                .andWhere(`lower(rid.status) = 'rejected'`);
            if (req.pemda_id)
                subQ2.andWhere(`gi2.pemda_id = :pemda_id`, { pemda_id: req.pemda_id });
            return subQ2;
        }, 'total_ditolak')
            // .addSelect("count(gi.id)", "total_keseluruhan")
            .addSelect(qb => {
            const subQ3 = qb
                .select(`count(gi2.id)`, 'count')
                .from(GovernmentInnovation_entity_1.default, 'gi2')
                .where('gi2.government_sector_id = gi.government_sector_id');
            if (req.pemda_id)
                subQ3.andWhere(`gi2.pemda_id = :pemda_id`, { pemda_id: req.pemda_id });
            return subQ3;
        }, 'total_keseluruhan')
            .leftJoin('gs.innovations', 'gi')
            .where(`gs.name is not null and gi.government_sector_id is not null`)
            .groupBy(`gs.name, gi.government_sector_id, gs.id`);
        const total = await query.getCount();
        const { limit, offset, page } = pagination_1.default.options(req.page, req.limit);
        query.limit(limit).offset(offset);
        const res = await query.printSql().getRawMany();
        let paging = pagination_1.default.build(url, page, limit, total);
        return { paging, res };
    }
    async laporanInisiatorInovasi(url = '', req) {
        // const query = this.repo.manager
        // 	.getRepository(LaporanInisiatorInovasi)
        // 	.createQueryBuilder()
        // 	.where(`lower(inisiator_inovasi) ilike lower(:q)`, {
        // 		q: `%${req.q ?? ""}%`,
        // 	});
        // if (req.pemda_id) {
        // 	query.andWhere(`pemda_id = :pemda_id`, { pemda_id: req.pemda_id });
        // }
        const query = this.repo.manager
            .getRepository(ProfilePemda_entity_1.ProfilPemda)
            .createQueryBuilder('pp')
            .select('gi.innovation_initiator', 'inisiator_inovasi')
            .addSelect(qb => {
            const subQ1 = qb
                .select(`count(rid.id)`, 'count')
                .from(ReviewInovasiDaerah_entity_1.ReviewInovasiDaerah, 'rid')
                .leftJoin(GovernmentInnovation_entity_1.default, 'gi2', 'gi2.id = rid.inovasi_id')
                .where('gi2.innovation_initiator = gi.innovation_initiator')
                .andWhere(`lower(rid.status) = 'accept'`);
            if (req.pemda_id)
                subQ1.andWhere(`gi2.pemda_id = :pemda_id`, { pemda_id: req.pemda_id });
            return subQ1;
        }, 'total_disetujui')
            .addSelect(qb => {
            const subQ2 = qb
                .select(`count(rid.id)`, 'count')
                .from(ReviewInovasiDaerah_entity_1.ReviewInovasiDaerah, 'rid')
                .leftJoin(GovernmentInnovation_entity_1.default, 'gi2', 'gi2.id = rid.inovasi_id')
                .where('gi2.innovation_initiator = gi.innovation_initiator')
                .andWhere(`lower(rid.status) = 'rejected'`);
            if (req.pemda_id)
                subQ2.andWhere(`gi2.pemda_id = :pemda_id`, { pemda_id: req.pemda_id });
            return subQ2;
        }, 'total_ditolak')
            // .addSelect("count(gi.id)", "total_keseluruhan")
            .addSelect(qb => {
            const subQ3 = qb
                .select(`count(gi2.id)`, 'count')
                .from(GovernmentInnovation_entity_1.default, 'gi2')
                .where('gi2.innovation_initiator = gi.innovation_initiator');
            if (req.pemda_id)
                subQ3.andWhere(`gi2.pemda_id = :pemda_id`, { pemda_id: req.pemda_id });
            return subQ3;
        }, 'total_keseluruhan')
            .leftJoin('pp.innovations', 'gi')
            .where(`gi.innovation_initiator is not null`)
            .groupBy(`gi.innovation_initiator`);
        const total = await query.getCount();
        const { limit, offset, page } = pagination_1.default.options(req.page, req.limit);
        query.limit(limit).offset(offset);
        const res = await query.getRawMany();
        let paging = pagination_1.default.build(url, page, limit, total);
        return { paging, res };
    }
    async laporanBentukInovasi(url = '', req) {
        // const query = this.repo.manager
        // 	.getRepository(LaporanBentukInovasi)
        // 	.createQueryBuilder()
        // 	.where(`lower(bentuk_inovasi) ilike lower(:q)`, {
        // 		q: `%${req.q ?? ""}%`,
        // 	});
        // if (req.pemda_id) {
        // 	query.andWhere(`pemda_id = :pemda_id`, { pemda_id: req.pemda_id });
        // }
        const query = this.repo.manager
            .getRepository(ProfilePemda_entity_1.ProfilPemda)
            .createQueryBuilder('pp')
            .select('gi.innovation_form', 'bentuk_inovasi')
            .addSelect(qb => {
            const subQ1 = qb
                .select(`count(rid.id)`, 'count')
                .from(ReviewInovasiDaerah_entity_1.ReviewInovasiDaerah, 'rid')
                .leftJoin(GovernmentInnovation_entity_1.default, 'gi2', 'gi2.id = rid.inovasi_id')
                .where('gi2.innovation_form = gi.innovation_form')
                .andWhere(`lower(rid.status) = 'accept'`);
            if (req.pemda_id)
                subQ1.andWhere(`gi2.pemda_id = :pemda_id`, { pemda_id: req.pemda_id });
            return subQ1;
        }, 'total_disetujui')
            .addSelect(qb => {
            const subQ2 = qb
                .select(`count(rid.id)`, 'count')
                .from(ReviewInovasiDaerah_entity_1.ReviewInovasiDaerah, 'rid')
                .leftJoin(GovernmentInnovation_entity_1.default, 'gi2', 'gi2.id = rid.inovasi_id')
                .where('gi2.innovation_form = gi.innovation_form')
                .andWhere(`lower(rid.status) = 'rejected'`);
            if (req.pemda_id)
                subQ2.andWhere(`gi2.pemda_id = :pemda_id`, { pemda_id: req.pemda_id });
            return subQ2;
        }, 'total_ditolak')
            .addSelect(qb => {
            const subQ3 = qb.select(`count(gi2.id)`, 'count').from(GovernmentInnovation_entity_1.default, 'gi2').where('gi2.innovation_form = gi.innovation_form');
            if (req.pemda_id)
                subQ3.andWhere(`gi2.pemda_id = :pemda_id`, { pemda_id: req.pemda_id });
            return subQ3;
        }, 'total_keseluruhan')
            // .addSelect("count(gi.id)", "total_keseluruhan")
            .leftJoin('pp.innovations', 'gi')
            .where(`gi.innovation_form is not null`)
            .groupBy(`gi.innovation_form`);
        const total = await query.getCount();
        const { limit, offset, page } = pagination_1.default.options(req.page, req.limit);
        query.limit(limit).offset(offset);
        const res = await query.getRawMany();
        let paging = pagination_1.default.build(url, page, limit, total);
        return { paging, res };
    }
    async laporanJenisInovasi(url = '', req) {
        // const query = this.repo.manager
        // 	.getRepository(LaporanJenisInovasi)
        // 	.createQueryBuilder()
        // 	.where(`lower(jenis_inovasi) ilike lower(:q)`, {
        // 		q: `%${req.q ?? ""}%`,
        // 	});
        // if (req.pemda_id) {
        // 	query.andWhere(`pemda_id = :pemda_id`, { pemda_id: req.pemda_id });
        // }
        const query = this.repo.manager
            .getRepository(ProfilePemda_entity_1.ProfilPemda)
            .createQueryBuilder('pp')
            .select('gi.innovation_type', 'jenis_inovasi')
            // .addSelect("count(gi.id)", "total_keseluruhan")
            .addSelect(qb => {
            const subQ1 = qb
                .select(`count(rid.id)`, 'count')
                .from(ReviewInovasiDaerah_entity_1.ReviewInovasiDaerah, 'rid')
                .leftJoin(GovernmentInnovation_entity_1.default, 'gi2', 'gi2.id = rid.inovasi_id')
                .where('gi2.innovation_type = gi.innovation_type')
                .andWhere(`lower(rid.status) = 'accept'`);
            if (req.pemda_id)
                subQ1.andWhere(`gi2.pemda_id = :pemda_id`, { pemda_id: req.pemda_id });
            return subQ1;
        }, 'total_disetujui')
            .addSelect(qb => {
            const subQ2 = qb
                .select(`count(rid.id)`, 'count')
                .from(ReviewInovasiDaerah_entity_1.ReviewInovasiDaerah, 'rid')
                .leftJoin(GovernmentInnovation_entity_1.default, 'gi2', 'gi2.id = rid.inovasi_id')
                .where('gi2.innovation_type = gi.innovation_type')
                .andWhere(`lower(rid.status) = 'rejected'`);
            if (req.pemda_id)
                subQ2.andWhere(`gi2.pemda_id = :pemda_id`, { pemda_id: req.pemda_id });
            return subQ2;
        }, 'total_ditolak')
            .addSelect(qb => {
            const subQ3 = qb.select(`count(gi2.id)`, 'count').from(GovernmentInnovation_entity_1.default, 'gi2').where('gi2.innovation_type = gi.innovation_type');
            if (req.pemda_id)
                subQ3.andWhere(`gi2.pemda_id = :pemda_id`, { pemda_id: req.pemda_id });
            return subQ3;
        }, 'total_keseluruhan')
            .leftJoin('pp.innovations', 'gi')
            .where(`gi.innovation_type is not null`)
            .groupBy(`gi.innovation_type`);
        const total = await query.getCount();
        const { limit, offset, page } = pagination_1.default.options(req.page, req.limit);
        query.limit(limit).offset(offset);
        const res = await query.getRawMany();
        let paging = pagination_1.default.build(url, page, limit, total);
        return { paging, res };
    }
    async laporanIndeks(url = '', req) {
        var _a;
        const query = this.repo.manager
            .getRepository(LaporanIndeks_entity_1.LaporanIndeks)
            .createQueryBuilder()
            .where(`lower(nama_daerah) ilike lower(:q)`, {
            q: `%${(_a = req.q) !== null && _a !== void 0 ? _a : ''}%`,
        });
        if (req.pemda_id) {
            query.andWhere(`pemda_id = :pemda_id`, { pemda_id: req.pemda_id });
        }
        if (req.opd_yang_menangani) {
            query.andWhere('lower(opd_yang_menangani) = lower(:opd_yang_menangani)', { opd_yang_menangani: req.opd_yang_menangani });
        }
        if (req.predikat && req.predikat !== 'semua') {
            if (req.predikat === 'belum mengisi data') {
                query.andWhere(`jumlah_inovasi = 0`);
            }
            else {
                let predikat = '';
                switch (req.predikat) {
                    case 'terinovatif':
                        predikat = 'sangat invovatif';
                        break;
                    default:
                        predikat = req.predikat;
                        break;
                }
                query.andWhere(`lower(predikat) = lower(:predikat)`, {
                    predikat,
                });
            }
        }
        const total = await query.getCount();
        const { limit, offset, page } = pagination_1.default.options(req.page, req.limit);
        query.limit(limit).offset(offset);
        const res = (await query.getMany()).map(response => (0, laporan_schema_1.toLaporanIndexResponse)(response));
        let paging = pagination_1.default.build(url, page, limit, total);
        return { paging, res };
    }
    async laporanIndeksdownload(type, req) {
        var _a;
        try {
            let where = {
                nama_daerah: (0, typeorm_1.ILike)(`%${(_a = req.q) !== null && _a !== void 0 ? _a : ''}%`),
            };
            if (req.pemda_id)
                where = Object.assign(Object.assign({}, where), { pemda_id: req.pemda_id });
            if (req.predikat && req.predikat !== 'semua') {
                if (req.predikat === 'belum mengisi data') {
                    where = Object.assign(Object.assign({}, where), { jumlah_inovasi: 0 });
                }
                else {
                    let predikat = '';
                    switch (req.predikat) {
                        case 'terinovatif':
                            predikat = 'sangat invovatif';
                            break;
                        default:
                            predikat = req.predikat;
                            break;
                    }
                    where = Object.assign(Object.assign({}, where), { predikat: (0, typeorm_1.ILike)(req.predikat) });
                }
            }
            const data = (await this.repo.manager.find(LaporanIndeks_entity_1.LaporanIndeks, {
                where,
            })).map(laporan => (0, laporan_schema_1.toLaporanIndexResponse)(laporan));
            if (type === 'pdf') {
                return await this.exportService.generatePdfFromUrl('http://localhost:3002/docs', 'test');
                throw new api_error_1.default({
                    httpCode: 403,
                    message: 'Download PDF under maintenance',
                });
            }
            const path = await this.exportService.download({
                data,
                name: 'indeks-rata-rata-opd',
            });
            return path;
        }
        catch (error) {
            throw error;
        }
    }
    async laporanUrusanInovasidownload(type, req) {
        var _a;
        try {
            if (type === 'pdf')
                throw new api_error_1.default({
                    httpCode: 403,
                    message: 'Download PDF under maintenance',
                });
            let where = {
                urusan_pemerintahan: (0, typeorm_1.ILike)(`%${(_a = req.q) !== null && _a !== void 0 ? _a : ''}%`),
            };
            if (req.pemda_id)
                where = Object.assign(Object.assign({}, where), { pemda_id: req.pemda_id });
            const data = await this.repo.manager.find(LaporanUrusanInovasi_entity_1.LaporanUrusanInovasi, {
                where,
            });
            const path = await this.exportService.download({
                data,
                name: 'indeks-urusan-inovasi',
            });
            return path;
        }
        catch (error) {
            throw error;
        }
    }
    async laporanInisiatorInovasidownload(type, req) {
        var _a;
        try {
            if (type === 'pdf')
                throw new api_error_1.default({
                    httpCode: 403,
                    message: 'Download PDF under maintenance',
                });
            let where = {
                inisiator_inovasi: (0, typeorm_1.ILike)(`%${(_a = req.q) !== null && _a !== void 0 ? _a : ''}%`),
            };
            if (req.pemda_id)
                where = Object.assign(Object.assign({}, where), { pemda_id: req.pemda_id });
            const data = await this.repo.manager.find(LaporanInisatorInovasi_entity_1.LaporanInisiatorInovasi, {
                where,
            });
            const path = await this.exportService.download({
                data,
                name: 'inisiator-inovasi',
            });
            return path;
        }
        catch (error) {
            throw error;
        }
    }
};
__decorate([
    (0, tsoa_1.Get)('/urusan_inovasi'),
    __param(0, (0, tsoa_1.Query)()),
    __param(0, (0, tsoa_1.Hidden)()),
    __param(1, (0, tsoa_1.Queries)()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Object, Object]),
    __metadata("design:returntype", Promise)
], LaporanService.prototype, "laporanUrusanInovasi", null);
__decorate([
    (0, tsoa_1.Get)('/inisiator_inovasi'),
    __param(0, (0, tsoa_1.Query)()),
    __param(0, (0, tsoa_1.Hidden)()),
    __param(1, (0, tsoa_1.Queries)()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Object, Object]),
    __metadata("design:returntype", Promise)
], LaporanService.prototype, "laporanInisiatorInovasi", null);
__decorate([
    (0, tsoa_1.Get)('/bentuk_inovasi'),
    __param(0, (0, tsoa_1.Query)()),
    __param(0, (0, tsoa_1.Hidden)()),
    __param(1, (0, tsoa_1.Queries)()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Object, Object]),
    __metadata("design:returntype", Promise)
], LaporanService.prototype, "laporanBentukInovasi", null);
__decorate([
    (0, tsoa_1.Get)('/jenis_inovasi'),
    __param(0, (0, tsoa_1.Query)()),
    __param(0, (0, tsoa_1.Hidden)()),
    __param(1, (0, tsoa_1.Queries)()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Object, Object]),
    __metadata("design:returntype", Promise)
], LaporanService.prototype, "laporanJenisInovasi", null);
__decorate([
    (0, tsoa_1.Get)('/indeks'),
    __param(0, (0, tsoa_1.Query)()),
    __param(0, (0, tsoa_1.Hidden)()),
    __param(1, (0, tsoa_1.Queries)()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Object, Object]),
    __metadata("design:returntype", Promise)
], LaporanService.prototype, "laporanIndeks", null);
__decorate([
    (0, tsoa_1.Tags)('Export'),
    (0, tsoa_1.Get)('/indeks/download/{type}'),
    __param(1, (0, tsoa_1.Queries)()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [String, Object]),
    __metadata("design:returntype", Promise)
], LaporanService.prototype, "laporanIndeksdownload", null);
__decorate([
    (0, tsoa_1.Tags)('Export'),
    (0, tsoa_1.Get)('/urusan_inovasi/download/{type}'),
    __param(1, (0, tsoa_1.Queries)()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [String, Object]),
    __metadata("design:returntype", Promise)
], LaporanService.prototype, "laporanUrusanInovasidownload", null);
__decorate([
    (0, tsoa_1.Tags)('Export'),
    (0, tsoa_1.Get)('/inisiator_inovasi/download/{type}'),
    __param(1, (0, tsoa_1.Queries)()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [String, Object]),
    __metadata("design:returntype", Promise)
], LaporanService.prototype, "laporanInisiatorInovasidownload", null);
LaporanService = __decorate([
    (0, tsoa_1.Route)('/laporan'),
    (0, tsoa_1.Tags)('Laporan'),
    (0, tsoa_1.Security)('bearer')
], LaporanService);
exports.default = LaporanService;
