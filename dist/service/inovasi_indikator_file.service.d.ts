/// <reference types="multer" />
import { InovasiIndikatorFileParams, ListInovasiIndikatorFiles } from '../schema/inovasi_indikator_file.schema';
import { PaginationResponse as PaginationResponseInterface } from '../interface/pagination.interface';
import { PaginationResponse, SuccessResponse } from '../types/response.type';
import { PaginationData } from '../schema/base.schema';
import { InovasiIndikatorFile } from '../entity/InovasiIndikatorFile.entity';
export default class InovasiIndikatorFileService {
    private repo;
    get(inovasi_id: string, indikator_id: string, url: string | undefined, username: string | undefined, req: InovasiIndikatorFileParams): Promise<PaginationResponse<PaginationResponseInterface, ListInovasiIndikatorFiles> | PaginationData<ListInovasiIndikatorFiles>>;
    detail(inovasi_id: number, indikator_id: number, file_id: number): Promise<SuccessResponse<InovasiIndikatorFile> | InovasiIndikatorFile>;
    upload(inovasi_id: string, indikator_id: string, nomor_surat: string, tanggal_surat: string, tentang: string, dokumen: Express.Multer.File, nama_dokumen?: string, username?: string): Promise<SuccessResponse<InovasiIndikatorFile> | InovasiIndikatorFile>;
    delete(inovasi_id: number, indikator_id: number, file_id: number): Promise<SuccessResponse<string> | boolean>;
}
