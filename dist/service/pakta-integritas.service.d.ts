/// <reference types="multer" />
import { PaktaIntegritas } from '../entity/PaktaIntegritas.entity';
import { PaginationResponse, SuccessResponse } from '../types/response.type';
import { ListPaktaIntegritas, PaktaIntegritasParams } from '../schema/pakta-integritas.schema';
import { PaginationResponse as PaginationResponseInterface } from '../interface/pagination.interface';
import { PaginationData } from '../schema/base.schema';
export default class PaktaIntegritasService {
    private repo;
    private fileService;
    create(user_id: string, file?: Express.Multer.File, username?: string): Promise<SuccessResponse<PaktaIntegritas> | PaktaIntegritas>;
    detail(id: number): Promise<SuccessResponse<PaktaIntegritas> | PaktaIntegritas>;
    getLatest(username?: string): Promise<SuccessResponse<PaktaIntegritas> | PaktaIntegritas>;
    get(url: string | undefined, username: string | undefined, req: PaktaIntegritasParams): Promise<PaginationResponse<PaginationResponseInterface, ListPaktaIntegritas> | PaginationData<ListPaktaIntegritas>>;
    delete(id: number): Promise<SuccessResponse<string> | boolean>;
    update(id: number, user_id: string, file?: Express.Multer.File, username?: string): Promise<SuccessResponse<string> | boolean>;
}
