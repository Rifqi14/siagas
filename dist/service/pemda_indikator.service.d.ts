import { PaginationResponse as PaginationResponseInterface } from '../interface/pagination.interface';
import { PaginationResponse, SuccessResponse } from '../types/response.type';
import { CreatePemdaIndikatorRequest, ListPemdaIndikator, PemdaIndikatorParams } from '../schema/pemda_indikator.schema';
import { PaginationData } from '../schema/base.schema';
import { PemdaIndikator } from '../entity/PemdaIndikator.entity';
export default class PemdaIndikatorService {
    private repo;
    create(pemda_id: string, data: CreatePemdaIndikatorRequest, username?: string): Promise<SuccessResponse<PemdaIndikator> | PemdaIndikator>;
    update(pemda_id: string, pemda_indikator_id: string, username: string | undefined, data: CreatePemdaIndikatorRequest): Promise<SuccessResponse<PemdaIndikator> | PemdaIndikator>;
    delete(pemda_id: string, pemda_indikator_id: string): Promise<SuccessResponse<string> | boolean>;
    get(pemda_id: string, url: string | undefined, username: string | undefined, req: PemdaIndikatorParams): Promise<PaginationResponse<PaginationResponseInterface, ListPemdaIndikator> | PaginationData<ListPemdaIndikator>>;
    detail(pemda_id: number, indikator_id: number): Promise<SuccessResponse<PemdaIndikator> | PemdaIndikator>;
}
