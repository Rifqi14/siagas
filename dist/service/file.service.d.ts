/// <reference types="multer" />
import File from '../entity/File.entity';
import { SuccessResponse } from '../types/response.type';
export default class FileService {
    private repo;
    create(file: Express.Multer.File, name?: string, username?: string): Promise<SuccessResponse<File> | File>;
}
