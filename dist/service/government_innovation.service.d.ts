/// <reference types="multer" />
import { DownloadType } from '../types/lib';
import GovernmentInnovation from '../entity/GovernmentInnovation.entity';
import { PaginationResponse as PaginationResponseInterface } from '../interface/pagination.interface';
import { GetGovernmentInnovationDownloadRequest, GetGovernmentInnovationRequest } from '../schema/government_innovation.schema';
import { PaginationResponse, SuccessResponse } from '../types/response.type';
export default class GovernmentInnovationService {
    private repo;
    private fileService;
    private exportService;
    constructor();
    create(nama_pemda: string, nama_inovasi?: string, tahapan_inovasi?: string, inisiator_inovasi?: string, jenis_inovasi?: string, bentuk_inovasi?: string, tematik?: string, urusan_pertama?: string, urusan_lain?: string, waktu_uji_coba?: string, waktu_penerapan?: string, rancang_bangun?: string, tujuan?: string, manfaat?: string, hasil_inovasi?: string, urusan_pemerintah?: string, anggaran_file?: Express.Multer.File, profile_file?: Express.Multer.File, foto?: Express.Multer.File, username?: string): Promise<GovernmentInnovation>;
    get(req: GetGovernmentInnovationRequest, url?: string, username?: string): Promise<PaginationResponse<PaginationResponseInterface, Array<GovernmentInnovation>> | {
        paging: PaginationResponseInterface;
        res: Array<GovernmentInnovation>;
    }>;
    download(type: DownloadType, username: string | undefined, req: GetGovernmentInnovationDownloadRequest): Promise<string>;
    delete(id: number): Promise<SuccessResponse<string> | boolean>;
    detail(id: number): Promise<SuccessResponse<GovernmentInnovation> | GovernmentInnovation>;
    downloadProfil(id: number, type: DownloadType): Promise<string>;
    update(id: number, nama_pemda: string, urusan_pemerintah: string, nama_inovasi: string, tahapan_inovasi: string, inisiator_inovasi: string, jenis_inovasi: string, bentuk_inovasi: string, tematik: string, urusan_pertama: string, urusan_lain: string, waktu_uji_coba: string, waktu_penerapan: string, rancang_bangun: string, tujuan: string, manfaat: string, hasil_inovasi: string, anggaran_file?: Express.Multer.File, profile_file?: Express.Multer.File, foto?: Express.Multer.File, username?: string): Promise<SuccessResponse<string> | boolean>;
}
