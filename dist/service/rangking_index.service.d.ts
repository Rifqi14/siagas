import { RangkingIndexParams, RangkingIndexParamsDownload, UpdateNominatorRequest } from "../schema/rangking_index.schema";
import { PaginationResponse, SuccessResponse } from "../types/response.type";
import { PaginationResponse as PaginationResponseInterface } from "../interface/pagination.interface";
import { RangkingIndex } from "../entity/RangkingIndex.entity";
import { PaginationData } from "../schema/base.schema";
import { DownloadType } from "../types/lib";
export default class RangkingIndexService {
    private repo;
    private exportService;
    get(url: string | undefined, username: string | undefined, req: RangkingIndexParams): Promise<PaginationResponse<PaginationResponseInterface, RangkingIndex[]> | PaginationData<RangkingIndex[]>>;
    nominator(id: number, req: UpdateNominatorRequest): Promise<SuccessResponse<string> | boolean>;
    download(type: DownloadType, username: string | undefined, req: RangkingIndexParamsDownload): Promise<string>;
}
