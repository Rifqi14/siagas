"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const rekap_indeks_akhir_repository_1 = __importDefault(require("../repository/rekap_indeks_akhir.repository"));
const tsoa_1 = require("tsoa");
const pagination_1 = __importDefault(require("../providers/pagination"));
const User_entity_1 = __importDefault(require("../entity/User.entity"));
const ProfilePemda_entity_1 = require("../entity/ProfilePemda.entity");
let RekapIndeksAkhirService = class RekapIndeksAkhirService {
    constructor() {
        this.repo = new rekap_indeks_akhir_repository_1.default();
    }
    async get(url = '', username = '', req) {
        var _a;
        const query = this.repo.repository
            .createQueryBuilder('ri')
            .where(`lower(nama_daerah) ilike lower(:q)`, { q: `%${(_a = req.q) !== null && _a !== void 0 ? _a : ''}%` });
        const total = await query.getCount();
        const { limit, offset, page, order } = pagination_1.default.options(req.page, req.limit);
        const user = await this.repo.repository.manager
            .getRepository(User_entity_1.default)
            .findOneByOrFail({ username });
        if (user && user.role && user.role.is_super_admin === 't') {
            query.andWhere(`ri.created_by = (:created_by)`, {
                created_by: username
            });
        }
        query.limit(limit).offset(offset);
        query.orderBy(`ri.indeks`, 'ASC');
        const res = await query.getMany();
        let paging = pagination_1.default.build(url, page, limit, total);
        return { paging, res };
    }
    async nominator(id, req) {
        const { affected } = await this.repo.repository.manager
            .getRepository(ProfilePemda_entity_1.ProfilPemda)
            .update({ id }, { nominator_rekap_indeks_akhir: req.nominator });
        if (affected && affected > 0) {
            return true;
        }
        return false;
    }
};
__decorate([
    (0, tsoa_1.Get)(''),
    __param(0, (0, tsoa_1.Query)()),
    __param(0, (0, tsoa_1.Hidden)()),
    __param(1, (0, tsoa_1.Query)()),
    __param(1, (0, tsoa_1.Hidden)()),
    __param(2, (0, tsoa_1.Queries)()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Object, Object, Object]),
    __metadata("design:returntype", Promise)
], RekapIndeksAkhirService.prototype, "get", null);
__decorate([
    (0, tsoa_1.Patch)('/{id}'),
    __param(1, (0, tsoa_1.Body)()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Number, Object]),
    __metadata("design:returntype", Promise)
], RekapIndeksAkhirService.prototype, "nominator", null);
RekapIndeksAkhirService = __decorate([
    (0, tsoa_1.Route)('/rekap_indeks_akhir'),
    (0, tsoa_1.Tags)('Verifikasi Index', 'Verifikasi Index | Rekap Indeks Akhir'),
    (0, tsoa_1.Security)('bearer')
], RekapIndeksAkhirService);
exports.default = RekapIndeksAkhirService;
