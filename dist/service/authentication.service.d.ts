import { LoginRequest, TokenPayload, TokenResponse } from '../schema/authentication.schema';
import { SuccessResponse } from '../types/response.type';
export default class AuthenticationService {
    private repo;
    constructor();
    login(req: LoginRequest): Promise<SuccessResponse<TokenResponse> | TokenResponse>;
    checkEmail(email: string): Promise<SuccessResponse<boolean> | boolean>;
    getMe(): Promise<SuccessResponse<TokenPayload>>;
}
