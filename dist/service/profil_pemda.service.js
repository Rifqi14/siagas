"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const User_entity_1 = __importDefault(require("../entity/User.entity"));
const profil_pemda_repository_1 = __importDefault(require("../repository/profil_pemda.repository"));
const tsoa_1 = require("tsoa");
const file_service_1 = __importDefault(require("./file.service"));
const api_error_1 = __importDefault(require("../providers/exceptions/api.error"));
const http_code_enum_1 = require("../types/http_code.enum");
const profil_pemda_schema_1 = require("../schema/profil_pemda.schema");
const pagination_1 = __importDefault(require("../providers/pagination"));
const data_source_1 = require("../data-source");
const Indicator_entity_1 = __importDefault(require("../entity/Indicator.entity"));
const PemdaIndikator_entity_1 = require("../entity/PemdaIndikator.entity");
const typeorm_1 = require("typeorm");
const export_service_1 = __importDefault(require("./export.service"));
let ProfilPemdaService = class ProfilPemdaService {
    constructor() {
        this.repo = new profil_pemda_repository_1.default();
        this.fileService = new file_service_1.default();
        this.exportService = new export_service_1.default();
    }
    async download(type, username = '', req) {
        try {
            if (type === 'pdf')
                throw new api_error_1.default({
                    httpCode: 403,
                    message: 'Download PDF under maintenance',
                });
            let where = {};
            if (req.q) {
                where = {
                    nama_daerah: (0, typeorm_1.ILike)(`%${req.q}%`),
                };
            }
            const data = await this.repo.find({
                where,
            });
            const path = await this.exportService.download({
                data: data.map((value, idx) => (0, profil_pemda_schema_1.toDownload)(value, idx + 1)),
                name: 'profil-pemda',
            });
            return path;
        }
        catch (error) {
            throw error;
        }
    }
    async downloadProfil(id, type) {
        try {
            if (type === 'pdf')
                throw new api_error_1.default({
                    httpCode: 403,
                    message: 'Download PDF under maintenance',
                });
            const data = await this.repo.findOneOrFail({ where: { id } });
            const path = await this.exportService.download({
                data: [(0, profil_pemda_schema_1.toDownload)(data, 1)],
                name: 'profil-pemda',
            });
            return path;
        }
        catch (error) {
            throw error;
        }
    }
    async create(nama_pemda, nama_daerah, opd_yang_menangani, alamat_pemda, email, no_telpon, nama_admin, file, username = '') {
        const queryRunner = data_source_1.AppDataSource.createQueryRunner();
        await queryRunner.startTransaction();
        try {
            const create = this.repo.create({
                nama_daerah,
                user: await this.repo.manager.getRepository(User_entity_1.default).findOneByOrFail({ nama_pemda }),
                opd_yang_menangani,
                alamat_pemda,
                email,
                no_telpon,
                nama_admin,
                created_by: username,
                updated_by: username,
            });
            if (file) {
                const uploaded = await this.fileService.create(file, undefined, username);
                create.document = uploaded;
            }
            const pemda = await this.repo.save(create);
            const indicators = await this.repo.manager.find(Indicator_entity_1.default, {
                where: { jenis_indikator: 'spd' },
            });
            pemda.indicators = indicators.map(indicator => {
                return this.repo.manager.create(PemdaIndikator_entity_1.PemdaIndikator, {
                    pemda,
                    indicator,
                    created_by: username,
                    updated_by: username,
                });
            });
            await this.repo.save(pemda);
            await queryRunner.commitTransaction();
            return create;
        }
        catch (error) {
            await queryRunner.rollbackTransaction();
            throw new api_error_1.default({
                httpCode: http_code_enum_1.HttpCode.INTERNAL_SERVER_ERROR,
                message: JSON.stringify(error),
            });
        }
    }
    async detail(id) {
        const res = await this.repo.findOne({
            where: { id: id },
            relations: { innovations: true },
        });
        if (!res) {
            throw new api_error_1.default({
                httpCode: http_code_enum_1.HttpCode.NOT_FOUND,
                message: 'Data not found',
            });
        }
        return res;
    }
    async get(url = '', username = '', req) {
        var _a;
        const query = this.repo
            .createQueryBuilder('e')
            .leftJoinAndSelect('e.user', 'u')
            .leftJoinAndSelect('e.document', 'd')
            .where(`lower(e.nama_daerah) like lower(:q)`, { q: `%${(_a = req.q) !== null && _a !== void 0 ? _a : ''}%` });
        const total = await query.getCount();
        const { limit, offset, page, order } = pagination_1.default.options(req.page, req.limit);
        const user = await this.repo.manager.getRepository(User_entity_1.default).findOneByOrFail({ username });
        if (user && user.role && user.role.is_super_admin === 't') {
            query.andWhere(`e.created_by = (:created_by)`, { created_by: username });
        }
        query.limit(limit).offset(offset);
        for (const key in order) {
            query.addOrderBy(`e.${key}`, order[key]);
        }
        const res = await query.getMany();
        let paging = pagination_1.default.build(url, page, limit, total);
        return { paging, res };
    }
    async delete(id) {
        const { affected } = await this.repo.delete({ id });
        if (affected && affected > 0) {
            return true;
        }
        return false;
    }
    async update(id, nama_pemda, nama_daerah, opd_yang_menangani, alamat_pemda, email, no_telpon, nama_admin, file, username = '') {
        const update = this.repo.create({
            nama_daerah,
            user: await this.repo.manager.getRepository(User_entity_1.default).findOneByOrFail({ nama_pemda }),
            opd_yang_menangani,
            alamat_pemda,
            email,
            no_telpon,
            nama_admin,
            updated_by: username,
        });
        if (file) {
            const uploaded = await this.fileService.create(file, undefined, username);
            update.document = uploaded;
        }
        const { affected } = await this.repo.update(id, update);
        if (!affected) {
            return false;
        }
        return true;
    }
};
__decorate([
    (0, tsoa_1.Tags)('Export'),
    (0, tsoa_1.Get)('/download/{type}'),
    __param(1, (0, tsoa_1.Query)()),
    __param(1, (0, tsoa_1.Hidden)()),
    __param(2, (0, tsoa_1.Queries)()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [String, Object, Object]),
    __metadata("design:returntype", Promise)
], ProfilPemdaService.prototype, "download", null);
__decorate([
    (0, tsoa_1.Tags)('Export'),
    (0, tsoa_1.Get)('/{id}/download/{type}'),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Number, String]),
    __metadata("design:returntype", Promise)
], ProfilPemdaService.prototype, "downloadProfil", null);
__decorate([
    (0, tsoa_1.Post)(''),
    __param(0, (0, tsoa_1.FormField)()),
    __param(1, (0, tsoa_1.FormField)()),
    __param(2, (0, tsoa_1.FormField)()),
    __param(3, (0, tsoa_1.FormField)()),
    __param(4, (0, tsoa_1.FormField)()),
    __param(5, (0, tsoa_1.FormField)()),
    __param(6, (0, tsoa_1.FormField)()),
    __param(7, (0, tsoa_1.UploadedFile)()),
    __param(8, (0, tsoa_1.Query)()),
    __param(8, (0, tsoa_1.Hidden)()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [String, String, String, String, String, String, String, Object, String]),
    __metadata("design:returntype", Promise)
], ProfilPemdaService.prototype, "create", null);
__decorate([
    (0, tsoa_1.Get)('/{id}'),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Number]),
    __metadata("design:returntype", Promise)
], ProfilPemdaService.prototype, "detail", null);
__decorate([
    (0, tsoa_1.Get)(''),
    __param(0, (0, tsoa_1.Query)('url')),
    __param(0, (0, tsoa_1.Hidden)()),
    __param(1, (0, tsoa_1.Query)()),
    __param(1, (0, tsoa_1.Hidden)()),
    __param(2, (0, tsoa_1.Queries)()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [String, String, Object]),
    __metadata("design:returntype", Promise)
], ProfilPemdaService.prototype, "get", null);
__decorate([
    (0, tsoa_1.Delete)('/{id}'),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Number]),
    __metadata("design:returntype", Promise)
], ProfilPemdaService.prototype, "delete", null);
__decorate([
    (0, tsoa_1.Patch)('/{id}'),
    __param(1, (0, tsoa_1.FormField)()),
    __param(2, (0, tsoa_1.FormField)()),
    __param(3, (0, tsoa_1.FormField)()),
    __param(4, (0, tsoa_1.FormField)()),
    __param(5, (0, tsoa_1.FormField)()),
    __param(6, (0, tsoa_1.FormField)()),
    __param(7, (0, tsoa_1.FormField)()),
    __param(8, (0, tsoa_1.UploadedFile)()),
    __param(9, (0, tsoa_1.Query)()),
    __param(9, (0, tsoa_1.Hidden)()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Number, String, String, String, String, String, String, String, Object, String]),
    __metadata("design:returntype", Promise)
], ProfilPemdaService.prototype, "update", null);
ProfilPemdaService = __decorate([
    (0, tsoa_1.Route)('/profil_pemda'),
    (0, tsoa_1.Tags)('Database Inovasi Daerah | Profil Pemda'),
    (0, tsoa_1.Security)('bearer')
], ProfilPemdaService);
exports.default = ProfilPemdaService;
