import Area from '../entity/Area.entity';
import { PaginationResponse as PaginationResponseInterface } from '../interface/pagination.interface';
import { CreateAreaRequest, GetAreaRequest } from '../schema/area.schema';
import { PaginationResponse, SuccessResponse } from '../types/response.type';
export default class AreaService {
    private repo;
    constructor();
    create(req: CreateAreaRequest): Promise<SuccessResponse<Area> | Area>;
    detail(id: number): Promise<SuccessResponse<Area> | Area>;
    get(url: string | undefined, req: GetAreaRequest): Promise<PaginationResponse<PaginationResponseInterface, Array<Area>> | {
        paging: PaginationResponseInterface;
        res: Array<Area>;
    }>;
    delete(id: number): Promise<SuccessResponse<string> | boolean>;
    update(id: number, req: CreateAreaRequest): Promise<SuccessResponse<string> | boolean>;
}
