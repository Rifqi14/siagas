"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const tsoa_1 = require("tsoa");
const government_profile_repository_1 = __importDefault(require("../repository/government_profile.repository"));
const file_service_1 = __importDefault(require("./file.service"));
const api_error_1 = __importDefault(require("../providers/exceptions/api.error"));
const pagination_1 = __importDefault(require("../providers/pagination"));
const fs_1 = require("fs");
const path_1 = require("path");
const http_code_enum_1 = require("../types/http_code.enum");
let GovernmentProfileService = class GovernmentProfileService {
    constructor() {
        this.repo = new government_profile_repository_1.default();
        this.fileService = new file_service_1.default();
    }
    async create(daerah, name, opd_menangani, alamat, email, nomor_telepon, nama_admin, file, username = '') {
        const entity = this.repo.create({
            region_id: parseInt(daerah),
            name,
            pic: opd_menangani,
            address: alamat,
            email,
            phone: nomor_telepon,
            admin_name: nama_admin
        });
        if (file) {
            const uploaded = await this.fileService.create(file, undefined, username);
            entity.file = uploaded;
        }
        await this.repo.insert(entity);
        return entity;
    }
    async get(req, url = 'http://localhost') {
        var _a;
        const query = this.repo
            .createQueryBuilder('gp')
            .leftJoinAndSelect('gp.region', 'r')
            .leftJoinAndSelect('gp.file', 'f')
            .where(`lower(gp.name) like lower(:name)`, {
            name: `%${(_a = req.q) !== null && _a !== void 0 ? _a : ''}%`
        });
        if (req.daerah) {
            query.andWhere(`r.name = :daerah`, { daerah: req.daerah });
        }
        const total = await query.getCount();
        const { limit, offset, page, order } = pagination_1.default.options(req.page, req.limit);
        query.limit(limit).offset(offset);
        const res = await query.getMany();
        let paging = pagination_1.default.build(url, page, limit, total);
        return { paging, res };
    }
    async delete(id) {
        const entity = await this.repo.findOneBy({ id: id });
        const { affected } = await this.repo.delete({ id: id });
        if (affected && affected > 0) {
            if (entity && entity.file) {
                (0, fs_1.unlink)((0, path_1.join)(__dirname, '..', '..', 'public', 'upload', 'inovasi_pemda', entity.file.name), err => {
                    if (err) {
                        console.log(`${entity.file.name} was deleted`);
                    }
                });
            }
            return true;
        }
        return false;
    }
    async detail(id) {
        const res = await this.repo.findOneBy({ id: id });
        if (!res) {
            throw new api_error_1.default({
                httpCode: http_code_enum_1.HttpCode.NOT_FOUND,
                message: 'Data not found'
            });
        }
        return res;
    }
    async update(id, daerah, name, opd_menangani, alamat, email, nomor_telepon, nama_admin, file, username = '') {
        const entity = this.repo.create({
            region_id: parseInt(daerah),
            name,
            pic: opd_menangani,
            address: alamat,
            email,
            phone: nomor_telepon,
            admin_name: nama_admin
        });
        if (file) {
            const uploaded = await this.fileService.create(file, undefined, username);
            entity.file = uploaded;
        }
        await this.repo.update(id, entity);
        return entity;
    }
};
__decorate([
    (0, tsoa_1.Post)(''),
    __param(0, (0, tsoa_1.FormField)()),
    __param(1, (0, tsoa_1.FormField)()),
    __param(2, (0, tsoa_1.FormField)()),
    __param(3, (0, tsoa_1.FormField)()),
    __param(4, (0, tsoa_1.FormField)()),
    __param(5, (0, tsoa_1.FormField)()),
    __param(6, (0, tsoa_1.FormField)()),
    __param(7, (0, tsoa_1.UploadedFile)()),
    __param(8, (0, tsoa_1.Query)()),
    __param(8, (0, tsoa_1.Hidden)()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [String, String, String, String, String, String, String, Object, String]),
    __metadata("design:returntype", Promise)
], GovernmentProfileService.prototype, "create", null);
__decorate([
    (0, tsoa_1.Get)(''),
    __param(0, (0, tsoa_1.Queries)()),
    __param(1, (0, tsoa_1.Query)('url')),
    __param(1, (0, tsoa_1.Hidden)()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Object, String]),
    __metadata("design:returntype", Promise)
], GovernmentProfileService.prototype, "get", null);
__decorate([
    (0, tsoa_1.Delete)('/{id}'),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Number]),
    __metadata("design:returntype", Promise)
], GovernmentProfileService.prototype, "delete", null);
__decorate([
    (0, tsoa_1.Get)('/{id}'),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Number]),
    __metadata("design:returntype", Promise)
], GovernmentProfileService.prototype, "detail", null);
__decorate([
    (0, tsoa_1.Patch)('/{id}'),
    __param(1, (0, tsoa_1.FormField)()),
    __param(2, (0, tsoa_1.FormField)()),
    __param(3, (0, tsoa_1.FormField)()),
    __param(4, (0, tsoa_1.FormField)()),
    __param(5, (0, tsoa_1.FormField)()),
    __param(6, (0, tsoa_1.FormField)()),
    __param(7, (0, tsoa_1.FormField)()),
    __param(8, (0, tsoa_1.UploadedFile)()),
    __param(9, (0, tsoa_1.Query)()),
    __param(9, (0, tsoa_1.Hidden)()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Number, String, String, String, String, String, String, String, Object, String]),
    __metadata("design:returntype", Promise)
], GovernmentProfileService.prototype, "update", null);
GovernmentProfileService = __decorate([
    (0, tsoa_1.Hidden)(),
    (0, tsoa_1.Route)('/profile_pemda'),
    (0, tsoa_1.Tags)('Profil Pemda'),
    (0, tsoa_1.Security)('bearer')
], GovernmentProfileService);
exports.default = GovernmentProfileService;
