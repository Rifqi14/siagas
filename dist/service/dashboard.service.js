"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const Dashboard_entity_1 = require("../entity/Dashboard.entity");
const PemdaIndikator_entity_1 = require("../entity/PemdaIndikator.entity");
const ProfilePemda_entity_1 = require("../entity/ProfilePemda.entity");
const User_entity_1 = __importDefault(require("../entity/User.entity"));
const api_error_1 = __importDefault(require("../providers/exceptions/api.error"));
const pagination_1 = __importDefault(require("../providers/pagination"));
const dashboard_repository_1 = __importDefault(require("../repository/dashboard.repository"));
const dashboard_schema_1 = require("../schema/dashboard.schema");
const tsoa_1 = require("tsoa");
const typeorm_1 = require("typeorm");
const export_service_1 = __importDefault(require("./export.service"));
let DashboardService = class DashboardService {
    constructor() {
        this.repo = new dashboard_repository_1.default();
        this.exportService = new export_service_1.default();
    }
    async get(url = "", username = "", req) {
        var _a;
        const query = this.repo.viewArsip
            .createQueryBuilder("a")
            .where(`lower(innovation_name) ilike lower(:q)`, {
            q: `%${(_a = req.q) !== null && _a !== void 0 ? _a : ""}%`,
        });
        if (req.tahapan && req.tahapan !== "semua") {
            query.andWhere(`lower(innovation_phase) ilike lower(:tahapan)`, {
                tahapan: req.tahapan.toLowerCase(),
            });
        }
        if (req.pemda_id) {
            query.andWhere(`pemda_id = :pemda_id`, { pemda_id: req.pemda_id });
        }
        const total = await query.getCount();
        const { limit, offset, page, order } = pagination_1.default.options(req.page, req.limit);
        if (username != "") {
            const user = await this.repo.viewArsip.manager
                .getRepository(User_entity_1.default)
                .findOneByOrFail({ username });
            if (user && user.role && user.role.is_super_admin === "t") {
                query.andWhere(`a.created_by = (:created_by)`, {
                    created_by: username,
                });
            }
        }
        query.limit(limit).offset(offset);
        const res = await query.getMany();
        let paging = pagination_1.default.build(url, page, limit, total);
        return { paging, res };
    }
    async arsip_download(type, username = "", req) {
        var _a;
        try {
            if (type === "pdf")
                throw new api_error_1.default({
                    httpCode: 403,
                    message: "Download PDF under maintenance",
                });
            let where = {
                innovation_name: (0, typeorm_1.ILike)(`%${(_a = req.q) !== null && _a !== void 0 ? _a : ""}%`),
            };
            if (req.tahapan && req.tahapan !== "semua")
                where = Object.assign(Object.assign({}, where), { innovation_phase: req.tahapan });
            if (req.pemda_id)
                where = Object.assign(Object.assign({}, where), { pemda_id: +req.pemda_id });
            if (username != "") {
                const user = await this.repo.manager
                    .getRepository(User_entity_1.default)
                    .findOneByOrFail({ username });
                if (user && user.role && user.role.is_super_admin === "t")
                    where = Object.assign(Object.assign({}, where), { created_by: username });
            }
            const data = await this.repo.viewArsip.find({ where });
            const path = await this.exportService.download({
                data: data.map((value, idx) => (0, dashboard_schema_1.toDashboardArsipDownload)(value, idx + 1)),
                name: "arsip",
            });
            return path;
        }
        catch (error) {
            throw error;
        }
    }
    async statistik_indikator(req) {
        const manager = this.repo.manager;
        const query = manager
            .createQueryBuilder(PemdaIndikator_entity_1.PemdaIndikator, "pi")
            .leftJoinAndSelect("pi.indicator", "pii")
            .leftJoinAndSelect("pi.files", "pif")
            .where(`pi.pemda_id = :pemda_id`, { pemda_id: req.pemda_id });
        const total_indikators = await query.getCount();
        const res = (await query.getMany()).map(value => {
            return {
                id: value.id.toString(),
                nama_indikator: value.indicator.nama_indikator,
                total_indikator: total_indikators,
                jumlah_upload: value.files ? value.files.length : 0,
            };
        });
        return res;
    }
    async statistik_inovasi() {
        const manager = this.repo.manager;
        const res = await manager.find(Dashboard_entity_1.Dashboard, {});
        const response = {
            indeks_rata_rata: res &&
                res.length > 0 &&
                res[0].total_inovasi > 0 &&
                res[0].total_pemda > 0
                ? res[0].total_inovasi / res[0].total_pemda
                : 0,
            total_inovasi: res && res.length > 0 ? +res[0].total_inovasi : 0,
            total_uji_coba: res && res.length > 0 ? +res[0].total_uji_coba : 0,
            total_penerapan: res && res.length > 0 ? +res[0].total_penerapan : 0,
            total_inisiatif: res && res.length > 0 ? +res[0].total_inisiatif : 0,
            daerah_inovasi_tertinggi: res && res.length > 0 ? res[0].daerah_tertinggi : "",
            daerah_inovasi_terendah: res && res.length > 0 ? res[0].daerah_terendah : "",
        };
        return response;
    }
    async opd_menangani() {
        const manager = this.repo.manager;
        const response = {
            total_badan_litbang: await manager.countBy(ProfilePemda_entity_1.ProfilPemda, {
                opd_yang_menangani: (0, typeorm_1.ILike)("Badan Litbang"),
            }),
            total_bappeda_litbang: await manager.countBy(ProfilePemda_entity_1.ProfilPemda, {
                opd_yang_menangani: (0, typeorm_1.ILike)("Bappeda Litbang"),
            }),
            total_diluar: await manager.countBy(ProfilePemda_entity_1.ProfilPemda, {
                opd_yang_menangani: (0, typeorm_1.ILike)("diluar"),
            }),
        };
        return response;
    }
};
__decorate([
    (0, tsoa_1.Get)("/arsip"),
    __param(0, (0, tsoa_1.Query)()),
    __param(0, (0, tsoa_1.Hidden)()),
    __param(1, (0, tsoa_1.Query)()),
    __param(1, (0, tsoa_1.Hidden)()),
    __param(2, (0, tsoa_1.Queries)()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Object, Object, Object]),
    __metadata("design:returntype", Promise)
], DashboardService.prototype, "get", null);
__decorate([
    (0, tsoa_1.Tags)("Export"),
    (0, tsoa_1.Get)("/arsip/download/{type}"),
    __param(1, (0, tsoa_1.Query)()),
    __param(1, (0, tsoa_1.Hidden)()),
    __param(2, (0, tsoa_1.Queries)()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [String, Object, Object]),
    __metadata("design:returntype", Promise)
], DashboardService.prototype, "arsip_download", null);
__decorate([
    (0, tsoa_1.Get)("/statistik_indikator"),
    __param(0, (0, tsoa_1.Queries)()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Object]),
    __metadata("design:returntype", Promise)
], DashboardService.prototype, "statistik_indikator", null);
__decorate([
    (0, tsoa_1.Get)("/statistik_inovasi"),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", []),
    __metadata("design:returntype", Promise)
], DashboardService.prototype, "statistik_inovasi", null);
__decorate([
    (0, tsoa_1.Get)("/statistik_opd"),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", []),
    __metadata("design:returntype", Promise)
], DashboardService.prototype, "opd_menangani", null);
DashboardService = __decorate([
    (0, tsoa_1.Route)("/dashboard"),
    (0, tsoa_1.Tags)("Dashboard")
], DashboardService);
exports.default = DashboardService;
