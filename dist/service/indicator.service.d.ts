import Indicator from '../entity/Indicator.entity';
import { PaginationResponse as PaginationResponseInterface } from '../interface/pagination.interface';
import { PaginationData } from '../schema/base.schema';
import { CreateIndicatorRequest, GetIndicatorRequest } from '../schema/indicator.schema';
import { PaginationResponse, SuccessResponse } from '../types/response.type';
export default class IndicatorService {
    private repo;
    constructor();
    /**
     *
     * jenis_indikator `ENUM [spd: Satuan Pemerintah Daerah, si: Satuan Inovasi, iv: Indikator Validasi, ipkd: Indikator Presentasi Kepala Daerah]`
     */
    create(req: CreateIndicatorRequest): Promise<SuccessResponse<Indicator> | Indicator>;
    detail(id: number): Promise<SuccessResponse<Indicator> | Indicator>;
    get(url: string | undefined, req: GetIndicatorRequest): Promise<PaginationResponse<PaginationResponseInterface, Indicator[]> | PaginationData<Indicator[]>>;
    delete(id: number): Promise<SuccessResponse<string> | boolean>;
    /**
     *
     * @param jenis_indikator `ENUM [spd: Satuan Pemerintah Daerah, si: Satuan Inovasi, iv: Indikator Validasi, ipkd: Indikator Presentasi Kepala Daerah]`
     */
    update(id: number, req: CreateIndicatorRequest): Promise<SuccessResponse<string> | boolean>;
}
