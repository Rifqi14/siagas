"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const EvaluasiInovasiDaerah_entity_1 = require("../entity/EvaluasiInovasiDaerah.entity");
const Indicator_entity_1 = __importDefault(require("../entity/Indicator.entity"));
const InovasiIndikator_entity_1 = require("../entity/InovasiIndikator.entity");
const ProfilePemda_entity_1 = require("../entity/ProfilePemda.entity");
const ReviewInovasiDaerah_entity_1 = require("../entity/ReviewInovasiDaerah.entity");
const User_entity_1 = __importDefault(require("../entity/User.entity"));
const api_error_1 = __importDefault(require("../providers/exceptions/api.error"));
const pagination_1 = __importDefault(require("../providers/pagination"));
const review_inovasi_daerah_repository_1 = __importDefault(require("../repository/review_inovasi_daerah.repository"));
const review_inovasi_daerah_schema_1 = require("../schema/review_inovasi_daerah.schema");
const http_code_enum_1 = require("../types/http_code.enum");
const tsoa_1 = require("tsoa");
const export_service_1 = __importDefault(require("./export.service"));
let ReviewInovasiDaerahService = class ReviewInovasiDaerahService {
    constructor() {
        this.repo = new review_inovasi_daerah_repository_1.default();
        this.exportService = new export_service_1.default();
    }
    async create(username = '', req) {
        const indicator = this.repo.create(Object.assign(Object.assign({}, req), { random_number: Math.floor(Math.random() * (1000000000000 - 100000000000)) + 100000000000, created_by: username, updated_by: username }));
        await this.repo.insert(indicator);
        return indicator;
    }
    async download(type, username = '', req) {
        try {
            if (type === 'pdf')
                throw new api_error_1.default({
                    httpCode: 403,
                    message: 'Download PDF under maintenance',
                });
            let where = {};
            if (req.pemda_id)
                where = Object.assign(Object.assign({}, where), { inovasi: { pemda_id: +req.pemda_id } });
            if (username) {
                const user = await this.repo.manager.getRepository(User_entity_1.default).findOneByOrFail({ username });
                if (user && user.role && user.role.is_super_admin === 't')
                    where = Object.assign(Object.assign({}, where), { created_by: username });
            }
            const data = await this.repo.find({
                where,
                relations: { inovasi: { profilPemda: true } },
            });
            const path = await this.exportService.download({
                data: data.map((value, idx) => (0, review_inovasi_daerah_schema_1.toReviewInovasiDaerahDownload)(value, idx + 1)),
                name: 'review-inovasi-daerah',
            });
            return path;
        }
        catch (error) {
            throw error;
        }
    }
    async detail(id) {
        const res = await this.repo.findOne({
            where: { id: +id },
            relations: {
                inovasi: {
                    profilPemda: true,
                },
                previews: {
                    evaluasi: true,
                },
            },
        });
        if (!res) {
            throw new api_error_1.default({
                httpCode: http_code_enum_1.HttpCode.NOT_FOUND,
                message: 'Data not found',
            });
        }
        return this.toResponse(res);
    }
    async getProfilInovasi(id) {
        const res = await this.repo.findOne({
            where: { id: +id },
            relations: {
                inovasi: {
                    profilPemda: true,
                    urusan: true,
                },
            },
        });
        if (!res) {
            throw new api_error_1.default({
                httpCode: http_code_enum_1.HttpCode.NOT_FOUND,
                message: 'Data not found',
            });
        }
        return await this.toProfilResponse(res);
    }
    async getIndikator(id) {
        const indikator = await this.repo.manager.find(Indicator_entity_1.default, {
            where: { jenis_indikator: 'si' },
        });
        const res = await this.repo.findOne({
            where: { id: +id },
            relations: {
                inovasi: true,
            },
        });
        if (!res) {
            throw new api_error_1.default({
                httpCode: http_code_enum_1.HttpCode.NOT_FOUND,
                message: 'Data not found',
            });
        }
        const data = indikator.map(async (indikator) => {
            const inovasi_indikator = await this.repo.manager.findOne(InovasiIndikator_entity_1.InovasiIndikator, {
                where: {
                    inovasi_id: res.inovasi_id,
                    indikator_id: indikator.id,
                },
                relations: {
                    evaluasi: true,
                },
            });
            const { evaluasi } = inovasi_indikator !== null && inovasi_indikator !== void 0 ? inovasi_indikator : { evaluasi: undefined };
            return this.toIndikatorRespons(indikator, evaluasi ? evaluasi[0] : undefined);
        });
        return await Promise.all(data);
    }
    async getIndikatorDetail(id, indikator_id) {
        const indikator = await this.repo.manager.findOneOrFail(Indicator_entity_1.default, {
            where: { jenis_indikator: 'si', id: +indikator_id },
        });
        const res = await this.repo.findOne({
            where: { id: +id },
            relations: {
                inovasi: true,
            },
        });
        if (!res) {
            throw new api_error_1.default({
                httpCode: http_code_enum_1.HttpCode.NOT_FOUND,
                message: 'Data not found',
            });
        }
        const inovasi_indikator = await this.repo.manager.findOneOrFail(InovasiIndikator_entity_1.InovasiIndikator, {
            where: {
                inovasi_id: res.inovasi_id,
                indikator_id: indikator.id,
            },
            relations: {
                evaluasi: true,
            },
        });
        const { evaluasi } = inovasi_indikator !== null && inovasi_indikator !== void 0 ? inovasi_indikator : { evaluasi: undefined };
        return this.toIndikatorRespons(indikator, evaluasi ? evaluasi[0] : undefined);
    }
    async updateEvaluasi(id, indikator_id, username = '', data) {
        const review_inovasi_daerah = await this.repo.findOneOrFail({ where: { id: id } });
        let inovasi_indikator = await this.repo.manager.findOne(InovasiIndikator_entity_1.InovasiIndikator, {
            where: { inovasi_id: review_inovasi_daerah.inovasi_id, indikator_id: +indikator_id },
        });
        if (!inovasi_indikator) {
            await this.repo.manager.create(InovasiIndikator_entity_1.InovasiIndikator, { inovasi_id: review_inovasi_daerah.inovasi_id, indikator_id: +indikator_id }).save();
        }
        inovasi_indikator = await this.repo.manager.findOneOrFail(InovasiIndikator_entity_1.InovasiIndikator, {
            where: { inovasi_id: review_inovasi_daerah.inovasi_id, indikator_id: +indikator_id },
        });
        const res = await this.repo.manager
            .create(EvaluasiInovasiDaerah_entity_1.EvaluasiInovasiDaerah, {
            id: data.evaluasi_id ? +data.evaluasi_id : undefined,
            inovasi_indikator_id: inovasi_indikator.id,
            data_saat_ini: data.data_saat_ini,
            keterangan: data.keterangan,
            kategori: data.kategori,
            created_by: data.evaluasi_id ? undefined : username,
            updated_by: username,
        })
            .save();
        return res;
    }
    async getEvaluasi(id, indikator_id, username = '') {
        const review = await this.repo.manager.findOneOrFail(ReviewInovasiDaerah_entity_1.ReviewInovasiDaerah, {
            where: { id },
        });
        const res = await this.repo.manager.findOne(InovasiIndikator_entity_1.InovasiIndikator, {
            where: {
                indikator_id,
                inovasi_id: review.inovasi_id,
                created_by: username,
            },
            relations: {
                evaluasi: true,
                inovasi: true,
            },
        });
        if (!res) {
            throw new api_error_1.default({
                httpCode: http_code_enum_1.HttpCode.NOT_FOUND,
                message: 'Data not found',
            });
        }
        return this.toEvaluasiResponse(res, review);
    }
    async get(url = '', username = '', req) {
        var _a, _b;
        const query = this.repo
            .createQueryBuilder(`e`)
            .leftJoinAndSelect('e.inovasi', 'i')
            .leftJoinAndSelect('i.profilPemda', 'ip')
            .leftJoinAndSelect('e.previews', 'ep')
            .leftJoinAndSelect('ep.evaluasi', 'epv')
            .where(`(lower(i.innovation_name) like lower(:q) or lower(ip.nama_daerah) ilike :nama_daerah)`, {
            q: `%${(_a = req.q) !== null && _a !== void 0 ? _a : ''}%`,
            nama_daerah: `%${(_b = req.q) !== null && _b !== void 0 ? _b : ''}%`,
        });
        if (req.pemda_id) {
            query.where('ip.id = :pemda_id', { pemda_id: req.pemda_id });
        }
        const user = await this.repo.manager.getRepository(User_entity_1.default).findOneByOrFail({ username });
        if (user && user.role && user.role.name.toLowerCase() === 'user') {
            query.andWhere(`e.created_by = (:created_by)`, { created_by: username });
        }
        const total = await query.getCount();
        const { limit, offset, page, order } = pagination_1.default.options(req.page, req.limit);
        query.limit(limit).offset(offset);
        for (const key in order) {
            query.addOrderBy(`e.${key}`, order[key]);
        }
        const res = (await query.getMany()).map(response => this.toResponse(response));
        let paging = pagination_1.default.build(url, page, limit, total);
        return { paging, res };
    }
    async delete(id) {
        const { affected } = await this.repo.delete({ id: id });
        if (affected && affected > 0) {
            return true;
        }
        return false;
    }
    async update(id, username = '', req) {
        const update = this.repo.create(Object.assign(Object.assign({}, req), { updated_by: username }));
        const { affected } = await this.repo.update(id, update);
        if (!affected) {
            return false;
        }
        return true;
    }
    toEvaluasiResponse(data, review) {
        var _a, _b, _c, _d;
        const { evaluasi } = data;
        const res = {
            review_id: (_a = review.id.toString()) !== null && _a !== void 0 ? _a : null,
            inovasi_id: (_b = data.inovasi_id.toString()) !== null && _b !== void 0 ? _b : null,
            nama_inovasi: (_d = (_c = data.inovasi) === null || _c === void 0 ? void 0 : _c.innovation_name) !== null && _d !== void 0 ? _d : null,
            evaluasi_id: null,
            data_saat_ini: null,
            kategori: null,
            judul: null,
            keterangan: null,
        };
        if (evaluasi && evaluasi.length > 0) {
            const latestEval = evaluasi[evaluasi.length - 1];
            res.evaluasi_id = latestEval.id.toString();
            res.kategori = latestEval.kategori;
            res.data_saat_ini = latestEval.data_saat_ini;
            res.keterangan = latestEval.keterangan;
        }
        return res;
    }
    toIndikatorRespons(data, evaluasi) {
        var _a, _b, _c, _d, _e, _f;
        let skor = 0;
        let skorEvaluasi = 0;
        if (evaluasi) {
            switch ((_a = evaluasi.data_saat_ini) === null || _a === void 0 ? void 0 : _a.toLowerCase()) {
                case 'sk kepala perangkat daerah':
                    skor = 6;
                    break;
                case 'sk kepala daerah':
                    skor = 3;
                    break;
                case 'peraturan kepala daerah':
                case 'daerah':
                    skor = 9;
                    break;
                default:
                    skor = 0;
                    break;
            }
        }
        skorEvaluasi = skor;
        if (evaluasi) {
            switch ((_b = evaluasi.catatan) === null || _b === void 0 ? void 0 : _b.toLowerCase()) {
                case 'sesuai':
                    skorEvaluasi += 50;
                    break;
                case 'tidak sesuai':
                    skorEvaluasi += 0;
                    break;
                default:
                    skorEvaluasi += 0;
                    break;
            }
        }
        const res = {
            indikator_id: (_d = (_c = data.id) === null || _c === void 0 ? void 0 : _c.toString()) !== null && _d !== void 0 ? _d : null,
            name: (_e = data.nama_indikator) !== null && _e !== void 0 ? _e : null,
            informasi: (_f = data.keterangan) !== null && _f !== void 0 ? _f : null,
            skor: skor !== null && skor !== void 0 ? skor : null,
            skor_evaluasi: skorEvaluasi !== null && skorEvaluasi !== void 0 ? skorEvaluasi : null,
        };
        return res;
    }
    async toProfilResponse(data) {
        var _a, _b, _c, _d, _e, _f, _g, _h, _j, _k, _l, _m, _o, _p, _q, _r, _s, _t, _u, _v;
        const { inovasi } = data;
        const { profilPemda, urusan } = inovasi;
        const profil = await this.repo.manager.findOne(ProfilePemda_entity_1.ProfilPemda, {
            where: {
                id: profilPemda === null || profilPemda === void 0 ? void 0 : profilPemda.id,
            },
        });
        const res = {
            id: (_a = profilPemda === null || profilPemda === void 0 ? void 0 : profilPemda.id.toString()) !== null && _a !== void 0 ? _a : null,
            nama_pemda: (_b = profilPemda === null || profilPemda === void 0 ? void 0 : profilPemda.nama_daerah) !== null && _b !== void 0 ? _b : null,
            nama_inovasi: (_c = inovasi.government_name) !== null && _c !== void 0 ? _c : null,
            tahapan_inovasi: (_d = inovasi.innovation_phase) !== null && _d !== void 0 ? _d : null,
            inisiator_daerah: (_e = profilPemda === null || profilPemda === void 0 ? void 0 : profilPemda.nama_daerah) !== null && _e !== void 0 ? _e : null,
            jenis_inovasi: (_f = inovasi.innovation_type) !== null && _f !== void 0 ? _f : null,
            bentuk_inovasi_daerah: (_g = inovasi.innovation_form) !== null && _g !== void 0 ? _g : null,
            tematik: (_h = inovasi.thematic) !== null && _h !== void 0 ? _h : null,
            detail_tematik: (_j = inovasi.thematic_detail) !== null && _j !== void 0 ? _j : null,
            urusan_pemerintah: (_k = urusan === null || urusan === void 0 ? void 0 : urusan.name) !== null && _k !== void 0 ? _k : null,
            tingkatan: (_l = inovasi.innovation_initiator) !== null && _l !== void 0 ? _l : null,
            waktu_uji_coba: (_m = inovasi.trial_time) !== null && _m !== void 0 ? _m : null,
            waktu_penerapan: (_o = inovasi.implementation_time) !== null && _o !== void 0 ? _o : null,
            opd: (_p = profil === null || profil === void 0 ? void 0 : profil.opd_yang_menangani) !== null && _p !== void 0 ? _p : null,
            pemda: profil
                ? {
                    id: (_r = (_q = profil === null || profil === void 0 ? void 0 : profil.id) === null || _q === void 0 ? void 0 : _q.toString()) !== null && _r !== void 0 ? _r : null,
                    email: (_s = profil === null || profil === void 0 ? void 0 : profil.email) !== null && _s !== void 0 ? _s : null,
                    nama_admin: (_t = profil === null || profil === void 0 ? void 0 : profil.nama_admin) !== null && _t !== void 0 ? _t : null,
                    name: (_u = profil === null || profil === void 0 ? void 0 : profil.nama_daerah) !== null && _u !== void 0 ? _u : null,
                    no_telp: (_v = profil === null || profil === void 0 ? void 0 : profil.no_telpon) !== null && _v !== void 0 ? _v : null,
                }
                : null,
            dokumen: null,
        };
        return res;
    }
    toResponse(data) {
        var _a, _b, _c;
        let skor = 0;
        switch (data.inovasi.innovation_phase) {
            case 'inisiatif':
                skor = 50;
                break;
            case 'uji coba':
                skor = 102;
                break;
            case 'penerapan':
                skor = 105;
                break;
            default:
                skor = 0;
                break;
        }
        let skor_verifikasi = skor;
        if (data.status == 'Rejected')
            skor_verifikasi = 10;
        if (data.status == 'Accept')
            skor_verifikasi += 50;
        return {
            id: data.id.toString(),
            judul: data.inovasi.innovation_name,
            nomor: data.random_number.toString(),
            waktu_penerapan: data.inovasi.implementation_time,
            skor: skor.toString(),
            skor_verifikasi: skor_verifikasi.toString(),
            waktu_pengiriman: data.inovasi.created_at.toISOString(),
            inovasi_id: (_a = data.inovasi_id) === null || _a === void 0 ? void 0 : _a.toString(),
            qc: (_b = data.status) !== null && _b !== void 0 ? _b : null,
            pemda: data.inovasi && data.inovasi.profilPemda
                ? {
                    id: data.inovasi.profilPemda.id.toString(),
                    nama_pemda: data.inovasi.profilPemda.nama_daerah,
                }
                : null,
            preview: data.previews && data.previews.length > 0
                ? {
                    id: data.previews[data.previews.length - 1].id.toString(),
                    pemda: data.inovasi.profilPemda
                        ? {
                            id: data.inovasi.profilPemda.id.toString(),
                            nama_pemda: data.inovasi.profilPemda.nama_daerah,
                        }
                        : null,
                    judul: data.inovasi.innovation_name,
                    status: (_c = data.status) !== null && _c !== void 0 ? _c : '',
                    alasan_ditolak: data.previews[data.previews.length - 1] && data.previews[data.previews.length - 1].evaluasi.length > 0
                        ? data.previews[data.previews.length - 1].evaluasi[data.previews[data.previews.length - 1].evaluasi.length - 1].keterangan
                        : '',
                }
                : null,
        };
    }
};
__decorate([
    (0, tsoa_1.Post)(''),
    __param(0, (0, tsoa_1.Query)()),
    __param(0, (0, tsoa_1.Hidden)()),
    __param(1, (0, tsoa_1.Body)()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [String, Object]),
    __metadata("design:returntype", Promise)
], ReviewInovasiDaerahService.prototype, "create", null);
__decorate([
    (0, tsoa_1.Tags)('Export'),
    (0, tsoa_1.Get)('/download/{type}'),
    __param(1, (0, tsoa_1.Query)()),
    __param(1, (0, tsoa_1.Hidden)()),
    __param(2, (0, tsoa_1.Queries)()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [String, Object, Object]),
    __metadata("design:returntype", Promise)
], ReviewInovasiDaerahService.prototype, "download", null);
__decorate([
    (0, tsoa_1.Get)('/{id}'),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Number]),
    __metadata("design:returntype", Promise)
], ReviewInovasiDaerahService.prototype, "detail", null);
__decorate([
    (0, tsoa_1.Get)('/{id}/profil_inovasi'),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Number]),
    __metadata("design:returntype", Promise)
], ReviewInovasiDaerahService.prototype, "getProfilInovasi", null);
__decorate([
    (0, tsoa_1.Get)('/{id}/indikator'),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Number]),
    __metadata("design:returntype", Promise)
], ReviewInovasiDaerahService.prototype, "getIndikator", null);
__decorate([
    (0, tsoa_1.Get)('/{id}/indikator/{indikator_id}'),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Number, String]),
    __metadata("design:returntype", Promise)
], ReviewInovasiDaerahService.prototype, "getIndikatorDetail", null);
__decorate([
    (0, tsoa_1.Patch)('/{id}/indikator/{indikator_id}/evaluasi'),
    __param(2, (0, tsoa_1.Query)()),
    __param(2, (0, tsoa_1.Hidden)()),
    __param(3, (0, tsoa_1.Body)()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Number, String, Object, Object]),
    __metadata("design:returntype", Promise)
], ReviewInovasiDaerahService.prototype, "updateEvaluasi", null);
__decorate([
    (0, tsoa_1.Get)('/{id}/indikator/{indikator_id}/evaluasi'),
    __param(2, (0, tsoa_1.Query)()),
    __param(2, (0, tsoa_1.Hidden)()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Number, Number, Object]),
    __metadata("design:returntype", Promise)
], ReviewInovasiDaerahService.prototype, "getEvaluasi", null);
__decorate([
    (0, tsoa_1.Get)(),
    __param(0, (0, tsoa_1.Query)('url')),
    __param(0, (0, tsoa_1.Hidden)()),
    __param(1, (0, tsoa_1.Query)()),
    __param(1, (0, tsoa_1.Hidden)()),
    __param(2, (0, tsoa_1.Queries)()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [String, String, Object]),
    __metadata("design:returntype", Promise)
], ReviewInovasiDaerahService.prototype, "get", null);
__decorate([
    (0, tsoa_1.Delete)('/{id}'),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Number]),
    __metadata("design:returntype", Promise)
], ReviewInovasiDaerahService.prototype, "delete", null);
__decorate([
    (0, tsoa_1.Patch)('/{id}'),
    __param(1, (0, tsoa_1.Query)()),
    __param(1, (0, tsoa_1.Hidden)()),
    __param(2, (0, tsoa_1.Body)()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Number, String, Object]),
    __metadata("design:returntype", Promise)
], ReviewInovasiDaerahService.prototype, "update", null);
ReviewInovasiDaerahService = __decorate([
    (0, tsoa_1.Route)('/review_inovasi_daerah'),
    (0, tsoa_1.Tags)('Verifikasi Index | Review Inovasi Daerah'),
    (0, tsoa_1.Security)('bearer')
], ReviewInovasiDaerahService);
exports.default = ReviewInovasiDaerahService;
