import Uptd from '../entity/Uptd.entity';
import { PaginationData } from '../schema/base.schema';
import { CreateUptdRequest, GetUptdRequest } from '../schema/uptd.schema';
import { PaginationResponse, SuccessResponse } from '../types/response.type';
import { PaginationResponse as PaginationResponseInterface } from '../interface/pagination.interface';
export default class UptdService {
    private repo;
    constructor();
    create(req: CreateUptdRequest, username?: string): Promise<SuccessResponse<Uptd> | Uptd>;
    detail(id: number): Promise<SuccessResponse<Uptd> | Uptd>;
    get(url: string | undefined, req: GetUptdRequest): Promise<PaginationResponse<PaginationResponseInterface, Uptd[]> | PaginationData<Uptd[]>>;
    delete(id: number): Promise<SuccessResponse<string> | boolean>;
    update(id: number, req: CreateUptdRequest, username?: string): Promise<SuccessResponse<string> | boolean>;
}
