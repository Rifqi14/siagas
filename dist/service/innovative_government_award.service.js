"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const innovative_government_award_repository_1 = __importDefault(require("../repository/innovative_government_award.repository"));
const innovative_government_award_schema_1 = require("../schema/innovative_government_award.schema");
const tsoa_1 = require("tsoa");
const pagination_1 = __importDefault(require("../providers/pagination"));
const PeringkatHasilReview_entity_1 = require("../entity/PeringkatHasilReview.entity");
const User_entity_1 = __importDefault(require("../entity/User.entity"));
const ProfilePemda_entity_1 = require("../entity/ProfilePemda.entity");
const api_error_1 = __importDefault(require("../providers/exceptions/api.error"));
const typeorm_1 = require("typeorm");
const export_service_1 = __importDefault(require("./export.service"));
let InnovativeGovernmentAwardService = class InnovativeGovernmentAwardService {
    constructor() {
        this.repo = new innovative_government_award_repository_1.default();
        this.exportService = new export_service_1.default();
    }
    async peringkat_hasil_review(url = '', username = '', req) {
        var _a;
        const query = this.repo.manager
            .createQueryBuilder(PeringkatHasilReview_entity_1.PeringkatHasilReview, 'phr')
            .where(`lower(nama_daerah) ilike lower(:q) or lower(predikat) ilike lower(:q)`, { q: `%${(_a = req.q) !== null && _a !== void 0 ? _a : ''}%` });
        const total = await query.getCount();
        const { limit, offset, page, order } = pagination_1.default.options(req.page, req.limit);
        const user = await this.repo.manager
            .getRepository(User_entity_1.default)
            .findOneByOrFail({ username });
        if (user && user.role && user.role.is_super_admin === 't') {
            query.andWhere(`phr.created_by = (:created_by)`, {
                created_by: username
            });
        }
        query.limit(limit).offset(offset);
        const res = (await query.getMany()).map(value => (0, innovative_government_award_schema_1.toPeringkatResponse)(value, total));
        let paging = pagination_1.default.build(url, page, limit, total);
        return { paging, res };
    }
    async peringkat_hasil_review_download(type, username = '', req) {
        var _a;
        try {
            if (type === 'pdf')
                throw new api_error_1.default({
                    httpCode: 403,
                    message: 'Download PDF under maintenance'
                });
            let where = {
                nama_daerah: (0, typeorm_1.ILike)(`%${(_a = req.q) !== null && _a !== void 0 ? _a : ''}%`)
            };
            const user = await this.repo.manager
                .getRepository(User_entity_1.default)
                .findOneByOrFail({ username });
            if (user && user.role && user.role.is_super_admin === 't') {
                where = Object.assign(Object.assign({}, where), { created_by: username });
            }
            const [data, total] = await this.repo.manager.findAndCount(PeringkatHasilReview_entity_1.PeringkatHasilReview, {
                where
            });
            const path = await this.exportService.download({
                data: data.map(laporan => (0, innovative_government_award_schema_1.toPeringkatResponse)(laporan, total)),
                name: 'peringkat-hasil-review'
            });
            return path;
        }
        catch (error) {
            throw error;
        }
    }
    async nominator_peringkat_hasil_review(pemda_id, req) {
        const { affected } = await this.repo.manager
            .getRepository(ProfilePemda_entity_1.ProfilPemda)
            .update({ id: pemda_id }, { nominator: req.nominator });
        if (affected && affected > 0) {
            return true;
        }
        return false;
    }
    async prestasi(url = '', username = '', req) {
        var _a;
        const query = this.repo.manager
            .createQueryBuilder(PeringkatHasilReview_entity_1.PeringkatHasilReview, 'phr')
            .where(`lower(nama_daerah) ilike lower(:q)`, { q: `%${(_a = req.q) !== null && _a !== void 0 ? _a : ''}%` });
        const total = await query.getCount();
        const { limit, offset, page, order } = pagination_1.default.options(req.page, req.limit);
        const user = await this.repo.manager
            .getRepository(User_entity_1.default)
            .findOneByOrFail({ username });
        if (user && user.role && user.role.is_super_admin === 't') {
            query.andWhere(`phr.created_by = (:created_by)`, {
                created_by: username
            });
        }
        query.limit(limit).offset(offset);
        const res = (await query.getMany()).map(value => (0, innovative_government_award_schema_1.toPrestasiResponse)(value, total));
        let paging = pagination_1.default.build(url, page, limit, total);
        return { paging, res };
    }
    async prestasi_download(type, username = '', req) {
        var _a;
        try {
            if (type === 'pdf')
                throw new api_error_1.default({
                    httpCode: 403,
                    message: 'Download PDF under maintenance'
                });
            let where = {
                nama_daerah: (0, typeorm_1.ILike)(`%${(_a = req.q) !== null && _a !== void 0 ? _a : ''}%`)
            };
            const user = await this.repo.manager
                .getRepository(User_entity_1.default)
                .findOneByOrFail({ username });
            if (user && user.role && user.role.is_super_admin === 't') {
                where = Object.assign(Object.assign({}, where), { created_by: username });
            }
            const [data, total] = await this.repo.manager.findAndCount(PeringkatHasilReview_entity_1.PeringkatHasilReview, {
                where
            });
            const path = await this.exportService.download({
                data: data.map(laporan => (0, innovative_government_award_schema_1.toPrestasiResponse)(laporan, total)),
                name: 'prestasi-dan-hasil-lapangan'
            });
            return path;
        }
        catch (error) {
            throw error;
        }
    }
    async rangking(url = '', username = '', req) {
        var _a;
        const query = this.repo.manager
            .createQueryBuilder(PeringkatHasilReview_entity_1.PeringkatHasilReview, 'phr')
            .where(`lower(nama_daerah) ilike lower(:q)`, { q: `%${(_a = req.q) !== null && _a !== void 0 ? _a : ''}%` });
        const total = await query.getCount();
        const { limit, offset, page, order } = pagination_1.default.options(req.page, req.limit);
        const user = await this.repo.manager
            .getRepository(User_entity_1.default)
            .findOneByOrFail({ username });
        if (user && user.role && user.role.is_super_admin === 't') {
            query.andWhere(`phr.created_by = (:created_by)`, {
                created_by: username
            });
        }
        query.limit(limit).offset(offset);
        const res = (await query.getMany()).map(value => (0, innovative_government_award_schema_1.toRangkingResponse)(value, total));
        let paging = pagination_1.default.build(url, page, limit, total);
        return { paging, res };
    }
    async rangking_download(type, username = '', req) {
        var _a;
        try {
            if (type === 'pdf')
                throw new api_error_1.default({
                    httpCode: 403,
                    message: 'Download PDF under maintenance'
                });
            let where = {
                nama_daerah: (0, typeorm_1.ILike)(`%${(_a = req.q) !== null && _a !== void 0 ? _a : ''}%`)
            };
            const user = await this.repo.manager
                .getRepository(User_entity_1.default)
                .findOneByOrFail({ username });
            if (user && user.role && user.role.is_super_admin === 't') {
                where = Object.assign(Object.assign({}, where), { created_by: username });
            }
            const [data, total] = await this.repo.manager.findAndCount(PeringkatHasilReview_entity_1.PeringkatHasilReview, {
                where
            });
            const path = await this.exportService.download({
                data: data.map(laporan => (0, innovative_government_award_schema_1.toRangkingResponse)(laporan, total)),
                name: 'rangking-siagas'
            });
            return path;
        }
        catch (error) {
            throw error;
        }
    }
};
__decorate([
    (0, tsoa_1.Get)('/peringkat_hasil_review'),
    __param(0, (0, tsoa_1.Query)()),
    __param(0, (0, tsoa_1.Hidden)()),
    __param(1, (0, tsoa_1.Query)()),
    __param(1, (0, tsoa_1.Hidden)()),
    __param(2, (0, tsoa_1.Queries)()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Object, Object, Object]),
    __metadata("design:returntype", Promise)
], InnovativeGovernmentAwardService.prototype, "peringkat_hasil_review", null);
__decorate([
    (0, tsoa_1.Tags)('Export'),
    (0, tsoa_1.Get)('/peringkat_hasil_review/download/{type}'),
    __param(1, (0, tsoa_1.Query)()),
    __param(1, (0, tsoa_1.Hidden)()),
    __param(2, (0, tsoa_1.Queries)()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [String, Object, Object]),
    __metadata("design:returntype", Promise)
], InnovativeGovernmentAwardService.prototype, "peringkat_hasil_review_download", null);
__decorate([
    (0, tsoa_1.Patch)('/peringkat_hasil_review/{pemda_id}'),
    __param(1, (0, tsoa_1.Body)()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Number, Object]),
    __metadata("design:returntype", Promise)
], InnovativeGovernmentAwardService.prototype, "nominator_peringkat_hasil_review", null);
__decorate([
    (0, tsoa_1.Get)('/prestasi'),
    __param(0, (0, tsoa_1.Query)()),
    __param(0, (0, tsoa_1.Hidden)()),
    __param(1, (0, tsoa_1.Query)()),
    __param(1, (0, tsoa_1.Hidden)()),
    __param(2, (0, tsoa_1.Queries)()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Object, Object, Object]),
    __metadata("design:returntype", Promise)
], InnovativeGovernmentAwardService.prototype, "prestasi", null);
__decorate([
    (0, tsoa_1.Tags)('Export'),
    (0, tsoa_1.Get)('/prestasi/download/{type}'),
    __param(1, (0, tsoa_1.Query)()),
    __param(1, (0, tsoa_1.Hidden)()),
    __param(2, (0, tsoa_1.Queries)()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [String, Object, Object]),
    __metadata("design:returntype", Promise)
], InnovativeGovernmentAwardService.prototype, "prestasi_download", null);
__decorate([
    (0, tsoa_1.Get)('/rangking'),
    __param(0, (0, tsoa_1.Query)()),
    __param(0, (0, tsoa_1.Hidden)()),
    __param(1, (0, tsoa_1.Query)()),
    __param(1, (0, tsoa_1.Hidden)()),
    __param(2, (0, tsoa_1.Queries)()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Object, Object, Object]),
    __metadata("design:returntype", Promise)
], InnovativeGovernmentAwardService.prototype, "rangking", null);
__decorate([
    (0, tsoa_1.Tags)('Export'),
    (0, tsoa_1.Get)('/rangking/download/{type}'),
    __param(1, (0, tsoa_1.Query)()),
    __param(1, (0, tsoa_1.Hidden)()),
    __param(2, (0, tsoa_1.Queries)()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [String, Object, Object]),
    __metadata("design:returntype", Promise)
], InnovativeGovernmentAwardService.prototype, "rangking_download", null);
InnovativeGovernmentAwardService = __decorate([
    (0, tsoa_1.Route)('/innovative_government_award'),
    (0, tsoa_1.Tags)('Innovative Government Award'),
    (0, tsoa_1.Security)('bearer')
], InnovativeGovernmentAwardService);
exports.default = InnovativeGovernmentAwardService;
