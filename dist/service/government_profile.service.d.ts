/// <reference types="multer" />
import GovernmentProfile from '../entity/GovernmentProfile.entity';
import { GetGovernmentProfileRequest } from '../schema/government_profile.schema';
import { PaginationResponse as PaginationResponseInterface } from '../interface/pagination.interface';
import { PaginationResponse, SuccessResponse } from '../types/response.type';
export default class GovernmentProfileService {
    private repo;
    private fileService;
    create(daerah: string, name: string, opd_menangani: string, alamat: string, email: string, nomor_telepon: string, nama_admin: string, file?: Express.Multer.File, username?: string): Promise<GovernmentProfile>;
    get(req: GetGovernmentProfileRequest, url?: string): Promise<PaginationResponse<PaginationResponseInterface, Array<GovernmentProfile>> | {
        paging: PaginationResponseInterface;
        res: Array<GovernmentProfile>;
    }>;
    delete(id: number): Promise<SuccessResponse<string> | boolean>;
    detail(id: number): Promise<SuccessResponse<GovernmentProfile> | GovernmentProfile>;
    update(id: number, daerah: string, name: string, opd_menangani: string, alamat: string, email: string, nomor_telepon: string, nama_admin: string, file?: Express.Multer.File, username?: string): Promise<GovernmentProfile>;
}
