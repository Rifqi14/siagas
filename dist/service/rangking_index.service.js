"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const rangking_index_repository_1 = __importDefault(require("../repository/rangking_index.repository"));
const rangking_index_schema_1 = require("../schema/rangking_index.schema");
const tsoa_1 = require("tsoa");
const pagination_1 = __importDefault(require("../providers/pagination"));
const User_entity_1 = __importDefault(require("../entity/User.entity"));
const ProfilePemda_entity_1 = require("../entity/ProfilePemda.entity");
const api_error_1 = __importDefault(require("../providers/exceptions/api.error"));
const typeorm_1 = require("typeorm");
const export_service_1 = __importDefault(require("./export.service"));
let RangkingIndexService = class RangkingIndexService {
    constructor() {
        this.repo = new rangking_index_repository_1.default();
        this.exportService = new export_service_1.default();
    }
    async get(url = "", username = "", req) {
        var _a;
        const query = this.repo.repository.createQueryBuilder("ri").where(`lower(nama_daerah) ilike lower(:q)`, { q: `%${(_a = req.q) !== null && _a !== void 0 ? _a : ""}%` });
        const total = await query.getCount();
        const { limit, offset, page, order } = pagination_1.default.options(req.page, req.limit);
        const user = await this.repo.repository.manager.getRepository(User_entity_1.default).findOneByOrFail({ username });
        if (user && user.role && user.role.is_super_admin === "t") {
            query.andWhere(`ri.created_by = (:created_by)`, {
                created_by: username,
            });
        }
        query.limit(limit).offset(offset);
        query.orderBy(`ri.indeks`, "ASC");
        const res = await query.getMany();
        let paging = pagination_1.default.build(url, page, limit, total);
        return { paging, res };
    }
    async nominator(id, req) {
        const { affected } = await this.repo.repository.manager.getRepository(ProfilePemda_entity_1.ProfilPemda).update({ id }, { nominator: req.nominator });
        if (affected && affected > 0) {
            return true;
        }
        return false;
    }
    async download(type, username = "", req) {
        try {
            if (type === "pdf")
                throw new api_error_1.default({
                    httpCode: 403,
                    message: "Download PDF under maintenance",
                });
            let where = {
                nama_daerah: (0, typeorm_1.Raw)(alias => { var _a; return `lower(${alias}) ilike lower('%${(_a = req.q) !== null && _a !== void 0 ? _a : ""}%')`; }),
            };
            if (username) {
                const user = await this.repo.repository.manager.getRepository(User_entity_1.default).findOneByOrFail({ username });
                if (user && user.role && user.role.is_super_admin === "t")
                    where = Object.assign(Object.assign({}, where), { created_by: username });
            }
            const data = await this.repo.repository.find({
                where,
            });
            const path = await this.exportService.download({
                data: data.map((v, idx) => (0, rangking_index_schema_1.toDownload)(v, idx + 1)),
                name: "rangking-index",
            });
            return path;
        }
        catch (error) {
            throw error;
        }
    }
};
__decorate([
    (0, tsoa_1.Get)(""),
    __param(0, (0, tsoa_1.Query)()),
    __param(0, (0, tsoa_1.Hidden)()),
    __param(1, (0, tsoa_1.Query)()),
    __param(1, (0, tsoa_1.Hidden)()),
    __param(2, (0, tsoa_1.Queries)()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Object, Object, Object]),
    __metadata("design:returntype", Promise)
], RangkingIndexService.prototype, "get", null);
__decorate([
    (0, tsoa_1.Patch)("/{id}"),
    __param(1, (0, tsoa_1.Body)()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Number, Object]),
    __metadata("design:returntype", Promise)
], RangkingIndexService.prototype, "nominator", null);
__decorate([
    (0, tsoa_1.Tags)("Export"),
    (0, tsoa_1.Get)("/download/{type}"),
    __param(1, (0, tsoa_1.Query)()),
    __param(1, (0, tsoa_1.Hidden)()),
    __param(2, (0, tsoa_1.Queries)()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [String, Object, Object]),
    __metadata("design:returntype", Promise)
], RangkingIndexService.prototype, "download", null);
RangkingIndexService = __decorate([
    (0, tsoa_1.Route)("/ranking_index"),
    (0, tsoa_1.Tags)("Verifikasi Index", "Verifikasi Index | Ranking Index"),
    (0, tsoa_1.Security)("bearer")
], RangkingIndexService);
exports.default = RangkingIndexService;
