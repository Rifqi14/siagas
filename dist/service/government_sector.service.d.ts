import GovernmentSector from '../entity/GovernmentSector';
import { PaginationResponse as PaginationResponseInterface } from '../interface/pagination.interface';
import { CreateGovernmentSectorRequest, GetGovernmentSectorRequest } from '../schema/government_sector.schema';
import { PaginationResponse, SuccessResponse } from '../types/response.type';
export default class GovernmentSectorService {
    private repo;
    constructor();
    /**
     * parameter name: Nama dari urusan pemerintah
     * paremeter deadline: diisi dengan format YYYY-MM-DD
     */
    create(req: CreateGovernmentSectorRequest): Promise<GovernmentSector>;
    get(req: GetGovernmentSectorRequest, url?: string): Promise<PaginationResponse<PaginationResponseInterface, Array<GovernmentSector>> | {
        paging: PaginationResponseInterface;
        res: Array<GovernmentSector>;
    }>;
    delete(id: number): Promise<SuccessResponse<string> | boolean>;
    detail(id: number): Promise<SuccessResponse<GovernmentSector> | GovernmentSector>;
    /**
     * parameter name: Nama dari urusan pemerintah
     * paremeter deadline: diisi dengan format YYYY-MM-DD
     */
    update(id: number, req: CreateGovernmentSectorRequest): Promise<SuccessResponse<string> | boolean>;
}
