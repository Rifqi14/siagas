"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
class HealthService {
    async health() {
        return 'Server is healthy';
    }
}
exports.default = HealthService;
