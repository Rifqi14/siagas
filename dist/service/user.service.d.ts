import User from '../entity/User.entity';
import { PaginationResponse as PaginationResponseInterface } from '../interface/pagination.interface';
import { PaginationData } from '../schema/base.schema';
import { CreateUserRequest, GetUserRequest, UserResponse, UserResponses } from '../schema/user.schema';
import { PaginationResponse, SuccessResponse } from '../types/response.type';
export default class UserService {
    private repo;
    constructor();
    create(req: CreateUserRequest): Promise<SuccessResponse<UserResponse> | User>;
    findAll(filter: GetUserRequest, url?: string, username?: string): Promise<PaginationResponse<PaginationResponseInterface, UserResponses> | Partial<PaginationData<User[]>>>;
    detail(id: number): Promise<SuccessResponse<User> | User>;
    update(id: number, req: CreateUserRequest): Promise<SuccessResponse<User> | User>;
    delete(id: number): Promise<SuccessResponse<string> | string>;
}
