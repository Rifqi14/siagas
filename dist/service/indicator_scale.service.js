"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const tsoa_1 = require("tsoa");
const api_error_1 = __importDefault(require("../providers/exceptions/api.error"));
const indicator_scale_repository_1 = __importDefault(require("../repository/indicator_scale.repository"));
const http_code_enum_1 = require("../types/http_code.enum");
const pagination_1 = __importDefault(require("../providers/pagination"));
const indicator_repository_1 = __importDefault(require("../repository/indicator.repository"));
let IndicatorScaleService = class IndicatorScaleService {
    constructor() {
        this.repo = new indicator_scale_repository_1.default();
        this.indicatorRepo = new indicator_repository_1.default();
    }
    async create(indikator_id, req) {
        const scale = this.repo.create({
            value: req.nilai,
            start_at: req.mulai_dari,
            finish_at: req.sampai_dengan,
            description: req.keterangan
        });
        const { identifiers } = await this.repo.insert(scale);
        const res = await this.repo.findOneBy({ id: identifiers[0].id });
        if (!res) {
            throw new api_error_1.default({
                httpCode: http_code_enum_1.HttpCode.NOT_FOUND,
                message: 'Error saved data'
            });
        }
        return res;
    }
    async detail(id) {
        const res = await this.repo.findOneBy({ id: id });
        if (!res) {
            throw new api_error_1.default({
                httpCode: http_code_enum_1.HttpCode.NOT_FOUND,
                message: 'Data not found'
            });
        }
        return res;
    }
    async get(indikator_id, url = '', req) {
        var _a;
        const query = this.repo
            .createQueryBuilder()
            .where(`lower(description) like lower(:q)`, { q: `%${(_a = req.q) !== null && _a !== void 0 ? _a : ''}%` })
            .andWhere(`indicator_id = :indikator_id`, { indikator_id: indikator_id });
        const total = await query.getCount();
        const { limit, offset, page, order } = pagination_1.default.options(req.page, req.limit);
        for (const key in order) {
            query.addOrderBy(key, order[key]);
        }
        query.limit(limit).offset(offset);
        const res = await query.getMany();
        let paging = pagination_1.default.build(url, page, limit, total);
        return { paging, res };
    }
    async delete(id) {
        const { affected } = await this.repo.delete({ id: id });
        if (affected && affected > 0) {
            return true;
        }
        return false;
    }
    async update(id, req) {
        const update = this.repo.create({
            value: req.nilai,
            start_at: req.mulai_dari,
            finish_at: req.sampai_dengan,
            description: req.keterangan
        });
        const { affected } = await this.repo.update(id, update);
        if (!affected) {
            return false;
        }
        return true;
    }
};
__decorate([
    (0, tsoa_1.Post)('/{indikator_id}/skala'),
    __param(1, (0, tsoa_1.Body)()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Number, Object]),
    __metadata("design:returntype", Promise)
], IndicatorScaleService.prototype, "create", null);
__decorate([
    (0, tsoa_1.Get)('/skala/{id}'),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Number]),
    __metadata("design:returntype", Promise)
], IndicatorScaleService.prototype, "detail", null);
__decorate([
    (0, tsoa_1.Get)('/{indikator_id}/skala'),
    __param(1, (0, tsoa_1.Query)('url')),
    __param(1, (0, tsoa_1.Hidden)()),
    __param(2, (0, tsoa_1.Queries)()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Number, String, Object]),
    __metadata("design:returntype", Promise)
], IndicatorScaleService.prototype, "get", null);
__decorate([
    (0, tsoa_1.Delete)('/skala/{id}'),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Number]),
    __metadata("design:returntype", Promise)
], IndicatorScaleService.prototype, "delete", null);
__decorate([
    (0, tsoa_1.Patch)('/skala/{id}'),
    __param(1, (0, tsoa_1.Body)()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Number, Object]),
    __metadata("design:returntype", Promise)
], IndicatorScaleService.prototype, "update", null);
IndicatorScaleService = __decorate([
    (0, tsoa_1.Route)('/indikator'),
    (0, tsoa_1.Tags)('Master | Indikator | Skala Indikator'),
    __metadata("design:paramtypes", [])
], IndicatorScaleService);
exports.default = IndicatorScaleService;
