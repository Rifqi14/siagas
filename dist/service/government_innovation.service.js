"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const fs_1 = require("fs");
const path_1 = require("path");
const data_source_1 = require("../data-source");
const GovernmentSector_1 = __importDefault(require("../entity/GovernmentSector"));
const Indicator_entity_1 = __importDefault(require("../entity/Indicator.entity"));
const InovasiIndikator_entity_1 = require("../entity/InovasiIndikator.entity");
const ProfilePemda_entity_1 = require("../entity/ProfilePemda.entity");
const ReviewInovasiDaerah_entity_1 = require("../entity/ReviewInovasiDaerah.entity");
const User_entity_1 = __importDefault(require("../entity/User.entity"));
const tsoa_1 = require("tsoa");
const typeorm_1 = require("typeorm");
const api_error_1 = __importDefault(require("../providers/exceptions/api.error"));
const pagination_1 = __importDefault(require("../providers/pagination"));
const government_innovation_repository_1 = __importDefault(require("../repository/government_innovation.repository"));
const government_innovation_schema_1 = require("../schema/government_innovation.schema");
const http_code_enum_1 = require("../types/http_code.enum");
const file_service_1 = __importDefault(require("./file.service"));
const export_service_1 = __importDefault(require("./export.service"));
let GovernmentInnovationService = class GovernmentInnovationService {
    constructor() {
        this.fileService = new file_service_1.default();
        this.exportService = new export_service_1.default();
        this.repo = new government_innovation_repository_1.default();
    }
    async create(nama_pemda, nama_inovasi, tahapan_inovasi, inisiator_inovasi, jenis_inovasi, bentuk_inovasi, tematik, urusan_pertama, urusan_lain, waktu_uji_coba, waktu_penerapan, rancang_bangun, tujuan, manfaat, hasil_inovasi, urusan_pemerintah, anggaran_file, profile_file, foto, username = '') {
        const queryRunner = data_source_1.AppDataSource.createQueryRunner();
        await queryRunner.startTransaction();
        try {
            const pemda = await queryRunner.manager.findOne(ProfilePemda_entity_1.ProfilPemda, {
                where: [{ nama_daerah: nama_pemda }, { nama_pemda: nama_pemda }],
                relations: { user: true },
            });
            const government_sector = await queryRunner.manager.findOne(GovernmentSector_1.default, { where: { name: urusan_pemerintah } });
            const entity = this.repo.create({
                government_name: pemda && pemda.user && pemda.user.nama_pemda ? pemda.user.nama_pemda : undefined,
                created_by: username,
                innovation_name: nama_inovasi,
                innovation_phase: tahapan_inovasi,
                innovation_initiator: inisiator_inovasi,
                innovation_type: jenis_inovasi,
                innovation_form: bentuk_inovasi,
                thematic: tematik,
                first_field: urusan_pertama,
                other_fields: urusan_lain,
                trial_time: waktu_uji_coba ? waktu_uji_coba : undefined,
                urusan: government_sector
                    ? government_sector
                    : queryRunner.manager.create(GovernmentSector_1.default, {
                        name: urusan_pemerintah,
                        deadline: new Date().toISOString(),
                    }),
                implementation_time: waktu_penerapan ? waktu_penerapan : undefined,
                design: rancang_bangun,
                purpose: tujuan,
                benefit: manfaat,
                result: hasil_inovasi,
                profilPemda: pemda ? pemda : undefined,
            });
            if (anggaran_file) {
                const file = await this.fileService.create(anggaran_file, undefined, username);
                entity.budgetFile = file;
            }
            if (profile_file) {
                const file = await this.fileService.create(profile_file, undefined, username);
                entity.profileFile = file;
            }
            if (foto) {
                const file = await this.fileService.create(foto, undefined, username);
                entity.fotoFile = file;
            }
            const inovasi = await this.repo.save(entity);
            const indikator = await this.repo.manager.findBy(Indicator_entity_1.default, {
                jenis_indikator: 'si',
            });
            inovasi.indikator = indikator.map(indikator => {
                return this.repo.manager.create(InovasiIndikator_entity_1.InovasiIndikator, {
                    indikator,
                    inovasi,
                    created_by: username,
                    updated_by: username,
                });
            });
            await this.repo.save(inovasi);
            let skor = 0;
            switch (tahapan_inovasi === null || tahapan_inovasi === void 0 ? void 0 : tahapan_inovasi.toLowerCase()) {
                case 'inisiatif':
                    skor = 50;
                    break;
                case 'uji coba':
                    skor = 102;
                    break;
                case 'penerapan':
                    skor = 105;
                    break;
                default:
                    skor = 0;
                    break;
            }
            await this.repo.manager.insert(ReviewInovasiDaerah_entity_1.ReviewInovasiDaerah, {
                random_number: Math.floor(Math.random() * (1000000000000 - 100000000000)) + 100000000000,
                skor: skor,
                status: 'Pending',
                inovasi: inovasi,
                created_by: username,
                updated_by: username,
            });
            await queryRunner.commitTransaction();
            return entity;
        }
        catch (error) {
            await queryRunner.rollbackTransaction();
            throw new api_error_1.default({
                httpCode: http_code_enum_1.HttpCode.INTERNAL_SERVER_ERROR,
                message: JSON.stringify(error),
            });
        }
    }
    async get(req, url = 'http://localhost', username = '') {
        var _a;
        const query = this.repo
            .createQueryBuilder('gi')
            .leftJoinAndSelect('gi.budgetFile', 'budgetFile')
            .leftJoinAndSelect('gi.profileFile', 'profileFile')
            .leftJoinAndSelect('gi.fotoFile', 'fotoFile');
        if (req.q) {
            query.andWhere(`lower(innovation_name) like lower(:name)`, {
                name: `%${(_a = req.q) !== null && _a !== void 0 ? _a : ''}%`,
            });
        }
        if (req.tahap) {
            query.andWhere(`innovation_phase = :phase`, { phase: req.tahap });
        }
        const user = await this.repo.manager.getRepository(User_entity_1.default).findOneByOrFail({ username });
        if (user && user.role && user.role.name.toLowerCase() === 'user') {
            query.andWhere('gi.created_by = (:created_by)', { created_by: username });
        }
        const total = await query.getCount();
        const { limit, offset, page, order } = pagination_1.default.options(req.page, req.limit);
        query.limit(limit).offset(offset);
        const res = await query.getMany();
        let paging = pagination_1.default.build(url, page, limit, total);
        return { paging, res };
    }
    async download(type, username = '', req) {
        var _a;
        try {
            if (type === 'pdf')
                throw new api_error_1.default({
                    httpCode: 403,
                    message: 'Download PDF under maintenance',
                });
            let where = {
                innovation_name: (0, typeorm_1.ILike)(`%${(_a = req.q) !== null && _a !== void 0 ? _a : ''}%`),
            };
            if (req.tahap)
                where = Object.assign(Object.assign({}, where), { innovation_phase: req.tahap });
            const user = await this.repo.manager.getRepository(User_entity_1.default).findOneByOrFail({ username });
            if (user && user.role && user.role.is_super_admin === 't')
                where = Object.assign(Object.assign({}, where), { created_by: username });
            const data = await this.repo.find({ where });
            const path = await this.exportService.download({
                data: data.map((value, idx) => (0, government_innovation_schema_1.toGovernmentInnovationExport)(value, idx + 1)),
                name: 'inovasi-daerah',
            });
            return path;
        }
        catch (error) {
            throw error;
        }
    }
    async delete(id) {
        const entity = await this.repo.findOneBy({ id: id });
        const { affected } = await this.repo.delete({ id: id });
        if (affected && affected > 0) {
            if (entity && entity.budgetFile) {
                (0, fs_1.unlink)((0, path_1.join)(__dirname, '..', '..', 'public', 'upload', 'inovasi_pemda', entity.budgetFile.name), err => {
                    if (err) {
                        console.log(`${entity.budgetFile.name} was deleted`);
                    }
                });
            }
            if (entity && entity.profileFile) {
                (0, fs_1.unlink)((0, path_1.join)(__dirname, '..', '..', 'public', 'upload', 'inovasi_pemda', entity.profileFile.name), err => {
                    if (err) {
                        console.log(`${entity.profileFile.name} was deleted`);
                    }
                });
            }
            return true;
        }
        return false;
    }
    async detail(id) {
        const res = await this.repo.findOne({
            where: { id: id },
            relations: { budgetFile: true, profileFile: true, fotoFile: true },
        });
        if (!res) {
            throw new api_error_1.default({
                httpCode: http_code_enum_1.HttpCode.NOT_FOUND,
                message: 'Data not found',
            });
        }
        return res;
    }
    async downloadProfil(id, type) {
        try {
            if (type === 'pdf')
                throw new api_error_1.default({
                    httpCode: 403,
                    message: 'Download PDF under maintenance',
                });
            const data = await this.repo.find({ where: { id } });
            const path = await this.exportService.download({
                data: data.map((value, idx) => (0, government_innovation_schema_1.toGovernmentInnovationProfilExportSchema)(value)),
                name: 'inovasi-daerah',
            });
            return path;
        }
        catch (error) {
            throw error;
        }
    }
    async update(id, nama_pemda, urusan_pemerintah, nama_inovasi, tahapan_inovasi, inisiator_inovasi, jenis_inovasi, bentuk_inovasi, tematik, urusan_pertama, urusan_lain, waktu_uji_coba, waktu_penerapan, rancang_bangun, tujuan, manfaat, hasil_inovasi, anggaran_file, profile_file, foto, username = '') {
        const queryRunner = data_source_1.AppDataSource.createQueryRunner();
        const pemda = await queryRunner.manager.findOne(ProfilePemda_entity_1.ProfilPemda, {
            where: [{ nama_daerah: nama_pemda }, { nama_pemda: nama_pemda }],
            relations: { user: true },
        });
        const government_sector = await queryRunner.manager.findOne(GovernmentSector_1.default, { where: { name: (0, typeorm_1.ILike)(`%${urusan_pemerintah}%`) } });
        const entity = this.repo.create({
            government_name: pemda && pemda.user && pemda.user.nama_pemda ? pemda.user.nama_pemda : undefined,
            created_by: username,
            innovation_name: nama_inovasi,
            innovation_phase: tahapan_inovasi,
            innovation_initiator: inisiator_inovasi,
            innovation_type: jenis_inovasi,
            innovation_form: bentuk_inovasi,
            thematic: tematik,
            first_field: urusan_pertama,
            other_fields: urusan_lain,
            trial_time: waktu_uji_coba,
            urusan: government_sector
                ? government_sector
                : queryRunner.manager.create(GovernmentSector_1.default, {
                    name: urusan_pemerintah,
                    deadline: new Date().toISOString(),
                }),
            implementation_time: waktu_penerapan,
            design: rancang_bangun,
            purpose: tujuan,
            benefit: manfaat,
            result: hasil_inovasi,
        });
        if (anggaran_file) {
            const file = await this.fileService.create(anggaran_file, undefined, username);
            entity.budgetFile = file;
        }
        if (profile_file) {
            const file = await this.fileService.create(profile_file, undefined, username);
            entity.profileFile = file;
        }
        if (foto) {
            const file = await this.fileService.create(foto, undefined, username);
            entity.profileFile = file;
        }
        const { affected } = await this.repo.update(id, entity);
        if (!affected) {
            return false;
        }
        return true;
    }
};
__decorate([
    (0, tsoa_1.Post)(''),
    __param(0, (0, tsoa_1.FormField)()),
    __param(1, (0, tsoa_1.FormField)()),
    __param(2, (0, tsoa_1.FormField)()),
    __param(3, (0, tsoa_1.FormField)()),
    __param(4, (0, tsoa_1.FormField)()),
    __param(5, (0, tsoa_1.FormField)()),
    __param(6, (0, tsoa_1.FormField)()),
    __param(7, (0, tsoa_1.FormField)()),
    __param(8, (0, tsoa_1.FormField)()),
    __param(9, (0, tsoa_1.FormField)()),
    __param(10, (0, tsoa_1.FormField)()),
    __param(11, (0, tsoa_1.FormField)()),
    __param(12, (0, tsoa_1.FormField)()),
    __param(13, (0, tsoa_1.FormField)()),
    __param(14, (0, tsoa_1.FormField)()),
    __param(15, (0, tsoa_1.FormField)()),
    __param(16, (0, tsoa_1.UploadedFile)('anggaran_file')),
    __param(17, (0, tsoa_1.UploadedFile)('profile_file')),
    __param(18, (0, tsoa_1.UploadedFile)('foto')),
    __param(19, (0, tsoa_1.Query)()),
    __param(19, (0, tsoa_1.Hidden)()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [String, String, String, String, String, String, String, String, String, String, String, String, String, String, String, String, Object, Object, Object, String]),
    __metadata("design:returntype", Promise)
], GovernmentInnovationService.prototype, "create", null);
__decorate([
    (0, tsoa_1.Get)(''),
    __param(0, (0, tsoa_1.Queries)()),
    __param(1, (0, tsoa_1.Query)('url')),
    __param(1, (0, tsoa_1.Hidden)()),
    __param(2, (0, tsoa_1.Query)()),
    __param(2, (0, tsoa_1.Hidden)()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Object, String, String]),
    __metadata("design:returntype", Promise)
], GovernmentInnovationService.prototype, "get", null);
__decorate([
    (0, tsoa_1.Tags)('Export'),
    (0, tsoa_1.Get)('/download/{type}'),
    __param(1, (0, tsoa_1.Query)()),
    __param(1, (0, tsoa_1.Hidden)()),
    __param(2, (0, tsoa_1.Queries)()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [String, Object, Object]),
    __metadata("design:returntype", Promise)
], GovernmentInnovationService.prototype, "download", null);
__decorate([
    (0, tsoa_1.Delete)('/{id}'),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Number]),
    __metadata("design:returntype", Promise)
], GovernmentInnovationService.prototype, "delete", null);
__decorate([
    (0, tsoa_1.Get)('/{id}'),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Number]),
    __metadata("design:returntype", Promise)
], GovernmentInnovationService.prototype, "detail", null);
__decorate([
    (0, tsoa_1.Tags)('Export'),
    (0, tsoa_1.Get)('/{id}/download/{type}'),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Number, String]),
    __metadata("design:returntype", Promise)
], GovernmentInnovationService.prototype, "downloadProfil", null);
__decorate([
    (0, tsoa_1.Patch)('/{id}'),
    __param(1, (0, tsoa_1.FormField)()),
    __param(2, (0, tsoa_1.FormField)()),
    __param(3, (0, tsoa_1.FormField)()),
    __param(4, (0, tsoa_1.FormField)()),
    __param(5, (0, tsoa_1.FormField)()),
    __param(6, (0, tsoa_1.FormField)()),
    __param(7, (0, tsoa_1.FormField)()),
    __param(8, (0, tsoa_1.FormField)()),
    __param(9, (0, tsoa_1.FormField)()),
    __param(10, (0, tsoa_1.FormField)()),
    __param(11, (0, tsoa_1.FormField)()),
    __param(12, (0, tsoa_1.FormField)()),
    __param(13, (0, tsoa_1.FormField)()),
    __param(14, (0, tsoa_1.FormField)()),
    __param(15, (0, tsoa_1.FormField)()),
    __param(16, (0, tsoa_1.FormField)()),
    __param(17, (0, tsoa_1.UploadedFile)('anggaran_file')),
    __param(18, (0, tsoa_1.UploadedFile)('profile_file')),
    __param(19, (0, tsoa_1.UploadedFile)('foto')),
    __param(20, (0, tsoa_1.Query)()),
    __param(20, (0, tsoa_1.Hidden)()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Number, String, String, String, String, String, String, String, String, String, String, String, String, String, String, String, String, Object, Object, Object, String]),
    __metadata("design:returntype", Promise)
], GovernmentInnovationService.prototype, "update", null);
GovernmentInnovationService = __decorate([
    (0, tsoa_1.Route)('/inovasi_pemerintah_daerah'),
    (0, tsoa_1.Tags)('Database Inovasi Daerah | Inovasi Daerah'),
    (0, tsoa_1.Security)('bearer'),
    __metadata("design:paramtypes", [])
], GovernmentInnovationService);
exports.default = GovernmentInnovationService;
