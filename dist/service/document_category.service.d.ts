import { PaginationResponse, SuccessResponse } from '../types/response.type';
import { PaginationResponse as PaginationResponseInterface } from '../interface/pagination.interface';
import { PaginationData } from '../schema/base.schema';
import { CreateDocumentCategoryRequest, GetDocumentCategoryRequest } from '../schema/document_category.schema';
import DocumentCategory from '../entity/DocumentCategory.entity';
export default class DocumentCategoryService {
    private repo;
    create(req: CreateDocumentCategoryRequest): Promise<SuccessResponse<Omit<DocumentCategory, ''>> | DocumentCategory>;
    detail(id: number): Promise<SuccessResponse<Omit<DocumentCategory, ''>> | DocumentCategory>;
    get(url: string | undefined, req: GetDocumentCategoryRequest): Promise<PaginationResponse<PaginationResponseInterface, Omit<DocumentCategory, 'details'>[]> | PaginationData<Omit<DocumentCategory, 'details'>[]>>;
    delete(id: number): Promise<SuccessResponse<string> | boolean>;
    update(id: number, req: CreateDocumentCategoryRequest): Promise<SuccessResponse<string> | boolean>;
}
