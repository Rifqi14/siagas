import { CreateExportRequest } from "../schema/export.schema";
type Props = {
    data: Array<Record<string, any>>;
    name: string;
};
export default class ExportService {
    export(req: CreateExportRequest, stream?: any): Promise<void>;
    download({ data, name }: Props): Promise<string>;
    generatePdfFromUrl(url: string, outputName: string): Promise<string>;
}
export {};
