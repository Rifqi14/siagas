import { PaginationResponse, SuccessResponse } from '../types/response.type';
import { PaginationResponse as PaginationResponseInterface } from '../interface/pagination.interface';
import { RekapIndeksAkhirParams, UpdateNominatorRequest } from '../schema/rekap_indeks_akhir.schema';
import { RekapIndeksAkhir } from '../entity/RekapIndeksAkhir.entity';
import { PaginationData } from '../schema/base.schema';
export default class RekapIndeksAkhirService {
    private repo;
    get(url: string | undefined, username: string | undefined, req: RekapIndeksAkhirParams): Promise<PaginationResponse<PaginationResponseInterface, RekapIndeksAkhir[]> | PaginationData<RekapIndeksAkhir[]>>;
    nominator(id: number, req: UpdateNominatorRequest): Promise<SuccessResponse<string> | boolean>;
}
