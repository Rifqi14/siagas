import { PeringkatHasilReviewNominatorRequest, PeringkatHasilReviewParams, PeringkatHasilReviewResponse, PrestasiResponse, RangkingResponse } from '../schema/innovative_government_award.schema';
import { PaginationResponse as PaginationResponseInterface } from '../interface/pagination.interface';
import { PaginationData } from '../schema/base.schema';
import { PaginationResponse, SuccessResponse } from '../types/response.type';
import { DownloadType } from '../types/lib';
export default class InnovativeGovernmentAwardService {
    private repo;
    private exportService;
    peringkat_hasil_review(url: string | undefined, username: string | undefined, req: PeringkatHasilReviewParams): Promise<PaginationResponse<PaginationResponseInterface, PeringkatHasilReviewResponse[]> | PaginationData<PeringkatHasilReviewResponse[]>>;
    peringkat_hasil_review_download(type: DownloadType, username: string | undefined, req: PeringkatHasilReviewParams): Promise<string>;
    nominator_peringkat_hasil_review(pemda_id: number, req: PeringkatHasilReviewNominatorRequest): Promise<SuccessResponse<string> | boolean>;
    prestasi(url: string | undefined, username: string | undefined, req: PeringkatHasilReviewParams): Promise<PaginationResponse<PaginationResponseInterface, PrestasiResponse[]> | PaginationData<PrestasiResponse[]>>;
    prestasi_download(type: DownloadType, username: string | undefined, req: PeringkatHasilReviewParams): Promise<string>;
    rangking(url: string | undefined, username: string | undefined, req: PeringkatHasilReviewParams): Promise<PaginationResponse<PaginationResponseInterface, RangkingResponse[]> | PaginationData<RangkingResponse[]>>;
    rangking_download(type: DownloadType, username: string | undefined, req: PeringkatHasilReviewParams): Promise<string>;
}
