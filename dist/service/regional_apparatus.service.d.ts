import RegionalApparatus from '../entity/RegionApparatus.entity';
import { PaginationData } from '../schema/base.schema';
import { CreateRegionApparatusRequest, GetRegionApparatusRequest } from '../schema/region_apparatus.scheme';
import { PaginationResponse, SuccessResponse } from '../types/response.type';
import { PaginationResponse as PaginationResponseInterface } from '../interface/pagination.interface';
export default class RegionalApparatusService {
    private repo;
    constructor();
    create(req: CreateRegionApparatusRequest, username?: string): Promise<SuccessResponse<RegionalApparatus> | RegionalApparatus>;
    detail(id: number): Promise<SuccessResponse<RegionalApparatus> | RegionalApparatus>;
    get(url: string | undefined, req: GetRegionApparatusRequest): Promise<PaginationResponse<PaginationResponseInterface, RegionalApparatus[]> | PaginationData<RegionalApparatus[]>>;
    delete(id: number): Promise<SuccessResponse<string> | boolean>;
    update(id: number, req: CreateRegionApparatusRequest, username?: string): Promise<SuccessResponse<string> | boolean>;
}
