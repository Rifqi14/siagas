import { DashboardArsip } from "../entity/DashboardArsip.entity";
import { PaginationResponse as PaginationResponseInterface } from "../interface/pagination.interface";
import { PaginationData } from "../schema/base.schema";
import { DashboardArsipDownloadRequest, DashboardArsipParams, DashboardOPDResponse, DashboardResponse, StatistikIndikatorInovasiParams, StatistikIndikatorInovasiResponse } from "../schema/dashboard.schema";
import { DownloadType } from "../types/lib";
import { PaginationResponse, SuccessResponse } from "../types/response.type";
export default class DashboardService {
    private repo;
    private exportService;
    get(url: string | undefined, username: string | undefined, req: DashboardArsipParams): Promise<PaginationResponse<PaginationResponseInterface, DashboardArsip[]> | PaginationData<DashboardArsip[]>>;
    arsip_download(type: DownloadType, username: string | undefined, req: DashboardArsipDownloadRequest): Promise<string>;
    statistik_indikator(req: StatistikIndikatorInovasiParams): Promise<SuccessResponse<StatistikIndikatorInovasiResponse[]> | StatistikIndikatorInovasiResponse[]>;
    statistik_inovasi(): Promise<SuccessResponse<DashboardResponse> | DashboardResponse>;
    opd_menangani(): Promise<SuccessResponse<DashboardOPDResponse> | DashboardOPDResponse>;
}
