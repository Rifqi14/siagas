"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const inovasi_indikator_file_repository_1 = __importDefault(require("../repository/inovasi_indikator_file.repository"));
const tsoa_1 = require("tsoa");
const pagination_1 = __importDefault(require("../providers/pagination"));
const api_error_1 = __importDefault(require("../providers/exceptions/api.error"));
const http_code_enum_1 = require("../types/http_code.enum");
const file_service_1 = __importDefault(require("./file.service"));
let InovasiIndikatorFileService = class InovasiIndikatorFileService {
    constructor() {
        this.repo = new inovasi_indikator_file_repository_1.default();
    }
    async get(inovasi_id, indikator_id, url = '', username = '', req) {
        const query = this.repo
            .createQueryBuilder(`iif`)
            .leftJoinAndSelect(`iif.file`, `f`)
            .where(`iif.inovasi_id = :inovasi_id and iif.indikator_id = :indikator_id`, {
            inovasi_id,
            indikator_id
        });
        // if (username) {
        //   query.where(`iif.created_by = :username`, { username });
        // }
        const total = await query.getCount();
        const { limit, offset, page, order } = pagination_1.default.options(req.page, req.limit);
        query.limit(limit).offset(offset);
        for (const key in order) {
            query.addOrderBy(`iif.${key}`, order[key]);
        }
        const res = await query.getMany();
        let paging = pagination_1.default.build(url, page, limit, total);
        return { paging, res };
    }
    async detail(inovasi_id, indikator_id, file_id) {
        const res = await this.repo.findOneBy({
            inovasi_id,
            indikator_id,
            file_id
        });
        if (!res) {
            throw new api_error_1.default({
                httpCode: http_code_enum_1.HttpCode.NOT_FOUND,
                message: 'Data not found'
            });
        }
        return res;
    }
    async upload(inovasi_id, indikator_id, nomor_surat, tanggal_surat, tentang, dokumen, nama_dokumen, username = '') {
        const create = this.repo.create({
            nomor_surat: nomor_surat,
            tentang: tentang,
            tanggal_surat: tanggal_surat,
            created_by: username,
            updated_by: username,
            inovasi_id: +inovasi_id,
            indikator_id: +indikator_id
        });
        if (dokumen) {
            const file = await new file_service_1.default().create(dokumen, nama_dokumen, username);
            create.file = file;
        }
        await this.repo.insert(create);
        return create;
    }
    async delete(inovasi_id, indikator_id, file_id) {
        const { affected } = await this.repo.delete({
            inovasi_id,
            indikator_id,
            file_id
        });
        if (affected && affected > 0) {
            return true;
        }
        return false;
    }
};
__decorate([
    (0, tsoa_1.Get)('/{inovasi_id}/{indikator_id}/files'),
    __param(2, (0, tsoa_1.Query)('url')),
    __param(2, (0, tsoa_1.Hidden)()),
    __param(3, (0, tsoa_1.Query)()),
    __param(3, (0, tsoa_1.Hidden)()),
    __param(4, (0, tsoa_1.Queries)()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [String, String, Object, Object, Object]),
    __metadata("design:returntype", Promise)
], InovasiIndikatorFileService.prototype, "get", null);
__decorate([
    (0, tsoa_1.Get)('/{inovasi_id}/{indikator_id}/files/{file_id}'),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Number, Number, Number]),
    __metadata("design:returntype", Promise)
], InovasiIndikatorFileService.prototype, "detail", null);
__decorate([
    (0, tsoa_1.Post)('/{inovasi_id}/{indikator_id}/upload'),
    __param(2, (0, tsoa_1.FormField)()),
    __param(3, (0, tsoa_1.FormField)()),
    __param(4, (0, tsoa_1.FormField)()),
    __param(5, (0, tsoa_1.UploadedFile)()),
    __param(6, (0, tsoa_1.FormField)()),
    __param(7, (0, tsoa_1.Query)()),
    __param(7, (0, tsoa_1.Hidden)()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [String, String, String, String, String, Object, String, Object]),
    __metadata("design:returntype", Promise)
], InovasiIndikatorFileService.prototype, "upload", null);
__decorate([
    (0, tsoa_1.Delete)('/{inovasi_id}/{indikator_id}/delete/{file_id}'),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Number, Number, Number]),
    __metadata("design:returntype", Promise)
], InovasiIndikatorFileService.prototype, "delete", null);
InovasiIndikatorFileService = __decorate([
    (0, tsoa_1.Route)('/inovasi_pemerintah_daerah/indikator'),
    (0, tsoa_1.Tags)('Database Inovasi Daerah | Inovasi Daerah | Inovasi Indikator | Inovasi Indikator File'),
    (0, tsoa_1.Security)('bearer')
], InovasiIndikatorFileService);
exports.default = InovasiIndikatorFileService;
