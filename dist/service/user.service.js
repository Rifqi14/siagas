"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const tsoa_1 = require("tsoa");
const data_source_1 = require("../data-source");
const Role_entity_1 = __importDefault(require("../entity/Role.entity"));
const User_entity_1 = __importDefault(require("../entity/User.entity"));
const pagination_1 = __importDefault(require("../providers/pagination"));
const user_repository_1 = __importDefault(require("../repository/user.repository"));
let UserService = class UserService {
    constructor() {
        this.repo = new user_repository_1.default();
    }
    async create(req) {
        const role = data_source_1.AppDataSource.getRepository(Role_entity_1.default).create({
            id: req.role_id,
        });
        const user = {
            username: req.username,
            email: req.email,
            full_name: req.nama_lengkap,
            nickname: req.nama_panggilan,
            password: req.password,
            nama_pemda: req.nama_pemda,
            role: role,
        };
        const create = this.repo.create(user);
        await this.repo.insert(create);
        return create;
    }
    async findAll(filter, url = 'http://localhost', username = '') {
        const query = this.repo.createQueryBuilder('u').leftJoinAndSelect('u.role', 'r').leftJoinAndSelect('u.createdBy', 'cb');
        if (filter.q)
            query
                .where(`lower(u.username) like :q`, {
                q: `%${filter.q.toLowerCase()}%`,
            })
                .orWhere(`lower(r.name) like :q`, { q: `%${filter.q.toLowerCase()}%` });
        if (filter.role_id) {
            query.where(`u.role_id = :role_id`, { role_id: filter.role_id });
        }
        const user = await this.repo.manager.getRepository(User_entity_1.default).findOneBy({ username });
        if (username && user && user.role && user.role.name.toLowerCase() === 'user') {
            query.andWhere('u.created_by = :created_by', { created_by: user.id });
        }
        const total = await query.getCount();
        const { limit, offset, page } = pagination_1.default.options(filter.page, filter.limit);
        if (filter.limit && filter.page)
            query.limit(limit).offset(offset);
        const res = await query.getMany();
        let paging = pagination_1.default.build(url, page, limit, total);
        return { paging: filter.limit && filter.page ? paging : undefined, res };
    }
    async detail(id) {
        const user = await this.repo.findOneByOrFail({ id: id });
        return user;
    }
    async update(id, req) {
        const { email, role_id, username, nama_lengkap, nama_panggilan } = req;
        const entity = this.repo.create({
            id,
            nickname: nama_panggilan,
            full_name: nama_lengkap,
            nama_pemda: req.nama_pemda,
            username,
            email,
            role_id,
        });
        return await this.repo.save(entity);
    }
    async delete(id) {
        const { affected } = await this.repo.delete(id);
        return affected && affected > 0 ? 'Success delete' : 'Error deleting';
    }
};
__decorate([
    (0, tsoa_1.Post)(''),
    __param(0, (0, tsoa_1.Body)()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Object]),
    __metadata("design:returntype", Promise)
], UserService.prototype, "create", null);
__decorate([
    (0, tsoa_1.Get)(''),
    __param(0, (0, tsoa_1.Queries)()),
    __param(1, (0, tsoa_1.Query)('url')),
    __param(1, (0, tsoa_1.Hidden)()),
    __param(2, (0, tsoa_1.Query)()),
    __param(2, (0, tsoa_1.Hidden)()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Object, String, String]),
    __metadata("design:returntype", Promise)
], UserService.prototype, "findAll", null);
__decorate([
    (0, tsoa_1.Get)('/{id}'),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Number]),
    __metadata("design:returntype", Promise)
], UserService.prototype, "detail", null);
__decorate([
    (0, tsoa_1.Patch)('/{id}'),
    __param(1, (0, tsoa_1.Body)()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Number, Object]),
    __metadata("design:returntype", Promise)
], UserService.prototype, "update", null);
__decorate([
    (0, tsoa_1.Delete)('/{id}'),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Number]),
    __metadata("design:returntype", Promise)
], UserService.prototype, "delete", null);
UserService = __decorate([
    (0, tsoa_1.Route)('/user'),
    (0, tsoa_1.Tags)('Konfigurasi | User Account'),
    (0, tsoa_1.Security)('bearer'),
    __metadata("design:paramtypes", [])
], UserService);
exports.default = UserService;
