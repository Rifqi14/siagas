import { RawlingParams, RawlingResponse } from '../schema/rawling.schema';
import { SuccessResponse } from '../types/response.type';
export default class RawlingService {
    private repo;
    rawling(req: RawlingParams): Promise<SuccessResponse<RawlingResponse> | RawlingResponse>;
}
