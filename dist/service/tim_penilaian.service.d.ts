import { TimPenilaian } from '../entity/TimPenilaian.entity';
import { CreateTimPenilaianRequest, ListTimPenilaian, ListTimPenilaianParams } from '../schema/tim_penilaian.schema';
import { PaginationResponse, SuccessResponse } from '../types/response.type';
import { PaginationResponse as PaginationResponseInterface } from '../interface/pagination.interface';
import { PaginationData } from '../schema/base.schema';
export default class TimPenilaianService {
    private repo;
    create(req: CreateTimPenilaianRequest, username?: string): Promise<SuccessResponse<TimPenilaian> | TimPenilaian>;
    detail(id: number): Promise<SuccessResponse<TimPenilaian> | TimPenilaian>;
    get(url: string | undefined, username: string | undefined, req: ListTimPenilaianParams): Promise<PaginationResponse<PaginationResponseInterface, ListTimPenilaian> | PaginationData<ListTimPenilaian>>;
    delete(id: number): Promise<SuccessResponse<string> | boolean>;
    update(id: number, req: CreateTimPenilaianRequest, username?: string): Promise<SuccessResponse<string> | boolean>;
}
