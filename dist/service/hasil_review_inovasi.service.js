"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const hasil_review_inovasi_repository_1 = __importDefault(require("../repository/hasil_review_inovasi.repository"));
const hasil_review_inovasi_schema_1 = require("../schema/hasil_review_inovasi.schema");
const tsoa_1 = require("tsoa");
const pagination_1 = __importDefault(require("../providers/pagination"));
const User_entity_1 = __importDefault(require("../entity/User.entity"));
const api_error_1 = __importDefault(require("../providers/exceptions/api.error"));
const http_code_enum_1 = require("../types/http_code.enum");
const typeorm_1 = require("typeorm");
const export_service_1 = __importDefault(require("./export.service"));
let HasilReviewInovasiService = class HasilReviewInovasiService {
    constructor() {
        this.repo = new hasil_review_inovasi_repository_1.default();
        this.exportService = new export_service_1.default();
    }
    async delete(review_inovasi_id) {
        const { affected } = await this.repo.delete({
            id: review_inovasi_id,
            status: (0, typeorm_1.ILike)('Accept'),
        });
        if (affected && affected > 0) {
            return true;
        }
        return false;
    }
    async detail(review_inovasi_id) {
        const res = await this.repo.findOne({
            where: { id: review_inovasi_id },
            relations: { inovasi: { pemda: true }, previews: { evaluasi: true } },
        });
        if (!res) {
            throw new api_error_1.default({
                httpCode: http_code_enum_1.HttpCode.NOT_FOUND,
                message: 'Data not found',
            });
        }
        return this.toResponse(res);
    }
    async get(url = '', username = '', req) {
        var _a;
        const query = this.repo
            .createQueryBuilder('hri')
            .leftJoinAndSelect('hri.inovasi', 'i')
            .leftJoinAndSelect('i.profilPemda', 'ip')
            .leftJoinAndSelect('hri.previews', 'ep')
            .leftJoinAndSelect('ep.evaluasi', 'epv')
            .where(`(lower(i.innovation_name) ilike lower(:q) or lower(ip.nama_daerah) ilike lower(:q))`, {
            q: `%${(_a = req.q) !== null && _a !== void 0 ? _a : ''}%`,
        })
            .andWhere(`lower(hri.status) = lower(:status)`, { status: 'accept' });
        if (req.pemda_id) {
            query.andWhere(`ip.id = :nama_pemda`, {
                nama_pemda: req.pemda_id,
            });
        }
        const total = await query.getCount();
        const { limit, offset, page, order } = pagination_1.default.options(req.page, req.limit);
        const user = await this.repo.manager.getRepository(User_entity_1.default).findOneByOrFail({ username });
        if (user && user.role && user.role.is_super_admin === 't') {
            query.andWhere(`hri.created_by = (:created_by)`, {
                created_by: username,
            });
        }
        query.limit(limit).offset(offset);
        for (const key in order) {
            query.addOrderBy(`hri.${key}`, order[key]);
        }
        const res = (await query.getMany()).map(response => this.toResponse(response));
        let paging = pagination_1.default.build(url, page, limit, total);
        return { paging, res };
    }
    async download(type, username = '', req) {
        try {
            if (type === 'pdf')
                throw new api_error_1.default({
                    httpCode: 403,
                    message: 'Download PDF under maintenance',
                });
            let where = {
                status: (0, typeorm_1.Raw)(alias => `lower(${alias}) = 'accept'`),
            };
            if (req.pemda_id)
                where = Object.assign(Object.assign({}, where), { inovasi: { pemda_id: +req.pemda_id } });
            if (username) {
                const user = await this.repo.manager.getRepository(User_entity_1.default).findOneByOrFail({ username });
                if (user && user.role && user.role.is_super_admin === 't')
                    where = Object.assign(Object.assign({}, where), { created_by: username });
            }
            const data = await this.repo.find({
                where,
                relations: { inovasi: { profilPemda: true } },
            });
            const path = await this.exportService.download({
                data: data.map((value, idx) => (0, hasil_review_inovasi_schema_1.toDownloadResponse)(value, idx + 1)),
                name: 'hasil-review-inovasi-daerah',
            });
            return path;
        }
        catch (error) {
            throw error;
        }
    }
    toResponse(data) {
        var _a, _b, _c, _d, _e, _f, _g, _h, _j, _k;
        let skor = 0;
        let kematangan = 0;
        switch (data.inovasi.innovation_phase.toLowerCase()) {
            case 'inisiatif':
                skor = 50;
                kematangan = 3;
                break;
            case 'uji coba':
                skor = 102;
                kematangan = 6;
                break;
            case 'penerapan':
                skor = 105;
                kematangan = 9;
                break;
            default:
                skor = 0;
                break;
        }
        let res = {
            review_inovasi_id: (_a = data.id.toString()) !== null && _a !== void 0 ? _a : null,
            nomor: (_b = data.random_number.toString()) !== null && _b !== void 0 ? _b : null,
            judul: (_c = data.inovasi.innovation_name) !== null && _c !== void 0 ? _c : null,
            pemda: (_h = {
                pemda_id: (_e = (_d = data.inovasi.profilPemda) === null || _d === void 0 ? void 0 : _d.id.toString()) !== null && _e !== void 0 ? _e : null,
                pemda_name: (_g = (_f = data.inovasi.profilPemda) === null || _f === void 0 ? void 0 : _f.nama_daerah) !== null && _g !== void 0 ? _g : null,
            }) !== null && _h !== void 0 ? _h : null,
            waktu_penerapan: (_j = data.inovasi.implementation_time) !== null && _j !== void 0 ? _j : null,
            kematangan: kematangan !== null && kematangan !== void 0 ? kematangan : null,
            skor_verifikasi: skor !== null && skor !== void 0 ? skor : null,
            qc: (_k = data.status) !== null && _k !== void 0 ? _k : null,
        };
        return res;
    }
};
__decorate([
    (0, tsoa_1.Delete)('/{review_inovasi_id}'),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Number]),
    __metadata("design:returntype", Promise)
], HasilReviewInovasiService.prototype, "delete", null);
__decorate([
    (0, tsoa_1.Get)('/{review_inovasi_id}'),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Number]),
    __metadata("design:returntype", Promise)
], HasilReviewInovasiService.prototype, "detail", null);
__decorate([
    (0, tsoa_1.Get)(),
    __param(0, (0, tsoa_1.Query)()),
    __param(0, (0, tsoa_1.Hidden)()),
    __param(1, (0, tsoa_1.Query)()),
    __param(1, (0, tsoa_1.Hidden)()),
    __param(2, (0, tsoa_1.Queries)()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Object, Object, Object]),
    __metadata("design:returntype", Promise)
], HasilReviewInovasiService.prototype, "get", null);
__decorate([
    (0, tsoa_1.Tags)('Export'),
    (0, tsoa_1.Get)('/download/{type}'),
    __param(1, (0, tsoa_1.Query)()),
    __param(1, (0, tsoa_1.Hidden)()),
    __param(2, (0, tsoa_1.Queries)()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [String, Object, Object]),
    __metadata("design:returntype", Promise)
], HasilReviewInovasiService.prototype, "download", null);
HasilReviewInovasiService = __decorate([
    (0, tsoa_1.Route)('/hasil_review_inovasi_daerah'),
    (0, tsoa_1.Tags)('Verifikasi Index', 'Verifikasi Index | Hasil Review Inovasi Daerah'),
    (0, tsoa_1.Security)('bearer')
], HasilReviewInovasiService);
exports.default = HasilReviewInovasiService;
