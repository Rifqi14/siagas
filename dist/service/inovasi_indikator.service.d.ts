import { InovasiIndikator } from '../entity/InovasiIndikator.entity';
import { PaginationResponse as PaginationResponseInterface } from '../interface/pagination.interface';
import { PaginationData } from '../schema/base.schema';
import { InovasiIndikatorParams, ListInovasiIndikator } from '../schema/inovasi_indikator.schema';
import { PaginationResponse, SuccessResponse } from '../types/response.type';
export default class InovasiIndikatorService {
    private repo;
    get(inovasi_id: string, url: string | undefined, username: string | undefined, req: InovasiIndikatorParams): Promise<PaginationResponse<PaginationResponseInterface, ListInovasiIndikator> | PaginationData<ListInovasiIndikator>>;
    detail(inovasi_id: number, indikator_id: number): Promise<SuccessResponse<InovasiIndikator> | InovasiIndikator>;
}
