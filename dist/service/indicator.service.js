"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const tsoa_1 = require("tsoa");
const api_error_1 = __importDefault(require("../providers/exceptions/api.error"));
const pagination_1 = __importDefault(require("../providers/pagination"));
const indicator_repository_1 = __importDefault(require("../repository/indicator.repository"));
const http_code_enum_1 = require("../types/http_code.enum");
let IndicatorService = class IndicatorService {
    constructor() {
        this.repo = new indicator_repository_1.default();
    }
    /**
     *
     * jenis_indikator `ENUM [spd: Satuan Pemerintah Daerah, si: Satuan Inovasi, iv: Indikator Validasi, ipkd: Indikator Presentasi Kepala Daerah]`
     */
    async create(req) {
        const indicator = this.repo.create(Object.assign({}, req));
        await this.repo.insert(indicator);
        return indicator;
    }
    async detail(id) {
        const res = await this.repo.findOneBy({ id: id });
        if (!res) {
            throw new api_error_1.default({
                httpCode: http_code_enum_1.HttpCode.NOT_FOUND,
                message: 'Data not found'
            });
        }
        return res;
    }
    async get(url = '', req) {
        var _a;
        const query = this.repo
            .createQueryBuilder()
            .where(`lower(nama_indikator) like lower(:q)`, { q: `%${(_a = req.q) !== null && _a !== void 0 ? _a : ''}%` });
        if (req.jenis_indikator) {
            query.andWhere(`jenis_indikator = :jenis_indikator`, {
                jenis_indikator: req.jenis_indikator
            });
        }
        const total = await query.getCount();
        const { limit, offset, page, order } = pagination_1.default.options(req.page, req.limit);
        query.limit(limit).offset(offset);
        for (const key in order) {
            query.addOrderBy(key, order[key]);
        }
        const res = await query.getMany();
        let paging = pagination_1.default.build(url, page, limit, total);
        return { paging, res };
    }
    async delete(id) {
        const { affected } = await this.repo.delete({ id: id });
        if (affected && affected > 0) {
            return true;
        }
        return false;
    }
    /**
     *
     * @param jenis_indikator `ENUM [spd: Satuan Pemerintah Daerah, si: Satuan Inovasi, iv: Indikator Validasi, ipkd: Indikator Presentasi Kepala Daerah]`
     */
    async update(id, req) {
        const update = this.repo.create(Object.assign({}, req));
        const { affected } = await this.repo.update(id, update);
        if (!affected) {
            return false;
        }
        return true;
    }
};
__decorate([
    (0, tsoa_1.Post)(''),
    __param(0, (0, tsoa_1.Body)()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Object]),
    __metadata("design:returntype", Promise)
], IndicatorService.prototype, "create", null);
__decorate([
    (0, tsoa_1.Get)('/{id}'),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Number]),
    __metadata("design:returntype", Promise)
], IndicatorService.prototype, "detail", null);
__decorate([
    (0, tsoa_1.Get)(),
    __param(0, (0, tsoa_1.Query)('url')),
    __param(0, (0, tsoa_1.Hidden)()),
    __param(1, (0, tsoa_1.Queries)()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [String, Object]),
    __metadata("design:returntype", Promise)
], IndicatorService.prototype, "get", null);
__decorate([
    (0, tsoa_1.Delete)('/{id}'),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Number]),
    __metadata("design:returntype", Promise)
], IndicatorService.prototype, "delete", null);
__decorate([
    (0, tsoa_1.Patch)('/{id}'),
    __param(1, (0, tsoa_1.Body)()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Number, Object]),
    __metadata("design:returntype", Promise)
], IndicatorService.prototype, "update", null);
IndicatorService = __decorate([
    (0, tsoa_1.Route)('/indikator'),
    (0, tsoa_1.Tags)('Master | Indikator'),
    (0, tsoa_1.Security)('bearer'),
    __metadata("design:paramtypes", [])
], IndicatorService);
exports.default = IndicatorService;
