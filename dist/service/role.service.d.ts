import Role from '../entity/Role.entity';
import { PaginationResponse as PaginationResponseInterface } from '../interface/pagination.interface';
import { PaginationData } from '../schema/base.schema';
import { CreateRoleRequest, GetRoleRequest } from '../schema/role.schema';
import { PaginationResponse, SuccessResponse } from '../types/response.type';
export default class RoleService {
    private repo;
    constructor();
    create(req: CreateRoleRequest): Promise<SuccessResponse<Role> | Role>;
    detail(id: number): Promise<SuccessResponse<Role> | Role>;
    get(url: string | undefined, req: GetRoleRequest): Promise<PaginationResponse<PaginationResponseInterface, Role[]> | PaginationData<Role[]>>;
    delete(id: number): Promise<SuccessResponse<string> | boolean>;
    update(id: number, req: CreateRoleRequest): Promise<SuccessResponse<string> | boolean>;
}
