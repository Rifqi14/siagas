"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const preview_inovasi_daerah_repository_1 = __importDefault(require("../repository/preview_inovasi_daerah.repository"));
const tsoa_1 = require("tsoa");
const api_error_1 = __importDefault(require("../providers/exceptions/api.error"));
const http_code_enum_1 = require("../types/http_code.enum");
const pagination_1 = __importDefault(require("../providers/pagination"));
const User_entity_1 = __importDefault(require("../entity/User.entity"));
let PreviewReviewInovasiDaerahService = class PreviewReviewInovasiDaerahService {
    constructor() {
        this.repo = new preview_inovasi_daerah_repository_1.default();
    }
    async create(review_id, username = '', req) {
        const indicator = this.repo.create(Object.assign(Object.assign({}, req), { review_inovasi_daerah_id: review_id, created_by: username, updated_by: username }));
        await this.repo.insert(indicator);
        return indicator;
    }
    async detail(review_id, id) {
        const res = await this.repo.findOneBy({
            id: id,
            review_inovasi_daerah_id: review_id
        });
        if (!res) {
            throw new api_error_1.default({
                httpCode: http_code_enum_1.HttpCode.NOT_FOUND,
                message: 'Data not found'
            });
        }
        return res;
    }
    async get(review_id, url = '', username = '', req) {
        const query = this.repo
            .createQueryBuilder(`e`)
            .leftJoinAndSelect('e.review_inovasi_daerah', 'i');
        const total = await query.getCount();
        const { limit, offset, page, order } = pagination_1.default.options(req.page, req.limit);
        const user = await this.repo.manager
            .getRepository(User_entity_1.default)
            .findOneByOrFail({ username });
        if (user && user.role && user.role.is_super_admin === 't') {
            query.andWhere(`e.created_by = (:created_by)`, { created_by: username });
        }
        query.limit(limit).offset(offset);
        for (const key in order) {
            query.addOrderBy(`e.${key}`, order[key]);
        }
        const res = await query.getMany();
        let paging = pagination_1.default.build(url, page, limit, total);
        return { paging, res };
    }
    async delete(review_id, id) {
        const { affected } = await this.repo.delete({
            id: id
        });
        if (affected && affected > 0) {
            return true;
        }
        return false;
    }
    async update(review_id, id, username = '', req) {
        const update = this.repo.create(Object.assign(Object.assign({}, req), { updated_by: username }));
        const { affected } = await this.repo.update({ id: id }, update);
        if (!affected) {
            return false;
        }
        return true;
    }
};
__decorate([
    (0, tsoa_1.Post)('/{review_id}/preview'),
    __param(1, (0, tsoa_1.Query)()),
    __param(1, (0, tsoa_1.Hidden)()),
    __param(2, (0, tsoa_1.Body)()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Number, String, Object]),
    __metadata("design:returntype", Promise)
], PreviewReviewInovasiDaerahService.prototype, "create", null);
__decorate([
    (0, tsoa_1.Get)('/{review_id}/preview/{id}'),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Number, Number]),
    __metadata("design:returntype", Promise)
], PreviewReviewInovasiDaerahService.prototype, "detail", null);
__decorate([
    (0, tsoa_1.Get)('/{review_id}/preview'),
    __param(1, (0, tsoa_1.Query)('url')),
    __param(1, (0, tsoa_1.Hidden)()),
    __param(2, (0, tsoa_1.Query)()),
    __param(2, (0, tsoa_1.Hidden)()),
    __param(3, (0, tsoa_1.Queries)()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Number, String, String, Object]),
    __metadata("design:returntype", Promise)
], PreviewReviewInovasiDaerahService.prototype, "get", null);
__decorate([
    (0, tsoa_1.Delete)('/{review_id}/preview/{id}'),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Number, Number]),
    __metadata("design:returntype", Promise)
], PreviewReviewInovasiDaerahService.prototype, "delete", null);
__decorate([
    (0, tsoa_1.Patch)('/{review_id}/preview/{id}'),
    __param(2, (0, tsoa_1.Query)()),
    __param(2, (0, tsoa_1.Hidden)()),
    __param(3, (0, tsoa_1.Body)()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Number, Number, String, Object]),
    __metadata("design:returntype", Promise)
], PreviewReviewInovasiDaerahService.prototype, "update", null);
PreviewReviewInovasiDaerahService = __decorate([
    (0, tsoa_1.Route)('/review_inovasi_daerah'),
    (0, tsoa_1.Tags)('Varifikasi Index | Preview Review Inovasi Daerah'),
    (0, tsoa_1.Security)('bearer')
], PreviewReviewInovasiDaerahService);
exports.default = PreviewReviewInovasiDaerahService;
