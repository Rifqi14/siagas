/// <reference types="multer" />
import Announcement from '../entity/Announcement.entity';
import { PaginationResponse as PaginationResponseInterface } from '../interface/pagination.interface';
import { GetAnnouncementRequest } from '../schema/announcement.schema';
import { PaginationData } from '../schema/base.schema';
import { PaginationResponse, SuccessResponse } from '../types/response.type';
export default class AnnouncementService {
    private repo;
    private fileService;
    create(content: string, title: string, slug: string, document?: Express.Multer.File, username?: string): Promise<SuccessResponse<Omit<Announcement, ''>> | Announcement>;
    detail(id: number, host?: string): Promise<SuccessResponse<Omit<Announcement, ''>> | Announcement>;
    get(url: string | undefined, host: string | undefined, req: GetAnnouncementRequest): Promise<PaginationResponse<PaginationResponseInterface, Omit<Announcement, ''>[]> | PaginationData<Omit<Announcement, ''>[]>>;
    delete(id: number): Promise<SuccessResponse<string> | boolean>;
    update(id: number, content: string, title: string, slug: string, document?: Express.Multer.File, username?: string): Promise<SuccessResponse<string> | boolean>;
}
