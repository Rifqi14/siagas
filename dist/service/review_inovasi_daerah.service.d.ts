import { EvaluasiInovasiDaerah } from '../entity/EvaluasiInovasiDaerah.entity';
import Indicator from '../entity/Indicator.entity';
import { InovasiIndikator } from '../entity/InovasiIndikator.entity';
import { ReviewInovasiDaerah } from '../entity/ReviewInovasiDaerah.entity';
import { PaginationResponse as PaginationResponseInterface } from '../interface/pagination.interface';
import { PaginationData } from '../schema/base.schema';
import { CreateReviewInovasiDaerahRequest, EvaluasiResponse, GetReviewInovasiRequest, IndikatorResponse, ProfilInovasiResponse, ReviewInovasiDaerahResponse, UpdateEvaluasiRequest } from '../schema/review_inovasi_daerah.schema';
import { DownloadType } from '../types/lib';
import { PaginationResponse, SuccessResponse } from '../types/response.type';
export default class ReviewInovasiDaerahService {
    private repo;
    private exportService;
    create(username: string | undefined, req: CreateReviewInovasiDaerahRequest): Promise<SuccessResponse<ReviewInovasiDaerah> | ReviewInovasiDaerah>;
    download(type: DownloadType, username: string | undefined, req: GetReviewInovasiRequest): Promise<string>;
    detail(id: number): Promise<SuccessResponse<ReviewInovasiDaerahResponse> | ReviewInovasiDaerahResponse>;
    getProfilInovasi(id: number): Promise<SuccessResponse<ProfilInovasiResponse> | ProfilInovasiResponse>;
    getIndikator(id: number): Promise<SuccessResponse<IndikatorResponse[]> | IndikatorResponse[]>;
    getIndikatorDetail(id: number, indikator_id: string): Promise<SuccessResponse<IndikatorResponse> | IndikatorResponse>;
    updateEvaluasi(id: number, indikator_id: string, username: string | undefined, data: UpdateEvaluasiRequest): Promise<SuccessResponse<EvaluasiInovasiDaerah> | EvaluasiInovasiDaerah>;
    getEvaluasi(id: number, indikator_id: number, username?: string): Promise<SuccessResponse<EvaluasiResponse> | EvaluasiResponse>;
    get(url: string | undefined, username: string | undefined, req: GetReviewInovasiRequest): Promise<PaginationResponse<PaginationResponseInterface, ReviewInovasiDaerahResponse[]> | PaginationData<ReviewInovasiDaerahResponse[]>>;
    delete(id: number): Promise<SuccessResponse<string> | boolean>;
    update(id: number, username: string | undefined, req: CreateReviewInovasiDaerahRequest): Promise<SuccessResponse<string> | boolean>;
    toEvaluasiResponse(data: InovasiIndikator, review: ReviewInovasiDaerah): EvaluasiResponse;
    toIndikatorRespons(data: Indicator, evaluasi?: EvaluasiInovasiDaerah): IndikatorResponse;
    toProfilResponse(data: ReviewInovasiDaerah): Promise<ProfilInovasiResponse>;
    toResponse(data: ReviewInovasiDaerah): ReviewInovasiDaerahResponse;
}
