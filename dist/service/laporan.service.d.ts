import { PaginationResponse as PaginationResponseInterface } from '../interface/pagination.interface';
import { PaginationData } from '../schema/base.schema';
import { LaporanBentukInovasiParams, LaporanIndeksParams, LaporanIndeksResponse, LaporanInisiatorInovasiParams, LaporanJenisInovasiParams, LaporanUrusanInovasiParams } from '../schema/laporan.schema';
import { DownloadType } from '../types/lib';
import { PaginationResponse } from '../types/response.type';
export default class LaporanService {
    private repo;
    private exportService;
    laporanUrusanInovasi(url: string | undefined, req: LaporanUrusanInovasiParams): Promise<PaginationResponse<PaginationResponseInterface, any[]> | PaginationData<any[]>>;
    laporanInisiatorInovasi(url: string | undefined, req: LaporanInisiatorInovasiParams): Promise<PaginationResponse<PaginationResponseInterface, any[]> | PaginationData<any[]>>;
    laporanBentukInovasi(url: string | undefined, req: LaporanBentukInovasiParams): Promise<PaginationResponse<PaginationResponseInterface, any[]> | PaginationData<any[]>>;
    laporanJenisInovasi(url: string | undefined, req: LaporanJenisInovasiParams): Promise<PaginationResponse<PaginationResponseInterface, any[]> | PaginationData<any[]>>;
    laporanIndeks(url: string | undefined, req: LaporanIndeksParams): Promise<PaginationResponse<PaginationResponseInterface, LaporanIndeksResponse[]> | PaginationData<LaporanIndeksResponse[]>>;
    laporanIndeksdownload(type: DownloadType, req: LaporanIndeksParams): Promise<string>;
    laporanUrusanInovasidownload(type: DownloadType, req: LaporanUrusanInovasiParams): Promise<string>;
    laporanInisiatorInovasidownload(type: DownloadType, req: LaporanInisiatorInovasiParams): Promise<string>;
}
