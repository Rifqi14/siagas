/// <reference types="multer" />
import Document from '../entity/Document.entity';
import { PaginationResponse as PaginationResponseInterface } from '../interface/pagination.interface';
import { PaginationData } from '../schema/base.schema';
import { GetDocumentRequest } from '../schema/document.schema';
import { PaginationResponse, SuccessResponse } from '../types/response.type';
export default class DocumentService {
    private repo;
    private fileService;
    create(content: string, title: string, category_id?: number, document?: Express.Multer.File, username?: string): Promise<SuccessResponse<Omit<Document, ''>> | Document>;
    detail(id: number, host?: string): Promise<SuccessResponse<Omit<Document, ''>> | Document>;
    get(url: string | undefined, host: string | undefined, req: GetDocumentRequest): Promise<PaginationResponse<PaginationResponseInterface, Omit<Document, ''>[]> | PaginationData<Omit<Document, ''>[]>>;
    delete(id: number): Promise<SuccessResponse<string> | boolean>;
    update(id: number, content: string, title: string, category_id?: number, document?: Express.Multer.File, username?: string): Promise<SuccessResponse<string> | boolean>;
}
