"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const pemda_indikator_repository_1 = __importDefault(require("../repository/pemda_indikator.repository"));
const tsoa_1 = require("tsoa");
const pagination_1 = __importDefault(require("../providers/pagination"));
const api_error_1 = __importDefault(require("../providers/exceptions/api.error"));
const http_code_enum_1 = require("../types/http_code.enum");
let PemdaIndikatorService = class PemdaIndikatorService {
    constructor() {
        this.repo = new pemda_indikator_repository_1.default();
    }
    async create(pemda_id, data, username = '') {
        const indikator = await this.repo
            .create({
            pemda_id: +pemda_id,
            informasi: data.informasi,
            keterangan: data.keterangan,
            indikator_id: data.indikator_id,
            catatan: data.catatan,
            created_by: username,
            updated_by: username
        })
            .save();
        return indikator;
    }
    async update(pemda_id, pemda_indikator_id, username = '', data) {
        const indikator = await this.repo.findOne({
            where: { id: +pemda_indikator_id }
        });
        if (!indikator) {
            throw new api_error_1.default({
                httpCode: http_code_enum_1.HttpCode.NOT_FOUND,
                message: 'Data not found'
            });
        }
        await this.repo
            .create(Object.assign(Object.assign(Object.assign({}, indikator), data), { id: +pemda_indikator_id, updated_by: username }))
            .save();
        return indikator;
    }
    async delete(pemda_id, pemda_indikator_id) {
        const { affected } = await this.repo.delete({ id: +pemda_indikator_id });
        if (affected && affected > 0) {
            return true;
        }
        return false;
    }
    async get(pemda_id, url = '', username = '', req) {
        const query = this.repo
            .createQueryBuilder(`pi`)
            .leftJoinAndSelect(`pi.indicator`, 'i')
            .leftJoinAndSelect(`pi.files`, 'f')
            .where(`pi.pemda_id = :pemda_id`, { pemda_id });
        const total = await query.getCount();
        const { limit, offset, page, order } = pagination_1.default.options(req.page, req.limit);
        query.limit(limit).offset(offset);
        for (const key in order) {
            query.addOrderBy(`pi.${key}`, order[key]);
        }
        const res = await query.getMany();
        let paging = pagination_1.default.build(url, page, limit, total);
        return { paging, res };
    }
    async detail(pemda_id, indikator_id) {
        const res = await this.repo.findOneBy({ pemda_id, indikator_id });
        if (!res) {
            throw new api_error_1.default({
                httpCode: http_code_enum_1.HttpCode.NOT_FOUND,
                message: 'Data not found'
            });
        }
        return res;
    }
};
__decorate([
    (0, tsoa_1.Post)('/{pemda_id}/indikator'),
    __param(1, (0, tsoa_1.Body)()),
    __param(2, (0, tsoa_1.Query)()),
    __param(2, (0, tsoa_1.Hidden)()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [String, Object, String]),
    __metadata("design:returntype", Promise)
], PemdaIndikatorService.prototype, "create", null);
__decorate([
    (0, tsoa_1.Patch)('/{pemda_id}/indikator/{pemda_indikator_id}'),
    __param(2, (0, tsoa_1.Query)()),
    __param(2, (0, tsoa_1.Hidden)()),
    __param(3, (0, tsoa_1.Body)()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [String, String, String, Object]),
    __metadata("design:returntype", Promise)
], PemdaIndikatorService.prototype, "update", null);
__decorate([
    (0, tsoa_1.Delete)('/{pemda_id}/indikator/{pemda_indikator_id}'),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [String, String]),
    __metadata("design:returntype", Promise)
], PemdaIndikatorService.prototype, "delete", null);
__decorate([
    (0, tsoa_1.Get)('/{pemda_id}/indikator'),
    __param(1, (0, tsoa_1.Query)('url')),
    __param(1, (0, tsoa_1.Hidden)()),
    __param(2, (0, tsoa_1.Query)()),
    __param(2, (0, tsoa_1.Hidden)()),
    __param(3, (0, tsoa_1.Queries)()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [String, String, String, Object]),
    __metadata("design:returntype", Promise)
], PemdaIndikatorService.prototype, "get", null);
__decorate([
    (0, tsoa_1.Get)('/{pemda_id}/indikator/{indikator_id}'),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Number, Number]),
    __metadata("design:returntype", Promise)
], PemdaIndikatorService.prototype, "detail", null);
PemdaIndikatorService = __decorate([
    (0, tsoa_1.Route)('/profil_pemda'),
    (0, tsoa_1.Tags)('Database Inovasi Daerah | Profil Pemda | Pemda Indikator'),
    (0, tsoa_1.Security)('bearer')
], PemdaIndikatorService);
exports.default = PemdaIndikatorService;
