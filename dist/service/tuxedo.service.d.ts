import { CreateTuxedoRequest, GetTuxedoRequest } from '../schema/tuxedo.schema';
import { PaginationResponse, SuccessResponse } from '../types/response.type';
import { PaginationResponse as PaginationResponseInterface } from '../interface/pagination.interface';
import Tuxedo from '../entity/Tuxedo.entity';
import { PaginationData } from '../schema/base.schema';
export default class TuxedoService {
    private repo;
    create(req: CreateTuxedoRequest): Promise<SuccessResponse<Omit<Tuxedo, ''>> | Tuxedo>;
    detail(id: number): Promise<SuccessResponse<Omit<Tuxedo, ''>> | Tuxedo>;
    get(url: string | undefined, req: GetTuxedoRequest): Promise<PaginationResponse<PaginationResponseInterface, Omit<Tuxedo, ''>[]> | PaginationData<Omit<Tuxedo, ''>[]>>;
    delete(id: number): Promise<SuccessResponse<string> | boolean>;
    update(id: number, req: CreateTuxedoRequest): Promise<SuccessResponse<string> | boolean>;
}
