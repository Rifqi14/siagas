"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const exceljs_1 = require("exceljs");
const fs_1 = require("fs");
const jspdf_1 = __importDefault(require("jspdf"));
const path_1 = require("path");
const process_1 = require("process");
const tsoa_1 = require("tsoa");
const excel_1 = require("../providers/excel");
const axios_1 = __importDefault(require("axios"));
let ExportService = class ExportService {
    async export(req, stream = (0, fs_1.createWriteStream)((0, path_1.join)(__dirname, "..", "..", "public", "tmp", "file.xlsx"))) {
        const excel = new excel_1.Excel();
        await excel
            .setWorksheet(req.name)
            .setHeader(req.headers)
            .addRow(req.data)
            .setStyleAll({
            border: {
                bottom: { style: "thin", color: { argb: "FF000000" } },
                top: { style: "thin", color: { argb: "FF000000" } },
                left: { style: "thin", color: { argb: "FF000000" } },
                right: { style: "thin", color: { argb: "FF000000" } },
            },
        })
            .styleHeaderCell({
            font: { bold: true },
            fill: {
                bgColor: { argb: "FFFF740F" },
                fgColor: { argb: "FFFF740F" },
                type: "pattern",
                pattern: "solid",
            },
            border: {
                bottom: { style: "double", color: { argb: "FF000000" } },
                top: { style: "thick", color: { argb: "FF000000" } },
                left: { style: "thick", color: { argb: "FF000000" } },
                right: { style: "thick", color: { argb: "FF000000" } },
            },
        })
            .write(req.format, { filename: req.name, stream });
    }
    async download({ data, name }) {
        try {
            const headers = Array.from(Object.keys(data[0]));
            // const rows: any[][] = [];
            // let i = 0;
            // while (i < data.length) {
            //   rows.push(Object.values(data[i]));
            //   i++;
            // }
            // Creating a workbook
            const book = new exceljs_1.Workbook();
            // Create worksheet
            const sheet = book.addWorksheet();
            sheet.columns = headers
                .filter(v => !v.includes("_id"))
                .map(key => ({
                key,
                header: key
                    .split("_")
                    .map(column => column.charAt(0).toUpperCase() + column.slice(1))
                    .join(" "),
            }));
            data.forEach(item => sheet.addRow(item));
            const exportPath = (0, path_1.join)((0, process_1.cwd)(), "public", "tmp", `${new Date().getTime().toString()}-${name}.xlsx`);
            await book.xlsx.writeFile(exportPath);
            return exportPath;
        }
        catch (error) {
            console.log(error);
            throw error;
        }
    }
    async generatePdfFromUrl(url, outputName) {
        try {
            const response = await axios_1.default.get(url);
            const content = response.data;
            const doc = new jspdf_1.default();
            doc.text(content, 10, 10);
            const exportPath = (0, path_1.join)((0, process_1.cwd)(), "public", "tmp", `${new Date().getTime().toString()}-${outputName}.pdf`);
            doc.save(exportPath);
            return exportPath;
        }
        catch (error) {
            console.log(error);
            throw error;
        }
    }
};
__decorate([
    (0, tsoa_1.Post)(""),
    __param(0, (0, tsoa_1.Body)()),
    __param(1, (0, tsoa_1.Query)()),
    __param(1, (0, tsoa_1.Hidden)()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Object, Object]),
    __metadata("design:returntype", Promise)
], ExportService.prototype, "export", null);
ExportService = __decorate([
    (0, tsoa_1.Route)("/export"),
    (0, tsoa_1.Tags)("Export")
], ExportService);
exports.default = ExportService;
