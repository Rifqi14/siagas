"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const fs_1 = require("fs");
const path_1 = require("path");
const api_error_1 = __importDefault(require("../providers/exceptions/api.error"));
const response_1 = __importDefault(require("../providers/response"));
const government_innovation_service_1 = __importDefault(require("../service/government_innovation.service"));
const http_code_enum_1 = require("../types/http_code.enum");
const base_controller_1 = __importDefault(require("./base.controller"));
class GovernmentInnovationController extends base_controller_1.default {
    constructor() {
        super(new government_innovation_service_1.default());
        this.create = async (req, res, next) => {
            const uploads = req.files;
            try {
                const { bentuk_inovasi, hasil_inovasi, inisiator_inovasi, jenis_inovasi, manfaat, nama_inovasi, nama_pemda, rancang_bangun, tahapan_inovasi, tematik, tujuan, urusan_lain, urusan_pertama, waktu_penerapan, waktu_uji_coba, urusan_pemerintah, } = req.body;
                let anggaran_file = undefined;
                let profile_file = undefined;
                let foto = undefined;
                if (uploads && uploads.anggaran_file)
                    anggaran_file = uploads.anggaran_file[0];
                if (uploads && uploads.profile_file)
                    profile_file = uploads.profile_file[0];
                if (uploads && uploads.foto)
                    foto = uploads.foto[0];
                const result = await this.service.create(nama_pemda, nama_inovasi, tahapan_inovasi, inisiator_inovasi, jenis_inovasi, bentuk_inovasi, tematik, urusan_pertama, urusan_lain, waktu_uji_coba, waktu_penerapan, rancang_bangun, tujuan, manfaat, hasil_inovasi, urusan_pemerintah, anggaran_file, profile_file, foto, res.locals.user.username);
                response_1.default.success(result).create(res);
            }
            catch (error) {
                if (uploads && uploads.anggaran_file) {
                    (0, fs_1.unlink)((0, path_1.join)(__dirname, '..', '..', 'public', 'upload', 'inovasi_pemda', uploads.anggaran_file[0].filename), err => {
                        if (err) {
                            console.log(`${uploads.anggaran_file[0].filename} was deleted`);
                        }
                    });
                }
                if (uploads && uploads.profile_file) {
                    (0, fs_1.unlink)((0, path_1.join)(__dirname, '..', '..', 'public', 'upload', 'inovasi_pemda', uploads.profile_file[0].filename), err => {
                        if (err) {
                            console.log(`${uploads.profile_file[0].filename} was deleted`);
                        }
                    });
                }
                next(error);
            }
        };
        this.download = async (req, res, next) => {
            try {
                const { username } = res.locals.user;
                const result = await this.service.download(req.params.type, username, req.query);
                res.download(result, err => {
                    if (!err)
                        (0, fs_1.unlinkSync)(result);
                });
            }
            catch (error) {
                next(error);
            }
        };
        this.downloadDetail = async (req, res, next) => {
            try {
                const { username } = res.locals.user;
                const result = await this.service.downloadProfil(req.params.id, req.params.type);
                res.download(result, err => {
                    if (!err)
                        (0, fs_1.unlinkSync)(result);
                });
            }
            catch (error) {
                next(error);
            }
        };
        this.get = async (req, res, next) => {
            try {
                const { username } = res.locals.user;
                const url = req.protocol + '://' + req.get('host') + req.originalUrl;
                const result = (await this.service.get(req.query, url, username));
                response_1.default.pagination(result.res, result.paging).create(res);
            }
            catch (error) {
                next(error);
            }
        };
        this.detail = async (req, res, next) => {
            try {
                const result = await this.service.detail(req.params.id);
                response_1.default.success(result).create(res);
            }
            catch (error) {
                next(error);
            }
        };
        this.update = async (req, res, next) => {
            const uploads = req.files;
            try {
                const { bentuk_inovasi, urusan_pemerintah, hasil_inovasi, inisiator_inovasi, jenis_inovasi, manfaat, nama_inovasi, nama_pemda, rancang_bangun, tahapan_inovasi, tematik, tujuan, urusan_lain, urusan_pertama, waktu_penerapan, waktu_uji_coba, } = req.body;
                let anggaran_file = undefined;
                let profile_file = undefined;
                let foto = undefined;
                if (uploads && uploads.anggaran_file)
                    anggaran_file = uploads.anggaran_file[0];
                if (uploads && uploads.profile_file)
                    profile_file = uploads.profile_file[0];
                if (uploads && uploads.foto)
                    foto = uploads.foto[0];
                const result = await this.service.update(req.params.id, nama_pemda, urusan_pemerintah, nama_inovasi, tahapan_inovasi, inisiator_inovasi, jenis_inovasi, bentuk_inovasi, tematik, urusan_pertama, urusan_lain, waktu_uji_coba, waktu_penerapan, rancang_bangun, tujuan, manfaat, hasil_inovasi, anggaran_file, profile_file, foto, res.locals.user.username);
                response_1.default.success(result).create(res);
            }
            catch (error) {
                next(error);
            }
        };
        this.delete = async (req, res, next) => {
            try {
                const result = await this.service.delete(req.params.id);
                if (!result) {
                    next(new api_error_1.default({
                        httpCode: http_code_enum_1.HttpCode.BAD_REQUEST,
                        message: 'Gagal hapus data',
                    }));
                }
                else {
                    response_1.default.success('Berhasil hapus data').create(res);
                }
            }
            catch (error) {
                next(error);
            }
        };
    }
}
exports.default = GovernmentInnovationController;
