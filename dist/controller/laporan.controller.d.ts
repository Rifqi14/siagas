import { NextFunction, Request, Response } from "express";
import { ParamsDictionary } from "express-serve-static-core";
import { ParsedQs } from "qs";
import { LaporanBentukInovasiParams, LaporanIndeksParams, LaporanInisiatorInovasiParams, LaporanJenisInovasiParams, LaporanUrusanInovasiParams } from "../schema/laporan.schema";
import LaporanService from "../service/laporan.service";
import { DownloadType } from "../types/lib";
import BaseController from "./base.controller";
export declare class LaporanController extends BaseController<LaporanService> {
    constructor();
    laporanUrusan: (req: Request<ParamsDictionary, any, any, ParsedQs & LaporanUrusanInovasiParams, Record<string, any>>, res: Response<any, Record<string, any>>, next: NextFunction) => Promise<void>;
    laporanInisiator: (req: Request<ParamsDictionary, any, any, ParsedQs & LaporanInisiatorInovasiParams, Record<string, any>>, res: Response<any, Record<string, any>>, next: NextFunction) => Promise<void>;
    laporanBentuk: (req: Request<ParamsDictionary, any, any, ParsedQs & LaporanBentukInovasiParams, Record<string, any>>, res: Response<any, Record<string, any>>, next: NextFunction) => Promise<void>;
    laporanIndeks: (req: Request<ParamsDictionary, any, any, ParsedQs & LaporanIndeksParams, Record<string, any>>, res: Response<any, Record<string, any>>, next: NextFunction) => Promise<void>;
    laporanIndeksDownload: (req: Request<ParamsDictionary & {
        type: DownloadType;
    }, any, any, ParsedQs & LaporanIndeksParams, Record<string, any>>, res: Response<any, Record<string, any>>, next: NextFunction) => Promise<void>;
    laporanUrusanInovasiDownload: (req: Request<ParamsDictionary & {
        type: DownloadType;
    }, any, any, ParsedQs & LaporanUrusanInovasiParams, Record<string, any>>, res: Response<any, Record<string, any>>, next: NextFunction) => Promise<void>;
    laporanInisiatorInovasiDownload: (req: Request<ParamsDictionary & {
        type: DownloadType;
    }, any, any, ParsedQs & LaporanInisiatorInovasiParams, Record<string, any>>, res: Response<any, Record<string, any>>, next: NextFunction) => Promise<void>;
    laporanJenisInovasi: (req: Request<ParamsDictionary, any, any, ParsedQs & LaporanJenisInovasiParams, Record<string, any>>, res: Response<any, Record<string, any>>, next: NextFunction) => Promise<void>;
}
