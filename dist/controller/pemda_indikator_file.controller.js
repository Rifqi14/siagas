"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.PemdaIndikatorFileController = void 0;
const api_error_1 = __importDefault(require("../providers/exceptions/api.error"));
const response_1 = __importDefault(require("../providers/response"));
const pemda_indikator_file_service_1 = __importDefault(require("../service/pemda_indikator_file.service"));
const http_code_enum_1 = require("../types/http_code.enum");
const base_controller_1 = __importDefault(require("./base.controller"));
class PemdaIndikatorFileController extends base_controller_1.default {
    constructor() {
        super(new pemda_indikator_file_service_1.default());
        this.create = async (req, res, next) => {
            throw new Error('Method not implemented');
        };
        this.upload = async (req, res, next) => {
            const document = req.file;
            if (!document)
                throw new api_error_1.default({
                    httpCode: http_code_enum_1.HttpCode.BAD_REQUEST,
                    message: 'Invalid request'
                });
            try {
                const result = await this.service.upload(req.params.pemda_id, req.params.indikator_id, req.body.nomor_dokumen, req.body.tanggal_dokumen, req.body.tentang, document, req.body.name, res.locals.user.username);
                response_1.default.success(result).create(res);
            }
            catch (error) {
                next(error);
            }
        };
        this.get = async (req, res, next) => {
            try {
                const host = req.protocol + '://' + req.get('host');
                const url = host + req.originalUrl;
                const result = (await this.service.get(req.params.pemda_id, req.params.indikator_id, url, res.locals.user.username, req.query));
                response_1.default.pagination(result.res, result.paging).create(res);
            }
            catch (error) {
                next(error);
            }
        };
        this.detail = async (req, res, next) => {
            try {
                const result = await this.service.detail(req.params.pemda_id, req.params.indikator_id, req.params.file_id);
                response_1.default.success(result).create(res);
            }
            catch (error) {
                next(error);
            }
        };
        this.update = async (req, res, next) => {
            throw new Error('Method not implemented');
        };
        this.delete = async (req, res, next) => {
            try {
                const { pemda_id, indikator_id, file_id } = req.params;
                const result = await this.service.delete(pemda_id, indikator_id, file_id);
                if (!result) {
                    next(new api_error_1.default({
                        httpCode: http_code_enum_1.HttpCode.BAD_REQUEST,
                        message: 'Gagal hapus data'
                    }));
                }
                response_1.default.success('Berhasil hapus data').create(res);
            }
            catch (error) {
                next(error);
            }
        };
    }
}
exports.PemdaIndikatorFileController = PemdaIndikatorFileController;
