"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const response_1 = __importDefault(require("../providers/response"));
const user_service_1 = __importDefault(require("../service/user.service"));
const base_controller_1 = __importDefault(require("./base.controller"));
class UserController extends base_controller_1.default {
    constructor() {
        super(new user_service_1.default());
        this.create = async (req, res, next) => {
            try {
                const result = await this.service.create(req.body);
                response_1.default.success(result).create(res);
            }
            catch (error) {
                next(error);
            }
        };
        this.get = async (req, res, next) => {
            try {
                const { username } = res.locals.user;
                const url = req.protocol + '://' + req.get('host') + req.originalUrl;
                const result = (await this.service.findAll(req.query, url, username));
                response_1.default.pagination(result.res, result.paging).create(res);
            }
            catch (error) {
                next(error);
            }
        };
        this.detail = async (req, res, next) => {
            try {
                const result = await this.service.detail(req.params.id);
                response_1.default.success(result).create(res);
            }
            catch (error) {
                next(error);
            }
        };
        this.update = async (req, res, next) => {
            try {
                const { id } = req.params;
                const result = await this.service.update(+id, req.body);
                response_1.default.success(result).create(res);
            }
            catch (error) {
                next(error);
            }
        };
        this.delete = async (req, res, next) => {
            try {
                const result = await this.service.delete(+req.params.id);
                response_1.default.success(result).create(res);
            }
            catch (error) {
                next(error);
            }
        };
    }
}
exports.default = UserController;
