"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.RekapIndeksAkhirController = void 0;
const base_controller_1 = __importDefault(require("./base.controller"));
const api_error_1 = __importDefault(require("../providers/exceptions/api.error"));
const response_1 = __importDefault(require("../providers/response"));
const rekap_indeks_akhir_service_1 = __importDefault(require("../service/rekap_indeks_akhir.service"));
const http_code_enum_1 = require("../types/http_code.enum");
class RekapIndeksAkhirController extends base_controller_1.default {
    constructor() {
        super(new rekap_indeks_akhir_service_1.default());
        this.get = async (req, res, next) => {
            try {
                const { username } = res.locals.user;
                const url = req.protocol + '://' + req.get('host') + req.originalUrl;
                const result = (await this.service.get(url, username, req.query));
                response_1.default.pagination(result.res, result.paging).create(res);
            }
            catch (error) {
                next(error);
            }
        };
        this.update = async (req, res, next) => {
            try {
                const { id } = req.params;
                const result = await this.service.nominator(id, req.body);
                if (!result) {
                    throw new api_error_1.default({
                        httpCode: http_code_enum_1.HttpCode.BAD_REQUEST,
                        message: 'Gagal ubah data'
                    });
                }
                response_1.default.success('Berhasil ubah data').create(res);
            }
            catch (error) {
                next(error);
            }
        };
    }
}
exports.RekapIndeksAkhirController = RekapIndeksAkhirController;
