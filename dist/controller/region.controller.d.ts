import { Request, Response, NextFunction } from 'express';
import { ParamsDictionary } from 'express-serve-static-core';
import { ParsedQs } from 'qs';
import Regions from '../entity/Region.entity';
import { CreateRegionRequest, GetRegionRequest } from '../schema/region.schema';
import { SuccessResponse } from '../types/response.type';
import RegionService from '../service/region.service';
import BaseController from './base.controller';
import { IBaseController } from './interface/ibase.controller';
declare class RegionController extends BaseController<RegionService> implements IBaseController {
    constructor();
    create: (req: Request<ParamsDictionary, SuccessResponse<Regions>, CreateRegionRequest, ParsedQs, Record<string, any>>, res: Response<any, Record<string, any>>, next: NextFunction) => Promise<void>;
    get: (req: Request<ParamsDictionary, any, any, ParsedQs & GetRegionRequest, Record<string, any>>, res: Response<any, Record<string, any>>, next: NextFunction) => Promise<void>;
    detail: (req: Request<ParamsDictionary & {
        id: number;
    }, any, any, ParsedQs, Record<string, any>>, res: Response<any, Record<string, any>>, next: NextFunction) => Promise<void>;
    update: (req: Request<ParamsDictionary & {
        id: number;
    }, any, CreateRegionRequest, ParsedQs, Record<string, any>>, res: Response<any, Record<string, any>>, next: NextFunction) => Promise<void>;
    delete: (req: Request<ParamsDictionary & {
        id: number;
    }, any, any, ParsedQs, Record<string, any>>, res: Response<any, Record<string, any>>, next: NextFunction) => Promise<void>;
}
export default RegionController;
