import { NextFunction, Request, Response } from 'express';
import { ParamsDictionary } from 'express-serve-static-core';
import { ParsedQs } from 'qs';
import { LoginRequest, TokenResponse } from '../schema/authentication.schema';
import { SuccessResponse } from '../types/response.type';
declare class AuthenticationController {
    private service;
    constructor();
    login: (req: Request<ParamsDictionary, any, LoginRequest, ParsedQs, Record<string, any>>, res: Response<SuccessResponse<TokenResponse>, Record<string, any>>, next: NextFunction) => Promise<void>;
    checkEmail: (req: Request<ParamsDictionary, any, any, ParsedQs & {
        email: string;
    }, Record<string, any>>, res: Response<SuccessResponse<boolean>, Record<string, any>>, next: NextFunction) => Promise<void>;
    me: (req: Request<ParamsDictionary, any, any, ParsedQs, Record<string, any>>, res: Response<SuccessResponse<boolean>, Record<string, any>>, next: NextFunction) => Promise<void>;
}
export default AuthenticationController;
