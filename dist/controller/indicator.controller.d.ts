import { NextFunction, Request, Response } from 'express';
import { ParamsDictionary } from 'express-serve-static-core';
import { ParsedQs } from 'qs';
import Indicator from '../entity/Indicator.entity';
import { CreateIndicatorRequest } from '../schema/indicator.schema';
import { GetRegionRequest } from '../schema/region.schema';
import IndicatorService from '../service/indicator.service';
import { SuccessResponse } from '../types/response.type';
import BaseController from './base.controller';
import { IBaseController } from './interface/ibase.controller';
declare class IndicatorController extends BaseController<IndicatorService> implements IBaseController {
    constructor();
    create: (req: Request<ParamsDictionary, any, CreateIndicatorRequest, ParsedQs, Record<string, any>>, res: Response<SuccessResponse<Indicator>, Record<string, any>>, next: NextFunction) => Promise<void>;
    get: (req: Request<ParamsDictionary, any, any, ParsedQs & GetRegionRequest, Record<string, any>>, res: Response<any, Record<string, any>>, next: NextFunction) => Promise<void>;
    detail: (req: Request<ParamsDictionary & {
        id: number;
    }, any, any, ParsedQs, Record<string, any>>, res: Response<any, Record<string, any>>, next: NextFunction) => Promise<void>;
    update: (req: Request<ParamsDictionary & {
        id: number;
    }, any, CreateIndicatorRequest, ParsedQs, Record<string, any>>, res: Response<any, Record<string, any>>, next: NextFunction) => Promise<void>;
    delete: (req: Request<ParamsDictionary & {
        id: number;
    }, any, any, ParsedQs, Record<string, any>>, res: Response<any, Record<string, any>>, next: NextFunction) => Promise<void>;
}
export default IndicatorController;
