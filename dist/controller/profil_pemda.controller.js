"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const profil_pemda_service_1 = __importDefault(require("../service/profil_pemda.service"));
const base_controller_1 = __importDefault(require("./base.controller"));
const response_1 = __importDefault(require("../providers/response"));
const fs_1 = require("fs");
const path_1 = require("path");
const api_error_1 = __importDefault(require("../providers/exceptions/api.error"));
const http_code_enum_1 = require("../types/http_code.enum");
class ProfilPemdaController extends base_controller_1.default {
    constructor() {
        super(new profil_pemda_service_1.default());
        this.create = async (req, res, next) => {
            const file = req.file;
            try {
                const { username } = res.locals.user;
                const { nama_pemda, alamat_pemda, email, nama_admin, nama_daerah, no_telpon, opd_yang_menangani } = req.body;
                const result = await this.service.create(nama_pemda, nama_daerah, opd_yang_menangani, alamat_pemda, email, no_telpon, nama_admin, file, username);
                response_1.default.success(result).create(res);
            }
            catch (error) {
                if (file) {
                    (0, fs_1.unlink)((0, path_1.join)(__dirname, '..', '..', 'public', 'upload', 'profil_pemda', file.filename), err => {
                        if (err) {
                            console.log(`${file.filename} was deleted`);
                        }
                    });
                }
                next(error);
            }
        };
        this.getDownload = async (req, res, next) => {
            try {
                const { username } = res.locals.user;
                const result = await this.service.download(req.params.type, username, req.query);
                res.download(result, err => !err && (0, fs_1.unlinkSync)(result));
            }
            catch (error) {
                next(error);
            }
        };
        this.downloadDetail = async (req, res, next) => {
            try {
                const { username } = res.locals.user;
                const result = await this.service.downloadProfil(req.params.id, req.params.type);
                res.download(result, err => {
                    if (!err)
                        (0, fs_1.unlinkSync)(result);
                });
            }
            catch (error) {
                next(error);
            }
        };
        this.get = async (req, res, next) => {
            try {
                const { username } = res.locals.user;
                const url = req.protocol + '://' + req.get('host') + req.originalUrl;
                const result = (await this.service.get(url, username, req.query));
                response_1.default.pagination(result.res, result.paging).create(res);
            }
            catch (error) {
                next(error);
            }
        };
        this.detail = async (req, res, next) => {
            try {
                const { id } = req.params;
                const result = await this.service.detail(id);
                response_1.default.success(result).create(res);
            }
            catch (error) {
                next(error);
            }
        };
        this.update = async (req, res, next) => {
            const file = req.file;
            try {
                const { id } = req.params;
                const { username } = res.locals.user;
                const { nama_pemda, alamat_pemda, email, nama_admin, nama_daerah, no_telpon, opd_yang_menangani } = req.body;
                const result = await this.service.update(id, nama_pemda, nama_daerah, opd_yang_menangani, alamat_pemda, email, no_telpon, nama_admin, file, username);
                if (!result) {
                    throw new api_error_1.default({
                        httpCode: http_code_enum_1.HttpCode.BAD_REQUEST,
                        message: 'Gagal ubah data',
                    });
                }
                response_1.default.success('Berhasil ubah data').create(res);
            }
            catch (error) {
                if (file) {
                    (0, fs_1.unlink)((0, path_1.join)(__dirname, '..', '..', 'public', 'upload', 'profil_pemda', file.filename), err => {
                        if (err) {
                            console.log(`${file.filename} was deleted`);
                        }
                    });
                }
                next(error);
            }
        };
        this.delete = async (req, res, next) => {
            try {
                const { id } = req.params;
                const result = await this.service.delete(id);
                if (!result) {
                    throw new api_error_1.default({
                        httpCode: http_code_enum_1.HttpCode.BAD_REQUEST,
                        message: 'Gagal hapus data',
                    });
                }
                response_1.default.success('Berhasil hapus data').create(res);
            }
            catch (error) {
                next(error);
            }
        };
    }
}
exports.default = ProfilPemdaController;
