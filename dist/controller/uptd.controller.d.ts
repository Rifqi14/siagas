import { NextFunction, Request, Response } from 'express';
import { ParamsDictionary } from 'express-serve-static-core';
import { ParsedQs } from 'qs';
import Uptd from '../entity/Uptd.entity';
import { GetRoleRequest } from '../schema/role.schema';
import { CreateUptdRequest } from '../schema/uptd.schema';
import UptdService from '../service/uptd.service';
import { SuccessResponse } from '../types/response.type';
import BaseController from './base.controller';
import { IBaseController } from './interface/ibase.controller';
declare class UptdController extends BaseController<UptdService> implements IBaseController {
    constructor();
    create: (req: Request<ParamsDictionary, any, CreateUptdRequest, ParsedQs, Record<string, any>>, res: Response<SuccessResponse<Uptd>, Record<string, any>>, next: NextFunction) => Promise<void>;
    get: (req: Request<ParamsDictionary, any, any, ParsedQs & GetRoleRequest, Record<string, any>>, res: Response<any, Record<string, any>>, next: NextFunction) => Promise<void>;
    detail: (req: Request<ParamsDictionary & {
        id: number;
    }, any, any, ParsedQs, Record<string, any>>, res: Response<any, Record<string, any>>, next: NextFunction) => Promise<void>;
    update: (req: Request<ParamsDictionary & {
        id: number;
    }, any, CreateUptdRequest, ParsedQs, Record<string, any>>, res: Response<any, Record<string, any>>, next: NextFunction) => Promise<void>;
    delete: (req: Request<ParamsDictionary & {
        id: number;
    }, any, any, ParsedQs, Record<string, any>>, res: Response<any, Record<string, any>>, next: NextFunction) => Promise<void>;
}
export default UptdController;
