import { NextFunction, Request, Response } from 'express';
import { ParamsDictionary } from 'express-serve-static-core';
import { ParsedQs } from 'qs';
import { PaginationResponse, SuccessResponse } from '../types/response.type';
import BaseController from './base.controller';
import { IBaseController } from './interface/ibase.controller';
import { PaginationResponse as PaginationResponseInterface } from '../interface/pagination.interface';
import DocumentCategoryService from '../service/document_category.service';
import { CreateDocumentCategoryRequest, GetDocumentCategoryRequest } from '../schema/document_category.schema';
import DocumentCategory from '../entity/DocumentCategory.entity';
declare class DocumentCategoryController extends BaseController<DocumentCategoryService> implements IBaseController {
    constructor();
    create: (req: Request<ParamsDictionary, any, CreateDocumentCategoryRequest, ParsedQs, Record<string, any>>, res: Response<SuccessResponse<DocumentCategory>, Record<string, any>>, next: NextFunction) => Promise<void>;
    get: (req: Request<ParamsDictionary, any, any, ParsedQs & GetDocumentCategoryRequest, Record<string, any>>, res: Response<PaginationResponse<PaginationResponseInterface, DocumentCategory>, Record<string, any>>, next: NextFunction) => Promise<void>;
    detail: (req: Request<ParamsDictionary & {
        id: number;
    }, any, any, ParsedQs, Record<string, any>>, res: Response<any, Record<string, any>>, next: NextFunction) => Promise<void>;
    update: (req: Request<ParamsDictionary & {
        id: number;
    }, any, CreateDocumentCategoryRequest, ParsedQs, Record<string, any>>, res: Response<SuccessResponse<string>, Record<string, any>>, next: NextFunction) => Promise<void>;
    delete: (req: Request<ParamsDictionary & {
        id: number;
    }, any, any, ParsedQs, Record<string, any>>, res: Response<SuccessResponse<string>, Record<string, any>>, next: NextFunction) => Promise<void>;
}
export default DocumentCategoryController;
