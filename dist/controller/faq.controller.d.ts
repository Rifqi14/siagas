import { NextFunction, Request, Response } from 'express';
import { ParamsDictionary } from 'express-serve-static-core';
import { ParsedQs } from 'qs';
import Faq from '../entity/Faq.entity';
import { PaginationResponse as PaginationResponseInterface } from '../interface/pagination.interface';
import { CreateFaqRequest, GetFaqRequest } from '../schema/faq.schema';
import FaqService from '../service/faq.service';
import { PaginationResponse, SuccessResponse } from '../types/response.type';
import BaseController from './base.controller';
import { IBaseController } from './interface/ibase.controller';
declare class FaqController extends BaseController<FaqService> implements IBaseController {
    constructor();
    create: (req: Request<ParamsDictionary, any, CreateFaqRequest, ParsedQs, Record<string, any>>, res: Response<SuccessResponse<Faq>, Record<string, any>>, next: NextFunction) => Promise<void>;
    get: (req: Request<ParamsDictionary, any, any, ParsedQs & GetFaqRequest, Record<string, any>>, res: Response<PaginationResponse<PaginationResponseInterface, Faq>, Record<string, any>>, next: NextFunction) => Promise<void>;
    detail: (req: Request<ParamsDictionary & {
        id: number;
    }, any, any, ParsedQs, Record<string, any>>, res: Response<any, Record<string, any>>, next: NextFunction) => Promise<void>;
    update: (req: Request<ParamsDictionary & {
        id: number;
    }, any, CreateFaqRequest, ParsedQs, Record<string, any>>, res: Response<SuccessResponse<string>, Record<string, any>>, next: NextFunction) => Promise<void>;
    delete: (req: Request<ParamsDictionary & {
        id: number;
    }, any, any, ParsedQs, Record<string, any>>, res: Response<SuccessResponse<string>, Record<string, any>>, next: NextFunction) => Promise<void>;
}
export default FaqController;
