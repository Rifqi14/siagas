"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.ReviewInovasiDaerahController = void 0;
const api_error_1 = __importDefault(require("../providers/exceptions/api.error"));
const response_1 = __importDefault(require("../providers/response"));
const review_inovasi_daerah_service_1 = __importDefault(require("../service/review_inovasi_daerah.service"));
const http_code_enum_1 = require("../types/http_code.enum");
const base_controller_1 = __importDefault(require("./base.controller"));
const fs_1 = require("fs");
class ReviewInovasiDaerahController extends base_controller_1.default {
    constructor() {
        super(new review_inovasi_daerah_service_1.default());
        this.create = async (req, res, next) => {
            try {
                const { username } = res.locals.user;
                const result = await this.service.create(username, req.body);
                response_1.default.success(result).create(res);
            }
            catch (error) {
                next(error);
            }
        };
        this.get = async (req, res, next) => {
            try {
                const { username } = res.locals.user;
                const url = req.protocol + '://' + req.get('host') + req.originalUrl;
                const result = (await this.service.get(url, username, req.query));
                response_1.default.pagination(result.res, result.paging).create(res);
            }
            catch (error) {
                next(error);
            }
        };
        this.getDownload = async (req, res, next) => {
            try {
                const { username } = res.locals.user;
                const result = await this.service.download(req.params.type, username, req.query);
                res.download(result, err => !err && (0, fs_1.unlinkSync)(result));
            }
            catch (error) {
                next(error);
            }
        };
        this.detail = async (req, res, next) => {
            try {
                const { id } = req.params;
                const result = await this.service.detail(id);
                response_1.default.success(result).create(res);
            }
            catch (error) {
                next(error);
            }
        };
        this.profilInovasi = async (req, res, next) => {
            try {
                const { id } = req.params;
                const result = await this.service.getProfilInovasi(id);
                response_1.default.success(result).create(res);
            }
            catch (error) {
                next(error);
            }
        };
        this.indikator = async (req, res, next) => {
            try {
                const { id } = req.params;
                const result = await this.service.getIndikator(id);
                response_1.default.success(result).create(res);
            }
            catch (error) {
                next(error);
            }
        };
        this.indikatorDetail = async (req, res, next) => {
            try {
                const { id, indikator_id } = req.params;
                const result = await this.service.getIndikatorDetail(id, indikator_id);
                response_1.default.success(result).create(res);
            }
            catch (error) {
                next(error);
            }
        };
        this.evaluasi = async (req, res, next) => {
            try {
                const { id, indikator_id } = req.params;
                const result = await this.service.updateEvaluasi(id, indikator_id, res.locals.user.username, req.body);
                response_1.default.success(result).create(res);
            }
            catch (error) {
                next(error);
            }
        };
        this.getEvaluasi = async (req, res, next) => {
            try {
                const { id, indikator_id } = req.params;
                const result = await this.service.getEvaluasi(id, indikator_id, res.locals.user.username);
                response_1.default.success(result).create(res);
            }
            catch (error) {
                next(error);
            }
        };
        this.update = async (req, res, next) => {
            try {
                const { id } = req.params;
                const { username } = res.locals.user;
                const result = await this.service.update(id, username, req.body);
                if (!result) {
                    throw new api_error_1.default({
                        httpCode: http_code_enum_1.HttpCode.BAD_REQUEST,
                        message: 'Gagal ubah data',
                    });
                }
                response_1.default.success('Berhasil ubah data').create(res);
            }
            catch (error) {
                next(error);
            }
        };
        this.delete = async (req, res, next) => {
            try {
                const { id } = req.params;
                const result = await this.service.delete(id);
                if (!result) {
                    throw new api_error_1.default({
                        httpCode: http_code_enum_1.HttpCode.BAD_REQUEST,
                        message: 'Gagal hapus data',
                    });
                }
                response_1.default.success('Berhasil hapus data').create(res);
            }
            catch (error) {
                next(error);
            }
        };
    }
}
exports.ReviewInovasiDaerahController = ReviewInovasiDaerahController;
