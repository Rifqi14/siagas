"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.DashboardController = void 0;
const base_controller_1 = __importDefault(require("./base.controller"));
const dashboard_service_1 = __importDefault(require("../service/dashboard.service"));
const response_1 = __importDefault(require("../providers/response"));
const fs_1 = require("fs");
class DashboardController extends base_controller_1.default {
    constructor() {
        super(new dashboard_service_1.default());
        this.arsip = async (req, res, next) => {
            var _a, _b;
            try {
                const { username } = (_b = (_a = res.locals) === null || _a === void 0 ? void 0 : _a.user) !== null && _b !== void 0 ? _b : { username: "" };
                const url = req.protocol + "://" + req.get("host") + req.originalUrl;
                const result = (await this.service.get(url, username, req.query));
                response_1.default.pagination(result.res, result.paging).create(res);
            }
            catch (error) {
                next(error);
            }
        };
        this.arsip_download = async (req, res, next) => {
            var _a, _b;
            try {
                const { username } = (_b = (_a = res.locals) === null || _a === void 0 ? void 0 : _a.user) !== null && _b !== void 0 ? _b : { username: "" };
                const result = await this.service.arsip_download(req.params.type, username, req.query);
                res.download(result, err => !err && (0, fs_1.unlinkSync)(result));
            }
            catch (error) {
                next(error);
            }
        };
        this.statistik_indikator = async (req, res, next) => {
            try {
                const result = (await this.service.statistik_indikator(req.query));
                response_1.default.success(result).create(res);
            }
            catch (error) {
                next(error);
            }
        };
        this.statistik_inovasi = async (req, res, next) => {
            try {
                const result = (await this.service.statistik_inovasi());
                response_1.default.success(result).create(res);
            }
            catch (error) {
                next(error);
            }
        };
        this.statistik_opd = async (req, res, next) => {
            try {
                const result = (await this.service.opd_menangani());
                response_1.default.success(result).create(res);
            }
            catch (error) {
                next(error);
            }
        };
    }
}
exports.DashboardController = DashboardController;
