"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const base_controller_1 = __importDefault(require("./base.controller"));
const response_1 = __importDefault(require("../providers/response"));
const api_error_1 = __importDefault(require("../providers/exceptions/api.error"));
const http_code_enum_1 = require("../types/http_code.enum");
const fs_1 = require("fs");
const path_1 = require("path");
const document_service_1 = __importDefault(require("../service/document.service"));
class DocumentController extends base_controller_1.default {
    constructor() {
        super(new document_service_1.default());
        this.create = async (req, res, next) => {
            const document = req.file;
            try {
                const result = await this.service.create(req.body.content, req.body.title, req.body.category_id, document, res.locals.user.username);
                response_1.default.success(result).create(res);
            }
            catch (error) {
                next(error);
            }
        };
        this.get = async (req, res, next) => {
            try {
                const host = req.protocol + '://' + req.get('host');
                const url = host + req.originalUrl;
                const result = (await this.service.get(url, host, req.query));
                response_1.default.pagination(result.res, result.paging).create(res);
            }
            catch (error) {
                next(error);
            }
        };
        this.detail = async (req, res, next) => {
            try {
                const host = req.protocol + '://' + req.get('host');
                const result = await this.service.detail(req.params.id, host);
                response_1.default.success(result).create(res);
            }
            catch (error) {
                next(error);
            }
        };
        this.update = async (req, res, next) => {
            const document = req.file;
            try {
                const result = await this.service.update(req.params.id, req.body.content, req.body.title, req.body.category_id, document, res.locals.user.username);
                if (!result) {
                    next(new api_error_1.default({
                        httpCode: http_code_enum_1.HttpCode.BAD_REQUEST,
                        message: 'Gagal ubah data'
                    }));
                }
                response_1.default.success('Berhasil ubah data').create(res);
            }
            catch (error) {
                (0, fs_1.unlink)((0, path_1.join)(__dirname, '..', '..', 'public', 'upload', 'dokumen', document.filename), err => {
                    if (err) {
                        console.log(`${document.filename} was deleted`);
                    }
                });
                next(error);
            }
        };
        this.delete = async (req, res, next) => {
            try {
                const { id } = req.params;
                const result = await this.service.delete(id);
                if (!result) {
                    next(new api_error_1.default({
                        httpCode: http_code_enum_1.HttpCode.BAD_REQUEST,
                        message: 'Gagal hapus data'
                    }));
                }
                response_1.default.success('Berhasil hapus data').create(res);
            }
            catch (error) {
                next(error);
            }
        };
    }
}
exports.default = DocumentController;
