import RawlingService from '../service/rawling.service';
import BaseController from './base.controller';
import { Request, Response, NextFunction } from 'express';
import { ParamsDictionary } from 'express-serve-static-core';
import { ParsedQs } from 'qs';
import { RawlingParams } from '../schema/rawling.schema';
export declare class RawlingController extends BaseController<RawlingService> {
    constructor();
    rawling: (req: Request<ParamsDictionary, any, any, ParsedQs & RawlingParams, Record<string, any>>, res: Response<any, Record<string, any>>, next: NextFunction) => Promise<void>;
}
