import { NextFunction, Request, Response } from 'express';
import { ParamsDictionary } from 'express-serve-static-core';
import { ParsedQs } from 'qs';
import RegionalApparatus from '../entity/RegionApparatus.entity';
import { CreateRegionApparatusRequest, GetRegionApparatusRequest } from '../schema/region_apparatus.scheme';
import RegionalApparatusService from '../service/regional_apparatus.service';
import { SuccessResponse } from '../types/response.type';
import BaseController from './base.controller';
import { IBaseController } from './interface/ibase.controller';
declare class RegionalApparatusController extends BaseController<RegionalApparatusService> implements IBaseController {
    constructor();
    create: (req: Request<ParamsDictionary, any, CreateRegionApparatusRequest, ParsedQs, Record<string, any>>, res: Response<SuccessResponse<RegionalApparatus>, Record<string, any>>, next: NextFunction) => Promise<void>;
    get: (req: Request<ParamsDictionary, any, any, ParsedQs & GetRegionApparatusRequest, Record<string, any>>, res: Response<any, Record<string, any>>, next: NextFunction) => Promise<void>;
    detail: (req: Request<ParamsDictionary & {
        id: number;
    }, any, any, ParsedQs, Record<string, any>>, res: Response<any, Record<string, any>>, next: NextFunction) => Promise<void>;
    update: (req: Request<ParamsDictionary & {
        id: number;
    }, any, CreateRegionApparatusRequest, ParsedQs, Record<string, any>>, res: Response<any, Record<string, any>>, next: NextFunction) => Promise<void>;
    delete: (req: Request<ParamsDictionary & {
        id: number;
    }, any, any, ParsedQs, Record<string, any>>, res: Response<any, Record<string, any>>, next: NextFunction) => Promise<void>;
}
export default RegionalApparatusController;
