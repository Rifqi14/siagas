import { NextFunction, Request, Response } from 'express';
import { ParamsDictionary } from 'express-serve-static-core';
import { ParsedQs } from 'qs';
import InovasiIndikatorFileService from '../service/inovasi_indikator_file.service';
import BaseController from './base.controller';
import { IBaseController } from './interface/ibase.controller';
import { InovasiIndikatorFileParams, UploadInovasiIndikatorFileRequest } from '../schema/inovasi_indikator_file.schema';
import { InovasiIndikatorFile } from '../entity/InovasiIndikatorFile.entity';
import { PaginationResponse, SuccessResponse } from '../types/response.type';
import { PaginationResponse as PaginationResponseInterface } from '../interface/pagination.interface';
export declare class InovasiIndikatorFileController extends BaseController<InovasiIndikatorFileService> implements IBaseController {
    constructor();
    create: (req: Request<ParamsDictionary, any, UploadInovasiIndikatorFileRequest, ParsedQs, Record<string, any>>, res: Response<any, Record<string, any>>, next: NextFunction) => Promise<void>;
    upload: (req: Request<ParamsDictionary & {
        inovasi_id: string;
        indikator_id: string;
    }, any, UploadInovasiIndikatorFileRequest, ParsedQs, Record<string, any>>, res: Response<SuccessResponse<InovasiIndikatorFile>, Record<string, any>>, next: NextFunction) => Promise<void>;
    get: (req: Request<ParamsDictionary & {
        inovasi_id: string;
        indikator_id: string;
    }, any, any, ParsedQs & InovasiIndikatorFileParams, Record<string, any>>, res: Response<PaginationResponse<PaginationResponseInterface, Document>, Record<string, any>>, next: NextFunction) => Promise<void>;
    detail: (req: Request<ParamsDictionary & {
        inovasi_id: number;
        indikator_id: number;
        file_id: number;
    }, any, any, ParsedQs, Record<string, any>>, res: Response<any, Record<string, any>>, next: NextFunction) => Promise<void>;
    update: (req: Request<ParamsDictionary & {
        id: number;
    }, any, any, ParsedQs, Record<string, any>>, res: Response<SuccessResponse<string>, Record<string, any>>, next: NextFunction) => Promise<void>;
    delete: (req: Request<ParamsDictionary & {
        inovasi_id: number;
        indikator_id: number;
        file_id: number;
    }, any, any, ParsedQs, Record<string, any>>, res: Response<SuccessResponse<string>, Record<string, any>>, next: NextFunction) => Promise<void>;
}
