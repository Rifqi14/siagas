import { NextFunction, Request, Response } from 'express';
import { ParamsDictionary } from 'express-serve-static-core';
import { ParsedQs } from 'qs';
import BaseController from './base.controller';
import PreviewReviewInovasiDaerahService from '../service/preview_inovasi_daerah.service';
import { IBaseController } from './interface/ibase.controller';
import { CreatePreviewInovasiDaerahRequest, GetPreviewInovasiRequest } from '../schema/preview_inovasi_daerah.schema';
import { SuccessResponse } from '../types/response.type';
import { PreviewReviewInovasiDaerah } from '../entity/PreviewReviewInovasiDaerah.entity';
export declare class PreviewReviewInovasiDaerahController extends BaseController<PreviewReviewInovasiDaerahService> implements IBaseController {
    constructor();
    create: (req: Request<ParamsDictionary & {
        review_id: number;
    }, any, CreatePreviewInovasiDaerahRequest, ParsedQs, Record<string, any>>, res: Response<SuccessResponse<PreviewReviewInovasiDaerah>, Record<string, any>>, next: NextFunction) => Promise<void>;
    get: (req: Request<ParamsDictionary & {
        review_id: number;
    }, any, any, ParsedQs & GetPreviewInovasiRequest, Record<string, any>>, res: Response<any, Record<string, any>>, next: NextFunction) => Promise<void>;
    detail: (req: Request<ParamsDictionary & {
        id: number;
        review_id: number;
    }, any, any, ParsedQs, Record<string, any>>, res: Response<any, Record<string, any>>, next: NextFunction) => Promise<void>;
    update: (req: Request<ParamsDictionary & {
        id: number;
        review_id: number;
    }, any, CreatePreviewInovasiDaerahRequest, ParsedQs, Record<string, any>>, res: Response<any, Record<string, any>>, next: NextFunction) => Promise<void>;
    delete: (req: Request<ParamsDictionary & {
        id: number;
        review_id: number;
    }, any, any, ParsedQs, Record<string, any>>, res: Response<any, Record<string, any>>, next: NextFunction) => Promise<void>;
}
