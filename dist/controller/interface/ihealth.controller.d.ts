import { NextFunction, Request, Response } from 'express';
export interface IHealthController {
    health(req: Request, res: Response, next: NextFunction): Promise<void>;
}
