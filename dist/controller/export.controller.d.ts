import { NextFunction, Request, Response } from 'express';
import { ParamsDictionary } from 'express-serve-static-core';
import { ParsedQs } from 'qs';
import { CreateExportRequest } from '../schema/export.schema';
declare class ExportController {
    export: (req: Request<ParamsDictionary, any, CreateExportRequest, ParsedQs, Record<string, any>>, res: Response<any, Record<string, any>>, next: NextFunction) => Promise<void>;
}
export default ExportController;
