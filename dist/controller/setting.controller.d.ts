import { NextFunction, Request, Response } from 'express';
import { ParamsDictionary } from 'express-serve-static-core';
import { ParsedQs } from 'qs';
import Setting from '../entity/Setting.entity';
import { GetClusterRequest } from '../schema/cluster.schema';
import { CreateSettingRequest } from '../schema/setting.schema';
import SettingService from '../service/setting.service';
import { PaginationResponse, SuccessResponse } from '../types/response.type';
import BaseController from './base.controller';
import { IBaseController } from './interface/ibase.controller';
import { PaginationResponse as PaginationResponseInterface } from '../interface/pagination.interface';
declare class SettingController extends BaseController<SettingService> implements IBaseController {
    constructor();
    create: (req: Request<ParamsDictionary, any, CreateSettingRequest, ParsedQs, Record<string, any>>, res: Response<SuccessResponse<Setting>, Record<string, any>>, next: NextFunction) => Promise<void>;
    get: (req: Request<ParamsDictionary, any, any, ParsedQs & GetClusterRequest, Record<string, any>>, res: Response<PaginationResponse<PaginationResponseInterface, Setting>, Record<string, any>>, next: NextFunction) => Promise<void>;
    detail: (req: Request<ParamsDictionary & {
        id: number;
    }, any, any, ParsedQs, Record<string, any>>, res: Response<any, Record<string, any>>, next: NextFunction) => Promise<void>;
    update: (req: Request<ParamsDictionary & {
        id: number;
    }, any, CreateSettingRequest, ParsedQs, Record<string, any>>, res: Response<SuccessResponse<string>, Record<string, any>>, next: NextFunction) => Promise<void>;
    delete: (req: Request<ParamsDictionary & {
        id: number;
    }, any, any, ParsedQs, Record<string, any>>, res: Response<SuccessResponse<string>, Record<string, any>>, next: NextFunction) => Promise<void>;
}
export default SettingController;
