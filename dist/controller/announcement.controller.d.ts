import { NextFunction, Request, Response } from 'express';
import { ParamsDictionary } from 'express-serve-static-core';
import { ParsedQs } from 'qs';
import AnnouncementService from '../service/announcement.service';
import BaseController from './base.controller';
import { IBaseController } from './interface/ibase.controller';
import { PaginationResponse as PaginationResponseInterface } from '../interface/pagination.interface';
import { PaginationResponse, SuccessResponse } from '../types/response.type';
import { CreateAnnouncementRequest, GetAnnouncementRequest } from '../schema/announcement.schema';
import Announcement from '../entity/Announcement.entity';
declare class AnnouncementController extends BaseController<AnnouncementService> implements IBaseController {
    constructor();
    create: (req: Request<ParamsDictionary, any, CreateAnnouncementRequest, ParsedQs, Record<string, any>>, res: Response<SuccessResponse<Announcement>, Record<string, any>>, next: NextFunction) => Promise<void>;
    get: (req: Request<ParamsDictionary, any, any, ParsedQs & GetAnnouncementRequest, Record<string, any>>, res: Response<PaginationResponse<PaginationResponseInterface, Announcement>, Record<string, any>>, next: NextFunction) => Promise<void>;
    detail: (req: Request<ParamsDictionary & {
        id: number;
    }, any, any, ParsedQs, Record<string, any>>, res: Response<any, Record<string, any>>, next: NextFunction) => Promise<void>;
    update: (req: Request<ParamsDictionary & {
        id: number;
    }, any, CreateAnnouncementRequest, ParsedQs, Record<string, any>>, res: Response<SuccessResponse<string>, Record<string, any>>, next: NextFunction) => Promise<void>;
    delete: (req: Request<ParamsDictionary & {
        id: number;
    }, any, any, ParsedQs, Record<string, any>>, res: Response<SuccessResponse<string>, Record<string, any>>, next: NextFunction) => Promise<void>;
}
export default AnnouncementController;
