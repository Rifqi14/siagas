import { NextFunction, Request, Response } from 'express';
import { ParamsDictionary } from 'express-serve-static-core';
import { ParsedQs } from 'qs';
import TuxedoService from '../service/tuxedo.service';
import { PaginationResponse, SuccessResponse } from '../types/response.type';
import BaseController from './base.controller';
import { IBaseController } from './interface/ibase.controller';
import { PaginationResponse as PaginationResponseInterface } from '../interface/pagination.interface';
import Tuxedo from '../entity/Tuxedo.entity';
import { CreateTuxedoRequest, GetTuxedoRequest } from '../schema/tuxedo.schema';
declare class TuxedoController extends BaseController<TuxedoService> implements IBaseController {
    constructor();
    create: (req: Request<ParamsDictionary, any, CreateTuxedoRequest, ParsedQs, Record<string, any>>, res: Response<SuccessResponse<Tuxedo>, Record<string, any>>, next: NextFunction) => Promise<void>;
    get: (req: Request<ParamsDictionary, any, any, ParsedQs & GetTuxedoRequest, Record<string, any>>, res: Response<PaginationResponse<PaginationResponseInterface, Tuxedo>, Record<string, any>>, next: NextFunction) => Promise<void>;
    detail: (req: Request<ParamsDictionary & {
        id: number;
    }, any, any, ParsedQs, Record<string, any>>, res: Response<any, Record<string, any>>, next: NextFunction) => Promise<void>;
    update: (req: Request<ParamsDictionary & {
        id: number;
    }, any, CreateTuxedoRequest, ParsedQs, Record<string, any>>, res: Response<SuccessResponse<string>, Record<string, any>>, next: NextFunction) => Promise<void>;
    delete: (req: Request<ParamsDictionary & {
        id: number;
    }, any, any, ParsedQs, Record<string, any>>, res: Response<SuccessResponse<string>, Record<string, any>>, next: NextFunction) => Promise<void>;
}
export default TuxedoController;
