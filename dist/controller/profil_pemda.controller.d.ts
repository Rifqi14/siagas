import { NextFunction, Request, Response } from 'express';
import { ParamsDictionary } from 'express-serve-static-core';
import { ParsedQs } from 'qs';
import ProfilPemdaService from '../service/profil_pemda.service';
import BaseController from './base.controller';
import { IBaseController } from './interface/ibase.controller';
import { CreateProfilPemdaRequest, ProfilPemdaFilterDownload, ProfilPemdaParams } from '../schema/profil_pemda.schema';
import { SuccessResponse } from '../types/response.type';
import { ProfilPemda } from '../entity/ProfilePemda.entity';
import { DownloadType } from '../types/lib';
declare class ProfilPemdaController extends BaseController<ProfilPemdaService> implements IBaseController {
    constructor();
    create: (req: Request<ParamsDictionary, any, CreateProfilPemdaRequest, ParsedQs, Record<string, any>>, res: Response<SuccessResponse<ProfilPemda>, Record<string, any>>, next: NextFunction) => Promise<void>;
    getDownload: (req: Request<ParamsDictionary & {
        type: DownloadType;
    }, any, any, ParsedQs & ProfilPemdaFilterDownload, Record<string, any>>, res: Response<any, Record<string, any>>, next: NextFunction) => Promise<void>;
    downloadDetail: (req: Request<ParamsDictionary & {
        type: DownloadType;
        id: number;
    }, any, any, ParsedQs, Record<string, any>>, res: Response<any, Record<string, any>>, next: NextFunction) => Promise<void>;
    get: (req: Request<ParamsDictionary, any, any, ParsedQs & ProfilPemdaParams, Record<string, any>>, res: Response<any, Record<string, any>>, next: NextFunction) => Promise<void>;
    detail: (req: Request<ParamsDictionary & {
        id: number;
    }, any, any, ParsedQs, Record<string, any>>, res: Response<any, Record<string, any>>, next: NextFunction) => Promise<void>;
    update: (req: Request<ParamsDictionary & {
        id: number;
    }, any, CreateProfilPemdaRequest, ParsedQs, Record<string, any>>, res: Response<any, Record<string, any>>, next: NextFunction) => Promise<void>;
    delete: (req: Request<ParamsDictionary & {
        id: number;
    }, any, any, ParsedQs, Record<string, any>>, res: Response<any, Record<string, any>>, next: NextFunction) => Promise<void>;
}
export default ProfilPemdaController;
