"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.RangkingIndexController = void 0;
const rangking_index_service_1 = __importDefault(require("../service/rangking_index.service"));
const base_controller_1 = __importDefault(require("./base.controller"));
const response_1 = __importDefault(require("../providers/response"));
const api_error_1 = __importDefault(require("../providers/exceptions/api.error"));
const http_code_enum_1 = require("../types/http_code.enum");
const fs_1 = require("fs");
class RangkingIndexController extends base_controller_1.default {
    constructor() {
        super(new rangking_index_service_1.default());
        this.get = async (req, res, next) => {
            try {
                const { username } = res.locals.user;
                const url = req.protocol + "://" + req.get("host") + req.originalUrl;
                const result = (await this.service.get(url, username, req.query));
                response_1.default.pagination(result.res, result.paging).create(res);
            }
            catch (error) {
                next(error);
            }
        };
        this.update = async (req, res, next) => {
            try {
                const { id } = req.params;
                const result = await this.service.nominator(id, req.body);
                if (!result) {
                    throw new api_error_1.default({
                        httpCode: http_code_enum_1.HttpCode.BAD_REQUEST,
                        message: "Gagal ubah data",
                    });
                }
                response_1.default.success("Berhasil ubah data").create(res);
            }
            catch (error) {
                next(error);
            }
        };
        this.getDownload = async (req, res, next) => {
            try {
                const { username } = res.locals.user;
                const result = await this.service.download(req.params.type, username, req.query);
                res.download(result, err => !err && (0, fs_1.unlinkSync)(result));
            }
            catch (error) {
                next(error);
            }
        };
    }
}
exports.RangkingIndexController = RangkingIndexController;
