import { NextFunction, Request, Response } from 'express';
import { ParamsDictionary } from 'express-serve-static-core';
import { ParsedQs } from 'qs';
import { ReviewInovasiDaerah } from '../entity/ReviewInovasiDaerah.entity';
import { CreateReviewInovasiDaerahRequest, GetReviewInovasiRequest, UpdateEvaluasiRequest } from '../schema/review_inovasi_daerah.schema';
import ReviewInovasiDaerahService from '../service/review_inovasi_daerah.service';
import { SuccessResponse } from '../types/response.type';
import BaseController from './base.controller';
import { IBaseController } from './interface/ibase.controller';
import { DownloadType } from '../types/lib';
export declare class ReviewInovasiDaerahController extends BaseController<ReviewInovasiDaerahService> implements IBaseController {
    constructor();
    create: (req: Request<ParamsDictionary, any, CreateReviewInovasiDaerahRequest, ParsedQs, Record<string, any>>, res: Response<SuccessResponse<ReviewInovasiDaerah>, Record<string, any>>, next: NextFunction) => Promise<void>;
    get: (req: Request<ParamsDictionary, any, any, ParsedQs & GetReviewInovasiRequest, Record<string, any>>, res: Response<any, Record<string, any>>, next: NextFunction) => Promise<void>;
    getDownload: (req: Request<ParamsDictionary & {
        type: DownloadType;
    }, any, any, ParsedQs & GetReviewInovasiRequest, Record<string, any>>, res: Response<any, Record<string, any>>, next: NextFunction) => Promise<void>;
    detail: (req: Request<ParamsDictionary & {
        id: number;
    }, any, any, ParsedQs, Record<string, any>>, res: Response<any, Record<string, any>>, next: NextFunction) => Promise<void>;
    profilInovasi: (req: Request<ParamsDictionary & {
        id: number;
    }, any, any, ParsedQs, Record<string, any>>, res: Response<any, Record<string, any>>, next: NextFunction) => Promise<void>;
    indikator: (req: Request<ParamsDictionary & {
        id: number;
    }, any, any, ParsedQs, Record<string, any>>, res: Response<any, Record<string, any>>, next: NextFunction) => Promise<void>;
    indikatorDetail: (req: Request<ParamsDictionary & {
        id: number;
        indikator_id: string;
    }, any, any, ParsedQs, Record<string, any>>, res: Response<any, Record<string, any>>, next: NextFunction) => Promise<void>;
    evaluasi: (req: Request<ParamsDictionary & {
        id: number;
        indikator_id: string;
    }, any, UpdateEvaluasiRequest, ParsedQs, Record<string, any>>, res: Response<any, Record<string, any>>, next: NextFunction) => Promise<void>;
    getEvaluasi: (req: Request<ParamsDictionary & {
        id: number;
        indikator_id: number;
    }, any, UpdateEvaluasiRequest, ParsedQs, Record<string, any>>, res: Response<any, Record<string, any>>, next: NextFunction) => Promise<void>;
    update: (req: Request<ParamsDictionary & {
        id: number;
    }, any, CreateReviewInovasiDaerahRequest, ParsedQs, Record<string, any>>, res: Response<any, Record<string, any>>, next: NextFunction) => Promise<void>;
    delete: (req: Request<ParamsDictionary & {
        id: number;
    }, any, any, ParsedQs, Record<string, any>>, res: Response<any, Record<string, any>>, next: NextFunction) => Promise<void>;
}
