import BaseController from './base.controller';
import { Request, Response, NextFunction } from 'express';
import { ParamsDictionary } from 'express-serve-static-core';
import { ParsedQs } from 'qs';
import { RekapIndeksAkhirParams, UpdateNominatorRequest } from '../schema/rekap_indeks_akhir.schema';
import RekapIndeksAkhirService from '../service/rekap_indeks_akhir.service';
export declare class RekapIndeksAkhirController extends BaseController<RekapIndeksAkhirService> {
    constructor();
    get: (req: Request<ParamsDictionary, any, any, ParsedQs & RekapIndeksAkhirParams, Record<string, any>>, res: Response<any, Record<string, any>>, next: NextFunction) => Promise<void>;
    update: (req: Request<ParamsDictionary & {
        id: number;
    }, UpdateNominatorRequest, any, ParsedQs, Record<string, any>>, res: Response<any, Record<string, any>>, next: NextFunction) => Promise<void>;
}
