import { NextFunction, Request, Response } from 'express';
import { ParamsDictionary } from 'express-serve-static-core';
import { ParsedQs } from 'qs';
import BaseController from './base.controller';
import { IBaseController } from './interface/ibase.controller';
import { PaginationResponse as PaginationResponseInterface } from '../interface/pagination.interface';
import { PaginationResponse, SuccessResponse } from '../types/response.type';
import DocumentService from '../service/document.service';
import { CreateDocumentRequest, GetDocumentRequest } from '../schema/document.schema';
import Document from '../entity/Document.entity';
declare class DocumentController extends BaseController<DocumentService> implements IBaseController {
    constructor();
    create: (req: Request<ParamsDictionary, any, CreateDocumentRequest, ParsedQs, Record<string, any>>, res: Response<SuccessResponse<Document>, Record<string, any>>, next: NextFunction) => Promise<void>;
    get: (req: Request<ParamsDictionary, any, any, ParsedQs & GetDocumentRequest, Record<string, any>>, res: Response<PaginationResponse<PaginationResponseInterface, Document>, Record<string, any>>, next: NextFunction) => Promise<void>;
    detail: (req: Request<ParamsDictionary & {
        id: number;
    }, any, any, ParsedQs, Record<string, any>>, res: Response<any, Record<string, any>>, next: NextFunction) => Promise<void>;
    update: (req: Request<ParamsDictionary & {
        id: number;
    }, any, CreateDocumentRequest, ParsedQs, Record<string, any>>, res: Response<SuccessResponse<string>, Record<string, any>>, next: NextFunction) => Promise<void>;
    delete: (req: Request<ParamsDictionary & {
        id: number;
    }, any, any, ParsedQs, Record<string, any>>, res: Response<SuccessResponse<string>, Record<string, any>>, next: NextFunction) => Promise<void>;
}
export default DocumentController;
