"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
class BaseController {
    constructor(service) {
        this.service = service;
    }
}
exports.default = BaseController;
