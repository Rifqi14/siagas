import BaseController from "./base.controller";
import { Request, Response, NextFunction } from "express";
import { ParamsDictionary } from "express-serve-static-core";
import { ParsedQs } from "qs";
import { DashboardArsipParams, StatistikIndikatorInovasiParams } from "../schema/dashboard.schema";
import DashboardService from "../service/dashboard.service";
import { DownloadType } from "../types/lib";
export declare class DashboardController extends BaseController<DashboardService> {
    constructor();
    arsip: (req: Request<ParamsDictionary, any, any, ParsedQs & DashboardArsipParams, Record<string, any>>, res: Response<any, Record<string, any>>, next: NextFunction) => Promise<void>;
    arsip_download: (req: Request<ParamsDictionary & {
        type: DownloadType;
    }, any, any, ParsedQs & DashboardArsipParams, Record<string, any>>, res: Response<any, Record<string, any>>, next: NextFunction) => Promise<void>;
    statistik_indikator: (req: Request<ParamsDictionary, any, any, ParsedQs & StatistikIndikatorInovasiParams, Record<string, any>>, res: Response<any, Record<string, any>>, next: NextFunction) => Promise<void>;
    statistik_inovasi: (req: Request<ParamsDictionary, any, any, ParsedQs, Record<string, any>>, res: Response<any, Record<string, any>>, next: NextFunction) => Promise<void>;
    statistik_opd: (req: Request<ParamsDictionary, any, any, ParsedQs, Record<string, any>>, res: Response<any, Record<string, any>>, next: NextFunction) => Promise<void>;
}
