import { NextFunction, Request, Response } from 'express';
import { ParamsDictionary } from 'express-serve-static-core';
import { ParsedQs } from 'qs';
import { PeringkatHasilReviewNominatorRequest, PeringkatHasilReviewParams } from '../schema/innovative_government_award.schema';
import InnovativeGovernmentAwardService from '../service/innovative_government_award.service';
import { DownloadType } from '../types/lib';
import BaseController from './base.controller';
export declare class InnovativeGovernmentAwardController extends BaseController<InnovativeGovernmentAwardService> {
    constructor();
    peringkat_hasil_review: (req: Request<ParamsDictionary, any, any, ParsedQs & PeringkatHasilReviewParams, Record<string, any>>, res: Response<any, Record<string, any>>, next: NextFunction) => Promise<void>;
    peringkat_hasil_review_download: (req: Request<ParamsDictionary & {
        type: DownloadType;
    }, any, any, ParsedQs & PeringkatHasilReviewParams, Record<string, any>>, res: Response<any, Record<string, any>>, next: NextFunction) => Promise<void>;
    prestasi_download: (req: Request<ParamsDictionary & {
        type: DownloadType;
    }, any, any, ParsedQs & PeringkatHasilReviewParams, Record<string, any>>, res: Response<any, Record<string, any>>, next: NextFunction) => Promise<void>;
    rangking_download: (req: Request<ParamsDictionary & {
        type: DownloadType;
    }, any, any, ParsedQs & PeringkatHasilReviewParams, Record<string, any>>, res: Response<any, Record<string, any>>, next: NextFunction) => Promise<void>;
    nominator_peringkat_hasil_review: (req: Request<ParamsDictionary & {
        pemda_id: number;
    }, PeringkatHasilReviewNominatorRequest, any, ParsedQs, Record<string, any>>, res: Response<any, Record<string, any>>, next: NextFunction) => Promise<void>;
    prestasi: (req: Request<ParamsDictionary, any, any, ParsedQs & PeringkatHasilReviewParams, Record<string, any>>, res: Response<any, Record<string, any>>, next: NextFunction) => Promise<void>;
    rangking: (req: Request<ParamsDictionary, any, any, ParsedQs & PeringkatHasilReviewParams, Record<string, any>>, res: Response<any, Record<string, any>>, next: NextFunction) => Promise<void>;
}
