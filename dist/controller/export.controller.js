"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const fs_1 = require("fs");
const path_1 = require("path");
const export_service_1 = __importDefault(require("../service/export.service"));
class ExportController {
    constructor() {
        this.export = async (req, res, next) => {
            try {
                const stream = (0, fs_1.createWriteStream)((0, path_1.join)(__dirname, '..', '..', 'public', 'tmp', `${req.body.name}.${req.body.format}`));
                const service = new export_service_1.default();
                await service.export(req.body, stream);
                stream.on('open', () => {
                    console.log('Opened');
                    stream.pipe(res);
                });
                stream.on('close', () => {
                    (0, fs_1.unlinkSync)((0, path_1.join)(__dirname, '..', '..', 'public', 'tmp', `${req.body.name}.${req.body.format}`));
                    console.log('Closed');
                });
                res.download((0, path_1.join)(__dirname, '..', '..', 'public', 'tmp', `${req.body.name}.xlsx`));
            }
            catch (error) {
                next(error);
            }
        };
    }
}
exports.default = ExportController;
