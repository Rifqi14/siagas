"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.RawlingController = void 0;
const rawling_service_1 = __importDefault(require("../service/rawling.service"));
const base_controller_1 = __importDefault(require("./base.controller"));
const response_1 = __importDefault(require("../providers/response"));
class RawlingController extends base_controller_1.default {
    constructor() {
        super(new rawling_service_1.default());
        this.rawling = async (req, res, next) => {
            try {
                const result = await this.service.rawling(req.query);
                response_1.default.success(result).create(res);
            }
            catch (error) {
                next(error);
            }
        };
    }
}
exports.RawlingController = RawlingController;
