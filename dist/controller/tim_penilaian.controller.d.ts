import { NextFunction, Request, Response } from 'express';
import { ParamsDictionary } from 'express-serve-static-core';
import { ParsedQs } from 'qs';
import TimPenilaianService from '../service/tim_penilaian.service';
import BaseController from './base.controller';
import { IBaseController } from './interface/ibase.controller';
import { SuccessResponse } from '../types/response.type';
import { TimPenilaian } from '../entity/TimPenilaian.entity';
import { CreateTimPenilaianRequest, ListTimPenilaianParams } from '../schema/tim_penilaian.schema';
declare class TimPenilaianController extends BaseController<TimPenilaianService> implements IBaseController {
    constructor();
    create: (req: Request<ParamsDictionary, any, CreateTimPenilaianRequest, ParsedQs, Record<string, any>>, res: Response<SuccessResponse<TimPenilaian>, Record<string, any>>, next: NextFunction) => Promise<void>;
    get: (req: Request<ParamsDictionary, any, any, ParsedQs & ListTimPenilaianParams, Record<string, any>>, res: Response<any, Record<string, any>>, next: NextFunction) => Promise<void>;
    detail: (req: Request<ParamsDictionary & {
        id: number;
    }, any, any, ParsedQs, Record<string, any>>, res: Response<any, Record<string, any>>, next: NextFunction) => Promise<void>;
    update: (req: Request<ParamsDictionary & {
        id: number;
    }, any, CreateTimPenilaianRequest, ParsedQs, Record<string, any>>, res: Response<any, Record<string, any>>, next: NextFunction) => Promise<void>;
    delete: (req: Request<ParamsDictionary & {
        id: number;
    }, any, any, ParsedQs, Record<string, any>>, res: Response<any, Record<string, any>>, next: NextFunction) => Promise<void>;
}
export default TimPenilaianController;
