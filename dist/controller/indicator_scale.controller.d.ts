import { Request, Response, NextFunction } from 'express';
import { ParamsDictionary } from 'express-serve-static-core';
import { ParsedQs } from 'qs';
import { CreateIndicatorScaleRequest, GetIndicatorScaleRequest } from '../schema/indicator_scale.scheme';
import IndicatorScaleService from '../service/indicator_scale.service';
import BaseController from './base.controller';
import { IBaseController } from './interface/ibase.controller';
declare class IndicatorScaleController extends BaseController<IndicatorScaleService> implements IBaseController {
    constructor();
    create: (req: Request<ParamsDictionary & {
        indikator_id: number;
    }, any, any, ParsedQs, Record<string, any>>, res: Response<any, Record<string, any>>, next: NextFunction) => Promise<void>;
    get: (req: Request<ParamsDictionary & {
        indikator_id: number;
    }, any, any, ParsedQs & GetIndicatorScaleRequest, Record<string, any>>, res: Response<any, Record<string, any>>, next: NextFunction) => Promise<void>;
    detail: (req: Request<ParamsDictionary & {
        id: number;
    }, any, any, ParsedQs, Record<string, any>>, res: Response<any, Record<string, any>>, next: NextFunction) => Promise<void>;
    update: (req: Request<ParamsDictionary & {
        id: number;
    }, any, CreateIndicatorScaleRequest, ParsedQs, Record<string, any>>, res: Response<any, Record<string, any>>, next: NextFunction) => Promise<void>;
    delete: (req: Request<ParamsDictionary & {
        id: number;
    }, any, any, ParsedQs, Record<string, any>>, res: Response<any, Record<string, any>>, next: NextFunction) => Promise<void>;
}
export default IndicatorScaleController;
