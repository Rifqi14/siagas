import { NextFunction, Request, Response } from 'express';
import { ParamsDictionary } from 'express-serve-static-core';
import { ParsedQs } from 'qs';
import { CreateGovernmentInnovationRequest, GetGovernmentInnovationDownloadRequest, GetGovernmentInnovationRequest } from '../schema/government_innovation.schema';
import GovernmentInnovationService from '../service/government_innovation.service';
import BaseController from './base.controller';
import { IBaseController } from './interface/ibase.controller';
import { DownloadType } from '../types/lib';
declare class GovernmentInnovationController extends BaseController<GovernmentInnovationService> implements IBaseController {
    constructor();
    create: (req: Request<ParamsDictionary, any, CreateGovernmentInnovationRequest, ParsedQs, Record<string, any>>, res: Response<any, Record<string, any>>, next: NextFunction) => Promise<void>;
    download: (req: Request<ParamsDictionary & {
        type: DownloadType;
    }, any, any, ParsedQs & GetGovernmentInnovationDownloadRequest, Record<string, any>>, res: Response<any, Record<string, any>>, next: NextFunction) => Promise<void>;
    downloadDetail: (req: Request<ParamsDictionary & {
        type: DownloadType;
        id: number;
    }, any, any, ParsedQs & GetGovernmentInnovationDownloadRequest, Record<string, any>>, res: Response<any, Record<string, any>>, next: NextFunction) => Promise<void>;
    get: (req: Request<ParamsDictionary, any, any, ParsedQs & GetGovernmentInnovationRequest, Record<string, any>>, res: Response<any, Record<string, any>>, next: NextFunction) => Promise<void>;
    detail: (req: Request<ParamsDictionary & {
        id: number;
    }, any, any, ParsedQs, Record<string, any>>, res: Response<any, Record<string, any>>, next: NextFunction) => Promise<void>;
    update: (req: Request<ParamsDictionary & {
        id: number;
    }, any, CreateGovernmentInnovationRequest, ParsedQs, Record<string, any>>, res: Response<any, Record<string, any>>, next: NextFunction) => Promise<void>;
    delete: (req: Request<ParamsDictionary & {
        id: number;
    }, any, any, ParsedQs, Record<string, any>>, res: Response<any, Record<string, any>>, next: NextFunction) => Promise<void>;
}
export default GovernmentInnovationController;
