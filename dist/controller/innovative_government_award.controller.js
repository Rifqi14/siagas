"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.InnovativeGovernmentAwardController = void 0;
const fs_1 = require("fs");
const api_error_1 = __importDefault(require("../providers/exceptions/api.error"));
const response_1 = __importDefault(require("../providers/response"));
const innovative_government_award_service_1 = __importDefault(require("../service/innovative_government_award.service"));
const http_code_enum_1 = require("../types/http_code.enum");
const base_controller_1 = __importDefault(require("./base.controller"));
class InnovativeGovernmentAwardController extends base_controller_1.default {
    constructor() {
        super(new innovative_government_award_service_1.default());
        this.peringkat_hasil_review = async (req, res, next) => {
            try {
                const { username } = res.locals.user;
                const url = req.protocol + '://' + req.get('host') + req.originalUrl;
                const result = (await this.service.peringkat_hasil_review(url, username, req.query));
                response_1.default.pagination(result.res, result.paging).create(res);
            }
            catch (error) {
                next(error);
            }
        };
        this.peringkat_hasil_review_download = async (req, res, next) => {
            try {
                const { username } = res.locals.user;
                const result = await this.service.peringkat_hasil_review_download(req.params.type, username, req.query);
                res.download(result, err => {
                    if (!err)
                        (0, fs_1.unlinkSync)(result);
                });
            }
            catch (error) {
                next(error);
            }
        };
        this.prestasi_download = async (req, res, next) => {
            try {
                const { username } = res.locals.user;
                const result = await this.service.prestasi_download(req.params.type, username, req.query);
                res.download(result, err => {
                    if (!err)
                        (0, fs_1.unlinkSync)(result);
                });
            }
            catch (error) {
                next(error);
            }
        };
        this.rangking_download = async (req, res, next) => {
            try {
                const { username } = res.locals.user;
                const result = await this.service.rangking_download(req.params.type, username, req.query);
                res.download(result, err => {
                    if (!err)
                        (0, fs_1.unlinkSync)(result);
                });
            }
            catch (error) {
                next(error);
            }
        };
        this.nominator_peringkat_hasil_review = async (req, res, next) => {
            try {
                const { pemda_id } = req.params;
                const result = await this.service.nominator_peringkat_hasil_review(pemda_id, req.body);
                if (!result) {
                    throw new api_error_1.default({
                        httpCode: http_code_enum_1.HttpCode.BAD_REQUEST,
                        message: 'Gagal ubah data'
                    });
                }
                response_1.default.success('Berhasil ubah data').create(res);
            }
            catch (error) {
                next(error);
            }
        };
        this.prestasi = async (req, res, next) => {
            try {
                const { username } = res.locals.user;
                const url = req.protocol + '://' + req.get('host') + req.originalUrl;
                const result = (await this.service.prestasi(url, username, req.query));
                response_1.default.pagination(result.res, result.paging).create(res);
            }
            catch (error) {
                next(error);
            }
        };
        this.rangking = async (req, res, next) => {
            try {
                const { username } = res.locals.user;
                const url = req.protocol + '://' + req.get('host') + req.originalUrl;
                const result = (await this.service.rangking(url, username, req.query));
                response_1.default.pagination(result.res, result.paging).create(res);
            }
            catch (error) {
                next(error);
            }
        };
    }
}
exports.InnovativeGovernmentAwardController = InnovativeGovernmentAwardController;
