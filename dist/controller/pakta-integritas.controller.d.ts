import { NextFunction, Request, Response } from 'express';
import { ParamsDictionary } from 'express-serve-static-core';
import { ParsedQs } from 'qs';
import PaktaIntegritasService from '../service/pakta-integritas.service';
import BaseController from './base.controller';
import { IBaseController } from './interface/ibase.controller';
import { CreatePaktaIntegritasRequest, PaktaIntegritasParams } from '../schema/pakta-integritas.schema';
import { PaktaIntegritas } from '../entity/PaktaIntegritas.entity';
import { SuccessResponse } from '../types/response.type';
declare class PaktaIntegritasController extends BaseController<PaktaIntegritasService> implements IBaseController {
    constructor();
    create: (req: Request<ParamsDictionary, any, CreatePaktaIntegritasRequest, ParsedQs, Record<string, any>>, res: Response<SuccessResponse<PaktaIntegritas>, Record<string, any>>, next: NextFunction) => Promise<void>;
    get: (req: Request<ParamsDictionary, any, any, ParsedQs & PaktaIntegritasParams, Record<string, any>>, res: Response<any, Record<string, any>>, next: NextFunction) => Promise<void>;
    getLatest: (req: Request<ParamsDictionary, any, any, ParsedQs, Record<string, any>>, res: Response<any, Record<string, any>>, next: NextFunction) => Promise<void>;
    detail: (req: Request<ParamsDictionary & {
        id: number;
    }, any, any, ParsedQs, Record<string, any>>, res: Response<any, Record<string, any>>, next: NextFunction) => Promise<void>;
    update: (req: Request<ParamsDictionary & {
        id: number;
    }, any, CreatePaktaIntegritasRequest, ParsedQs, Record<string, any>>, res: Response<any, Record<string, any>>, next: NextFunction) => Promise<void>;
    delete: (req: Request<ParamsDictionary & {
        id: number;
    }, any, any, ParsedQs, Record<string, any>>, res: Response<any, Record<string, any>>, next: NextFunction) => Promise<void>;
}
export default PaktaIntegritasController;
