"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const file_service_1 = __importDefault(require("../service/file.service"));
const base_controller_1 = __importDefault(require("./base.controller"));
const api_error_1 = __importDefault(require("../providers/exceptions/api.error"));
const http_code_enum_1 = require("../types/http_code.enum");
const response_1 = __importDefault(require("../providers/response"));
class FileController extends base_controller_1.default {
    constructor() {
        super(new file_service_1.default());
        this.upload = async (req, res, next) => {
            try {
                const body = req.body;
                const file = req.file;
                if (!file) {
                    next(new api_error_1.default({
                        httpCode: http_code_enum_1.HttpCode.BAD_REQUEST,
                        message: 'File invalid'
                    }));
                    return;
                }
                body.file = file;
                const result = await this.service.create(file, req.body.name, res.locals.user.username);
                response_1.default.success(result).create(res);
            }
            catch (error) {
                next(error);
            }
        };
    }
}
exports.default = FileController;
