import RangkingIndexService from "../service/rangking_index.service";
import BaseController from "./base.controller";
import { Request, Response, NextFunction } from "express";
import { ParamsDictionary } from "express-serve-static-core";
import { ParsedQs } from "qs";
import { RangkingIndexParams, RangkingIndexParamsDownload, UpdateNominatorRequest } from "../schema/rangking_index.schema";
import { DownloadType } from "../types/lib";
export declare class RangkingIndexController extends BaseController<RangkingIndexService> {
    constructor();
    get: (req: Request<ParamsDictionary, any, any, ParsedQs & RangkingIndexParams, Record<string, any>>, res: Response<any, Record<string, any>>, next: NextFunction) => Promise<void>;
    update: (req: Request<ParamsDictionary & {
        id: number;
    }, UpdateNominatorRequest, any, ParsedQs, Record<string, any>>, res: Response<any, Record<string, any>>, next: NextFunction) => Promise<void>;
    getDownload: (req: Request<ParamsDictionary & {
        type: DownloadType;
    }, any, any, ParsedQs & RangkingIndexParamsDownload, Record<string, any>>, res: Response<any, Record<string, any>>, next: NextFunction) => Promise<void>;
}
