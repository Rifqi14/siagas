import { NextFunction, Request, Response } from 'express';
import { ParamsDictionary } from 'express-serve-static-core';
import { UploadFileRequest } from '../schema/file.schema';
import { ParsedQs } from 'qs';
import FileService from '../service/file.service';
import BaseController from './base.controller';
import { SuccessResponse } from '../types/response.type';
import File from '../entity/File.entity';
declare class FileController extends BaseController<FileService> {
    constructor();
    upload: (req: Request<ParamsDictionary, any, UploadFileRequest, ParsedQs, Record<string, any>>, res: Response<SuccessResponse<File>, Record<string, any>>, next: NextFunction) => Promise<void>;
}
export default FileController;
