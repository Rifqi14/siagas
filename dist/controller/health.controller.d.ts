import { Request, Response, NextFunction } from 'express';
import { ParamsDictionary } from 'express-serve-static-core';
import { ParsedQs } from 'qs';
import HealthService from '../service/health.service';
import BaseController from './base.controller';
import { IHealthController } from './interface/ihealth.controller';
declare class HealthController extends BaseController<HealthService> implements IHealthController {
    constructor(service?: HealthService);
    health(req: Request<ParamsDictionary, any, any, ParsedQs, Record<string, any>>, res: Response<any, Record<string, any>>, next: NextFunction): Promise<void>;
    template(req: Request<ParamsDictionary, any, any, ParsedQs, Record<string, any>>, res: Response<any, Record<string, any>>, next: NextFunction): Promise<void>;
}
export default HealthController;
