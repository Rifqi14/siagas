import { Request, Response, NextFunction } from 'express';
import { ParamsDictionary } from 'express-serve-static-core';
import { ParsedQs } from 'qs';
import { SuccessResponse } from '../types/response.type';
import BaseController from './base.controller';
import { IBaseController } from './interface/ibase.controller';
import AreaService from '../service/area.service';
import Area from '../entity/Area.entity';
import { CreateAreaRequest, GetAreaRequest } from '../schema/area.schema';
declare class AreaController extends BaseController<AreaService> implements IBaseController {
    constructor();
    create: (req: Request<ParamsDictionary, SuccessResponse<Area>, CreateAreaRequest, ParsedQs, Record<string, any>>, res: Response<any, Record<string, any>>, next: NextFunction) => Promise<void>;
    get: (req: Request<ParamsDictionary, any, any, ParsedQs & GetAreaRequest, Record<string, any>>, res: Response<any, Record<string, any>>, next: NextFunction) => Promise<void>;
    detail: (req: Request<ParamsDictionary & {
        id: number;
    }, any, any, ParsedQs, Record<string, any>>, res: Response<any, Record<string, any>>, next: NextFunction) => Promise<void>;
    update: (req: Request<ParamsDictionary & {
        id: number;
    }, any, CreateAreaRequest, ParsedQs, Record<string, any>>, res: Response<any, Record<string, any>>, next: NextFunction) => Promise<void>;
    delete: (req: Request<ParamsDictionary & {
        id: number;
    }, any, any, ParsedQs, Record<string, any>>, res: Response<any, Record<string, any>>, next: NextFunction) => Promise<void>;
}
export default AreaController;
