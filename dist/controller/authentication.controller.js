"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const response_1 = __importDefault(require("../providers/response"));
const authentication_service_1 = __importDefault(require("../service/authentication.service"));
const http_code_enum_1 = require("../types/http_code.enum");
class AuthenticationController {
    constructor() {
        this.login = async (req, res, next) => {
            try {
                const result = await this.service.login(req.body);
                response_1.default.success(result).create(res);
            }
            catch (error) {
                next(error);
            }
        };
        this.checkEmail = async (req, res, next) => {
            try {
                const result = await this.service.checkEmail(req.query.email);
                let message = 'Email yang anda masukkan sudah digunakan';
                if (!result) {
                    message = 'Email yang anda masukkan belum digunakan';
                }
                response_1.default.success(result, http_code_enum_1.HttpCode.OK, message).create(res);
            }
            catch (error) {
                next(error);
            }
        };
        this.me = async (req, res, next) => {
            try {
                const me = res.locals.user;
                response_1.default.success(me).create(res);
            }
            catch (error) {
                next(error);
            }
        };
        this.service = new authentication_service_1.default();
    }
}
exports.default = AuthenticationController;
