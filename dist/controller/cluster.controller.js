"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const cluster_service_1 = __importDefault(require("../service/cluster.service"));
const base_controller_1 = __importDefault(require("./base.controller"));
const response_1 = __importDefault(require("../providers/response"));
const api_error_1 = __importDefault(require("../providers/exceptions/api.error"));
const http_code_enum_1 = require("../types/http_code.enum");
class ClusterController extends base_controller_1.default {
    constructor() {
        super(new cluster_service_1.default());
        this.create = async (req, res, next) => {
            try {
                const result = await this.service.create(req.body);
                response_1.default.success(result).create(res);
            }
            catch (error) {
                next(error);
            }
        };
        this.get = async (req, res, next) => {
            try {
                const url = req.protocol + '://' + req.get('host') + req.originalUrl;
                const result = (await this.service.get(url, req.query));
                response_1.default.pagination(result.res, result.paging).create(res);
            }
            catch (error) {
                next(error);
            }
        };
        this.detail = async (req, res, next) => {
            try {
                const result = await this.service.detail(req.params.id);
                response_1.default.success(result).create(res);
            }
            catch (error) {
                next(error);
            }
        };
        this.update = async (req, res, next) => {
            try {
                const result = await this.service.update(req.params.id, req.body);
                if (!result) {
                    next(new api_error_1.default({
                        httpCode: http_code_enum_1.HttpCode.BAD_REQUEST,
                        message: 'Gagal ubah data'
                    }));
                }
                response_1.default.success('Berhasil ubah data').create(res);
            }
            catch (error) {
                next(error);
            }
        };
        this.delete = async (req, res, next) => {
            try {
                const { id } = req.params;
                const result = await this.service.delete(id);
                if (!result) {
                    next(new api_error_1.default({
                        httpCode: http_code_enum_1.HttpCode.BAD_REQUEST,
                        message: 'Gagal hapus data'
                    }));
                }
                response_1.default.success('Berhasil hapus data').create(res);
            }
            catch (error) {
                next(error);
            }
        };
        /**
         *  Detail Section
         */
        this.createDetail = async (req, res, next) => {
            try {
                const result = await this.service.createDetail(req.params.id, req.body);
                response_1.default.success(result).create(res);
            }
            catch (error) {
                next(error);
            }
        };
        this.getDetails = async (req, res, next) => {
            try {
                const url = req.protocol + '://' + req.get('host') + req.originalUrl;
                const result = (await this.service.getDetails(req.params.id, url, req.body));
                response_1.default.pagination(result.res, result.paging).create(res);
            }
            catch (error) {
                next(error);
            }
        };
        this.getDetail = async (req, res, next) => {
            try {
                const result = await this.service.getDetail(req.params.id, req.params.region_id);
                response_1.default.success(result).create(res);
            }
            catch (error) {
                next(error);
            }
        };
        this.updateDetail = async (req, res, next) => {
            try {
                const result = await this.service.updateDetail(req.params.id, req.params.region_id, req.body);
                if (!result) {
                    next(new api_error_1.default({
                        httpCode: http_code_enum_1.HttpCode.BAD_REQUEST,
                        message: 'Gagal update data'
                    }));
                }
                response_1.default.success('Success update data').create(res);
            }
            catch (error) {
                next(error);
            }
        };
        this.deleteDetail = async (req, res, next) => {
            try {
                const result = await this.service.deleteDetail(req.params.id, req.params.region_id);
                if (!result) {
                    next(new api_error_1.default({
                        httpCode: http_code_enum_1.HttpCode.BAD_REQUEST,
                        message: 'Gagal hapus data'
                    }));
                }
                response_1.default.success('Success hapus data').create(res);
            }
            catch (error) {
                next(error);
            }
        };
    }
}
exports.default = ClusterController;
