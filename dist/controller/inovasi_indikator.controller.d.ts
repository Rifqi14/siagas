import { NextFunction, Request, Response } from 'express';
import { ParamsDictionary } from 'express-serve-static-core';
import { ParsedQs } from 'qs';
import InovasiIndikatorService from '../service/inovasi_indikator.service';
import BaseController from './base.controller';
import { IBaseController } from './interface/ibase.controller';
import { InovasiIndikatorParams } from '../schema/inovasi_indikator.schema';
declare class InovasiIndikatorController extends BaseController<InovasiIndikatorService> implements IBaseController {
    constructor();
    create: (req: Request<ParamsDictionary, any, any, ParsedQs, Record<string, any>>, res: Response<any, Record<string, any>>, next: NextFunction) => Promise<void>;
    get: (req: Request<ParamsDictionary & {
        pemda_id: string;
    }, any, any, ParsedQs & InovasiIndikatorParams, Record<string, any>>, res: Response<any, Record<string, any>>, next: NextFunction) => Promise<void>;
    detail: (req: Request<ParamsDictionary & {
        inovasi_id: number;
        indikator_id: number;
    }, any, any, ParsedQs, Record<string, any>>, res: Response<any, Record<string, any>>, next: NextFunction) => Promise<void>;
    update: (req: Request<ParamsDictionary & {
        id: number;
    }, any, any, ParsedQs, Record<string, any>>, res: Response<any, Record<string, any>>, next: NextFunction) => Promise<void>;
    delete: (req: Request<ParamsDictionary & {
        id: number;
    }, any, any, ParsedQs, Record<string, any>>, res: Response<any, Record<string, any>>, next: NextFunction) => Promise<void>;
}
export default InovasiIndikatorController;
