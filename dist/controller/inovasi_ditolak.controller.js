"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.InovasiDitolakController = void 0;
const inovasi_ditolak_service_1 = __importDefault(require("../service/inovasi_ditolak.service"));
const base_controller_1 = __importDefault(require("./base.controller"));
const response_1 = __importDefault(require("../providers/response"));
const api_error_1 = __importDefault(require("../providers/exceptions/api.error"));
const http_code_enum_1 = require("../types/http_code.enum");
const fs_1 = require("fs");
class InovasiDitolakController extends base_controller_1.default {
    constructor() {
        super(new inovasi_ditolak_service_1.default());
        this.create = async (req, res, next) => {
            throw new Error('Method not implemented.');
        };
        this.get = async (req, res, next) => {
            try {
                const { username } = res.locals.user;
                const url = req.protocol + '://' + req.get('host') + req.originalUrl;
                const result = (await this.service.get(url, username, req.query));
                response_1.default.pagination(result.res, result.paging).create(res);
            }
            catch (error) {
                next(error);
            }
        };
        this.detail = async (req, res, next) => {
            try {
                const { review_inovasi_id } = req.params;
                const result = await this.service.detail(review_inovasi_id);
                response_1.default.success(result).create(res);
            }
            catch (error) {
                next(error);
            }
        };
        this.update = async (req, res, next) => {
            try {
                const { review_inovasi_id } = req.params;
                const { username } = res.locals.user;
                const result = await this.service.setujui(review_inovasi_id, username);
                if (!result) {
                    throw new api_error_1.default({
                        httpCode: http_code_enum_1.HttpCode.BAD_REQUEST,
                        message: 'Gagal ubah data',
                    });
                }
                response_1.default.success('Berhasil ubah data').create(res);
            }
            catch (error) {
                next(error);
            }
        };
        this.delete = async (req, res, next) => {
            throw new Error('Method not implemented.');
        };
        this.getDownload = async (req, res, next) => {
            try {
                const { username } = res.locals.user;
                const result = await this.service.download(req.params.type, username, req.query);
                res.download(result, err => !err && (0, fs_1.unlinkSync)(result));
            }
            catch (error) {
                next(error);
            }
        };
    }
}
exports.InovasiDitolakController = InovasiDitolakController;
