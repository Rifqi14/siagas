import { Request, Response, NextFunction } from 'express';
import { ParamsDictionary } from 'express-serve-static-core';
import { ParsedQs } from 'qs';
import { CreateClusterRequest, GetClusterRequest } from '../schema/cluster.schema';
import ClusterService from '../service/cluster.service';
import BaseController from './base.controller';
import { IBaseController } from './interface/ibase.controller';
import { SuccessResponse, PaginationResponse } from '../types/response.type';
import { PaginationResponse as PaginationResponseInterface } from '../interface/pagination.interface';
import Cluster from '../entity/Cluster.entity';
import { CreateClusterDetailRequest, GetClusterDetailRequest } from '../schema/clusterDetail.schema';
import ClusterDetail from '../entity/ClusterDetail.entity';
declare class ClusterController extends BaseController<ClusterService> implements IBaseController {
    constructor();
    create: (req: Request<ParamsDictionary, any, CreateClusterRequest, ParsedQs, Record<string, any>>, res: Response<SuccessResponse<Cluster>, Record<string, any>>, next: NextFunction) => Promise<void>;
    get: (req: Request<ParamsDictionary, any, any, ParsedQs & GetClusterRequest, Record<string, any>>, res: Response<PaginationResponse<PaginationResponseInterface, Cluster>, Record<string, any>>, next: NextFunction) => Promise<void>;
    detail: (req: Request<ParamsDictionary & {
        id: number;
    }, any, any, ParsedQs, Record<string, any>>, res: Response<any, Record<string, any>>, next: NextFunction) => Promise<void>;
    update: (req: Request<ParamsDictionary & {
        id: number;
    }, any, CreateClusterRequest, ParsedQs, Record<string, any>>, res: Response<SuccessResponse<string>, Record<string, any>>, next: NextFunction) => Promise<void>;
    delete: (req: Request<ParamsDictionary & {
        id: number;
    }, any, any, ParsedQs, Record<string, any>>, res: Response<SuccessResponse<string>, Record<string, any>>, next: NextFunction) => Promise<void>;
    /**
     *  Detail Section
     */
    createDetail: (req: Request<ParamsDictionary & {
        id: number;
    }, any, CreateClusterDetailRequest, ParsedQs, Record<string, any>>, res: Response<SuccessResponse<ClusterDetail>, Record<string, any>>, next: NextFunction) => Promise<void>;
    getDetails: (req: Request<ParamsDictionary & {
        id: number;
    }, any, any, ParsedQs & GetClusterDetailRequest, Record<string, any>>, res: Response<PaginationResponse<PaginationResponseInterface, ClusterDetail[]>, Record<string, any>>, next: NextFunction) => Promise<void>;
    getDetail: (req: Request<ParamsDictionary & {
        id: number;
        region_id: number;
    }, any, any, ParsedQs, Record<string, any>>, res: Response<SuccessResponse<ClusterDetail>, Record<string, any>>, next: NextFunction) => Promise<void>;
    updateDetail: (req: Request<ParamsDictionary & {
        id: number;
        region_id: number;
    }, any, CreateClusterDetailRequest, ParsedQs, Record<string, any>>, res: Response<SuccessResponse<ClusterDetail>, Record<string, any>>, next: NextFunction) => Promise<void>;
    deleteDetail: (req: Request<ParamsDictionary & {
        id: number;
        region_id: number;
    }, any, any, ParsedQs, Record<string, any>>, res: Response<any, Record<string, any>>, next: NextFunction) => Promise<void>;
}
export default ClusterController;
