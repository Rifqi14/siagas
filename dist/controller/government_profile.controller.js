"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const fs_1 = require("fs");
const path_1 = require("path");
const api_error_1 = __importDefault(require("../providers/exceptions/api.error"));
const response_1 = __importDefault(require("../providers/response"));
const government_profile_service_1 = __importDefault(require("../service/government_profile.service"));
const http_code_enum_1 = require("../types/http_code.enum");
const base_controller_1 = __importDefault(require("./base.controller"));
class GovernmentProfileController extends base_controller_1.default {
    constructor() {
        super(new government_profile_service_1.default());
        this.create = async (req, res, next) => {
            const file = req.file;
            try {
                const { alamat, daerah, email, nama_admin, name, nomor_telepon, opd_menangani } = req.body;
                const result = await this.service.create(daerah, name, opd_menangani, alamat, email, nomor_telepon, nama_admin, file, res.locals.user.username);
                response_1.default.success(result).create(res);
            }
            catch (error) {
                if (file) {
                    (0, fs_1.unlink)((0, path_1.join)(__dirname, '..', '..', 'public', 'upload', 'profil_pemda', file.filename), err => {
                        if (err) {
                            console.log(`${file.filename} was deleted`);
                        }
                    });
                }
                next(error);
            }
        };
        this.get = async (req, res, next) => {
            try {
                const url = req.protocol + '://' + req.get('host') + req.originalUrl;
                const result = (await this.service.get(req.query, url));
                response_1.default.pagination(result.res, result.paging).create(res);
            }
            catch (error) {
                next(error);
            }
        };
        this.detail = async (req, res, next) => {
            try {
                const result = await this.service.detail(req.params.id);
                response_1.default.success(result).create(res);
            }
            catch (error) {
                next(error);
            }
        };
        this.update = async (req, res, next) => {
            const file = req.file;
            try {
                const { alamat, daerah, email, nama_admin, name, nomor_telepon, opd_menangani } = req.body;
                const result = await this.service.update(req.params.id, daerah, name, opd_menangani, alamat, email, nomor_telepon, nama_admin, file, res.locals.user.username);
                response_1.default.success(result).create(res);
            }
            catch (error) {
                next(error);
            }
        };
        this.delete = async (req, res, next) => {
            try {
                const result = await this.service.delete(req.params.id);
                if (!result) {
                    next(new api_error_1.default({
                        httpCode: http_code_enum_1.HttpCode.BAD_REQUEST,
                        message: 'Gagal hapus data'
                    }));
                }
                else {
                    response_1.default.success('Berhasil hapus data').create(res);
                }
            }
            catch (error) {
                next(error);
            }
        };
    }
}
exports.default = GovernmentProfileController;
