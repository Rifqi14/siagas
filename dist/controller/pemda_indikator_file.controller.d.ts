import { NextFunction, Request, Response } from 'express';
import { ParamsDictionary } from 'express-serve-static-core';
import { ParsedQs } from 'qs';
import { InovasiIndikatorFile } from '../entity/InovasiIndikatorFile.entity';
import { PemdaIndikatorFileParams, UploadPemdaIndikatorFileRequest } from '../schema/pemda_indikator_file.schema';
import PemdaIndikatorFileService from '../service/pemda_indikator_file.service';
import { PaginationResponse, SuccessResponse } from '../types/response.type';
import { PaginationResponse as PaginationResponseInterface } from '../interface/pagination.interface';
import BaseController from './base.controller';
import { IBaseController } from './interface/ibase.controller';
export declare class PemdaIndikatorFileController extends BaseController<PemdaIndikatorFileService> implements IBaseController {
    constructor();
    create: (req: Request<ParamsDictionary, any, UploadPemdaIndikatorFileRequest, ParsedQs, Record<string, any>>, res: Response<any, Record<string, any>>, next: NextFunction) => Promise<void>;
    upload: (req: Request<ParamsDictionary & {
        pemda_id: string;
        indikator_id: string;
    }, any, UploadPemdaIndikatorFileRequest, ParsedQs, Record<string, any>>, res: Response<SuccessResponse<InovasiIndikatorFile>, Record<string, any>>, next: NextFunction) => Promise<void>;
    get: (req: Request<ParamsDictionary & {
        pemda_id: string;
        indikator_id: string;
    }, any, any, ParsedQs & PemdaIndikatorFileParams, Record<string, any>>, res: Response<PaginationResponse<PaginationResponseInterface, Document>, Record<string, any>>, next: NextFunction) => Promise<void>;
    detail: (req: Request<ParamsDictionary & {
        pemda_id: number;
        indikator_id: number;
        file_id: number;
    }, any, any, ParsedQs, Record<string, any>>, res: Response<any, Record<string, any>>, next: NextFunction) => Promise<void>;
    update: (req: Request<ParamsDictionary & {
        id: number;
    }, any, any, ParsedQs, Record<string, any>>, res: Response<SuccessResponse<string>, Record<string, any>>, next: NextFunction) => Promise<void>;
    delete: (req: Request<ParamsDictionary & {
        pemda_id: number;
        indikator_id: number;
        file_id: number;
    }, any, any, ParsedQs, Record<string, any>>, res: Response<SuccessResponse<string>, Record<string, any>>, next: NextFunction) => Promise<void>;
}
