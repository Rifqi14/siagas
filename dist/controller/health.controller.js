"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const response_1 = __importDefault(require("../providers/response"));
const health_service_1 = __importDefault(require("../service/health.service"));
const base_controller_1 = __importDefault(require("./base.controller"));
class HealthController extends base_controller_1.default {
    constructor(service = new health_service_1.default()) {
        super(service);
    }
    async health(req, res, next) {
        try {
            response_1.default.success('Server is healthy').create(res);
        }
        catch (error) {
            next(error);
        }
    }
    async template(req, res, next) {
        try {
            return res.render('main');
        }
        catch (error) {
            next(error);
        }
    }
}
exports.default = HealthController;
