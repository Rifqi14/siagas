import { Request, Response, NextFunction } from 'express';
import { ParamsDictionary } from 'express-serve-static-core';
import { ParsedQs } from 'qs';
import { CreateGovernmentSectorRequest, GetGovernmentSectorRequest } from '../schema/government_sector.schema';
import { SuccessResponse } from '../types/response.type';
import GovernmentSectorService from '../service/government_sector.service';
import BaseController from './base.controller';
import { IGovernmentSector } from './interface/igovernment_sector.controller';
import GovernmentSector from '../entity/GovernmentSector';
declare class GovernmentSectorController extends BaseController<GovernmentSectorService> implements IGovernmentSector {
    constructor();
    detail: (req: Request<ParamsDictionary & {
        id: number;
    }, any, any, ParsedQs, Record<string, any>>, res: Response<any, Record<string, any>>, next: NextFunction) => Promise<void>;
    update: (req: Request<ParamsDictionary & {
        id: number;
    }, SuccessResponse<GovernmentSector>, CreateGovernmentSectorRequest, ParsedQs, Record<string, any>>, res: Response<any, Record<string, any>>, next: NextFunction) => Promise<void>;
    delete: (req: Request<ParamsDictionary & {
        id: number;
    }, any, any, ParsedQs, Record<string, any>>, res: Response<any, Record<string, any>>, next: NextFunction) => Promise<void>;
    get: (req: Request<ParamsDictionary, any, any, ParsedQs & GetGovernmentSectorRequest, Record<string, any>>, res: Response<any, Record<string, any>>, next: NextFunction) => Promise<void>;
    create: (req: Request<ParamsDictionary, SuccessResponse<GovernmentSector>, CreateGovernmentSectorRequest, ParsedQs, Record<string, any>>, res: Response<any, Record<string, any>>, next: NextFunction) => Promise<void>;
}
export default GovernmentSectorController;
