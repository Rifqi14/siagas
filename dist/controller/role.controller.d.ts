import { Request, Response, NextFunction } from 'express';
import { ParamsDictionary } from 'express-serve-static-core';
import { ParsedQs } from 'qs';
import Role from '../entity/Role.entity';
import { CreateRoleRequest, GetRoleRequest } from '../schema/role.schema';
import RoleService from '../service/role.service';
import { SuccessResponse } from '../types/response.type';
import BaseController from './base.controller';
import { IBaseController } from './interface/ibase.controller';
declare class RoleController extends BaseController<RoleService> implements IBaseController {
    constructor();
    create: (req: Request<ParamsDictionary, any, CreateRoleRequest, ParsedQs, Record<string, any>>, res: Response<SuccessResponse<Role>, Record<string, any>>, next: NextFunction) => Promise<void>;
    get: (req: Request<ParamsDictionary, any, any, ParsedQs & GetRoleRequest, Record<string, any>>, res: Response<any, Record<string, any>>, next: NextFunction) => Promise<void>;
    detail: (req: Request<ParamsDictionary & {
        id: number;
    }, any, any, ParsedQs, Record<string, any>>, res: Response<any, Record<string, any>>, next: NextFunction) => Promise<void>;
    update: (req: Request<ParamsDictionary & {
        id: number;
    }, any, CreateRoleRequest, ParsedQs, Record<string, any>>, res: Response<any, Record<string, any>>, next: NextFunction) => Promise<void>;
    delete: (req: Request<ParamsDictionary & {
        id: number;
    }, any, any, ParsedQs, Record<string, any>>, res: Response<any, Record<string, any>>, next: NextFunction) => Promise<void>;
}
export default RoleController;
