"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.PreviewReviewInovasiDaerahController = void 0;
const base_controller_1 = __importDefault(require("./base.controller"));
const preview_inovasi_daerah_service_1 = __importDefault(require("../service/preview_inovasi_daerah.service"));
const response_1 = __importDefault(require("../providers/response"));
const api_error_1 = __importDefault(require("../providers/exceptions/api.error"));
const http_code_enum_1 = require("../types/http_code.enum");
class PreviewReviewInovasiDaerahController extends base_controller_1.default {
    constructor() {
        super(new preview_inovasi_daerah_service_1.default());
        this.create = async (req, res, next) => {
            try {
                const { username } = res.locals.user;
                const result = await this.service.create(req.params.review_id, username, req.body);
                response_1.default.success(result).create(res);
            }
            catch (error) {
                next(error);
            }
        };
        this.get = async (req, res, next) => {
            try {
                const { username } = res.locals.user;
                const url = req.protocol + '://' + req.get('host') + req.originalUrl;
                const result = (await this.service.get(req.params.review_id, url, username, req.query));
                response_1.default.pagination(result.res, result.paging).create(res);
            }
            catch (error) {
                next(error);
            }
        };
        this.detail = async (req, res, next) => {
            try {
                const { id } = req.params;
                const result = await this.service.detail(req.params.review_id, id);
                response_1.default.success(result).create(res);
            }
            catch (error) {
                next(error);
            }
        };
        this.update = async (req, res, next) => {
            try {
                const { id } = req.params;
                const { username } = res.locals.user;
                const result = await this.service.update(req.params.review_id, id, username, req.body);
                if (!result) {
                    throw new api_error_1.default({
                        httpCode: http_code_enum_1.HttpCode.BAD_REQUEST,
                        message: 'Gagal ubah data'
                    });
                }
                response_1.default.success('Berhasil ubah data').create(res);
            }
            catch (error) {
                next(error);
            }
        };
        this.delete = async (req, res, next) => {
            try {
                const { id } = req.params;
                const result = await this.service.delete(req.params.review_id, id);
                if (!result) {
                    throw new api_error_1.default({
                        httpCode: http_code_enum_1.HttpCode.BAD_REQUEST,
                        message: 'Gagal hapus data'
                    });
                }
                response_1.default.success('Berhasil hapus data').create(res);
            }
            catch (error) {
                next(error);
            }
        };
    }
}
exports.PreviewReviewInovasiDaerahController = PreviewReviewInovasiDaerahController;
