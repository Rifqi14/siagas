"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.LaporanController = void 0;
const fs_1 = require("fs");
const response_1 = __importDefault(require("../providers/response"));
const laporan_service_1 = __importDefault(require("../service/laporan.service"));
const base_controller_1 = __importDefault(require("./base.controller"));
class LaporanController extends base_controller_1.default {
    constructor() {
        super(new laporan_service_1.default());
        this.laporanUrusan = async (req, res, next) => {
            try {
                const url = req.protocol + "://" + req.get("host") + req.originalUrl;
                const result = (await this.service.laporanUrusanInovasi(url, req.query));
                response_1.default.pagination(result.res, result.paging).create(res);
            }
            catch (error) {
                next(error);
            }
        };
        this.laporanInisiator = async (req, res, next) => {
            try {
                const url = req.protocol + "://" + req.get("host") + req.originalUrl;
                const result = (await this.service.laporanInisiatorInovasi(url, req.query));
                response_1.default.pagination(result.res, result.paging).create(res);
            }
            catch (error) {
                next(error);
            }
        };
        this.laporanBentuk = async (req, res, next) => {
            try {
                const url = req.protocol + "://" + req.get("host") + req.originalUrl;
                const result = (await this.service.laporanBentukInovasi(url, req.query));
                response_1.default.pagination(result.res, result.paging).create(res);
            }
            catch (error) {
                next(error);
            }
        };
        this.laporanIndeks = async (req, res, next) => {
            try {
                const url = req.protocol + "://" + req.get("host") + req.originalUrl;
                const result = (await this.service.laporanIndeks(url, req.query));
                response_1.default.pagination(result.res, result.paging).create(res);
            }
            catch (error) {
                next(error);
            }
        };
        this.laporanIndeksDownload = async (req, res, next) => {
            try {
                const result = await this.service.laporanIndeksdownload(req.params.type, req.query);
                res.download(result, err => {
                    if (!err)
                        (0, fs_1.unlinkSync)(result);
                    else
                        console.log(err);
                });
            }
            catch (error) {
                next(error);
            }
        };
        this.laporanUrusanInovasiDownload = async (req, res, next) => {
            try {
                const result = await this.service.laporanUrusanInovasidownload(req.params.type, req.query);
                res.download(result, err => {
                    if (!err)
                        (0, fs_1.unlinkSync)(result);
                    else
                        console.log(err);
                });
            }
            catch (error) {
                next(error);
            }
        };
        this.laporanInisiatorInovasiDownload = async (req, res, next) => {
            try {
                const result = await this.service.laporanInisiatorInovasidownload(req.params.type, req.query);
                res.download(result, err => {
                    if (!err)
                        (0, fs_1.unlinkSync)(result);
                    else
                        console.log(err);
                });
            }
            catch (error) {
                next(error);
            }
        };
        this.laporanJenisInovasi = async (req, res, next) => {
            try {
                const url = req.protocol + "://" + req.get("host") + req.originalUrl;
                const result = (await this.service.laporanJenisInovasi(url, req.query));
                response_1.default.pagination(result.res, result.paging).create(res);
            }
            catch (error) {
                next(error);
            }
        };
    }
}
exports.LaporanController = LaporanController;
