import { NextFunction, Request, Response } from "express";
import { ParamsDictionary } from "express-serve-static-core";
import { ParsedQs } from "qs";
import { Distrik } from "../entity/Distrik.entity";
import { CreateDistrikRequest, GetDistrikRequest } from "../schema/distrik.schema";
import DistrikService from "../service/distrik.service";
import { SuccessResponse } from "../types/response.type";
import BaseController from "./base.controller";
import { IBaseController } from "./interface/ibase.controller";
declare class DistrikController extends BaseController<DistrikService> implements IBaseController {
    constructor();
    create: (req: Request<ParamsDictionary, SuccessResponse<Distrik>, CreateDistrikRequest, ParsedQs, Record<string, any>>, res: Response<any, Record<string, any>>, next: NextFunction) => Promise<void>;
    get: (req: Request<ParamsDictionary, any, any, ParsedQs & GetDistrikRequest, Record<string, any>>, res: Response<any, Record<string, any>>, next: NextFunction) => Promise<void>;
    detail: (req: Request<ParamsDictionary & {
        id: number;
    }, any, any, ParsedQs, Record<string, any>>, res: Response<any, Record<string, any>>, next: NextFunction) => Promise<void>;
    update: (req: Request<ParamsDictionary & {
        id: number;
    }, any, CreateDistrikRequest, ParsedQs, Record<string, any>>, res: Response<any, Record<string, any>>, next: NextFunction) => Promise<void>;
    delete: (req: Request<ParamsDictionary & {
        id: number;
    }, any, any, ParsedQs, Record<string, any>>, res: Response<any, Record<string, any>>, next: NextFunction) => Promise<void>;
}
export default DistrikController;
