"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const inovasi_indikator_service_1 = __importDefault(require("../service/inovasi_indikator.service"));
const base_controller_1 = __importDefault(require("./base.controller"));
const api_error_1 = __importDefault(require("../providers/exceptions/api.error"));
const http_code_enum_1 = require("../types/http_code.enum");
const response_1 = __importDefault(require("../providers/response"));
class InovasiIndikatorController extends base_controller_1.default {
    constructor() {
        super(new inovasi_indikator_service_1.default());
        this.create = async (req, res, next) => {
            throw new api_error_1.default({
                httpCode: http_code_enum_1.HttpCode.NOT_FOUND,
                message: 'Method not implemented'
            });
        };
        this.get = async (req, res, next) => {
            try {
                const { username } = res.locals.user;
                const url = req.protocol + '://' + req.get('host') + req.originalUrl;
                const result = (await this.service.get(req.params.pemda_id, url, username, req.query));
                response_1.default.pagination(result.res, result.paging).create(res);
            }
            catch (error) {
                next(error);
            }
        };
        this.detail = async (req, res, next) => {
            try {
                const { inovasi_id, indikator_id } = req.params;
                const result = await this.service.detail(inovasi_id, indikator_id);
                response_1.default.success(result).create(res);
            }
            catch (error) {
                next(error);
            }
        };
        this.update = async (req, res, next) => {
            throw new api_error_1.default({
                httpCode: http_code_enum_1.HttpCode.NOT_FOUND,
                message: 'Method not implemented'
            });
        };
        this.delete = async (req, res, next) => {
            throw new api_error_1.default({
                httpCode: http_code_enum_1.HttpCode.NOT_FOUND,
                message: 'Method not implemented'
            });
        };
    }
}
exports.default = InovasiIndikatorController;
