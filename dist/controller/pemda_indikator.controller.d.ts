import { NextFunction, Request, Response } from 'express';
import { ParamsDictionary } from 'express-serve-static-core';
import { ParsedQs } from 'qs';
import PemdaIndikatorService from '../service/pemda_indikator.service';
import BaseController from './base.controller';
import { IBaseController } from './interface/ibase.controller';
import { CreatePemdaIndikatorRequest, PemdaIndikatorParams } from '../schema/pemda_indikator.schema';
declare class PemdaIndikatorController extends BaseController<PemdaIndikatorService> implements IBaseController {
    constructor();
    create: (req: Request<ParamsDictionary & {
        pemda_id: string;
    }, any, CreatePemdaIndikatorRequest, ParsedQs, Record<string, any>>, res: Response<any, Record<string, any>>, next: NextFunction) => Promise<void>;
    get: (req: Request<ParamsDictionary & {
        pemda_id: string;
    }, any, any, ParsedQs & PemdaIndikatorParams, Record<string, any>>, res: Response<any, Record<string, any>>, next: NextFunction) => Promise<void>;
    detail: (req: Request<ParamsDictionary & {
        pemda_id: number;
        indikator_id: number;
    }, any, any, ParsedQs, Record<string, any>>, res: Response<any, Record<string, any>>, next: NextFunction) => Promise<void>;
    update: (req: Request<ParamsDictionary & {
        pemda_id: string;
        pemda_indikator_id: string;
    }, any, any, ParsedQs, Record<string, any>>, res: Response<any, Record<string, any>>, next: NextFunction) => Promise<void>;
    delete: (req: Request<ParamsDictionary & {
        pemda_id: string;
        pemda_indikator_id: string;
    }, any, any, ParsedQs, Record<string, any>>, res: Response<any, Record<string, any>>, next: NextFunction) => Promise<void>;
}
export default PemdaIndikatorController;
