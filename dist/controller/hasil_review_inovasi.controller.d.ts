import HasilReviewInovasiService from "../service/hasil_review_inovasi.service";
import BaseController from "./base.controller";
import { IBaseController } from "./interface/ibase.controller";
import { Request, Response, NextFunction } from "express";
import { ParamsDictionary } from "express-serve-static-core";
import { ParsedQs } from "qs";
import { HasilReviewParams } from "../schema/hasil_review_inovasi.schema";
import { DownloadType } from "../types/lib";
export declare class HasilReviewInovasiController extends BaseController<HasilReviewInovasiService> implements IBaseController {
    constructor();
    create: (req: Request<ParamsDictionary, any, any, ParsedQs, Record<string, any>>, res: Response<any, Record<string, any>>, next: NextFunction) => Promise<void>;
    get: (req: Request<ParamsDictionary, any, any, ParsedQs & HasilReviewParams, Record<string, any>>, res: Response<any, Record<string, any>>, next: NextFunction) => Promise<void>;
    detail: (req: Request<ParamsDictionary & {
        review_inovasi_id: number;
    }, any, any, ParsedQs, Record<string, any>>, res: Response<any, Record<string, any>>, next: NextFunction) => Promise<void>;
    update: (req: Request<ParamsDictionary, any, any, ParsedQs, Record<string, any>>, res: Response<any, Record<string, any>>, next: NextFunction) => Promise<void>;
    delete: (req: Request<ParamsDictionary & {
        review_inovasi_id: number;
    }, any, any, ParsedQs, Record<string, any>>, res: Response<any, Record<string, any>>, next: NextFunction) => Promise<void>;
    getDownload: (req: Request<ParamsDictionary & {
        type: DownloadType;
    }, any, any, ParsedQs & HasilReviewParams, Record<string, any>>, res: Response<any, Record<string, any>>, next: NextFunction) => Promise<void>;
}
