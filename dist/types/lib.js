"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.AppBaseEntity = exports.CustomBaseEntity = void 0;
const User_entity_1 = __importDefault(require("../entity/User.entity"));
const typeorm_1 = require("typeorm");
class CustomBaseEntity extends typeorm_1.BaseEntity {
}
__decorate([
    (0, typeorm_1.Column)(),
    __metadata("design:type", String)
], CustomBaseEntity.prototype, "created_by", void 0);
__decorate([
    (0, typeorm_1.Column)(),
    __metadata("design:type", String)
], CustomBaseEntity.prototype, "updated_by", void 0);
__decorate([
    (0, typeorm_1.ManyToOne)(() => User_entity_1.default),
    (0, typeorm_1.JoinColumn)({ name: 'created_by', referencedColumnName: 'username' }),
    __metadata("design:type", User_entity_1.default)
], CustomBaseEntity.prototype, "userCreator", void 0);
__decorate([
    (0, typeorm_1.ManyToOne)(() => User_entity_1.default),
    (0, typeorm_1.JoinColumn)({ name: 'updated_by', referencedColumnName: 'username' }),
    __metadata("design:type", User_entity_1.default)
], CustomBaseEntity.prototype, "userUpdater", void 0);
exports.CustomBaseEntity = CustomBaseEntity;
class AppBaseEntity extends CustomBaseEntity {
}
__decorate([
    (0, typeorm_1.CreateDateColumn)(),
    __metadata("design:type", Date)
], AppBaseEntity.prototype, "created_at", void 0);
__decorate([
    (0, typeorm_1.UpdateDateColumn)(),
    __metadata("design:type", Date)
], AppBaseEntity.prototype, "updated_at", void 0);
exports.AppBaseEntity = AppBaseEntity;
