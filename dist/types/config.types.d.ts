export type ConfigType = {
    app_name: string;
    app_url: string;
    app_port: string;
    database_host: string;
    database_name: string;
    database_user: string;
    database_password: string;
    database_port: string;
    token_expired_in_day: number;
    private_key_location: string;
    public_key_location: string;
};
