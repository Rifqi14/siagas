type ResponseStatus = 'success' | 'error' | 'validation';
export type BaseResponse = {
    status: ResponseStatus;
    isSuccess: boolean;
    code: number;
    message: string;
};
export type ValidationMessage = {
    object: string;
    message: string;
};
export type SuccessResponse<T> = BaseResponse & {
    data: T;
};
export type PaginationResponse<P, T> = SuccessResponse<T> & {
    pagination: P;
};
export type ErrorResponse = BaseResponse & {
    error: Error;
};
export type ValidationResponse = BaseResponse & {
    error: Array<ValidationMessage>;
};
export type ResponseType<T, P> = SuccessResponse<T> | PaginationResponse<P, T> | ErrorResponse | ValidationResponse;
export {};
