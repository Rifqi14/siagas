import User from '../entity/User.entity';
import { BaseEntity } from 'typeorm';
export type RequiredField<T, K extends keyof T> = T & Required<Pick<T, K>>;
export type OmitType<T, K extends keyof T> = Omit<T, K>;
export type Nullable<T> = {
    [K in keyof T]: T[K] | null;
};
export declare class CustomBaseEntity extends BaseEntity {
    created_by: string;
    updated_by: string;
    userCreator: User;
    userUpdater: User;
}
export declare class AppBaseEntity extends CustomBaseEntity {
    created_at: Date;
    updated_at: Date;
}
export type DownloadType = 'xlsx' | 'pdf';
