"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.UpdateIndicatorTable1682917269807 = void 0;
const typeorm_1 = require("typeorm");
class UpdateIndicatorTable1682917269807 {
    async up(queryRunner) {
        await queryRunner.changeColumns('indicators', [
            {
                oldColumn: new typeorm_1.TableColumn({
                    name: 'parent',
                    type: 'varchar',
                    length: '255',
                    isNullable: true
                }),
                newColumn: new typeorm_1.TableColumn({
                    name: 'parent_id',
                    type: 'bigint',
                    isNullable: true
                })
            },
            {
                oldColumn: new typeorm_1.TableColumn({
                    name: 'mandatory',
                    type: 'varchar',
                    length: '255',
                    isNullable: true
                }),
                newColumn: new typeorm_1.TableColumn({
                    name: 'mandatory',
                    type: 'boolean',
                    default: false
                })
            }
        ]);
        await queryRunner.createForeignKey('indicators', new typeorm_1.TableForeignKey({
            columnNames: ['parent_id'],
            referencedColumnNames: ['id'],
            referencedTableName: 'indicators',
            name: 'fk_parent_id_indicators'
        }));
        await queryRunner.dropColumn('indicators', new typeorm_1.TableColumn({
            name: 'sub',
            type: 'varchar',
            length: '255',
            isNullable: true
        }));
    }
    async down(queryRunner) {
        await queryRunner.addColumn('indicators', new typeorm_1.TableColumn({
            name: 'sub',
            type: 'varchar',
            length: '255',
            isNullable: true
        }));
        await queryRunner.dropForeignKey('indicators', new typeorm_1.TableForeignKey({
            columnNames: ['parent_id'],
            referencedColumnNames: ['id'],
            referencedTableName: 'indicators',
            name: 'fk_parent_id_indicators'
        }));
        await queryRunner.changeColumns('indicators', [
            {
                newColumn: new typeorm_1.TableColumn({
                    name: 'parent',
                    type: 'varchar',
                    length: '255',
                    isNullable: true
                }),
                oldColumn: new typeorm_1.TableColumn({
                    name: 'parent_id',
                    type: 'bigint',
                    isNullable: true
                })
            },
            {
                newColumn: new typeorm_1.TableColumn({
                    name: 'mandatory',
                    type: 'varchar',
                    length: '255',
                    isNullable: true
                }),
                oldColumn: new typeorm_1.TableColumn({
                    name: 'mandatory',
                    type: 'boolean',
                    default: false
                })
            }
        ]);
    }
}
exports.UpdateIndicatorTable1682917269807 = UpdateIndicatorTable1682917269807;
