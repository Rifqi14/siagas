"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.CreateGovernmentSectorTable1678226927035 = void 0;
const typeorm_1 = require("typeorm");
class CreateGovernmentSectorTable1678226927035 {
    async up(queryRunner) {
        await queryRunner.createTable(new typeorm_1.Table({
            name: 'government_sectors',
            columns: [
                {
                    name: 'id',
                    type: 'serial4'
                },
                {
                    name: 'name',
                    type: 'varchar',
                    length: '255'
                },
                {
                    name: 'created_at',
                    type: 'timestamp',
                    default: 'now()'
                },
                {
                    name: 'updated_at',
                    type: 'timestamp',
                    default: 'now()'
                }
            ]
        }), true);
        await queryRunner.createPrimaryKey('government_sectors', ['id'], 'pk_government_sectors');
        await queryRunner.createIndex('government_sectors', new typeorm_1.TableIndex({
            columnNames: ['name'],
            name: 'idx_government_sectors_name'
        }));
    }
    async down(queryRunner) {
        await queryRunner.dropTable('government_sectors', true);
    }
}
exports.CreateGovernmentSectorTable1678226927035 = CreateGovernmentSectorTable1678226927035;
