"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.CreateLaporanJenisInovasiView1694252198602 = void 0;
class CreateLaporanJenisInovasiView1694252198602 {
    async up(queryRunner) {
        await queryRunner.query(`create or replace view laporan_jenis_inovasi as select
            gi.pemda_id,
                gi.innovation_type as jenis_inovasi,
            (select count(rid.id) from review_inovasi_daerah rid left join government_innovations gi2 on gi2.id = rid.inovasi_id where lower(gi2.innovation_type) = lower(gi.innovation_type) and lower(rid.status) = 'accept' and gi2.pemda_id = gi.pemda_id) total_disetujui,
            (select count(rid.id) from review_inovasi_daerah rid left join government_innovations gi2 on gi2.id = rid.inovasi_id where lower(gi2.innovation_type) = lower(gi.innovation_type) and (lower(rid.status) = 'pending' or lower(rid.status) = 'rejected') and gi2.pemda_id = gi.pemda_id) total_ditolak,
            (select count(rid.id) from review_inovasi_daerah rid left join government_innovations gi2 on gi2.id = rid.inovasi_id where lower(gi2.innovation_type) = lower(gi.innovation_type) and gi2.pemda_id = gi.pemda_id) total_keseluruhan
            from profil_pemda pp
            right join government_innovations gi on gi.pemda_id = pp.id
            group by gi.innovation_type, gi.pemda_id`);
    }
    async down(queryRunner) {
        await queryRunner.query(`drop view laporan_jenis_inovasi`);
    }
}
exports.CreateLaporanJenisInovasiView1694252198602 = CreateLaporanJenisInovasiView1694252198602;
