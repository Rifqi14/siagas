"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.AddHelperColumnToSettingTable1678783581974 = void 0;
const typeorm_1 = require("typeorm");
class AddHelperColumnToSettingTable1678783581974 {
    async up(queryRunner) {
        await queryRunner.addColumn('settings', new typeorm_1.TableColumn({
            name: 'helper',
            type: 'varchar',
            isNullable: true,
            comment: 'Helper digunakan untuk menambahkan text bantuan ketika mengisi value'
        }));
    }
    async down(queryRunner) {
        await queryRunner.dropColumn('settings', 'helper');
    }
}
exports.AddHelperColumnToSettingTable1678783581974 = AddHelperColumnToSettingTable1678783581974;
