import { MigrationInterface, QueryRunner } from 'typeorm';
export declare class AddDocumentCategoryToEvaluasiInovasi1691572448206 implements MigrationInterface {
    up(queryRunner: QueryRunner): Promise<void>;
    down(queryRunner: QueryRunner): Promise<void>;
}
