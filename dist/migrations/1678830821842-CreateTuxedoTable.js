"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.CreateTuxedoTable1678830821842 = void 0;
const typeorm_1 = require("typeorm");
class CreateTuxedoTable1678830821842 {
    async up(queryRunner) {
        await queryRunner.createTable(new typeorm_1.Table({
            name: 'tuxedos',
            columns: [
                {
                    name: 'id',
                    type: 'serial8',
                    primaryKeyConstraintName: 'pk_tuxedos',
                    isPrimary: true
                },
                {
                    name: 'title',
                    type: 'varchar',
                    length: '255'
                },
                {
                    name: 'slug',
                    type: 'varchar',
                    length: '255'
                },
                {
                    name: 'section',
                    type: 'varchar',
                    length: '255'
                },
                {
                    name: 'content',
                    type: 'text'
                },
                {
                    name: 'created_at',
                    type: 'timestamp',
                    default: 'now()'
                },
                {
                    name: 'updated_at',
                    type: 'timestamp',
                    default: 'now()'
                }
            ],
            indices: [
                {
                    columnNames: ['title'],
                    name: 'fk_tuxedos_title'
                },
                {
                    columnNames: ['slug'],
                    name: 'fk_tuxedos_slug'
                }
            ]
        }), true);
    }
    async down(queryRunner) {
        await queryRunner.dropTable('tuxedos', true);
    }
}
exports.CreateTuxedoTable1678830821842 = CreateTuxedoTable1678830821842;
