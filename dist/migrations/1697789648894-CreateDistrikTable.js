"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.CreateDistrikTable1697789648894 = void 0;
const typeorm_1 = require("typeorm");
class CreateDistrikTable1697789648894 {
    async up(queryRunner) {
        await queryRunner.createTable(new typeorm_1.Table({
            name: "distrik",
            columns: [
                { name: "id", type: "serial8", isPrimary: true },
                { name: "nama_distrik", type: "varchar" },
                { name: "created_at", type: "timestamp", default: "now()" },
                { name: "updated_at", type: "timestamp", default: "now()" },
            ],
        }));
    }
    async down(queryRunner) {
        await queryRunner.dropTable(new typeorm_1.Table({
            name: "distrik",
            columns: [
                { name: "id", type: "serial8", isPrimary: true },
                { name: "nama_distrik", type: "varchar" },
                { name: "created_at", type: "timestamp", default: "now()" },
                { name: "updated_at", type: "timestamp", default: "now()" },
            ],
        }));
    }
}
exports.CreateDistrikTable1697789648894 = CreateDistrikTable1697789648894;
