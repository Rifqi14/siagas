"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.CreateInovasiIndikatorFileTable1689088128340 = void 0;
const typeorm_1 = require("typeorm");
class CreateInovasiIndikatorFileTable1689088128340 {
    async up(queryRunner) {
        await queryRunner.createTable(new typeorm_1.Table({
            name: 'inovasi_indikator_file',
            columns: [
                { name: 'id', type: 'bigserial', isPrimary: true },
                { name: 'inovasi_indikator_id', type: 'bigint', isNullable: true },
                { name: 'file_id', type: 'bigint', isNullable: true },
                { name: 'nomor_surat', type: 'varchar', isNullable: true },
                { name: 'tanggal_surat', type: 'date', isNullable: true },
                { name: 'nama_surat', type: 'varchar', isNullable: true },
                { name: 'created_by', type: 'varchar' },
                { name: 'updated_by', type: 'varchar' },
                { name: 'created_at', type: 'timestamp', default: `now()` },
                { name: 'updated_at', type: 'timestamp', default: `now()` }
            ],
            foreignKeys: [
                {
                    columnNames: ['inovasi_indikator_id'],
                    referencedColumnNames: ['id'],
                    referencedTableName: 'inovasi_indikator',
                    onDelete: 'cascade'
                },
                {
                    columnNames: ['file_id'],
                    referencedColumnNames: ['id'],
                    referencedTableName: 'files',
                    onDelete: 'cascade'
                },
                {
                    columnNames: ['created_by'],
                    referencedColumnNames: ['username'],
                    referencedTableName: 'users'
                },
                {
                    columnNames: ['updated_by'],
                    referencedColumnNames: ['username'],
                    referencedTableName: 'users'
                }
            ]
        }), true);
    }
    async down(queryRunner) {
        await queryRunner.dropTable(new typeorm_1.Table({ name: 'inovasi_indikator_file' }), true);
    }
}
exports.CreateInovasiIndikatorFileTable1689088128340 = CreateInovasiIndikatorFileTable1689088128340;
