"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.CreateEvaluasiTable1691078558362 = void 0;
const typeorm_1 = require("typeorm");
class CreateEvaluasiTable1691078558362 {
    async up(queryRunner) {
        await queryRunner.createTable(new typeorm_1.Table({
            name: 'evaluasi_review_inovasi_daerah',
            columns: [
                {
                    name: 'id',
                    type: 'bigserial',
                    isPrimary: true,
                    primaryKeyConstraintName: 'pk_evaluasi_review_inovasi_daerah'
                },
                {
                    name: 'data_saat_ini',
                    type: 'varchar',
                    isNullable: true
                },
                {
                    name: 'catatan',
                    type: 'varchar',
                    isNullable: true
                },
                {
                    name: 'keterangan',
                    type: 'varchar',
                    isNullable: true
                },
                {
                    name: 'preview_review_inovasi_daerah_id',
                    type: 'bigint',
                    isNullable: true
                },
                {
                    name: 'document_id',
                    type: 'bigint',
                    isNullable: true
                },
                { name: 'created_by', type: 'varchar' },
                { name: 'updated_by', type: 'varchar' },
                { name: 'created_at', type: 'timestamp', default: `now()` },
                { name: 'updated_at', type: 'timestamp', default: `now()` }
            ],
            foreignKeys: [
                {
                    columnNames: ['preview_review_inovasi_daerah_id'],
                    referencedColumnNames: ['id'],
                    referencedTableName: 'preview_review_inovasi_daerah',
                    onDelete: 'cascade'
                },
                {
                    columnNames: ['document_id'],
                    referencedColumnNames: ['id'],
                    referencedTableName: 'files',
                    onDelete: 'cascade'
                },
                {
                    columnNames: ['created_by'],
                    referencedColumnNames: ['username'],
                    referencedTableName: 'users'
                },
                {
                    columnNames: ['updated_by'],
                    referencedColumnNames: ['username'],
                    referencedTableName: 'users'
                }
            ]
        }), true);
    }
    async down(queryRunner) {
        await queryRunner.dropTable(new typeorm_1.Table({
            name: 'evaluasi_review_inovasi_daerah'
        }), true);
    }
}
exports.CreateEvaluasiTable1691078558362 = CreateEvaluasiTable1691078558362;
