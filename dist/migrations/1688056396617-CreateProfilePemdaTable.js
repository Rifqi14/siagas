"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.CreateProfilePemdaTable1688056396617 = void 0;
const typeorm_1 = require("typeorm");
class CreateProfilePemdaTable1688056396617 {
    async up(queryRunner) {
        await queryRunner.createTable(new typeorm_1.Table({
            name: 'profil_pemda',
            columns: [
                { name: 'id', type: 'bigserial', isPrimary: true },
                { name: 'user_id', type: 'bigint' },
                { name: 'document_id', type: 'bigint', isNullable: true },
                { name: 'nama_daerah', type: 'varchar', isNullable: true },
                { name: 'opd_yang_menangani', type: 'varchar', isNullable: true },
                { name: 'alamat_pemda', type: 'varchar', isNullable: true },
                { name: 'email', type: 'varchar', isNullable: true },
                { name: 'no_telpon', type: 'varchar', isNullable: true },
                { name: 'nama_admin', type: 'varchar', isNullable: true },
                { name: 'created_by', type: 'varchar' },
                { name: 'updated_by', type: 'varchar' },
                { name: 'created_at', type: 'timestamp', default: `now()` },
                { name: 'updated_at', type: 'timestamp', default: `now()` }
            ],
            foreignKeys: [
                {
                    columnNames: ['user_id'],
                    referencedColumnNames: ['id'],
                    referencedTableName: 'users'
                },
                {
                    columnNames: ['created_by'],
                    referencedColumnNames: ['username'],
                    referencedTableName: 'users'
                },
                {
                    columnNames: ['updated_by'],
                    referencedColumnNames: ['username'],
                    referencedTableName: 'users'
                },
                {
                    columnNames: ['document_id'],
                    referencedColumnNames: ['id'],
                    referencedTableName: 'files'
                }
            ]
        }), true);
    }
    async down(queryRunner) {
        await queryRunner.dropTable(new typeorm_1.Table({ name: 'profil_pemda' }), true);
    }
}
exports.CreateProfilePemdaTable1688056396617 = CreateProfilePemdaTable1688056396617;
