"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.CreateGovernmentProfileTable1679642508013 = void 0;
const typeorm_1 = require("typeorm");
class CreateGovernmentProfileTable1679642508013 {
    async up(queryRunner) {
        await queryRunner.createTable(new typeorm_1.Table({
            name: 'government_profiles',
            columns: [
                {
                    name: 'id',
                    type: 'serial8',
                    isPrimary: true,
                    primaryKeyConstraintName: 'pk_government_profiles'
                },
                { name: 'region_id', type: 'bigint' },
                { name: 'name', type: 'varchar' },
                { name: 'pic', type: 'varchar' },
                { name: 'address', type: 'varchar' },
                { name: 'email', type: 'varchar' },
                { name: 'phone', type: 'varchar' },
                { name: 'admin_name', type: 'varchar' },
                { name: 'file_id', type: 'bigint', isNullable: true },
                { name: 'created_at', type: 'timestamp', default: 'now()' },
                { name: 'updated_at', type: 'timestamp', default: 'now()' }
            ],
            foreignKeys: [
                {
                    name: 'fk_region_id',
                    columnNames: ['region_id'],
                    referencedColumnNames: ['id'],
                    referencedTableName: 'regions',
                    onDelete: 'set null'
                },
                {
                    name: 'fk_file_id',
                    columnNames: ['file_id'],
                    referencedColumnNames: ['id'],
                    referencedTableName: 'files',
                    onDelete: 'set null'
                }
            ]
        }), true);
    }
    async down(queryRunner) {
        await queryRunner.dropTable('government_profiles', true);
    }
}
exports.CreateGovernmentProfileTable1679642508013 = CreateGovernmentProfileTable1679642508013;
