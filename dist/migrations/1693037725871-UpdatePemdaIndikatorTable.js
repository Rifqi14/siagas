"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.UpdatePemdaIndikatorTable1693037725871 = void 0;
const typeorm_1 = require("typeorm");
class UpdatePemdaIndikatorTable1693037725871 {
    async up(queryRunner) {
        await queryRunner.addColumns('pemda_indikator', [
            new typeorm_1.TableColumn({
                name: 'skor_verifikasi',
                type: 'numeric',
                isNullable: true
            }),
            new typeorm_1.TableColumn({
                name: 'data_saat_ini',
                type: 'varchar',
                isNullable: true
            })
        ]);
        await queryRunner.changeColumn('pemda_indikator', 'nilai', new typeorm_1.TableColumn({ name: 'skor', type: 'numeric', isNullable: true }));
    }
    async down(queryRunner) {
        await queryRunner.changeColumn('pemda_indikator', 'skor', new typeorm_1.TableColumn({ name: 'nilai', type: 'numeric', isNullable: true }));
        await queryRunner.dropColumns('pemda_indikator', [
            new typeorm_1.TableColumn({
                name: 'skor_verifikasi',
                type: 'numeric',
                isNullable: true
            }),
            new typeorm_1.TableColumn({
                name: 'data_saat_ini',
                type: 'varchar',
                isNullable: true
            })
        ]);
    }
}
exports.UpdatePemdaIndikatorTable1693037725871 = UpdatePemdaIndikatorTable1693037725871;
