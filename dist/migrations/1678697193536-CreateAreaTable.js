"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.CreateAreaTable1678697193536 = void 0;
const typeorm_1 = require("typeorm");
class CreateAreaTable1678697193536 {
    async up(queryRunner) {
        await queryRunner.createTable(new typeorm_1.Table({
            name: 'areas',
            columns: [
                {
                    name: 'id',
                    type: 'serial8'
                },
                {
                    name: 'region_id',
                    type: 'int'
                },
                {
                    name: 'name',
                    type: 'varchar',
                    length: '255'
                },
                {
                    name: 'created_at',
                    type: 'timestamp',
                    default: 'now()'
                },
                {
                    name: 'updated_at',
                    type: 'timestamp',
                    default: 'now()'
                }
            ]
        }), true);
        await queryRunner.createPrimaryKey('areas', ['id'], 'pk_areas');
        await queryRunner.createForeignKey('areas', new typeorm_1.TableForeignKey({
            columnNames: ['region_id'],
            referencedColumnNames: ['id'],
            referencedTableName: 'regions',
            name: 'fk_areas_region_id',
            onDelete: 'cascade'
        }));
        await queryRunner.createIndex('areas', new typeorm_1.TableIndex({ columnNames: ['name'], name: 'idx_areas_name' }));
    }
    async down(queryRunner) {
        await queryRunner.dropTable('areas', true);
    }
}
exports.CreateAreaTable1678697193536 = CreateAreaTable1678697193536;
