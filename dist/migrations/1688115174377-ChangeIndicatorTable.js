"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.ChangeIndicatorTable1688115174377 = void 0;
const typeorm_1 = require("typeorm");
class ChangeIndicatorTable1688115174377 {
    async up(queryRunner) {
        await queryRunner.dropColumns('indicators', [
            'serial_number',
            'type',
            'indicator',
            'description',
            'supporting_data',
            'file_type',
            'document_form',
            'file_format',
            'value',
            'indicator_type',
            'group',
            'parent_id',
            'mandatory',
            'created_at',
            'updated_at'
        ]);
        await queryRunner.addColumns('indicators', [
            new typeorm_1.TableColumn({
                name: 'no_urut',
                type: 'bigint',
                isNullable: true
            }),
            new typeorm_1.TableColumn({
                name: 'jenis_indikator',
                type: 'varchar',
                isNullable: true
            }),
            new typeorm_1.TableColumn({
                name: 'nama_indikator',
                type: 'varchar',
                isNullable: true
            }),
            new typeorm_1.TableColumn({
                name: 'keterangan',
                type: 'varchar',
                isNullable: true
            }),
            new typeorm_1.TableColumn({
                name: 'nama_dokumen_pendukung',
                type: 'varchar',
                isNullable: true
            }),
            new typeorm_1.TableColumn({
                name: 'bobot',
                type: 'bigint',
                isNullable: true
            }),
            new typeorm_1.TableColumn({
                name: 'jenis_file',
                type: 'varchar',
                isNullable: true
            }),
            new typeorm_1.TableColumn({
                name: 'parent',
                type: 'varchar',
                isNullable: true
            }),
            new typeorm_1.TableColumn({
                name: 'mandatory',
                type: 'varchar',
                isNullable: true
            }),
            new typeorm_1.TableColumn({
                name: 'created_by',
                type: 'varchar',
                isNullable: true
            }),
            new typeorm_1.TableColumn({
                name: 'updated_by',
                type: 'varchar',
                isNullable: true
            }),
            new typeorm_1.TableColumn({
                name: 'created_at',
                type: 'timestamp',
                default: `now()`
            }),
            new typeorm_1.TableColumn({
                name: 'updated_at',
                type: 'timestamp',
                default: `now()`
            })
        ]);
        await queryRunner.createForeignKeys('indicators', [
            new typeorm_1.TableForeignKey({
                columnNames: ['created_by'],
                referencedColumnNames: ['username'],
                referencedTableName: 'users'
            }),
            new typeorm_1.TableForeignKey({
                columnNames: ['updated_by'],
                referencedColumnNames: ['username'],
                referencedTableName: 'users'
            })
        ]);
    }
    async down(queryRunner) {
        await queryRunner.dropForeignKeys('indicators', [
            new typeorm_1.TableForeignKey({
                columnNames: ['created_by'],
                referencedColumnNames: ['username'],
                referencedTableName: 'users'
            }),
            new typeorm_1.TableForeignKey({
                columnNames: ['updated_by'],
                referencedColumnNames: ['username'],
                referencedTableName: 'users'
            })
        ]);
        await queryRunner.dropColumns('indicators', [
            new typeorm_1.TableColumn({
                name: 'no_urut',
                type: 'bigint',
                isNullable: true
            }),
            new typeorm_1.TableColumn({
                name: 'jenis_indikator',
                type: 'varchar',
                isNullable: true
            }),
            new typeorm_1.TableColumn({
                name: 'nama_indikator',
                type: 'varchar',
                isNullable: true
            }),
            new typeorm_1.TableColumn({
                name: 'keterangan',
                type: 'varchar',
                isNullable: true
            }),
            new typeorm_1.TableColumn({
                name: 'nama_dokumen_pendukung',
                type: 'varchar',
                isNullable: true
            }),
            new typeorm_1.TableColumn({
                name: 'bobot',
                type: 'bigint',
                isNullable: true
            }),
            new typeorm_1.TableColumn({
                name: 'jenis_file',
                type: 'varchar',
                isNullable: true
            }),
            new typeorm_1.TableColumn({
                name: 'parent',
                type: 'varchar',
                isNullable: true
            }),
            new typeorm_1.TableColumn({
                name: 'mandatory',
                type: 'varchar',
                isNullable: true
            }),
            new typeorm_1.TableColumn({
                name: 'created_by',
                type: 'varchar',
                isNullable: true
            }),
            new typeorm_1.TableColumn({
                name: 'updated_by',
                type: 'varchar',
                isNullable: true
            }),
            new typeorm_1.TableColumn({
                name: 'created_at',
                type: 'timestamp',
                default: `now()`
            }),
            new typeorm_1.TableColumn({
                name: 'updated_at',
                type: 'timestamp',
                default: `now()`
            })
        ]);
        await queryRunner.addColumns('indicators', [
            new typeorm_1.TableColumn({
                name: 'id',
                type: 'serial4'
            }),
            new typeorm_1.TableColumn({
                name: 'serial_number',
                type: 'bigint',
                isNullable: true
            }),
            new typeorm_1.TableColumn({
                name: 'type',
                type: 'varchar',
                length: '255',
                isNullable: true
            }),
            new typeorm_1.TableColumn({
                name: 'indicator',
                type: 'varchar',
                length: '255',
                isNullable: true
            }),
            new typeorm_1.TableColumn({
                name: 'description',
                type: 'text',
                isNullable: true
            }),
            new typeorm_1.TableColumn({
                name: 'supporting_data',
                type: 'varchar',
                length: '255',
                isNullable: true
            }),
            new typeorm_1.TableColumn({
                name: 'file_type',
                type: 'varchar',
                length: '255',
                isNullable: true
            }),
            new typeorm_1.TableColumn({
                name: 'document_form',
                type: 'varchar',
                length: '255',
                isNullable: true
            }),
            new typeorm_1.TableColumn({
                name: 'file_format',
                type: 'varchar',
                length: '255',
                isNullable: true
            }),
            new typeorm_1.TableColumn({
                name: 'value',
                type: 'varchar',
                length: '255',
                isNullable: true
            }),
            new typeorm_1.TableColumn({
                name: 'indicator_type',
                type: 'varchar',
                length: '255',
                isNullable: true
            }),
            new typeorm_1.TableColumn({
                name: 'group',
                type: 'varchar',
                length: '255',
                isNullable: true
            }),
            new typeorm_1.TableColumn({
                name: 'parent_id',
                type: 'varchar',
                length: '255',
                isNullable: true
            }),
            new typeorm_1.TableColumn({
                name: 'mandatory',
                type: 'varchar',
                length: '255',
                isNullable: true
            }),
            new typeorm_1.TableColumn({
                name: 'created_at',
                type: 'timestamp',
                default: 'now()'
            }),
            new typeorm_1.TableColumn({
                name: 'updated_at',
                type: 'timestamp',
                default: 'now()'
            })
        ]);
    }
}
exports.ChangeIndicatorTable1688115174377 = ChangeIndicatorTable1688115174377;
