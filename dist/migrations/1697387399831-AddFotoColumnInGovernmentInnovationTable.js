"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.AddFotoColumnInGovernmentInnovationTable1697387399831 = void 0;
const typeorm_1 = require("typeorm");
class AddFotoColumnInGovernmentInnovationTable1697387399831 {
    async up(queryRunner) {
        await queryRunner.addColumn("government_innovations", new typeorm_1.TableColumn({
            name: "foto_id",
            type: "bigint",
            isNullable: true,
        }));
        await queryRunner.createForeignKey("government_innovations", new typeorm_1.TableForeignKey({
            name: "fk_foto",
            columnNames: ["foto_id"],
            referencedColumnNames: ["id"],
            referencedTableName: "files",
        }));
    }
    async down(queryRunner) {
        await queryRunner.dropForeignKey("government_innovations", new typeorm_1.TableForeignKey({
            name: "fk_foto",
            columnNames: ["foto_id"],
            referencedColumnNames: ["id"],
            referencedTableName: "files",
        }));
        await queryRunner.dropColumn("government_innovations", new typeorm_1.TableColumn({
            name: "foto_id",
            type: "bigint",
            isNullable: true,
        }));
    }
}
exports.AddFotoColumnInGovernmentInnovationTable1697387399831 = AddFotoColumnInGovernmentInnovationTable1697387399831;
