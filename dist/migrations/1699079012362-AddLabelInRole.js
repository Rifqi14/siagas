"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.AddLabelInRole1699079012362 = void 0;
const typeorm_1 = require("typeorm");
class AddLabelInRole1699079012362 {
    async up(queryRunner) {
        await queryRunner.addColumn('roles', new typeorm_1.TableColumn({ name: 'label', type: 'varchar', isNullable: true }));
    }
    async down(queryRunner) {
        await queryRunner.dropColumn('roles', new typeorm_1.TableColumn({ name: 'label', type: 'varchar', isNullable: true }));
    }
}
exports.AddLabelInRole1699079012362 = AddLabelInRole1699079012362;
