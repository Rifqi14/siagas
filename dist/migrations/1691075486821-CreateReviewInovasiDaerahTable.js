"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.CreateReviewInovasiDaerahTable1691075486821 = void 0;
const typeorm_1 = require("typeorm");
class CreateReviewInovasiDaerahTable1691075486821 {
    async up(queryRunner) {
        await queryRunner.createTable(new typeorm_1.Table({
            name: 'review_inovasi_daerah',
            columns: [
                {
                    name: 'id',
                    type: 'serial',
                    isPrimary: true,
                    primaryKeyConstraintName: 'pk_review_inovasi_daerah'
                },
                {
                    name: 'random_number',
                    type: 'bigint',
                    isNullable: true
                },
                {
                    name: 'skor',
                    type: 'float',
                    default: 0
                },
                {
                    name: 'status',
                    type: 'varchar',
                    isNullable: true
                },
                { name: 'inovasi_id', type: 'bigint', isNullable: true },
                { name: 'created_by', type: 'varchar' },
                { name: 'updated_by', type: 'varchar' },
                { name: 'created_at', type: 'timestamp', default: `now()` },
                { name: 'updated_at', type: 'timestamp', default: `now()` }
            ],
            foreignKeys: [
                {
                    columnNames: ['inovasi_id'],
                    referencedColumnNames: ['id'],
                    referencedTableName: 'government_innovations',
                    onDelete: 'cascade'
                },
                {
                    columnNames: ['created_by'],
                    referencedColumnNames: ['username'],
                    referencedTableName: 'users'
                },
                {
                    columnNames: ['updated_by'],
                    referencedColumnNames: ['username'],
                    referencedTableName: 'users'
                }
            ]
        }), true);
    }
    async down(queryRunner) {
        await queryRunner.dropTable(new typeorm_1.Table({ name: 'review_inovasi_daerah' }), true);
    }
}
exports.CreateReviewInovasiDaerahTable1691075486821 = CreateReviewInovasiDaerahTable1691075486821;
