"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.AddEmailColumnToUserTable1678658467059 = void 0;
const typeorm_1 = require("typeorm");
class AddEmailColumnToUserTable1678658467059 {
    async up(queryRunner) {
        await queryRunner.addColumns('users', [
            new typeorm_1.TableColumn({ name: 'email', type: 'varchar', length: '255' }),
            new typeorm_1.TableColumn({
                name: 'full_name',
                type: 'varchar',
                length: '255',
                isNullable: true
            }),
            new typeorm_1.TableColumn({
                name: 'nickname',
                type: 'varchar',
                length: '255',
                isNullable: true
            })
        ]);
        await queryRunner.createIndices('users', [
            new typeorm_1.TableIndex({
                columnNames: ['email'],
                name: 'idx_users_email',
                isUnique: true
            }),
            new typeorm_1.TableIndex({
                columnNames: ['nickname'],
                name: 'idx_users_nickname'
            }),
            new typeorm_1.TableIndex({
                columnNames: ['full_name'],
                name: 'idx_users_full_name'
            })
        ]);
    }
    async down(queryRunner) {
        await queryRunner.dropIndex('users', 'idx_users_email');
        await queryRunner.dropIndex('users', 'idx_users_nickname');
        await queryRunner.dropIndex('users', 'idx_users_full_name');
        await queryRunner.dropColumns('users', ['email', 'full_name', 'nickname']);
    }
}
exports.AddEmailColumnToUserTable1678658467059 = AddEmailColumnToUserTable1678658467059;
