"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.CreateRegionalApparatusTable1678653781946 = void 0;
const typeorm_1 = require("typeorm");
class CreateRegionalApparatusTable1678653781946 {
    async up(queryRunner) {
        await queryRunner.createTable(new typeorm_1.Table({
            name: 'regional_apparatus',
            columns: [
                {
                    name: 'id',
                    type: 'serial4'
                },
                {
                    name: 'name',
                    type: 'varchar',
                    length: '255'
                },
                {
                    name: 'created_at',
                    type: 'timestamp',
                    default: 'now()'
                },
                {
                    name: 'created_by',
                    type: 'varchar',
                    length: '255',
                    isNullable: true
                },
                {
                    name: 'updated_at',
                    type: 'timestamp',
                    default: 'now()'
                },
                {
                    name: 'updated_by',
                    type: 'varchar',
                    length: '255',
                    isNullable: true
                }
            ]
        }), true);
        await queryRunner.createPrimaryKey('regional_apparatus', ['id'], 'pk_regional_apparatus');
        await queryRunner.createForeignKeys('regional_apparatus', [
            new typeorm_1.TableForeignKey({
                columnNames: ['created_by'],
                referencedColumnNames: ['username'],
                referencedTableName: 'users',
                name: 'fk_regional_apparatus_created_by',
                onDelete: 'set null'
            }),
            new typeorm_1.TableForeignKey({
                columnNames: ['updated_by'],
                referencedColumnNames: ['username'],
                referencedTableName: 'users',
                name: 'fk_regional_apparatus_updated_by',
                onDelete: 'set null'
            })
        ]);
        await queryRunner.createIndex('regional_apparatus', new typeorm_1.TableIndex({
            columnNames: ['name'],
            name: 'fk_regional_apparatus_name'
        }));
    }
    async down(queryRunner) {
        await queryRunner.dropTable('regional_apparatus', true);
    }
}
exports.CreateRegionalApparatusTable1678653781946 = CreateRegionalApparatusTable1678653781946;
