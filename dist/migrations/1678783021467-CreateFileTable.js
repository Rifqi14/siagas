"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.CreateFileTable1678783021467 = void 0;
const typeorm_1 = require("typeorm");
class CreateFileTable1678783021467 {
    async up(queryRunner) {
        await queryRunner.createTable(new typeorm_1.Table({
            name: 'files',
            columns: [
                {
                    name: 'id',
                    type: 'serial8',
                    primaryKeyConstraintName: 'pk_files',
                    isPrimary: true
                },
                { name: 'name', type: 'varchar', length: '255' },
                { name: 'path', type: 'varchar', length: '255' },
                { name: 'extension', type: 'varchar', length: '255' },
                { name: 'size', type: 'varchar', length: '255' },
                { name: 'uploaded_by', type: 'varchar', length: '255' },
                { name: 'created_at', type: 'timestamp', default: 'now()' },
                { name: 'updated_at', type: 'timestamp', default: 'now()' }
            ],
            indices: [{ name: 'fk_files_name', columnNames: ['name'] }]
        }), true);
    }
    async down(queryRunner) {
        await queryRunner.dropTable(new typeorm_1.Table({ name: 'files' }), true);
    }
}
exports.CreateFileTable1678783021467 = CreateFileTable1678783021467;
