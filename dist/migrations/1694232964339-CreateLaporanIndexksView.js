"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.CreateLaporanIndexksView1694232964339 = void 0;
class CreateLaporanIndexksView1694232964339 {
    async up(queryRunner) {
        await queryRunner.query(`create or replace view laporan_indeks as SELECT
                pp.id as pemda_id,
                pp.nama_daerah,
                pp.opd_yang_menangani,
                (
                  SELECT
                    count(gi_1.id) AS count
                  FROM
                    government_innovations gi_1
                  WHERE
                    gi_1.pemda_id = pp.id
                ) AS jumlah_inovasi,
                sum(
                  CASE
                    WHEN lower(gi.innovation_phase::text) = 'inisiatif'::text THEN 50
                    WHEN lower(gi.innovation_phase::text) = 'uji coba'::text THEN 102
                    WHEN lower(gi.innovation_phase::text) = 'penerapan'::text THEN 105
                    ELSE 0
                  END
                ) AS total_skor_mandiri,
                CASE
                  WHEN count(gi.id) > 0 THEN sum(
                    CASE
                      WHEN lower(gi.innovation_phase::text) = 'inisiatif'::text THEN 50
                      WHEN lower(gi.innovation_phase::text) = 'uji coba'::text THEN 102
                      WHEN lower(gi.innovation_phase::text) = 'penerapan'::text THEN 105
                      ELSE 0
                    END
                  )::double precision / (
                    (
                      SELECT
                        count(gi_1.id) AS count
                      FROM
                        government_innovations gi_1
                      WHERE
                        gi_1.pemda_id = pp.id
                    )
                  )::double precision
                  ELSE 0::double precision
                END AS nilai_indeks,
                count(iif.id) AS total_file,
                CASE
                  WHEN count(iif.id) > 10 THEN 'Sangat Inovatif'::text
                  WHEN count(iif.id) <= 10
                  AND count(iif.id) >= 6 THEN 'Inovatif'::text
                  WHEN count(iif.id) <= 5
                  AND count(iif.id) > 0 THEN 'Kurang Inovatif'::text
                  ELSE 'Tidak Inovatif'::text
                END AS predikat,
                row_number() OVER (
                  ORDER BY
                    (
                      CASE
                        WHEN count(gi.id) > 0 THEN sum(
                          CASE
                            WHEN lower(gi.innovation_phase::text) = 'inisiatif'::text THEN 50
                            WHEN lower(gi.innovation_phase::text) = 'uji coba'::text THEN 102
                            WHEN lower(gi.innovation_phase::text) = 'penerapan'::text THEN 105
                            ELSE 0
                          END
                        )::double precision / (
                          (
                            SELECT
                              count(gi_1.id) AS count
                            FROM
                              government_innovations gi_1
                            WHERE
                              gi_1.pemda_id = pp.id
                          )
                        )::double precision
                        ELSE 0::double precision
                      END
                    ) DESC
                ) AS indeks,
                pp.nominator,
                pp.created_by
              FROM
                profil_pemda pp
                LEFT JOIN government_innovations gi ON gi.pemda_id = pp.id
                LEFT JOIN inovasi_indikator ii ON ii.inovasi_id = gi.id
                LEFT JOIN inovasi_indikator_file iif ON iif.inovasi_indikator_id = ii.id
              GROUP BY
                pp.nama_daerah,
                pp.id;`);
    }
    async down(queryRunner) {
        await queryRunner.query(`drop view laporan_indeks`);
    }
}
exports.CreateLaporanIndexksView1694232964339 = CreateLaporanIndexksView1694232964339;
