"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.CreateClusterTable1678762165098 = void 0;
const typeorm_1 = require("typeorm");
class CreateClusterTable1678762165098 {
    async up(queryRunner) {
        await queryRunner.createTable(new typeorm_1.Table({
            name: 'clusters',
            columns: [
                {
                    name: 'id',
                    type: 'serial8'
                },
                {
                    name: 'name',
                    type: 'varchar',
                    length: '255'
                },
                {
                    name: 'created_at',
                    type: 'timestamp',
                    default: 'now()'
                },
                {
                    name: 'updated_at',
                    type: 'timestamp',
                    default: 'now()'
                }
            ]
        }), true);
        await queryRunner.createPrimaryKey('clusters', ['id'], 'pk_clusters');
    }
    async down(queryRunner) {
        await queryRunner.dropTable('clusters', true);
    }
}
exports.CreateClusterTable1678762165098 = CreateClusterTable1678762165098;
