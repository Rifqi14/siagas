import { MigrationInterface, QueryRunner } from 'typeorm';
export declare class AddEmailColumnToUserTable1678658467059 implements MigrationInterface {
    up(queryRunner: QueryRunner): Promise<void>;
    down(queryRunner: QueryRunner): Promise<void>;
}
