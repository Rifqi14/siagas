"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.CreateEvaluasiInovasiDaerahTable1691567938146 = void 0;
const typeorm_1 = require("typeorm");
class CreateEvaluasiInovasiDaerahTable1691567938146 {
    async up(queryRunner) {
        await queryRunner.createTable(new typeorm_1.Table({
            name: 'evaluasi_inovasi_daerah',
            columns: [
                {
                    name: 'id',
                    type: 'bigserial',
                    isPrimary: true,
                    primaryKeyConstraintName: 'pk_evaluasi_inovasi_daerah'
                },
                { name: 'document_id', type: 'bigint', isNullable: true },
                {
                    name: 'inovasi_indikator_id',
                    type: 'bigint',
                    isNullable: true
                },
                { name: 'data_saat_ini', type: 'varchar', isNullable: true },
                { name: 'catatan', type: 'varchar', isNullable: true },
                { name: 'keterangan', type: 'varchar', isNullable: true },
                { name: 'tentang', type: 'varchar', isNullable: true },
                { name: 'nomor_surat', type: 'varchar', isNullable: true },
                { name: 'nomor', type: 'varchar', isNullable: true },
                { name: 'tanggal_surat', type: 'date', isNullable: true },
                { name: 'created_by', type: 'varchar' },
                { name: 'updated_by', type: 'varchar' },
                { name: 'created_at', type: 'timestamp', default: `now()` },
                { name: 'updated_at', type: 'timestamp', default: `now()` }
            ],
            foreignKeys: [
                {
                    columnNames: ['inovasi_indikator_id'],
                    referencedColumnNames: ['id'],
                    referencedTableName: 'inovasi_indikator',
                    onDelete: 'cascade'
                },
                {
                    columnNames: ['document_id'],
                    referencedColumnNames: ['id'],
                    referencedTableName: 'files',
                    onDelete: 'cascade'
                },
                {
                    columnNames: ['created_by'],
                    referencedColumnNames: ['username'],
                    referencedTableName: 'users'
                },
                {
                    columnNames: ['updated_by'],
                    referencedColumnNames: ['username'],
                    referencedTableName: 'users'
                }
            ]
        }), true);
    }
    async down(queryRunner) {
        await queryRunner.dropTable(new typeorm_1.Table({ name: 'evaluasi_inovasi_daerah' }), true);
    }
}
exports.CreateEvaluasiInovasiDaerahTable1691567938146 = CreateEvaluasiInovasiDaerahTable1691567938146;
