"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.CreateGovernmentInnovationTable1679604402590 = void 0;
const typeorm_1 = require("typeorm");
class CreateGovernmentInnovationTable1679604402590 {
    async up(queryRunner) {
        await queryRunner.createTable(new typeorm_1.Table({
            name: 'government_innovations',
            columns: [
                {
                    name: 'id',
                    type: 'serial8',
                    isPrimary: true,
                    primaryKeyConstraintName: 'pk_government_innovations'
                },
                {
                    name: 'government_name',
                    type: 'varchar',
                    isNullable: true
                },
                {
                    name: 'created_by',
                    type: 'varchar',
                    isNullable: true
                },
                {
                    name: 'innovation_name',
                    type: 'varchar',
                    isNullable: true
                },
                {
                    name: 'innovation_phase',
                    type: 'varchar',
                    isNullable: true
                },
                {
                    name: 'innovation_initiator',
                    type: 'varchar',
                    isNullable: true
                },
                {
                    name: 'innovation_type',
                    type: 'varchar',
                    isNullable: true
                },
                {
                    name: 'innovation_form',
                    type: 'varchar',
                    isNullable: true
                },
                {
                    name: 'thematic',
                    type: 'varchar',
                    isNullable: true
                },
                {
                    name: 'first_field',
                    type: 'varchar',
                    isNullable: true
                },
                {
                    name: 'other_fields',
                    type: 'varchar',
                    isNullable: true
                },
                {
                    name: 'trial_time',
                    type: 'date',
                    isNullable: true
                },
                {
                    name: 'implementation_time',
                    type: 'date',
                    isNullable: true
                },
                {
                    name: 'design',
                    type: 'text',
                    isNullable: true
                },
                {
                    name: 'purpose',
                    type: 'text',
                    isNullable: true
                },
                {
                    name: 'benefit',
                    type: 'text',
                    isNullable: true
                },
                {
                    name: 'result',
                    type: 'text',
                    isNullable: true
                },
                {
                    name: 'budget_file_id',
                    type: 'bigint',
                    isNullable: true
                },
                {
                    name: 'profile_file_id',
                    type: 'bigint',
                    isNullable: true
                },
                {
                    name: 'created_at',
                    type: 'timestamp',
                    default: 'now()'
                },
                {
                    name: 'updated_at',
                    type: 'timestamp',
                    default: 'now()'
                }
            ],
            foreignKeys: [
                {
                    name: 'fk_budget',
                    columnNames: ['budget_file_id'],
                    referencedColumnNames: ['id'],
                    referencedTableName: 'files'
                },
                {
                    name: 'fk_profile',
                    columnNames: ['profile_file_id'],
                    referencedColumnNames: ['id'],
                    referencedTableName: 'files'
                }
            ]
        }), true);
    }
    async down(queryRunner) {
        await queryRunner.dropTable('government_innovations', true);
    }
}
exports.CreateGovernmentInnovationTable1679604402590 = CreateGovernmentInnovationTable1679604402590;
