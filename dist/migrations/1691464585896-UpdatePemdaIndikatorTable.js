"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.UpdatePemdaIndikatorTable1691464585896 = void 0;
const typeorm_1 = require("typeorm");
class UpdatePemdaIndikatorTable1691464585896 {
    async up(queryRunner) {
        await queryRunner.addColumns('pemda_indikator', [
            new typeorm_1.TableColumn({
                name: 'keterangan',
                type: 'text',
                isNullable: true
            }),
            new typeorm_1.TableColumn({
                name: 'catatan',
                type: 'text',
                isNullable: true
            })
        ]);
    }
    async down(queryRunner) {
        await queryRunner.dropColumns('pemda_indikator', [
            new typeorm_1.TableColumn({
                name: 'keterangan',
                type: 'text',
                isNullable: true
            }),
            new typeorm_1.TableColumn({
                name: 'catatan',
                type: 'text',
                isNullable: true
            })
        ]);
    }
}
exports.UpdatePemdaIndikatorTable1691464585896 = UpdatePemdaIndikatorTable1691464585896;
