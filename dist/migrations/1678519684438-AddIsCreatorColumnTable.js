"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.AddIsCreatorColumnTable1678519684438 = void 0;
const typeorm_1 = require("typeorm");
class AddIsCreatorColumnTable1678519684438 {
    async up(queryRunner) {
        await queryRunner.addColumn('roles', new typeorm_1.TableColumn({
            name: 'is_creator',
            type: 'bpchar',
            enum: ['y', 't'],
            default: `'t'`
        }));
    }
    async down(queryRunner) {
        await queryRunner.dropColumn('roles', 'is_creator');
    }
}
exports.AddIsCreatorColumnTable1678519684438 = AddIsCreatorColumnTable1678519684438;
