"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.CreateRoleTable1678180735650 = void 0;
const typeorm_1 = require("typeorm");
class CreateRoleTable1678180735650 {
    async up(queryRunner) {
        await queryRunner.createTable(new typeorm_1.Table({
            name: 'roles',
            columns: [
                {
                    name: 'id',
                    type: 'serial4',
                    isPrimary: true
                },
                {
                    name: 'name',
                    type: 'varchar',
                    length: '255'
                },
                {
                    name: 'created_at',
                    type: 'timestamp',
                    default: 'now()'
                },
                {
                    name: 'updated_at',
                    type: 'timestamp',
                    default: 'now()'
                }
            ]
        }), true);
        await queryRunner.createIndex('roles', new typeorm_1.TableIndex({
            columnNames: ['name'],
            name: 'idx_roles_name'
        }));
    }
    async down(queryRunner) {
        await queryRunner.dropTable('roles', true);
    }
}
exports.CreateRoleTable1678180735650 = CreateRoleTable1678180735650;
