"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.UpdateEvaluasiInovasiDaerahTable1696776584663 = void 0;
const typeorm_1 = require("typeorm");
class UpdateEvaluasiInovasiDaerahTable1696776584663 {
    async up(queryRunner) {
        await queryRunner.dropColumns('evaluasi_inovasi_daerah', [
            'document_category_id',
            'judul',
            'document_id',
            'nomor_surat',
            'nomor',
            'tanggal_surat'
        ]);
        await queryRunner.addColumns('evaluasi_inovasi_daerah', [
            new typeorm_1.TableColumn({ name: 'kategori', type: 'varchar', isNullable: true })
        ]);
    }
    async down(queryRunner) {
        await queryRunner.addColumns('evaluasi_inovasi_daerah', [
            new typeorm_1.TableColumn({
                name: 'judul',
                type: 'varchar',
                isNullable: true
            }),
            new typeorm_1.TableColumn({
                name: 'document_category_id',
                type: 'bigint',
                isNullable: true
            }),
            new typeorm_1.TableColumn({
                name: 'document_id',
                type: 'bigint',
                isNullable: true
            }),
            new typeorm_1.TableColumn({
                name: 'nomor_surat',
                type: 'varchar',
                isNullable: true
            }),
            new typeorm_1.TableColumn({
                name: 'nomor',
                type: 'varchar',
                isNullable: true
            }),
            new typeorm_1.TableColumn({
                name: 'tanggal_surat',
                type: 'date',
                isNullable: true
            })
        ]);
        await queryRunner.dropColumn('evaluasi_inovasi_daerah', 'kategori');
        await queryRunner.createForeignKeys('evaluasi_inovasi_daerah', [
            new typeorm_1.TableForeignKey({
                columnNames: ['document_category_id'],
                referencedColumnNames: ['id'],
                referencedTableName: 'document_categories'
            }),
            new typeorm_1.TableForeignKey({
                columnNames: ['document_id'],
                referencedColumnNames: ['id'],
                referencedTableName: 'documents'
            })
        ]);
    }
}
exports.UpdateEvaluasiInovasiDaerahTable1696776584663 = UpdateEvaluasiInovasiDaerahTable1696776584663;
