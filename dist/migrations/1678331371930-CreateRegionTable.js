"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.CreateRegionTable1678331371930 = void 0;
const typeorm_1 = require("typeorm");
class CreateRegionTable1678331371930 {
    async up(queryRunner) {
        await queryRunner.createTable(new typeorm_1.Table({
            name: 'regions',
            columns: [
                {
                    name: 'id',
                    type: 'serial4'
                },
                {
                    name: 'name',
                    type: 'varchar',
                    length: '255'
                },
                {
                    name: 'created_at',
                    type: 'timestamp',
                    default: 'now()'
                },
                {
                    name: 'updated_at',
                    type: 'timestamp',
                    default: 'now()'
                }
            ]
        }), true);
        await queryRunner.createPrimaryKey('regions', ['id'], 'pk_regions');
        await queryRunner.createIndex('regions', new typeorm_1.TableIndex({
            columnNames: ['name'],
            name: 'idx_regions_name'
        }));
    }
    async down(queryRunner) {
        await queryRunner.dropTable('regions', true);
    }
}
exports.CreateRegionTable1678331371930 = CreateRegionTable1678331371930;
