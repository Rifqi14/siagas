"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.AddDeadlineColumnToGovernmentSectorTable1680645415390 = void 0;
const typeorm_1 = require("typeorm");
class AddDeadlineColumnToGovernmentSectorTable1680645415390 {
    async up(queryRunner) {
        await queryRunner.addColumn('government_sectors', new typeorm_1.TableColumn({
            name: 'deadline',
            type: 'date'
        }));
    }
    async down(queryRunner) {
        await queryRunner.dropColumn('government_sectors', 'deadline');
    }
}
exports.AddDeadlineColumnToGovernmentSectorTable1680645415390 = AddDeadlineColumnToGovernmentSectorTable1680645415390;
