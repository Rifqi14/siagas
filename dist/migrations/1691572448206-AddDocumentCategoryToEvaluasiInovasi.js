"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.AddDocumentCategoryToEvaluasiInovasi1691572448206 = void 0;
const typeorm_1 = require("typeorm");
class AddDocumentCategoryToEvaluasiInovasi1691572448206 {
    async up(queryRunner) {
        await queryRunner.addColumns('evaluasi_inovasi_daerah', [
            new typeorm_1.TableColumn({
                name: 'judul',
                type: 'varchar',
                isNullable: true
            }),
            new typeorm_1.TableColumn({
                name: 'document_category_id',
                type: 'bigint',
                isNullable: true
            })
        ]);
        await queryRunner.createForeignKey('evaluasi_inovasi_daerah', new typeorm_1.TableForeignKey({
            columnNames: ['document_category_id'],
            referencedColumnNames: ['id'],
            referencedTableName: 'document_categories'
        }));
    }
    async down(queryRunner) {
        await queryRunner.dropForeignKey('evaluasi_inovasi_daerah', new typeorm_1.TableForeignKey({
            columnNames: ['document_category_id'],
            referencedColumnNames: ['id'],
            referencedTableName: 'document_categories'
        }));
        await queryRunner.dropColumns('evaluasi_inovasi_daerah', [
            new typeorm_1.TableColumn({
                name: 'judul',
                type: 'varchar',
                isNullable: true
            }),
            new typeorm_1.TableColumn({
                name: 'document_category_id',
                type: 'bigint',
                isNullable: true
            })
        ]);
    }
}
exports.AddDocumentCategoryToEvaluasiInovasi1691572448206 = AddDocumentCategoryToEvaluasiInovasi1691572448206;
