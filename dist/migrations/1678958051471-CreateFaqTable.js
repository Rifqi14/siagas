"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.CreateFaqTable1678958051471 = void 0;
const typeorm_1 = require("typeorm");
class CreateFaqTable1678958051471 {
    async up(queryRunner) {
        await queryRunner.createTable(new typeorm_1.Table({
            name: 'faq',
            columns: [
                {
                    name: 'id',
                    type: 'serial8',
                    isPrimary: true,
                    primaryKeyConstraintName: 'pk_faq'
                },
                {
                    name: 'question',
                    type: 'text'
                },
                {
                    name: 'answer',
                    type: 'text'
                },
                {
                    name: 'created_at',
                    type: 'timestamp',
                    default: 'now()'
                },
                {
                    name: 'updated_at',
                    type: 'timestamp',
                    default: 'now()'
                }
            ]
        }), true);
    }
    async down(queryRunner) {
        await queryRunner.dropTable('faq', true);
    }
}
exports.CreateFaqTable1678958051471 = CreateFaqTable1678958051471;
