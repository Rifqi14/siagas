import { MigrationInterface, QueryRunner } from 'typeorm';
export declare class AddIsCreatorColumnTable1678519684438 implements MigrationInterface {
    up(queryRunner: QueryRunner): Promise<void>;
    down(queryRunner: QueryRunner): Promise<void>;
}
