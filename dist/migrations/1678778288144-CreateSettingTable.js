"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.CreateSettingTable1678778288144 = void 0;
const typeorm_1 = require("typeorm");
class CreateSettingTable1678778288144 {
    async up(queryRunner) {
        await queryRunner.createTable(new typeorm_1.Table({
            name: 'settings',
            columns: [
                {
                    name: 'id',
                    type: 'serial8',
                    isPrimary: true,
                    primaryKeyConstraintName: 'pk_settings'
                },
                {
                    name: 'key',
                    type: 'varchar',
                    length: '255'
                },
                {
                    name: 'value',
                    type: 'varchar',
                    length: '255'
                },
                {
                    name: 'created_at',
                    type: 'timestamp',
                    default: 'now()'
                },
                {
                    name: 'updated_at',
                    type: 'timestamp',
                    default: 'now()'
                }
            ],
            uniques: [
                {
                    columnNames: ['key'],
                    name: 'unique_settings_key'
                }
            ],
            indices: [
                {
                    columnNames: ['key'],
                    name: 'idx_settings_key'
                }
            ]
        }), true);
    }
    async down(queryRunner) {
        await queryRunner.dropTable('settings', true);
    }
}
exports.CreateSettingTable1678778288144 = CreateSettingTable1678778288144;
