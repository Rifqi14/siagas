"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.AddUpdateByInUserEntity1699073022930 = void 0;
const typeorm_1 = require("typeorm");
class AddUpdateByInUserEntity1699073022930 {
    async up(queryRunner) {
        await queryRunner.addColumn('users', new typeorm_1.TableColumn({
            name: 'created_by',
            type: 'bigint',
            isNullable: true,
        }));
        await queryRunner.createForeignKey('users', new typeorm_1.TableForeignKey({
            columnNames: ['created_by'],
            referencedColumnNames: ['id'],
            referencedTableName: 'users',
            onDelete: 'set null',
            name: 'fk_created_by_users',
        }));
    }
    async down(queryRunner) {
        await queryRunner.dropForeignKey('users', 'fk_created_by_users');
        await queryRunner.dropColumn('users', 'created_by');
    }
}
exports.AddUpdateByInUserEntity1699073022930 = AddUpdateByInUserEntity1699073022930;
