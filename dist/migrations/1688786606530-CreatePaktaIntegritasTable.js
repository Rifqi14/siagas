"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.CreatePaktaIntegritasTable1688786606530 = void 0;
const typeorm_1 = require("typeorm");
class CreatePaktaIntegritasTable1688786606530 {
    async up(queryRunner) {
        await queryRunner.createTable(new typeorm_1.Table({
            name: 'pakta_integritas',
            columns: [
                { name: 'id', type: 'bigserial', isPrimary: true },
                { name: 'user_id', type: 'bigint', isNullable: true },
                { name: 'upload_id', type: 'bigint', isNullable: true },
                { name: 'created_by', type: 'varchar' },
                { name: 'updated_by', type: 'varchar' },
                { name: 'created_at', type: 'timestamp', default: `now()` },
                { name: 'updated_at', type: 'timestamp', default: `now()` }
            ],
            foreignKeys: [
                {
                    columnNames: ['user_id'],
                    referencedColumnNames: ['id'],
                    referencedTableName: 'users'
                },
                {
                    columnNames: ['created_by'],
                    referencedColumnNames: ['username'],
                    referencedTableName: 'users'
                },
                {
                    columnNames: ['updated_by'],
                    referencedColumnNames: ['username'],
                    referencedTableName: 'users'
                },
                {
                    columnNames: ['upload_id'],
                    referencedColumnNames: ['id'],
                    referencedTableName: 'files'
                }
            ]
        }), true);
    }
    async down(queryRunner) {
        await queryRunner.dropTable(new typeorm_1.Table({ name: 'pakta_integritas' }), true);
    }
}
exports.CreatePaktaIntegritasTable1688786606530 = CreatePaktaIntegritasTable1688786606530;
