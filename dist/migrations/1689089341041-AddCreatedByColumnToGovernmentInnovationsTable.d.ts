import { MigrationInterface, QueryRunner } from 'typeorm';
export declare class AddCreatedByColumnToGovernmentInnovationsTable1689089341041 implements MigrationInterface {
    up(queryRunner: QueryRunner): Promise<void>;
    down(queryRunner: QueryRunner): Promise<void>;
}
