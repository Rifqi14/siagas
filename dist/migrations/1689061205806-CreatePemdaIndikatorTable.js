"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.CreatePemdaIndikatorTable1689061205806 = void 0;
const typeorm_1 = require("typeorm");
class CreatePemdaIndikatorTable1689061205806 {
    async up(queryRunner) {
        await queryRunner.createTable(new typeorm_1.Table({
            name: 'pemda_indikator',
            columns: [
                { name: 'id', type: 'bigserial', isPrimary: true },
                { name: 'pemda_id', type: 'bigint', isNullable: true },
                { name: 'indikator_id', type: 'bigint', isNullable: true },
                { name: 'informasi', type: 'varchar', isNullable: true },
                { name: 'nilai', type: 'numeric', isNullable: true },
                { name: 'nilai_sebelum', type: 'numeric', isNullable: true },
                { name: 'nilai_sesudah', type: 'numeric', isNullable: true },
                { name: 'created_by', type: 'varchar' },
                { name: 'updated_by', type: 'varchar' },
                { name: 'created_at', type: 'timestamp', default: `now()` },
                { name: 'updated_at', type: 'timestamp', default: `now()` }
            ],
            foreignKeys: [
                {
                    columnNames: ['pemda_id'],
                    referencedColumnNames: ['id'],
                    referencedTableName: 'profil_pemda',
                    onDelete: 'cascade'
                },
                {
                    columnNames: ['indikator_id'],
                    referencedColumnNames: ['id'],
                    referencedTableName: 'indicators',
                    onDelete: 'cascade'
                },
                {
                    columnNames: ['created_by'],
                    referencedColumnNames: ['username'],
                    referencedTableName: 'users'
                },
                {
                    columnNames: ['updated_by'],
                    referencedColumnNames: ['username'],
                    referencedTableName: 'users'
                }
            ]
        }), true);
    }
    async down(queryRunner) {
        await queryRunner.dropTable(new typeorm_1.Table({ name: 'pemda_indikator' }), true);
    }
}
exports.CreatePemdaIndikatorTable1689061205806 = CreatePemdaIndikatorTable1689061205806;
