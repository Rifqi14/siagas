"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.CreateDocumentCategoryTable1679016247399 = void 0;
const typeorm_1 = require("typeorm");
class CreateDocumentCategoryTable1679016247399 {
    async up(queryRunner) {
        await queryRunner.createTable(new typeorm_1.Table({
            name: 'document_categories',
            columns: [
                {
                    name: 'id',
                    type: 'serial8',
                    isPrimary: true,
                    primaryKeyConstraintName: 'pk_document_categories'
                },
                {
                    name: 'name',
                    type: 'varchar',
                    length: '255'
                },
                {
                    name: 'slug',
                    type: 'varchar',
                    length: '255'
                },
                {
                    name: 'created_at',
                    type: 'timestamp',
                    default: 'now()'
                },
                {
                    name: 'updated_at',
                    type: 'timestamp',
                    default: 'now()'
                }
            ],
            indices: [
                {
                    columnNames: ['name'],
                    name: 'idx_name'
                },
                {
                    columnNames: ['slug'],
                    name: 'idx_slug'
                }
            ]
        }), true);
    }
    async down(queryRunner) {
        await queryRunner.dropTable('document_categories', true);
    }
}
exports.CreateDocumentCategoryTable1679016247399 = CreateDocumentCategoryTable1679016247399;
