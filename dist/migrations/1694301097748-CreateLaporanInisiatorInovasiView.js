"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.CreateLaporanInisiatorInovasiView1694301097748 = void 0;
class CreateLaporanInisiatorInovasiView1694301097748 {
    async up(queryRunner) {
        await queryRunner.query(`create or replace view laporan_inisiator_inovasi as select
            gi.pemda_id,
            gi.innovation_initiator as inisiator_inovasi,
            (select count(rid.id) from review_inovasi_daerah rid left join government_innovations gi2 on gi2.id = rid.inovasi_id where lower(gi2.innovation_initiator) = lower(gi.innovation_initiator) and lower(rid.status) = 'accept' and gi2.pemda_id = gi.pemda_id) total_disetujui,
            (select count(rid.id) from review_inovasi_daerah rid left join government_innovations gi2 on gi2.id = rid.inovasi_id where lower(gi2.innovation_initiator) = lower(gi.innovation_initiator) and (lower(rid.status) = 'pending' or lower(rid.status) = 'rejected') and gi2.pemda_id = gi.pemda_id) total_ditolak,
            (select count(rid.id) from review_inovasi_daerah rid left join government_innovations gi2 on gi2.id = rid.inovasi_id where lower(gi2.innovation_initiator) = lower(gi.innovation_initiator) and gi2.pemda_id = gi.pemda_id) total_keseluruhan
        from profil_pemda pp
        right join government_innovations gi on gi.pemda_id = pp.id
        group by gi.innovation_initiator, gi.pemda_id`);
    }
    async down(queryRunner) {
        await queryRunner.query(`drop view laporan_inisiator_inovasi`);
    }
}
exports.CreateLaporanInisiatorInovasiView1694301097748 = CreateLaporanInisiatorInovasiView1694301097748;
