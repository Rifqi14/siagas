"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.AddCreatedByColumnToGovernmentInnovationsTable1689089341041 = void 0;
const typeorm_1 = require("typeorm");
class AddCreatedByColumnToGovernmentInnovationsTable1689089341041 {
    async up(queryRunner) {
        await queryRunner.addColumns('government_innovations', [
            new typeorm_1.TableColumn({ isNullable: true, name: 'updated_by', type: 'varchar' })
        ]);
        await queryRunner.createForeignKeys('government_innovations', [
            new typeorm_1.TableForeignKey({
                columnNames: ['updated_by'],
                referencedColumnNames: ['username'],
                referencedTableName: 'users'
            })
        ]);
    }
    async down(queryRunner) {
        await queryRunner.dropForeignKeys('government_innovations', [
            new typeorm_1.TableForeignKey({
                columnNames: ['updated_by'],
                referencedColumnNames: ['username'],
                referencedTableName: 'users'
            })
        ]);
        await queryRunner.dropColumns('government_innovations', [
            new typeorm_1.TableColumn({ isNullable: true, name: 'updated_by', type: 'varchar' })
        ]);
    }
}
exports.AddCreatedByColumnToGovernmentInnovationsTable1689089341041 = AddCreatedByColumnToGovernmentInnovationsTable1689089341041;
