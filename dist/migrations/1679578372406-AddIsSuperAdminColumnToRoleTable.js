"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.AddIsSuperAdminColumnToRoleTable1679578372406 = void 0;
const typeorm_1 = require("typeorm");
class AddIsSuperAdminColumnToRoleTable1679578372406 {
    async up(queryRunner) {
        await queryRunner.addColumn('roles', new typeorm_1.TableColumn({
            name: 'is_super_admin',
            type: 'bpchar',
            enum: ['y', 't'],
            default: `'t'`
        }));
    }
    async down(queryRunner) {
        await queryRunner.dropColumn('roles', 'is_super_admin');
    }
}
exports.AddIsSuperAdminColumnToRoleTable1679578372406 = AddIsSuperAdminColumnToRoleTable1679578372406;
