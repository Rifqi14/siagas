"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.UpdateGovernmentInnovationTable1688833567130 = void 0;
const typeorm_1 = require("typeorm");
class UpdateGovernmentInnovationTable1688833567130 {
    async up(queryRunner) {
        await queryRunner.addColumns('government_innovations', [
            new typeorm_1.TableColumn({
                name: 'thematic_detail',
                type: 'text',
                isNullable: true
            }),
            new typeorm_1.TableColumn({
                name: 'government_sector_id',
                type: 'bigint',
                isNullable: true
            })
        ]);
        await queryRunner.createUniqueConstraint('users', new typeorm_1.TableUnique({ columnNames: ['nama_pemda'] }));
        await queryRunner.createForeignKeys('government_innovations', [
            new typeorm_1.TableForeignKey({
                columnNames: ['government_name'],
                referencedColumnNames: ['nama_pemda'],
                referencedTableName: 'users'
            }),
            new typeorm_1.TableForeignKey({
                columnNames: ['government_sector_id'],
                referencedColumnNames: ['id'],
                referencedTableName: 'government_sectors'
            })
        ]);
    }
    async down(queryRunner) {
        await queryRunner.dropForeignKeys('government_innovations', [
            new typeorm_1.TableForeignKey({
                columnNames: ['government_name'],
                referencedColumnNames: ['nama_pemda'],
                referencedTableName: 'users'
            }),
            new typeorm_1.TableForeignKey({
                columnNames: ['government_sector_id'],
                referencedColumnNames: ['id'],
                referencedTableName: 'government_sectors'
            })
        ]);
        await queryRunner.dropUniqueConstraint('users', new typeorm_1.TableUnique({ columnNames: ['nama_pemda'] }));
        await queryRunner.dropColumns('government_innovations', [
            new typeorm_1.TableColumn({
                name: 'thematic_detail',
                type: 'text',
                isNullable: true
            }),
            new typeorm_1.TableColumn({
                name: 'government_sector_id',
                type: 'bigint',
                isNullable: true
            })
        ]);
    }
}
exports.UpdateGovernmentInnovationTable1688833567130 = UpdateGovernmentInnovationTable1688833567130;
