"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.CreatePreviewReviewInovasiDaerahTable1691077843241 = void 0;
const typeorm_1 = require("typeorm");
class CreatePreviewReviewInovasiDaerahTable1691077843241 {
    async up(queryRunner) {
        await queryRunner.createTable(new typeorm_1.Table({
            name: 'preview_review_inovasi_daerah',
            columns: [
                {
                    name: 'id',
                    type: 'bigserial',
                    isPrimary: true,
                    primaryKeyConstraintName: 'pk_preview_review_inovasi_daerah'
                },
                {
                    name: 'komentar',
                    type: 'varchar',
                    isNullable: true
                },
                {
                    name: 'review_inovasi_daerah_id',
                    type: 'bigint',
                    isNullable: true
                },
                { name: 'created_by', type: 'varchar' },
                { name: 'updated_by', type: 'varchar' },
                { name: 'created_at', type: 'timestamp', default: `now()` },
                { name: 'updated_at', type: 'timestamp', default: `now()` }
            ],
            foreignKeys: [
                {
                    columnNames: ['review_inovasi_daerah_id'],
                    referencedColumnNames: ['id'],
                    referencedTableName: 'review_inovasi_daerah',
                    onDelete: 'cascade'
                },
                {
                    columnNames: ['created_by'],
                    referencedColumnNames: ['username'],
                    referencedTableName: 'users'
                },
                {
                    columnNames: ['updated_by'],
                    referencedColumnNames: ['username'],
                    referencedTableName: 'users'
                }
            ]
        }), true);
    }
    async down(queryRunner) {
        await queryRunner.dropTable(new typeorm_1.Table({ name: 'preview_review_inovasi_daerah' }), true);
    }
}
exports.CreatePreviewReviewInovasiDaerahTable1691077843241 = CreatePreviewReviewInovasiDaerahTable1691077843241;
