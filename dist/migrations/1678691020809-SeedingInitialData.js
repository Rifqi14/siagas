"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.SeedingInitialData1678691020809 = void 0;
const role_json_1 = __importDefault(require("../data/seeder/role.json"));
const user_json_1 = __importDefault(require("../data/seeder/user.json"));
const Role_entity_1 = __importDefault(require("../entity/Role.entity"));
const User_entity_1 = __importDefault(require("../entity/User.entity"));
class SeedingInitialData1678691020809 {
    async up(queryRunner) {
        const repo = queryRunner.manager.getRepository(Role_entity_1.default);
        role_json_1.default.forEach(async (value) => {
            const role = await repo.findOneBy({ name: value.name });
            if (role) {
                return;
            }
            await repo
                .insert({ is_creator: value.is_creator, name: value.name })
                .then(val => {
                console.log(`Success seeder ${value.name} data`);
            }, () => {
                console.log(`Error seeder ${value.name} data`);
            });
        });
        const repoUser = queryRunner.manager.getRepository(User_entity_1.default);
        let usersEntity = [];
        for (let i = 0; i < user_json_1.default.length; i++) {
            const user = await repoUser.findOneBy({ username: user_json_1.default[i].username });
            const role = await repo.findOneBy({ name: user_json_1.default[i].role });
            if (!role) {
                return;
            }
            if (user) {
                return;
            }
            const create = repoUser.create({
                username: user_json_1.default[i].username,
                password: user_json_1.default[i].password,
                email: user_json_1.default[i].email,
                nickname: user_json_1.default[i].nama_panggilan,
                full_name: user_json_1.default[i].nama_lengkap,
                role: role
            });
            usersEntity.push(create);
        }
        await repoUser.insert(usersEntity);
    }
    async down(queryRunner) {
        const repoUser = queryRunner.manager.getRepository(User_entity_1.default);
        await user_json_1.default.forEach(async (value) => {
            const user = await repoUser.findOneBy({ username: value.username });
            if (!user) {
                return;
            }
            await repoUser.delete(user.id);
        });
        const repo = queryRunner.manager.getRepository(Role_entity_1.default);
        await role_json_1.default.forEach(async (value) => {
            const role = await repo.findOneBy({ name: value.name });
            if (!role) {
                return;
            }
            await repo.delete(role.id);
        });
    }
}
exports.SeedingInitialData1678691020809 = SeedingInitialData1678691020809;
