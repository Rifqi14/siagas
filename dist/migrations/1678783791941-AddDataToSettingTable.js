"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.AddDataToSettingTable1678783791941 = void 0;
const setting_json_1 = __importDefault(require("../data/seeder/setting.json"));
const Setting_entity_1 = __importDefault(require("../entity/Setting.entity"));
class AddDataToSettingTable1678783791941 {
    async up(queryRunner) {
        const repo = queryRunner.manager.getRepository(Setting_entity_1.default);
        const entites = [];
        for (let i = 0; i < setting_json_1.default.length; i++) {
            const exists = await repo.exist({ where: { key: setting_json_1.default[i].key } });
            if (exists) {
                return;
            }
            const data = repo.create({
                key: setting_json_1.default[i].key,
                value: setting_json_1.default[i].value,
                helper: setting_json_1.default[i].helper
            });
            entites.push(data);
        }
        await repo.insert(entites);
    }
    async down(queryRunner) { }
}
exports.AddDataToSettingTable1678783791941 = AddDataToSettingTable1678783791941;
