"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.AddNamaPemdaToUserTable1688058282677 = void 0;
const typeorm_1 = require("typeorm");
class AddNamaPemdaToUserTable1688058282677 {
    async up(queryRunner) {
        await queryRunner.addColumn('users', new typeorm_1.TableColumn({ name: 'nama_pemda', type: 'varchar', isNullable: true }));
    }
    async down(queryRunner) {
        await queryRunner.dropColumn('users', new typeorm_1.TableColumn({ name: 'nama_pemda', type: 'varchar', isNullable: true }));
    }
}
exports.AddNamaPemdaToUserTable1688058282677 = AddNamaPemdaToUserTable1688058282677;
