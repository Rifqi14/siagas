"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.AddColumnToProfilPemdaTable1692020072236 = void 0;
const typeorm_1 = require("typeorm");
class AddColumnToProfilPemdaTable1692020072236 {
    async up(queryRunner) {
        await queryRunner.addColumns('profil_pemda', [
            new typeorm_1.TableColumn({
                name: 'nama_pemda',
                type: 'varchar',
                isNullable: true
            }),
            new typeorm_1.TableColumn({ name: 'nominator', type: 'varchar', isNullable: true })
        ]);
    }
    async down(queryRunner) {
        await queryRunner.dropColumns('profil_pemda', [
            new typeorm_1.TableColumn({
                name: 'nama_pemda',
                type: 'varchar',
                isNullable: true
            }),
            new typeorm_1.TableColumn({ name: 'nominator', type: 'varchar', isNullable: true })
        ]);
    }
}
exports.AddColumnToProfilPemdaTable1692020072236 = AddColumnToProfilPemdaTable1692020072236;
