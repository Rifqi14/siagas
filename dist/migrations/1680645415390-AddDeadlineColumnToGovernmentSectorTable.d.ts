import { MigrationInterface, QueryRunner } from 'typeorm';
export declare class AddDeadlineColumnToGovernmentSectorTable1680645415390 implements MigrationInterface {
    up(queryRunner: QueryRunner): Promise<void>;
    down(queryRunner: QueryRunner): Promise<void>;
}
