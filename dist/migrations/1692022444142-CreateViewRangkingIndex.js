"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.CreateViewRangkingIndex1692022444142 = void 0;
class CreateViewRangkingIndex1692022444142 {
    async up(queryRunner) {
        await queryRunner.query(`create or replace view rangking_index as
            select
                pp.id,
                pp.nama_daerah,
            (select count(gi.id) from government_innovations gi where gi.pemda_id = pp.id) as jumlah_inovasi,
            sum(
                case
                when lower(gi.innovation_phase) = 'inisiatif' then 50
                    when lower(gi.innovation_phase) = 'uji coba' then 102
                    when lower(gi.innovation_phase) = 'penerapan' then 105
                else 0
                end
            ) as total_skor_mandiri,
            case
                when count(gi.id) > 0 then sum(
                case
                    when lower(gi.innovation_phase) = 'inisiatif' then 50
                    when lower(gi.innovation_phase) = 'uji coba' then 102
                    when lower(gi.innovation_phase) = 'penerapan' then 105
                    else 0
                end
                ) / (select count(gi.id) from government_innovations gi where gi.pemda_id = pp.id)::float
                else 0::float
            end as nilai_indeks,
            count(iif.id) as total_file,
            case
                when count(iif.id) > 10 then 'Sangat Inovatif'
                when count(iif.id) <= 10 and count(iif.id) >= 6 then 'Inovatif'
                when count(iif.id) <= 5 and count(iif.id) > 0 then 'Kurang Inovatif'
                else 'Tidak Inovatif'
            end as predikat,
            row_number () over (
                order by case
                when count(gi.id) > 0 then sum(
                    case
                    when lower(gi.innovation_phase) = 'inisiatif' then 50
                    when lower(gi.innovation_phase) = 'uji coba' then 102
                    when lower(gi.innovation_phase) = 'penerapan' then 105
                    else 0
                    end
                ) / (select count(gi.id) from government_innovations gi where gi.pemda_id = pp.id)::float
                else 0::float
                end desc
                ) as indeks,
            pp.nominator as nominator,
            pp.created_by as created_by
            from profil_pemda pp
            left join government_innovations gi on gi.pemda_id = pp.id
            left join inovasi_indikator ii on ii.inovasi_id = gi.id
            left join inovasi_indikator_file iif on iif.inovasi_indikator_id = ii.id
            group by pp.nama_daerah, pp.id`);
    }
    async down(queryRunner) {
        await queryRunner.query('drop view rangking_index');
    }
}
exports.CreateViewRangkingIndex1692022444142 = CreateViewRangkingIndex1692022444142;
