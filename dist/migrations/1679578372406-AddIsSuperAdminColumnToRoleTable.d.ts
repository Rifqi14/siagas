import { MigrationInterface, QueryRunner } from 'typeorm';
export declare class AddIsSuperAdminColumnToRoleTable1679578372406 implements MigrationInterface {
    up(queryRunner: QueryRunner): Promise<void>;
    down(queryRunner: QueryRunner): Promise<void>;
}
