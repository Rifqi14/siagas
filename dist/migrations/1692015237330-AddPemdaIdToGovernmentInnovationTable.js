"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.AddPemdaIdToGovernmentInnovationTable1692015237330 = void 0;
const typeorm_1 = require("typeorm");
class AddPemdaIdToGovernmentInnovationTable1692015237330 {
    async up(queryRunner) {
        await queryRunner.addColumn('government_innovations', new typeorm_1.TableColumn({ name: 'pemda_id', type: 'bigint', isNullable: true }));
        await queryRunner.createForeignKey('government_innovations', new typeorm_1.TableForeignKey({
            name: 'fk_pemda_id_government_innovations',
            columnNames: ['pemda_id'],
            referencedColumnNames: ['id'],
            referencedTableName: 'profil_pemda',
            onDelete: 'cascade'
        }));
    }
    async down(queryRunner) {
        await queryRunner.dropForeignKey('government_innovations', new typeorm_1.TableForeignKey({
            name: 'fk_pemda_id_government_innovations',
            columnNames: ['pemda_id'],
            referencedColumnNames: ['id'],
            referencedTableName: 'profil_pemda',
            onDelete: 'cascade'
        }));
        await queryRunner.dropColumn('government_innovations', new typeorm_1.TableColumn({ name: 'pemda_id', type: 'bigint', isNullable: true }));
    }
}
exports.AddPemdaIdToGovernmentInnovationTable1692015237330 = AddPemdaIdToGovernmentInnovationTable1692015237330;
