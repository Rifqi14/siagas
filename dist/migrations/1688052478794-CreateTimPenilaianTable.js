"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.CreateTimPenilaianTable1688052478794 = void 0;
const typeorm_1 = require("typeorm");
class CreateTimPenilaianTable1688052478794 {
    async up(queryRunner) {
        await queryRunner.createTable(new typeorm_1.Table({
            name: 'tim_penilaian',
            columns: [
                { name: 'id', type: 'bigserial', isPrimary: true },
                { name: 'asn_username', type: 'varchar', isNullable: true },
                { name: 'nama', type: 'varchar', isNullable: true },
                { name: 'instansi', type: 'varchar', isNullable: true },
                { name: 'created_by', type: 'varchar', isNullable: true },
                { name: 'updated_by', type: 'varchar', isNullable: true },
                { name: 'created_at', type: 'timestamp', default: `now()` },
                { name: 'updated_at', type: 'timestamp', default: `now()` }
            ],
            foreignKeys: [
                {
                    columnNames: ['created_by'],
                    referencedColumnNames: ['username'],
                    referencedTableName: 'users'
                },
                {
                    columnNames: ['updated_by'],
                    referencedColumnNames: ['username'],
                    referencedTableName: 'users'
                }
            ]
        }), true);
    }
    async down(queryRunner) {
        await queryRunner.dropTable(new typeorm_1.Table({ name: 'tim_penilaian' }), true);
    }
}
exports.CreateTimPenilaianTable1688052478794 = CreateTimPenilaianTable1688052478794;
