"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.AddColumToPemdaIndikatorFile1696782503905 = void 0;
const typeorm_1 = require("typeorm");
class AddColumToPemdaIndikatorFile1696782503905 {
    async up(queryRunner) {
        await queryRunner.addColumns('pemda_indikator_file', [
            new typeorm_1.TableColumn({ name: 'pemda_id', type: 'bigint', isNullable: true }),
            new typeorm_1.TableColumn({
                name: 'indikator_id',
                type: 'bigint',
                isNullable: true
            })
        ]);
        await queryRunner.createForeignKeys('pemda_indikator_file', [
            new typeorm_1.TableForeignKey({
                columnNames: ['pemda_id'],
                referencedColumnNames: ['id'],
                referencedTableName: 'profil_pemda'
            }),
            new typeorm_1.TableForeignKey({
                columnNames: ['indikator_id'],
                referencedColumnNames: ['id'],
                referencedTableName: 'indicators'
            })
        ]);
    }
    async down(queryRunner) {
        await queryRunner.dropColumns('pemda_indikator_file', [
            new typeorm_1.TableColumn({ name: 'pemda_id', type: 'bigint', isNullable: true }),
            new typeorm_1.TableColumn({
                name: 'indikator_id',
                type: 'bigint',
                isNullable: true
            })
        ]);
        await queryRunner.dropForeignKeys('pemda_indikator_file', [
            new typeorm_1.TableForeignKey({
                columnNames: ['pemda_id'],
                referencedColumnNames: ['id'],
                referencedTableName: 'profil_pemda'
            }),
            new typeorm_1.TableForeignKey({
                columnNames: ['indikator_id'],
                referencedColumnNames: ['id'],
                referencedTableName: 'indicators'
            })
        ]);
    }
}
exports.AddColumToPemdaIndikatorFile1696782503905 = AddColumToPemdaIndikatorFile1696782503905;
