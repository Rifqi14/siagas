"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.CreateClusterDetailTable1678763517930 = void 0;
const typeorm_1 = require("typeorm");
class CreateClusterDetailTable1678763517930 {
    async up(queryRunner) {
        await queryRunner.createTable(new typeorm_1.Table({
            name: 'cluster_details',
            columns: [
                {
                    name: 'cluster_id',
                    type: 'bigint'
                },
                {
                    name: 'region_id',
                    type: 'bigint'
                },
                {
                    name: 'created_at',
                    type: 'timestamp',
                    default: 'now()'
                },
                {
                    name: 'updated_at',
                    type: 'timestamp',
                    default: 'now()'
                }
            ]
        }), true);
        await queryRunner.createPrimaryKey('cluster_details', ['cluster_id', 'region_id'], 'pk_cluster_details');
        await queryRunner.createForeignKeys('cluster_details', [
            new typeorm_1.TableForeignKey({
                columnNames: ['cluster_id'],
                referencedColumnNames: ['id'],
                referencedTableName: 'clusters',
                name: 'fk_cluster_details_cluster_id',
                onDelete: 'cascade'
            }),
            new typeorm_1.TableForeignKey({
                columnNames: ['region_id'],
                referencedColumnNames: ['id'],
                referencedTableName: 'regions',
                name: 'fk_cluster_details_region_id',
                onDelete: 'cascade'
            })
        ]);
    }
    async down(queryRunner) {
        await queryRunner.dropTable('cluster_details', true);
    }
}
exports.CreateClusterDetailTable1678763517930 = CreateClusterDetailTable1678763517930;
