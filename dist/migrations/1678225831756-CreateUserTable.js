"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.CreateUserTable1678225831756 = void 0;
const typeorm_1 = require("typeorm");
class CreateUserTable1678225831756 {
    async up(queryRunner) {
        await queryRunner.createTable(new typeorm_1.Table({
            name: 'users',
            columns: [
                {
                    name: 'id',
                    type: 'serial4',
                    isPrimary: true
                },
                {
                    name: 'role_id',
                    type: 'int4',
                    isNullable: true
                },
                {
                    name: 'username',
                    type: 'varchar',
                    length: '255'
                },
                {
                    name: 'password',
                    type: 'varchar',
                    length: '255'
                },
                {
                    name: 'created_at',
                    type: 'timestamp',
                    default: 'now()'
                },
                {
                    name: 'updated_at',
                    type: 'timestamp',
                    default: 'now()'
                }
            ]
        }), true);
        await queryRunner.createIndex('users', new typeorm_1.TableIndex({
            columnNames: ['username'],
            name: 'idx_users_username',
            isUnique: true
        }));
        await queryRunner.createForeignKey('users', new typeorm_1.TableForeignKey({
            name: 'fk_users_role_id',
            columnNames: ['role_id'],
            referencedColumnNames: ['id'],
            referencedTableName: 'roles',
            onDelete: 'set null'
        }));
    }
    async down(queryRunner) {
        await queryRunner.dropTable('users', true);
    }
}
exports.CreateUserTable1678225831756 = CreateUserTable1678225831756;
