"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.CreateAnnouncementTable1678868516337 = void 0;
const typeorm_1 = require("typeorm");
class CreateAnnouncementTable1678868516337 {
    async up(queryRunner) {
        await queryRunner.createTable(new typeorm_1.Table({
            name: 'announcements',
            columns: [
                {
                    name: 'id',
                    type: 'serial8',
                    primaryKeyConstraintName: 'pk_announcement',
                    isPrimary: true
                },
                {
                    name: 'title',
                    type: 'varchar',
                    length: '255'
                },
                {
                    name: 'slug',
                    type: 'varchar',
                    length: '255'
                },
                {
                    name: 'file_id',
                    type: 'bigint'
                },
                {
                    name: 'content',
                    type: 'text'
                },
                {
                    name: 'created_at',
                    type: 'timestamp',
                    default: 'now()'
                },
                {
                    name: 'updated_at',
                    type: 'timestamp',
                    default: 'now()'
                }
            ],
            foreignKeys: [
                {
                    columnNames: ['file_id'],
                    referencedColumnNames: ['id'],
                    referencedTableName: 'files',
                    name: 'fk_files'
                }
            ],
            indices: [
                {
                    columnNames: ['title'],
                    name: 'fk_announcement_title'
                },
                {
                    columnNames: ['slug'],
                    name: 'fk_announcement_slug'
                }
            ]
        }), true);
    }
    async down(queryRunner) {
        await queryRunner.dropTable('announcements', true);
    }
}
exports.CreateAnnouncementTable1678868516337 = CreateAnnouncementTable1678868516337;
