import { MigrationInterface, QueryRunner } from 'typeorm';
export declare class AddColumnToInovasiIndikatorFile1696784487557 implements MigrationInterface {
    up(queryRunner: QueryRunner): Promise<void>;
    down(queryRunner: QueryRunner): Promise<void>;
}
