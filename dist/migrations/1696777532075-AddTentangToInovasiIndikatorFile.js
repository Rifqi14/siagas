"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.AddTentangToInovasiIndikatorFile1696777532075 = void 0;
const typeorm_1 = require("typeorm");
class AddTentangToInovasiIndikatorFile1696777532075 {
    async up(queryRunner) {
        await queryRunner.dropColumn('inovasi_indikator_file', 'nama_surat');
        await queryRunner.addColumn('inovasi_indikator_file', new typeorm_1.TableColumn({ name: 'tentang', type: 'varchar', isNullable: true }));
    }
    async down(queryRunner) {
        await queryRunner.addColumn('inovasi_indikator_file', new typeorm_1.TableColumn({ name: 'nama_surat', type: 'varchar', isNullable: true }));
        await queryRunner.dropColumn('inovasi_indikator_file', 'tentang');
    }
}
exports.AddTentangToInovasiIndikatorFile1696777532075 = AddTentangToInovasiIndikatorFile1696777532075;
