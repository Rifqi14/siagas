"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.AddColumnToInovasiIndikatorFile1696784487557 = void 0;
const typeorm_1 = require("typeorm");
class AddColumnToInovasiIndikatorFile1696784487557 {
    async up(queryRunner) {
        await queryRunner.addColumns('inovasi_indikator_file', [
            new typeorm_1.TableColumn({ name: 'inovasi_id', type: 'bigint', isNullable: true }),
            new typeorm_1.TableColumn({
                name: 'indikator_id',
                type: 'bigint',
                isNullable: true
            })
        ]);
        await queryRunner.createForeignKeys('inovasi_indikator_file', [
            new typeorm_1.TableForeignKey({
                columnNames: ['inovasi_id'],
                referencedColumnNames: ['id'],
                referencedTableName: 'government_innovations'
            }),
            new typeorm_1.TableForeignKey({
                columnNames: ['indikator_id'],
                referencedColumnNames: ['id'],
                referencedTableName: 'indicators'
            })
        ]);
    }
    async down(queryRunner) {
        await queryRunner.dropColumns('inovasi_indikator_file', [
            new typeorm_1.TableColumn({ name: 'inovasi_id', type: 'bigint', isNullable: true }),
            new typeorm_1.TableColumn({
                name: 'indikator_id',
                type: 'bigint',
                isNullable: true
            })
        ]);
        await queryRunner.dropForeignKeys('inovasi_indikator_file', [
            new typeorm_1.TableForeignKey({
                columnNames: ['inovasi_id'],
                referencedColumnNames: ['id'],
                referencedTableName: 'government_innovations'
            }),
            new typeorm_1.TableForeignKey({
                columnNames: ['indikator_id'],
                referencedColumnNames: ['id'],
                referencedTableName: 'indicators'
            })
        ]);
    }
}
exports.AddColumnToInovasiIndikatorFile1696784487557 = AddColumnToInovasiIndikatorFile1696784487557;
