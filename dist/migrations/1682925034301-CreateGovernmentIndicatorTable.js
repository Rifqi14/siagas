"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.CreateGovernmentIndicatorTable1682925034301 = void 0;
const typeorm_1 = require("typeorm");
class CreateGovernmentIndicatorTable1682925034301 {
    async up(queryRunner) {
        await queryRunner.createTable(new typeorm_1.Table({
            name: 'government_indicators',
            columns: [
                { name: 'id', type: 'serial4' },
                { name: 'value', type: 'varchar', isNullable: true },
                { name: 'value_before', type: 'varchar', isNullable: true },
                { name: 'value_after', type: 'varchar', isNullable: true },
                { name: 'other_value', type: 'varchar', isNullable: true },
                { name: 'score', type: 'varchar', isNullable: true },
                { name: 'scale_id', type: 'bigint', isNullable: true },
                { name: 'indicator_id', type: 'bigint' },
                { name: 'gov_profile_id', type: 'bigint' },
                { name: 'created_at', type: 'timestamp', default: 'now()' },
                { name: 'updated_at', type: 'timestamp', default: 'now()' },
                { name: 'deleted_at', type: 'timestamp', isNullable: true }
            ],
            foreignKeys: [
                {
                    columnNames: ['scale_id'],
                    referencedColumnNames: ['id'],
                    referencedTableName: 'indicator_scales'
                },
                {
                    columnNames: ['indicator_id'],
                    referencedColumnNames: ['id'],
                    referencedTableName: 'indicators'
                },
                {
                    columnNames: ['gov_profile_id'],
                    referencedColumnNames: ['id'],
                    referencedTableName: 'government_profiles'
                }
            ],
            uniques: [
                {
                    columnNames: ['gov_profile_id', 'indicator_id']
                }
            ]
        }), true);
    }
    async down(queryRunner) {
        await queryRunner.dropTable(new typeorm_1.Table({
            name: 'government_indicators'
        }), true);
    }
}
exports.CreateGovernmentIndicatorTable1682925034301 = CreateGovernmentIndicatorTable1682925034301;
