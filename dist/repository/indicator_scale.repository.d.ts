import IndicatorScale from '../entity/IndicatorScale.entity';
import BaseRepository from './interface/base.repository';
export default class IndicatorScaleRepository extends BaseRepository<IndicatorScale> {
    constructor();
}
