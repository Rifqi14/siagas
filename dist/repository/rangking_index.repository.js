"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const data_source_1 = require("../data-source");
const RangkingIndex_entity_1 = require("../entity/RangkingIndex.entity");
class RangkingIndexRepository {
    constructor() {
        this.repository = data_source_1.AppDataSource.getRepository(RangkingIndex_entity_1.RangkingIndex);
    }
}
exports.default = RangkingIndexRepository;
