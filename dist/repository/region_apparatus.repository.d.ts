import RegionalApparatus from '../entity/RegionApparatus.entity';
import BaseRepository from './interface/base.repository';
export default class RegionApparatusRepository extends BaseRepository<RegionalApparatus> {
    constructor();
}
