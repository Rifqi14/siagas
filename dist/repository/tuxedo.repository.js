"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const Tuxedo_entity_1 = __importDefault(require("../entity/Tuxedo.entity"));
const base_repository_1 = __importDefault(require("./interface/base.repository"));
class TuxedoRepository extends base_repository_1.default {
    constructor() {
        super(Tuxedo_entity_1.default);
    }
}
exports.default = TuxedoRepository;
