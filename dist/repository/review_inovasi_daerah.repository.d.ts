import { ReviewInovasiDaerah } from '../entity/ReviewInovasiDaerah.entity';
import BaseRepository from './interface/base.repository';
export default class ReviewInovasiDaerahRepository extends BaseRepository<ReviewInovasiDaerah> {
    constructor();
}
