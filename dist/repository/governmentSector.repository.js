"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const GovernmentSector_1 = __importDefault(require("../entity/GovernmentSector"));
const base_repository_1 = __importDefault(require("./interface/base.repository"));
class GovernmentSectorRepository extends base_repository_1.default {
    constructor() {
        super(GovernmentSector_1.default);
    }
}
exports.default = GovernmentSectorRepository;
