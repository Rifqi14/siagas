import ClusterDetail from '../entity/ClusterDetail.entity';
import BaseRepository from './interface/base.repository';
export default class ClusterDetailRepository extends BaseRepository<ClusterDetail> {
    constructor();
}
