import GovernmentProfile from '../entity/GovernmentProfile.entity';
import BaseRepository from './interface/base.repository';
export default class GovernmentProfileRepository extends BaseRepository<GovernmentProfile> {
    constructor();
}
