import Announcement from '../entity/Announcement.entity';
import BaseRepository from './interface/base.repository';
export default class AnnouncementRepository extends BaseRepository<Announcement> {
    constructor();
}
