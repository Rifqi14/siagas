"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const PemdaIndikator_entity_1 = require("../entity/PemdaIndikator.entity");
const base_repository_1 = __importDefault(require("./interface/base.repository"));
class PemdaIndikatorRepository extends base_repository_1.default {
    constructor() {
        super(PemdaIndikator_entity_1.PemdaIndikator);
    }
}
exports.default = PemdaIndikatorRepository;
