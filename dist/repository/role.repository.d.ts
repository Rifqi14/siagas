import Role from '../entity/Role.entity';
import BaseRepository from './interface/base.repository';
export default class RoleRepository extends BaseRepository<Role> {
    constructor();
    role(): void;
}
