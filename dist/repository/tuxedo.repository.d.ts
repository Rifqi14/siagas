import Tuxedo from '../entity/Tuxedo.entity';
import BaseRepository from './interface/base.repository';
export default class TuxedoRepository extends BaseRepository<Tuxedo> {
    constructor();
}
