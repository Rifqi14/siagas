"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const data_source_1 = require("../data-source");
class InnovativeGovernmentAwardRepository {
    constructor() {
        this.manager = data_source_1.AppDataSource.manager;
    }
}
exports.default = InnovativeGovernmentAwardRepository;
