"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const DocumentCategory_entity_1 = __importDefault(require("../entity/DocumentCategory.entity"));
const base_repository_1 = __importDefault(require("./interface/base.repository"));
class DocumentCategoryRepository extends base_repository_1.default {
    constructor() {
        super(DocumentCategory_entity_1.default);
    }
}
exports.default = DocumentCategoryRepository;
