"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const Distrik_entity_1 = require("../entity/Distrik.entity");
const base_repository_1 = __importDefault(require("./interface/base.repository"));
class DistrikRepository extends base_repository_1.default {
    constructor() {
        super(Distrik_entity_1.Distrik);
    }
}
exports.default = DistrikRepository;
