import User from '../entity/User.entity';
import BaseRepository from './interface/base.repository';
export default class UserRepository extends BaseRepository<User> {
    constructor();
}
