import { Distrik } from "../entity/Distrik.entity";
import BaseRepository from "./interface/base.repository";
export default class DistrikRepository extends BaseRepository<Distrik> {
    constructor();
}
