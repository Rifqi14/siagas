"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const data_source_1 = require("../data-source");
const DashboardArsip_entity_1 = require("../entity/DashboardArsip.entity");
class DashboardRepository {
    constructor() {
        this.viewArsip = data_source_1.AppDataSource.getRepository(DashboardArsip_entity_1.DashboardArsip);
        this.manager = data_source_1.AppDataSource.manager;
    }
}
exports.default = DashboardRepository;
