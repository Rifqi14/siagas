import { PemdaIndikatorFile } from '../entity/PemdaIndikatorFile.entity';
import BaseRepository from './interface/base.repository';
export default class PemdaIndikatorFileRepository extends BaseRepository<PemdaIndikatorFile> {
    constructor();
}
