import Setting from '../entity/Setting.entity';
import BaseRepository from './interface/base.repository';
export default class SettingRepository extends BaseRepository<Setting> {
    constructor();
}
