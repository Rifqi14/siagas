import Document from '../entity/Document.entity';
import BaseRepository from './interface/base.repository';
export default class DocumentRepository extends BaseRepository<Document> {
    constructor();
}
