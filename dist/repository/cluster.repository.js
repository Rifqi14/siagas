"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const Cluster_entity_1 = __importDefault(require("../entity/Cluster.entity"));
const base_repository_1 = __importDefault(require("./interface/base.repository"));
class ClusterRepository extends base_repository_1.default {
    constructor() {
        super(Cluster_entity_1.default);
    }
}
exports.default = ClusterRepository;
