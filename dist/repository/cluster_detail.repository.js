"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const ClusterDetail_entity_1 = __importDefault(require("../entity/ClusterDetail.entity"));
const base_repository_1 = __importDefault(require("./interface/base.repository"));
class ClusterDetailRepository extends base_repository_1.default {
    constructor() {
        super(ClusterDetail_entity_1.default);
    }
}
exports.default = ClusterDetailRepository;
