"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const typeorm_1 = require("typeorm");
const data_source_1 = require("../../data-source");
class BaseRepository extends typeorm_1.Repository {
    constructor(entity) {
        super(entity, data_source_1.AppDataSource.manager);
        this.repository = data_source_1.AppDataSource.getRepository(entity);
    }
}
exports.default = BaseRepository;
