export interface Read<T> {
    find(item: Partial<T>): Promise<Array<T>>;
    findById(id: number): Promise<T>;
    count(item: Partial<T>): Promise<number>;
    exist(item: Partial<T>): Promise<boolean>;
}
