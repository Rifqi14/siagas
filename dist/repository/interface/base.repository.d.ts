import { BaseEntity, EntityTarget, Repository } from 'typeorm';
export default class BaseRepository<T extends BaseEntity> extends Repository<T> {
    protected repository: Repository<T>;
    constructor(entity: EntityTarget<T>);
}
