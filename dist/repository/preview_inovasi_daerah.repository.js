"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const PreviewReviewInovasiDaerah_entity_1 = require("../entity/PreviewReviewInovasiDaerah.entity");
const base_repository_1 = __importDefault(require("./interface/base.repository"));
class PreviewInovasiDaerahRepository extends base_repository_1.default {
    constructor() {
        super(PreviewReviewInovasiDaerah_entity_1.PreviewReviewInovasiDaerah);
    }
}
exports.default = PreviewInovasiDaerahRepository;
