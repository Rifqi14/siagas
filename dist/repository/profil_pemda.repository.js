"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const ProfilePemda_entity_1 = require("../entity/ProfilePemda.entity");
const base_repository_1 = __importDefault(require("./interface/base.repository"));
class ProfilPemdaRepository extends base_repository_1.default {
    constructor() {
        super(ProfilePemda_entity_1.ProfilPemda);
    }
}
exports.default = ProfilPemdaRepository;
