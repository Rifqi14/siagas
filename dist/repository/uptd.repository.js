"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const Uptd_entity_1 = __importDefault(require("../entity/Uptd.entity"));
const base_repository_1 = __importDefault(require("./interface/base.repository"));
class UptdRepository extends base_repository_1.default {
    constructor() {
        super(Uptd_entity_1.default);
    }
}
exports.default = UptdRepository;
