import { ReviewInovasiDaerah } from '../entity/ReviewInovasiDaerah.entity';
import BaseRepository from './interface/base.repository';
export default class HasilReviewInovasiRepository extends BaseRepository<ReviewInovasiDaerah> {
    constructor();
}
