import Indicator from '../entity/Indicator.entity';
import BaseRepository from './interface/base.repository';
export default class IndicatorRepository extends BaseRepository<Indicator> {
    constructor();
}
