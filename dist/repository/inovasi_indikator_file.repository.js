"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const InovasiIndikatorFile_entity_1 = require("../entity/InovasiIndikatorFile.entity");
const base_repository_1 = __importDefault(require("./interface/base.repository"));
class InovasiIndikatorFileRepository extends base_repository_1.default {
    constructor() {
        super(InovasiIndikatorFile_entity_1.InovasiIndikatorFile);
    }
}
exports.default = InovasiIndikatorFileRepository;
