"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const ReviewInovasiDaerah_entity_1 = require("../entity/ReviewInovasiDaerah.entity");
const base_repository_1 = __importDefault(require("./interface/base.repository"));
class HasilReviewInovasiRepository extends base_repository_1.default {
    constructor() {
        super(ReviewInovasiDaerah_entity_1.ReviewInovasiDaerah);
    }
}
exports.default = HasilReviewInovasiRepository;
