import GovernmentInnovation from '../entity/GovernmentInnovation.entity';
import BaseRepository from './interface/base.repository';
export default class GovernmentInnovationRepository extends BaseRepository<GovernmentInnovation> {
    constructor();
}
