import DocumentCategory from '../entity/DocumentCategory.entity';
import BaseRepository from './interface/base.repository';
export default class DocumentCategoryRepository extends BaseRepository<DocumentCategory> {
    constructor();
}
