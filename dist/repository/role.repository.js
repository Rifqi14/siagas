"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const Role_entity_1 = __importDefault(require("../entity/Role.entity"));
const base_repository_1 = __importDefault(require("./interface/base.repository"));
const role_json_1 = __importDefault(require("../data/seeder/role.json"));
class RoleRepository extends base_repository_1.default {
    constructor() {
        super(Role_entity_1.default);
    }
    role() {
        role_json_1.default.forEach(async (role) => {
            const res = await this.findOneBy({ name: role.name });
            if (res) {
                return;
            }
            this.insert({ is_creator: role.is_creator, name: role.name }).then(val => {
                console.log(`Success seeder ${role.name} data`);
            }, () => {
                console.log(`Error seeder ${role.name} data`);
            });
        });
    }
}
exports.default = RoleRepository;
