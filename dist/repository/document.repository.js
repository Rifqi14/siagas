"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const Document_entity_1 = __importDefault(require("../entity/Document.entity"));
const base_repository_1 = __importDefault(require("./interface/base.repository"));
class DocumentRepository extends base_repository_1.default {
    constructor() {
        super(Document_entity_1.default);
    }
}
exports.default = DocumentRepository;
