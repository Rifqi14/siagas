"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const data_source_1 = require("../data-source");
const RekapIndeksAkhir_entity_1 = require("../entity/RekapIndeksAkhir.entity");
class RekapIndeksAkhirRepository {
    constructor() {
        this.repository = data_source_1.AppDataSource.getRepository(RekapIndeksAkhir_entity_1.RekapIndeksAkhir);
    }
}
exports.default = RekapIndeksAkhirRepository;
