import Regions from '../entity/Region.entity';
import BaseRepository from './interface/base.repository';
export default class RegionRepository extends BaseRepository<Regions> {
    constructor();
}
