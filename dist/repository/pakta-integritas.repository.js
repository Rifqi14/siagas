"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const PaktaIntegritas_entity_1 = require("../entity/PaktaIntegritas.entity");
const base_repository_1 = __importDefault(require("./interface/base.repository"));
class PaktaIntegritasRepository extends base_repository_1.default {
    constructor() {
        super(PaktaIntegritas_entity_1.PaktaIntegritas);
    }
}
exports.default = PaktaIntegritasRepository;
