"use strict";
var __createBinding = (this && this.__createBinding) || (Object.create ? (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    var desc = Object.getOwnPropertyDescriptor(m, k);
    if (!desc || ("get" in desc ? !m.__esModule : desc.writable || desc.configurable)) {
      desc = { enumerable: true, get: function() { return m[k]; } };
    }
    Object.defineProperty(o, k2, desc);
}) : (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    o[k2] = m[k];
}));
var __setModuleDefault = (this && this.__setModuleDefault) || (Object.create ? (function(o, v) {
    Object.defineProperty(o, "default", { enumerable: true, value: v });
}) : function(o, v) {
    o["default"] = v;
});
var __importStar = (this && this.__importStar) || function (mod) {
    if (mod && mod.__esModule) return mod;
    var result = {};
    if (mod != null) for (var k in mod) if (k !== "default" && Object.prototype.hasOwnProperty.call(mod, k)) __createBinding(result, mod, k);
    __setModuleDefault(result, mod);
    return result;
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.AppDataSource = void 0;
const typeorm_1 = require("typeorm");
const config_1 = __importDefault(require("./providers/config"));
const path = __importStar(require("path"));
const migrationsDir = path.normalize(path.join(__dirname, 'migrations', '*.{ts,js}'));
const entityDir = path.normalize(path.join(__dirname, 'entity', '*.{ts,js}'));
if (process.platform == 'win32') {
    migrationsDir.replace(/\\/g, '/');
    entityDir.replace(/\\/g, '/');
}
exports.AppDataSource = new typeorm_1.DataSource({
    type: 'postgres',
    host: config_1.default.load().database_host,
    port: parseInt(config_1.default.load().database_port),
    username: config_1.default.load().database_user,
    password: config_1.default.load().database_password,
    database: config_1.default.load().database_name,
    synchronize: false,
    entities: [entityDir],
    migrations: [migrationsDir],
    logging: ['error', 'warn', 'log'],
    migrationsTableName: `${config_1.default.load().app_name.toLowerCase()}_migrations`,
});
exports.AppDataSource.initialize()
    .then(() => {
    console.log('Data Source has been initialized!');
})
    .catch(err => {
    console.error('Error during Data Source initialization', err);
});
