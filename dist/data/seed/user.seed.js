"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.userSeed = void 0;
const Role_entity_1 = __importDefault(require("../../entity/Role.entity"));
const User_entity_1 = __importDefault(require("../../entity/User.entity"));
const user_json_1 = __importDefault(require("../seeder/user.json"));
const userSeed = async (ds) => {
    const repo = ds.getRepository(User_entity_1.default);
    const roleRepo = ds.getRepository(Role_entity_1.default);
    const entities = [];
    for (let i = 0; i < user_json_1.default.length; i++) {
        const { email, nama_lengkap, nama_panggilan, password, role, username } = user_json_1.default[i];
        const entity = await repo.findOneBy({ username });
        if (!entity) {
            const data = repo.create({
                email,
                full_name: nama_lengkap,
                nickname: nama_panggilan,
                password,
                username
            });
            const roleEntity = await roleRepo.findOneBy({ name: role });
            if (roleEntity) {
                data.role = roleEntity;
            }
            entities.push(data);
        }
    }
    await repo.insert(entities).then(() => console.log(`Upsert user data`), err => console.log(`Error upsert data: ${err}`));
};
exports.userSeed = userSeed;
