"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.seed = void 0;
const role_seed_1 = require("./role.seed");
const user_seed_1 = require("./user.seed");
const seed = async (ds) => {
    await (0, role_seed_1.roleSeed)(ds);
    await (0, user_seed_1.userSeed)(ds);
};
exports.seed = seed;
