"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.roleSeed = void 0;
const Role_entity_1 = __importDefault(require("../../entity/Role.entity"));
const role_json_1 = __importDefault(require("../seeder/role.json"));
const roleSeed = async (ds) => {
    const repo = ds.getRepository(Role_entity_1.default);
    const entities = [];
    for (let i = 0; i < role_json_1.default.length; i++) {
        const { name, is_creator, is_super_admin } = role_json_1.default[i];
        const entity = await repo.findOneBy({ name });
        if (!entity) {
            const data = repo.create({
                name,
                is_creator,
                is_super_admin
            });
            entities.push(data);
        }
    }
    await repo.insert(entities).then(() => console.log(`Upsert role data`), err => console.log(`Error upsert data: ${err}`));
};
exports.roleSeed = roleSeed;
