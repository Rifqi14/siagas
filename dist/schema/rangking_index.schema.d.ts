import { RangkingIndex } from "../entity/RangkingIndex.entity";
import { BasePaginationRequest } from "./base.schema";
export interface RangkingIndexParams extends Partial<BasePaginationRequest> {
    q?: string;
}
export interface RangkingIndexParamsDownload {
    q?: string;
}
export interface UpdateNominatorRequest {
    nominator: "Ya" | "Tidak";
}
export interface RangkingIndexDownloadResponse {
    no: number;
    nama_daerah: string;
    jumlah_inovasi: string;
    total_skor: string;
    nilai_indeks: string;
    total_file: string;
    predikat: string;
    indeks: string;
    nominator: string;
}
export declare const toDownload: (data: RangkingIndex, no: number) => RangkingIndexDownloadResponse;
