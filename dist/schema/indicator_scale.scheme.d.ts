import { BasePaginationRequest } from './base.schema';
export interface CreateIndicatorScaleRequest {
    nilai: number;
    mulai_dari: number;
    sampai_dengan: number;
    keterangan: string;
}
export interface GetIndicatorScaleRequest extends BasePaginationRequest {
    /**
     * Global search. Mencari berdasarkan kolom keterangan
     */
    q?: string;
}
