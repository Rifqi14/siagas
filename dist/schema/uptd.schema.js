"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.createUptdSchema = void 0;
const zod_1 = require("zod");
exports.createUptdSchema = (0, zod_1.object)({
    body: (0, zod_1.object)({
        name: (0, zod_1.string)({ required_error: 'Name is required' }),
        regional_apparatus_id: (0, zod_1.number)({
            required_error: 'Regional apparatus id required'
        })
    })
});
