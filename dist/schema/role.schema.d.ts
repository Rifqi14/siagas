import { z } from 'zod';
import { BasePaginationRequest } from './base.schema';
export declare const createRoleSchema: z.ZodObject<{
    body: z.ZodObject<{
        name: z.ZodString;
    }, "strip", z.ZodTypeAny, {
        name: string;
    }, {
        name: string;
    }>;
}, "strip", z.ZodTypeAny, {
    body: {
        name: string;
    };
}, {
    body: {
        name: string;
    };
}>;
export type CreateRoleRequest = z.infer<typeof createRoleSchema>['body'];
export interface GetRoleRequest extends BasePaginationRequest {
    name?: string;
}
