"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.toGovernmentInnovationProfilExportSchema = exports.GovernmentInnovationProfilExportSchema = exports.toGovernmentInnovationExport = exports.GovernmentInnovationExportSchema = exports.createGovernmentInnovationSchema = void 0;
const zod_1 = require("zod");
exports.createGovernmentInnovationSchema = (0, zod_1.object)({
    body: (0, zod_1.object)({
        nama_pemda: (0, zod_1.string)(),
        dibuat_oleh: (0, zod_1.string)(),
        nama_inovasi: (0, zod_1.string)(),
        tahapan_inovasi: (0, zod_1.string)(),
        inisiator_inovasi: (0, zod_1.string)(),
        jenis_inovasi: (0, zod_1.string)(),
        bentuk_inovasi: (0, zod_1.string)(),
        tematik: (0, zod_1.string)(),
        urusan_pemerintah: (0, zod_1.string)(),
        urusan_pertama: (0, zod_1.string)(),
        urusan_lain: (0, zod_1.string)(),
        waktu_uji_coba: (0, zod_1.string)(),
        waktu_penerapan: (0, zod_1.string)(),
        rancang_bangun: (0, zod_1.string)(),
        tujuan: (0, zod_1.string)(),
        manfaat: (0, zod_1.string)(),
        hasil_inovasi: (0, zod_1.string)(),
    }),
});
class GovernmentInnovationExportSchema {
}
exports.GovernmentInnovationExportSchema = GovernmentInnovationExportSchema;
const toGovernmentInnovationExport = (data, no) => {
    var _a, _b, _c, _d, _e, _f, _g, _h;
    let estimasi = 0;
    switch (data.innovation_phase.toLowerCase()) {
        case "inisiatif":
            estimasi = 3;
            break;
        case "uji coba":
            estimasi = 6;
            break;
        case "penerapan":
            estimasi = 9;
            break;
    }
    return {
        no,
        nama_pemda: (_b = (_a = data.profilPemda) === null || _a === void 0 ? void 0 : _a.nama_daerah) !== null && _b !== void 0 ? _b : "",
        nama_inovasi: (_c = data.innovation_name) !== null && _c !== void 0 ? _c : "",
        tahapan_inovasi: (_d = data.innovation_phase) !== null && _d !== void 0 ? _d : "",
        urusan_pemerintahan: (_f = (_e = data.urusan) === null || _e === void 0 ? void 0 : _e.name) !== null && _f !== void 0 ? _f : "",
        waktu_uji_coba_inovasi: (_g = data.trial_time) !== null && _g !== void 0 ? _g : "",
        waktu_penerapan: (_h = data.implementation_time) !== null && _h !== void 0 ? _h : "",
        estimasi_skor_kematangan: estimasi,
    };
};
exports.toGovernmentInnovationExport = toGovernmentInnovationExport;
class GovernmentInnovationProfilExportSchema {
}
exports.GovernmentInnovationProfilExportSchema = GovernmentInnovationProfilExportSchema;
const toGovernmentInnovationProfilExportSchema = (data) => {
    var _a, _b, _c, _d, _e, _f, _g, _h, _j, _k, _l, _m, _o, _p, _q, _r, _s, _t, _u, _v, _w, _x, _y;
    return {
        nama_pemda: (_b = (_a = data.profilPemda) === null || _a === void 0 ? void 0 : _a.nama_daerah) !== null && _b !== void 0 ? _b : "",
        nama_inovasi: (_c = data.innovation_name) !== null && _c !== void 0 ? _c : "",
        tahapan_inovasi: (_d = data.innovation_phase) !== null && _d !== void 0 ? _d : "",
        inisiator_daerah: (_e = data.innovation_initiator) !== null && _e !== void 0 ? _e : "",
        jenis_inovasi: (_f = data.innovation_type) !== null && _f !== void 0 ? _f : "",
        bentuk_inovasi_daerah: (_g = data.innovation_form) !== null && _g !== void 0 ? _g : "",
        tematik: (_h = data.thematic) !== null && _h !== void 0 ? _h : "",
        detail_tematik: (_j = data.thematic_detail) !== null && _j !== void 0 ? _j : "",
        urusan_pemerintahan: (_l = (_k = data.urusan) === null || _k === void 0 ? void 0 : _k.name) !== null && _l !== void 0 ? _l : "",
        tingkatan: "",
        waktu_uji_coba_inovasi: (_m = data.trial_time) !== null && _m !== void 0 ? _m : "",
        waktu_penerapan: (_o = data.implementation_time) !== null && _o !== void 0 ? _o : "",
        opd_yang_menangani: (_q = (_p = data.userCreator) === null || _p === void 0 ? void 0 : _p.nama_pemda) !== null && _q !== void 0 ? _q : "",
        alamat_pemda: (_s = (_r = data.profilPemda) === null || _r === void 0 ? void 0 : _r.alamat_pemda) !== null && _s !== void 0 ? _s : "",
        email: (_u = (_t = data.userCreator) === null || _t === void 0 ? void 0 : _t.email) !== null && _u !== void 0 ? _u : "",
        no_telp: (_w = (_v = data.profilPemda) === null || _v === void 0 ? void 0 : _v.no_telpon) !== null && _w !== void 0 ? _w : "",
        nama_admin: (_y = (_x = data.profilPemda) === null || _x === void 0 ? void 0 : _x.nama_admin) !== null && _y !== void 0 ? _y : "",
    };
};
exports.toGovernmentInnovationProfilExportSchema = toGovernmentInnovationProfilExportSchema;
