import { z } from "zod";
import { BasePaginationRequest } from "./base.schema";
export declare const createDistrikSchema: z.ZodObject<{
    body: z.ZodObject<{
        name: z.ZodString;
    }, "strip", z.ZodTypeAny, {
        name: string;
    }, {
        name: string;
    }>;
}, "strip", z.ZodTypeAny, {
    body: {
        name: string;
    };
}, {
    body: {
        name: string;
    };
}>;
export type CreateDistrikRequest = z.infer<typeof createDistrikSchema>["body"];
export interface GetDistrikRequest extends BasePaginationRequest {
    name?: string;
}
