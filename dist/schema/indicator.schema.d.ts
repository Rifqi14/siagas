import { BasePaginationRequest } from './base.schema';
export interface CreateIndicatorRequest {
    no_urut?: number;
    jenis_indikator?: string;
    nama_indikator?: string;
    keterangan?: string;
    nama_dokumen_pendukung?: string;
    bobot?: number;
    jenis_file?: string;
    parent?: string;
    mandatory?: string;
}
export interface GetIndicatorRequest extends BasePaginationRequest {
    /**
     * Global search, sekarang cari berdasarkan kolom indikator serta tipe
     */
    q?: string;
    jenis_indikator?: string;
}
