"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.toDownload = void 0;
const toDownload = (data, no) => {
    var _a, _b, _c, _d, _e, _f, _g, _h, _j, _k, _l, _m, _o, _p, _q;
    return {
        no,
        nama_daerah: (_a = data.nama_daerah) !== null && _a !== void 0 ? _a : "",
        jumlah_inovasi: (_c = (_b = data.jumlah_inovasi) === null || _b === void 0 ? void 0 : _b.toString()) !== null && _c !== void 0 ? _c : "",
        total_skor: (_e = (_d = data.total_skor_mandiri) === null || _d === void 0 ? void 0 : _d.toString()) !== null && _e !== void 0 ? _e : "",
        nilai_indeks: (_g = (_f = data.nilai_indeks) === null || _f === void 0 ? void 0 : _f.toString()) !== null && _g !== void 0 ? _g : "",
        total_file: (_j = (_h = data.total_file) === null || _h === void 0 ? void 0 : _h.toString()) !== null && _j !== void 0 ? _j : "",
        predikat: (_l = (_k = data.predikat) === null || _k === void 0 ? void 0 : _k.toString()) !== null && _l !== void 0 ? _l : "",
        indeks: (_o = (_m = data.indeks) === null || _m === void 0 ? void 0 : _m.toString()) !== null && _o !== void 0 ? _o : "",
        nominator: (_q = (_p = data.nominator) === null || _p === void 0 ? void 0 : _p.toString()) !== null && _q !== void 0 ? _q : "",
    };
};
exports.toDownload = toDownload;
