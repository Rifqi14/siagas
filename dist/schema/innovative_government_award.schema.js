"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.toRangkingResponse = exports.toPrestasiResponse = exports.toPeringkatResponse = void 0;
const toPeringkatResponse = (data, jumlah_pemda) => {
    var _a, _b, _c, _d, _e, _f;
    return {
        pemda_id: (_a = data.id) !== null && _a !== void 0 ? _a : 0,
        nama_pemda: (_b = data.nama_daerah) !== null && _b !== void 0 ? _b : '',
        jumlah_inovasi: (_c = data.jumlah_inovasi) !== null && _c !== void 0 ? _c : 0,
        isp: (_d = data.isp) !== null && _d !== void 0 ? _d : 0,
        rata_rata: data.jumlah_inovasi ? data.jumlah_inovasi / jumlah_pemda : 0,
        skor_total: data.total_skor_verifikasi && data.jumlah_inovasi
            ? data.total_skor_verifikasi + data.jumlah_inovasi * 0.38
            : 0,
        predikat: (_e = data.predikat) !== null && _e !== void 0 ? _e : '',
        nominator: (_f = data.nominator) !== null && _f !== void 0 ? _f : 'Tidak'
    };
};
exports.toPeringkatResponse = toPeringkatResponse;
const toPrestasiResponse = (data, jumlah_pemda) => {
    var _a, _b, _c, _d;
    return {
        pemda_id: (_a = data.id) !== null && _a !== void 0 ? _a : 0,
        nama_pemda: (_b = data.nama_daerah) !== null && _b !== void 0 ? _b : '',
        skor_pengukuran: data.jumlah_inovasi
            ? data.jumlah_inovasi / jumlah_pemda
            : 0,
        presentasi: (_c = data.skor) !== null && _c !== void 0 ? _c : 0,
        validasi_lapangan: (_d = data.skor_evaluasi) !== null && _d !== void 0 ? _d : 0,
        skor_akhir: data.total_skor_verifikasi && data.jumlah_inovasi
            ? data.total_skor_verifikasi + data.jumlah_inovasi * 0.38
            : 0
    };
};
exports.toPrestasiResponse = toPrestasiResponse;
const toRangkingResponse = (data, jumlah_pemda) => {
    var _a, _b, _c;
    return {
        pemda_id: (_a = data.id) !== null && _a !== void 0 ? _a : 0,
        nama_pemda: (_b = data.nama_daerah) !== null && _b !== void 0 ? _b : '',
        skor_indeks: data.total_skor_mandiri &&
            data.jumlah_inovasi &&
            data.total_skor_mandiri / data.jumlah_inovasi > 0
            ? data.total_skor_mandiri / data.jumlah_inovasi
            : 0,
        skor_penilaian: data.total_skor_verifikasi,
        skor_akhir: data.total_skor_verifikasi && data.jumlah_inovasi
            ? data.total_skor_verifikasi + data.jumlah_inovasi * 0.38
            : 0,
        predikat: (_c = data.predikat) !== null && _c !== void 0 ? _c : ''
    };
};
exports.toRangkingResponse = toRangkingResponse;
