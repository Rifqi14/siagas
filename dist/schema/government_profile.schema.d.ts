/// <reference types="multer" />
import { z } from 'zod';
import { BasePaginationRequest } from './base.schema';
export declare const createGovernmentProfileSchema: z.ZodObject<{
    body: z.ZodObject<{
        daerah: z.ZodString;
        name: z.ZodString;
        opd_menangani: z.ZodString;
        alamat: z.ZodString;
        email: z.ZodString;
        nomor_telepon: z.ZodString;
        nama_admin: z.ZodString;
    }, "strip", z.ZodTypeAny, {
        name: string;
        email: string;
        nama_admin: string;
        daerah: string;
        opd_menangani: string;
        alamat: string;
        nomor_telepon: string;
    }, {
        name: string;
        email: string;
        nama_admin: string;
        daerah: string;
        opd_menangani: string;
        alamat: string;
        nomor_telepon: string;
    }>;
}, "strip", z.ZodTypeAny, {
    body: {
        name: string;
        email: string;
        nama_admin: string;
        daerah: string;
        opd_menangani: string;
        alamat: string;
        nomor_telepon: string;
    };
}, {
    body: {
        name: string;
        email: string;
        nama_admin: string;
        daerah: string;
        opd_menangani: string;
        alamat: string;
        nomor_telepon: string;
    };
}>;
export type CreateGovernmentProfileRequest = z.infer<typeof createGovernmentProfileSchema>['body'] & {
    file?: Express.Multer.File;
};
export interface GetGovernmentProfileRequest extends BasePaginationRequest {
    daerah?: string;
    q?: string;
}
