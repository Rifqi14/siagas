import { PreviewReviewInovasiDaerah } from '../entity/PreviewReviewInovasiDaerah.entity';
import { BasePaginationRequest } from './base.schema';
export interface CreatePreviewInovasiDaerahRequest extends Pick<PreviewReviewInovasiDaerah, 'komentar'> {
}
export interface GetPreviewInovasiRequest extends BasePaginationRequest {
}
