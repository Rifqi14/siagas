"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.uploadFileSchema = void 0;
const zod_1 = require("zod");
exports.uploadFileSchema = (0, zod_1.object)({
    body: (0, zod_1.object)({
        name: (0, zod_1.string)().optional(),
        file: (0, zod_1.any)({ required_error: 'File is required' })
    })
});
