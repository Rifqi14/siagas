"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.toDashboardArsipDownload = void 0;
const toDashboardArsipDownload = (data, idx) => {
    var _a, _b, _c, _d, _e, _f;
    return {
        no: idx,
        nama_pemda: (_a = data.nama_daerah) !== null && _a !== void 0 ? _a : "",
        nama_inovasi: (_b = data.innovation_name) !== null && _b !== void 0 ? _b : "",
        tahapan_inovasi: (_c = data.innovation_phase) !== null && _c !== void 0 ? _c : "",
        waktu_uji_coba: (_d = data.trial_time) !== null && _d !== void 0 ? _d : "",
        waktu_penerapan: (_e = data.implementation_time) !== null && _e !== void 0 ? _e : "",
        kematangan: (_f = data.skor) !== null && _f !== void 0 ? _f : 0,
    };
};
exports.toDashboardArsipDownload = toDashboardArsipDownload;
