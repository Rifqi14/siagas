/// <reference types="multer" />
import { PaktaIntegritas } from '../entity/PaktaIntegritas.entity';
import { z } from 'zod';
import { BasePaginationRequest } from './base.schema';
declare const createPaktaIntegritas: z.ZodObject<{
    body: z.ZodObject<{
        user_id: z.ZodString;
    }, "strip", z.ZodTypeAny, {
        user_id: string;
    }, {
        user_id: string;
    }>;
}, "strip", z.ZodTypeAny, {
    body: {
        user_id: string;
    };
}, {
    body: {
        user_id: string;
    };
}>;
export type CreatePaktaIntegritasRequest = z.infer<typeof createPaktaIntegritas>['body'] & {
    file?: Express.Multer.File;
};
export interface PaktaIntegritasParams extends BasePaginationRequest {
    q?: string;
}
export type ListPaktaIntegritas = PaktaIntegritas[];
export {};
