import { z } from 'zod';
import { BasePaginationRequest } from './base.schema';
export declare const createSettingSchema: z.ZodObject<{
    body: z.ZodObject<{
        key: z.ZodEffects<z.ZodEffects<z.ZodString, string, string>, string, string>;
        value: z.ZodString;
        helper: z.ZodString;
    }, "strip", z.ZodTypeAny, {
        value: string;
        key: string;
        helper: string;
    }, {
        value: string;
        key: string;
        helper: string;
    }>;
}, "strip", z.ZodTypeAny, {
    body: {
        value: string;
        key: string;
        helper: string;
    };
}, {
    body: {
        value: string;
        key: string;
        helper: string;
    };
}>;
export type CreateSettingRequest = z.infer<typeof createSettingSchema>['body'];
export interface GetSettingRequest extends BasePaginationRequest {
    q?: string;
}
