/// <reference types="multer" />
import { z } from 'zod';
export declare const uploadFileSchema: z.ZodObject<{
    body: z.ZodObject<{
        name: z.ZodOptional<z.ZodString>;
        file: z.ZodAny;
    }, "strip", z.ZodTypeAny, {
        name?: string | undefined;
        file?: any;
    }, {
        name?: string | undefined;
        file?: any;
    }>;
}, "strip", z.ZodTypeAny, {
    body: {
        name?: string | undefined;
        file?: any;
    };
}, {
    body: {
        name?: string | undefined;
        file?: any;
    };
}>;
export type UploadFileRequest = {
    name?: string;
    file: Express.Multer.File;
};
