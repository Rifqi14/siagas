import { LaporanBentukInovasi } from '../entity/LaporanBentukInovasi.entity';
import { LaporanInisiatorInovasi } from '../entity/LaporanInisatorInovasi.entity';
import { LaporanJenisInovasi } from '../entity/LaporanJenisInovasi.entity';
import { LaporanUrusanInovasi } from '../entity/LaporanUrusanInovasi.entity';
export interface RawlingParams {
    pemda_id: number;
}
export interface RawlingResponse {
    jenis_inovasi: LaporanJenisInovasi[];
    urusan_pemerintah: LaporanUrusanInovasi[];
    inisiator: LaporanInisiatorInovasi[];
    bentuk_inovasi: LaporanBentukInovasi[];
}
