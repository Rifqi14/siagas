"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.createClusterDetailSchema = void 0;
const zod_1 = require("zod");
exports.createClusterDetailSchema = (0, zod_1.object)({
    body: (0, zod_1.object)({
        region_id: (0, zod_1.number)({ required_error: 'Region is required' })
    })
});
