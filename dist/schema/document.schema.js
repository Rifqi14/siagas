"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.createDocumentSchema = void 0;
const zod_1 = require("zod");
exports.createDocumentSchema = (0, zod_1.object)({
    body: (0, zod_1.object)({
        title: (0, zod_1.string)({ required_error: 'Title is required' }),
        content: (0, zod_1.string)()
    })
});
