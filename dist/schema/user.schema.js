"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.createUserSchema = void 0;
const zod_1 = require("zod");
const data_source_1 = require("../data-source");
const User_entity_1 = __importDefault(require("../entity/User.entity"));
exports.createUserSchema = (0, zod_1.object)({
    body: (0, zod_1.object)({
        role_id: (0, zod_1.number)({ required_error: 'Role is required' }),
        nama_lengkap: (0, zod_1.string)().optional(),
        nama_pemda: (0, zod_1.string)().optional(),
        nama_panggilan: (0, zod_1.string)().optional(),
        email: (0, zod_1.string)({ required_error: 'Email is required' }).email('Format email not valid'),
        username: (0, zod_1.string)({ required_error: 'Username is required' })
            .min(6, 'Username minimal 6 character length')
            .refine(async (value) => {
            const ds = data_source_1.AppDataSource;
            const repo = ds.getRepository(User_entity_1.default);
            const user = await repo.exist({ where: { username: value } });
            return !user;
        }, { message: 'Username already used' }),
        password: (0, zod_1.string)({ required_error: 'Password is required' }).min(6, 'Password minimal 6 character length'),
    }),
});
