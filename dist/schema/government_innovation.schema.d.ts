/// <reference types="multer" />
import { z } from "zod";
import { BasePaginationRequest } from "./base.schema";
import GovernmentInnovation from "../entity/GovernmentInnovation.entity";
export declare const createGovernmentInnovationSchema: z.ZodObject<{
    body: z.ZodObject<{
        nama_pemda: z.ZodString;
        dibuat_oleh: z.ZodString;
        nama_inovasi: z.ZodString;
        tahapan_inovasi: z.ZodString;
        inisiator_inovasi: z.ZodString;
        jenis_inovasi: z.ZodString;
        bentuk_inovasi: z.ZodString;
        tematik: z.ZodString;
        urusan_pemerintah: z.ZodString;
        urusan_pertama: z.ZodString;
        urusan_lain: z.ZodString;
        waktu_uji_coba: z.ZodString;
        waktu_penerapan: z.ZodString;
        rancang_bangun: z.ZodString;
        tujuan: z.ZodString;
        manfaat: z.ZodString;
        hasil_inovasi: z.ZodString;
    }, "strip", z.ZodTypeAny, {
        nama_pemda: string;
        dibuat_oleh: string;
        nama_inovasi: string;
        tahapan_inovasi: string;
        inisiator_inovasi: string;
        jenis_inovasi: string;
        bentuk_inovasi: string;
        tematik: string;
        urusan_pemerintah: string;
        urusan_pertama: string;
        urusan_lain: string;
        waktu_uji_coba: string;
        waktu_penerapan: string;
        rancang_bangun: string;
        tujuan: string;
        manfaat: string;
        hasil_inovasi: string;
    }, {
        nama_pemda: string;
        dibuat_oleh: string;
        nama_inovasi: string;
        tahapan_inovasi: string;
        inisiator_inovasi: string;
        jenis_inovasi: string;
        bentuk_inovasi: string;
        tematik: string;
        urusan_pemerintah: string;
        urusan_pertama: string;
        urusan_lain: string;
        waktu_uji_coba: string;
        waktu_penerapan: string;
        rancang_bangun: string;
        tujuan: string;
        manfaat: string;
        hasil_inovasi: string;
    }>;
}, "strip", z.ZodTypeAny, {
    body: {
        nama_pemda: string;
        dibuat_oleh: string;
        nama_inovasi: string;
        tahapan_inovasi: string;
        inisiator_inovasi: string;
        jenis_inovasi: string;
        bentuk_inovasi: string;
        tematik: string;
        urusan_pemerintah: string;
        urusan_pertama: string;
        urusan_lain: string;
        waktu_uji_coba: string;
        waktu_penerapan: string;
        rancang_bangun: string;
        tujuan: string;
        manfaat: string;
        hasil_inovasi: string;
    };
}, {
    body: {
        nama_pemda: string;
        dibuat_oleh: string;
        nama_inovasi: string;
        tahapan_inovasi: string;
        inisiator_inovasi: string;
        jenis_inovasi: string;
        bentuk_inovasi: string;
        tematik: string;
        urusan_pemerintah: string;
        urusan_pertama: string;
        urusan_lain: string;
        waktu_uji_coba: string;
        waktu_penerapan: string;
        rancang_bangun: string;
        tujuan: string;
        manfaat: string;
        hasil_inovasi: string;
    };
}>;
export type CreateGovernmentInnovationRequest = z.infer<typeof createGovernmentInnovationSchema>["body"] & {
    anggaran_file?: Express.Multer.File;
    profile_file?: Express.Multer.File;
};
export interface GetGovernmentInnovationRequest extends BasePaginationRequest {
    tahap?: string;
    q?: string;
}
export interface GetGovernmentInnovationDownloadRequest {
    tahap?: string;
    q?: string;
}
export declare class GovernmentInnovationExportSchema {
    no: number;
    nama_pemda: string;
    nama_inovasi: string;
    tahapan_inovasi: string;
    urusan_pemerintahan: string;
    waktu_uji_coba_inovasi: string;
    waktu_penerapan: string;
    estimasi_skor_kematangan: number;
}
export declare const toGovernmentInnovationExport: (data: GovernmentInnovation, no: number) => GovernmentInnovationExportSchema;
export declare class GovernmentInnovationProfilExportSchema {
    nama_pemda: string;
    nama_inovasi: string;
    tahapan_inovasi: string;
    inisiator_daerah: string;
    jenis_inovasi: string;
    bentuk_inovasi_daerah: string;
    tematik: string;
    detail_tematik: string;
    urusan_pemerintahan: string;
    tingkatan: string;
    waktu_uji_coba_inovasi: string;
    waktu_penerapan: string;
    opd_yang_menangani: string;
    alamat_pemda: string;
    email: string;
    no_telp: string;
    nama_admin: string;
}
export declare const toGovernmentInnovationProfilExportSchema: (data: GovernmentInnovation) => GovernmentInnovationProfilExportSchema;
