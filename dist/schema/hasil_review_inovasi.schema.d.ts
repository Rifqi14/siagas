import { ReviewInovasiDaerah } from "../entity/ReviewInovasiDaerah.entity";
import { BasePaginationRequest } from "./base.schema";
export interface HasilReviewParams extends Partial<BasePaginationRequest> {
    q?: string;
    pemda_id?: string;
}
export interface DownloadHasilReview {
    q?: string;
    pemda_id?: string;
}
export interface HasilReviewResponse {
    review_inovasi_id: string | null;
    nomor: string | null;
    judul: string | null;
    pemda: {
        pemda_id: string | null;
        pemda_name: string | null;
    } | null;
    waktu_penerapan: string | null;
    kematangan: number | null;
    skor_verifikasi: number | null;
    qc: string | null;
}
export interface DownloadHasilReviewResponse {
    no: number;
    nomor: string;
    judul: string;
    nama_pemda: string;
    waktu_penerapan: string;
    kematangan: string;
    skor_verifikasi: string;
    QC: string;
}
export declare const toDownloadResponse: (data: ReviewInovasiDaerah, no: number) => DownloadHasilReviewResponse;
