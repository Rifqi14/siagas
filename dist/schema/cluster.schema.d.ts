import { z } from 'zod';
import { BasePaginationRequest } from './base.schema';
export declare const createClusterSchema: z.ZodObject<{
    body: z.ZodObject<{
        name: z.ZodString;
    }, "strip", z.ZodTypeAny, {
        name: string;
    }, {
        name: string;
    }>;
}, "strip", z.ZodTypeAny, {
    body: {
        name: string;
    };
}, {
    body: {
        name: string;
    };
}>;
export type CreateClusterRequest = z.infer<typeof createClusterSchema>['body'];
export interface GetClusterRequest extends BasePaginationRequest {
    q?: string;
}
