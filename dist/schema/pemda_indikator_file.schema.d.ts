/// <reference types="multer" />
import { z } from 'zod';
import { BasePaginationRequest } from './base.schema';
import { PemdaIndikatorFile } from '../entity/PemdaIndikatorFile.entity';
export declare const uploadPemdaIndikatorFileSchema: z.ZodObject<{
    body: z.ZodObject<{
        nomor_dokumen: z.ZodString;
        tanggal_dokumen: z.ZodString;
        tentang: z.ZodString;
        name: z.ZodOptional<z.ZodString>;
    }, "strip", z.ZodTypeAny, {
        tentang: string;
        nomor_dokumen: string;
        tanggal_dokumen: string;
        name?: string | undefined;
    }, {
        tentang: string;
        nomor_dokumen: string;
        tanggal_dokumen: string;
        name?: string | undefined;
    }>;
}, "strip", z.ZodTypeAny, {
    body: {
        tentang: string;
        nomor_dokumen: string;
        tanggal_dokumen: string;
        name?: string | undefined;
    };
}, {
    body: {
        tentang: string;
        nomor_dokumen: string;
        tanggal_dokumen: string;
        name?: string | undefined;
    };
}>;
export type UploadPemdaIndikatorFileRequest = z.infer<typeof uploadPemdaIndikatorFileSchema>['body'] & {
    dokumen?: Express.Multer.File;
};
export interface PemdaIndikatorFileParams extends BasePaginationRequest {
    q?: string;
}
export type ListPemdaIndikatorFiles = PemdaIndikatorFile[];
