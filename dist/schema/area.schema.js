"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.createAreaSchema = void 0;
const zod_1 = require("zod");
exports.createAreaSchema = (0, zod_1.object)({
    body: (0, zod_1.object)({
        name: (0, zod_1.string)({ required_error: 'Name is required' }),
        region_id: (0, zod_1.number)({ required_error: 'Region is required' })
    })
});
