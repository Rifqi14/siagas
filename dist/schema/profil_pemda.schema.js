"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.toDownload = void 0;
const zod_1 = require("zod");
const createProfilPemda = zod_1.z.object({
    body: zod_1.z.object({
        nama_pemda: zod_1.z.string(),
        nama_daerah: zod_1.z.string().optional(),
        opd_yang_menangani: zod_1.z.string().optional(),
        alamat_pemda: zod_1.z.string().optional(),
        email: zod_1.z.string().optional(),
        no_telpon: zod_1.z.string().optional(),
        nama_admin: zod_1.z.string().optional(),
    }),
});
const toDownload = (data, no) => {
    var _a, _b, _c, _d, _e, _f, _g, _h;
    return {
        no,
        daerah: (_a = data.nama_daerah) !== null && _a !== void 0 ? _a : '',
        nama_pemda: (_c = (_b = data.user) === null || _b === void 0 ? void 0 : _b.nama_pemda) !== null && _c !== void 0 ? _c : '',
        opd_yang_menangani: (_d = data.opd_yang_menangani) !== null && _d !== void 0 ? _d : '',
        alamat_pemda: (_e = data.alamat_pemda) !== null && _e !== void 0 ? _e : '',
        email: (_f = data.email) !== null && _f !== void 0 ? _f : '',
        no_telepon: (_g = data.no_telpon) !== null && _g !== void 0 ? _g : '',
        nama_admin: (_h = data.nama_admin) !== null && _h !== void 0 ? _h : '',
    };
};
exports.toDownload = toDownload;
