import { z } from 'zod';
import { BasePaginationRequest } from './base.schema';
export declare const createAreaSchema: z.ZodObject<{
    body: z.ZodObject<{
        name: z.ZodString;
        region_id: z.ZodNumber;
    }, "strip", z.ZodTypeAny, {
        name: string;
        region_id: number;
    }, {
        name: string;
        region_id: number;
    }>;
}, "strip", z.ZodTypeAny, {
    body: {
        name: string;
        region_id: number;
    };
}, {
    body: {
        name: string;
        region_id: number;
    };
}>;
export type CreateAreaRequest = z.infer<typeof createAreaSchema>['body'];
export interface GetAreaRequest extends BasePaginationRequest {
    q?: string;
}
