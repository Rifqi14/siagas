import { PemdaIndikator } from '../entity/PemdaIndikator.entity';
import { BasePaginationRequest } from './base.schema';
export interface CreatePemdaIndikatorRequest {
    keterangan?: string;
    informasi?: string;
    indikator_id?: number;
    catatan?: string;
}
export interface PemdaIndikatorParams extends BasePaginationRequest {
    q?: string;
}
export type ListPemdaIndikator = PemdaIndikator[];
