import { z } from 'zod';
import { BasePaginationRequest } from './base.schema';
import { TimPenilaian } from '../entity/TimPenilaian.entity';
export declare const createTimPenilaianSchema: z.ZodObject<{
    body: z.ZodObject<{
        asn_username: z.ZodOptional<z.ZodString>;
        nama: z.ZodOptional<z.ZodString>;
        instansi: z.ZodOptional<z.ZodString>;
    }, "strip", z.ZodTypeAny, {
        asn_username?: string | undefined;
        nama?: string | undefined;
        instansi?: string | undefined;
    }, {
        asn_username?: string | undefined;
        nama?: string | undefined;
        instansi?: string | undefined;
    }>;
}, "strip", z.ZodTypeAny, {
    body: {
        asn_username?: string | undefined;
        nama?: string | undefined;
        instansi?: string | undefined;
    };
}, {
    body: {
        asn_username?: string | undefined;
        nama?: string | undefined;
        instansi?: string | undefined;
    };
}>;
export type CreateTimPenilaianRequest = z.infer<typeof createTimPenilaianSchema>['body'];
export interface ListTimPenilaianParams extends BasePaginationRequest {
    q?: string;
}
export type ListTimPenilaian = TimPenilaian[];
