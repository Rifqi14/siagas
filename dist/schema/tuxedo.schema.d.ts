import { z } from 'zod';
import { BasePaginationRequest } from './base.schema';
export declare const createTuxedoSchema: z.ZodObject<{
    body: z.ZodObject<{
        title: z.ZodString;
        slug: z.ZodString;
        section: z.ZodString;
        content: z.ZodString;
    }, "strip", z.ZodTypeAny, {
        title: string;
        slug: string;
        content: string;
        section: string;
    }, {
        title: string;
        slug: string;
        content: string;
        section: string;
    }>;
}, "strip", z.ZodTypeAny, {
    body: {
        title: string;
        slug: string;
        content: string;
        section: string;
    };
}, {
    body: {
        title: string;
        slug: string;
        content: string;
        section: string;
    };
}>;
export type CreateTuxedoRequest = z.infer<typeof createTuxedoSchema>['body'];
export interface GetTuxedoRequest extends BasePaginationRequest {
    q?: string;
}
