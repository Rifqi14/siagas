import { PaginationResponse } from '../interface/pagination.interface';
export type BasePaginationRequest = {
    limit: number;
    page: number;
};
export type PartialRequest = {
    [key: string]: string | number | boolean | object | Array<object> | Array<string> | Array<number>;
};
export interface PaginationData<T> {
    paging: PaginationResponse;
    res: T;
}
