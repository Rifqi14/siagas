"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.createSettingSchema = void 0;
const zod_1 = require("zod");
exports.createSettingSchema = (0, zod_1.object)({
    body: (0, zod_1.object)({
        key: (0, zod_1.string)({ required_error: 'Key is required' })
            .refine(data => !/[A-Z]/g.test(data), {
            message: 'Key cannot contain upper case',
            path: ['body', 'key']
        })
            .refine(data => !data.includes(' '), {
            message: 'Key cannot contain space or blank space',
            path: ['body', 'key']
        }),
        value: (0, zod_1.string)({ required_error: 'Value is required' }),
        helper: (0, zod_1.string)()
    })
});
