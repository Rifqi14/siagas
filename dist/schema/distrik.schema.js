"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.createDistrikSchema = void 0;
const zod_1 = require("zod");
exports.createDistrikSchema = (0, zod_1.object)({
    body: (0, zod_1.object)({
        name: (0, zod_1.string)({ required_error: "Name is required" }),
    }),
});
