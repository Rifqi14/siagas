import { InovasiIndikator } from '../entity/InovasiIndikator.entity';
import { BasePaginationRequest } from './base.schema';
export interface InovasiIndikatorParams extends BasePaginationRequest {
    q?: string;
}
export type ListInovasiIndikator = InovasiIndikator[];
