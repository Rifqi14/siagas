/// <reference types="multer" />
import { z } from 'zod';
import { BasePaginationRequest } from './base.schema';
import { ProfilPemda } from '../entity/ProfilePemda.entity';
declare const createProfilPemda: z.ZodObject<{
    body: z.ZodObject<{
        nama_pemda: z.ZodString;
        nama_daerah: z.ZodOptional<z.ZodString>;
        opd_yang_menangani: z.ZodOptional<z.ZodString>;
        alamat_pemda: z.ZodOptional<z.ZodString>;
        email: z.ZodOptional<z.ZodString>;
        no_telpon: z.ZodOptional<z.ZodString>;
        nama_admin: z.ZodOptional<z.ZodString>;
    }, "strip", z.ZodTypeAny, {
        nama_pemda: string;
        nama_daerah?: string | undefined;
        opd_yang_menangani?: string | undefined;
        alamat_pemda?: string | undefined;
        email?: string | undefined;
        no_telpon?: string | undefined;
        nama_admin?: string | undefined;
    }, {
        nama_pemda: string;
        nama_daerah?: string | undefined;
        opd_yang_menangani?: string | undefined;
        alamat_pemda?: string | undefined;
        email?: string | undefined;
        no_telpon?: string | undefined;
        nama_admin?: string | undefined;
    }>;
}, "strip", z.ZodTypeAny, {
    body: {
        nama_pemda: string;
        nama_daerah?: string | undefined;
        opd_yang_menangani?: string | undefined;
        alamat_pemda?: string | undefined;
        email?: string | undefined;
        no_telpon?: string | undefined;
        nama_admin?: string | undefined;
    };
}, {
    body: {
        nama_pemda: string;
        nama_daerah?: string | undefined;
        opd_yang_menangani?: string | undefined;
        alamat_pemda?: string | undefined;
        email?: string | undefined;
        no_telpon?: string | undefined;
        nama_admin?: string | undefined;
    };
}>;
export type CreateProfilPemdaRequest = z.infer<typeof createProfilPemda>['body'] & {
    file?: Express.Multer.File;
};
export interface ProfilPemdaParams extends BasePaginationRequest {
    q?: string;
}
export type ListProfilPemda = ProfilPemda[];
export interface ProfilPemdaFilterDownload {
    q?: string;
}
export interface ProfilPemdaDownloadResponse {
    no: number;
    daerah: string;
    nama_pemda: string;
    opd_yang_menangani: string;
    alamat_pemda: string;
    email: string;
    no_telepon: string;
    nama_admin: string;
}
export declare const toDownload: (data: ProfilPemda, no: number) => ProfilPemdaDownloadResponse;
export {};
