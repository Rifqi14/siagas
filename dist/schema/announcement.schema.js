"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.createAnnouncementSchema = void 0;
const zod_1 = require("zod");
exports.createAnnouncementSchema = (0, zod_1.object)({
    body: (0, zod_1.object)({
        title: (0, zod_1.string)(),
        slug: (0, zod_1.string)(),
        content: (0, zod_1.string)(),
        document: (0, zod_1.any)({ required_error: 'Document is required' })
    })
});
