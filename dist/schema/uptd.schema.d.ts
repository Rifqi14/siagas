import { z } from 'zod';
import { BasePaginationRequest } from './base.schema';
export declare const createUptdSchema: z.ZodObject<{
    body: z.ZodObject<{
        name: z.ZodString;
        regional_apparatus_id: z.ZodNumber;
    }, "strip", z.ZodTypeAny, {
        name: string;
        regional_apparatus_id: number;
    }, {
        name: string;
        regional_apparatus_id: number;
    }>;
}, "strip", z.ZodTypeAny, {
    body: {
        name: string;
        regional_apparatus_id: number;
    };
}, {
    body: {
        name: string;
        regional_apparatus_id: number;
    };
}>;
export type CreateUptdRequest = z.infer<typeof createUptdSchema>['body'];
export interface GetUptdRequest extends BasePaginationRequest {
    q?: string;
}
