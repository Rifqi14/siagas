"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.createDocumentCategorySchema = void 0;
const zod_1 = require("zod");
exports.createDocumentCategorySchema = (0, zod_1.object)({
    body: (0, zod_1.object)({
        name: (0, zod_1.string)({ required_error: 'Name is required' }),
        slug: (0, zod_1.string)({ required_error: 'Slug is required' })
            .refine(data => !/[A-Z]/g.test(data), {
            message: 'Slug cannot contain upper case',
            path: ['body', 'slug']
        })
            .refine(data => !data.includes(' '), {
            message: 'Slug cannot contain space or blank space',
            path: ['body', 'slug']
        })
    })
});
