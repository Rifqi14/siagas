import { z } from 'zod';
import { BasePaginationRequest } from './base.schema';
export declare const createGovernmentSectorSchema: z.ZodObject<{
    body: z.ZodObject<{
        name: z.ZodString;
        deadline: z.ZodString;
    }, "strip", z.ZodTypeAny, {
        name: string;
        deadline: string;
    }, {
        name: string;
        deadline: string;
    }>;
}, "strip", z.ZodTypeAny, {
    body: {
        name: string;
        deadline: string;
    };
}, {
    body: {
        name: string;
        deadline: string;
    };
}>;
export type CreateGovernmentSectorRequest = z.infer<typeof createGovernmentSectorSchema>['body'];
export interface GetGovernmentSectorRequest extends BasePaginationRequest {
    name?: string;
}
