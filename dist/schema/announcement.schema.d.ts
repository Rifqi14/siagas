import { z } from 'zod';
import { BasePaginationRequest } from './base.schema';
export declare const createAnnouncementSchema: z.ZodObject<{
    body: z.ZodObject<{
        title: z.ZodString;
        slug: z.ZodString;
        content: z.ZodString;
        document: z.ZodAny;
    }, "strip", z.ZodTypeAny, {
        title: string;
        slug: string;
        content: string;
        document?: any;
    }, {
        title: string;
        slug: string;
        content: string;
        document?: any;
    }>;
}, "strip", z.ZodTypeAny, {
    body: {
        title: string;
        slug: string;
        content: string;
        document?: any;
    };
}, {
    body: {
        title: string;
        slug: string;
        content: string;
        document?: any;
    };
}>;
export type CreateAnnouncementRequest = Omit<z.infer<typeof createAnnouncementSchema>['body'], 'document'>;
export interface GetAnnouncementRequest extends BasePaginationRequest {
    q?: string;
    category?: string;
}
