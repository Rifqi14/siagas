import { z } from 'zod';
import { BasePaginationRequest } from './base.schema';
export declare const createRegionApparatusScheme: z.ZodObject<{
    body: z.ZodObject<{
        name: z.ZodString;
    }, "strip", z.ZodTypeAny, {
        name: string;
    }, {
        name: string;
    }>;
}, "strip", z.ZodTypeAny, {
    body: {
        name: string;
    };
}, {
    body: {
        name: string;
    };
}>;
export type CreateRegionApparatusRequest = z.infer<typeof createRegionApparatusScheme>['body'];
export interface GetRegionApparatusRequest extends BasePaginationRequest {
    q?: string;
}
