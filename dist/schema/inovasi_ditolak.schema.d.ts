import { ReviewInovasiDaerah } from '../entity/ReviewInovasiDaerah.entity';
import { BasePaginationRequest } from './base.schema';
export interface InovasiDitolakParams extends Partial<BasePaginationRequest> {
    q?: string;
    pemda_id?: number;
}
export interface InovasiDitolakResponse {
    review_inovasi_id: string | null;
    nomor: string | null;
    judul: string | null;
    pemda: {
        pemda_id: string | null;
        pemda_name: string | null;
    } | null;
    waktu_penerapan: string | null;
    kematangan: number | null;
    skor_verifikasi: number | null;
    qc: string | null;
}
export interface DownloadInovasiDitolakResponse {
    no: number;
    nomor: string;
    judul: string;
    nama_pemda: string;
    waktu_penerapan: string;
    kematangan: string;
    skor_verifikasi: string;
    QC: string;
}
export declare const toDownloadResponse: (data: ReviewInovasiDaerah, no: number) => DownloadInovasiDitolakResponse;
