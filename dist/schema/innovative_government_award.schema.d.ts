import { PeringkatHasilReview } from '../entity/PeringkatHasilReview.entity';
import { BasePaginationRequest } from './base.schema';
export interface PeringkatHasilReviewParams extends Partial<BasePaginationRequest> {
    q?: string;
    pemda_id?: number;
}
export interface PeringkatHasilReviewResponse {
    pemda_id: number;
    nama_pemda: string;
    jumlah_inovasi: number;
    isp: number;
    rata_rata: number;
    skor_total: number;
    predikat: string;
    nominator: string;
}
export interface PeringkatHasilReviewNominatorRequest {
    nominator: string;
}
export declare const toPeringkatResponse: (data: PeringkatHasilReview, jumlah_pemda: number) => PeringkatHasilReviewResponse;
export interface PrestasiResponse {
    pemda_id: number;
    nama_pemda: string;
    skor_pengukuran: number;
    presentasi: number;
    validasi_lapangan: number;
    skor_akhir: number;
}
export declare const toPrestasiResponse: (data: PeringkatHasilReview, jumlah_pemda: number) => PrestasiResponse;
export interface RangkingResponse {
    pemda_id: number;
    nama_pemda: string;
    skor_indeks: number;
    skor_penilaian: number;
    skor_akhir: number;
    predikat: string;
}
export declare const toRangkingResponse: (data: PeringkatHasilReview, jumlah_pemda: number) => RangkingResponse;
