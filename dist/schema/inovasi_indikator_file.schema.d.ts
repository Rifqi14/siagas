/// <reference types="multer" />
import { z } from 'zod';
import { BasePaginationRequest } from './base.schema';
import { InovasiIndikatorFile } from '../entity/InovasiIndikatorFile.entity';
export declare const uploadInovasiIndikatorFileSchema: z.ZodObject<{
    body: z.ZodObject<{
        nomor_surat: z.ZodString;
        tanggal_surat: z.ZodString;
        tentang: z.ZodString;
        name: z.ZodOptional<z.ZodString>;
    }, "strip", z.ZodTypeAny, {
        nomor_surat: string;
        tanggal_surat: string;
        tentang: string;
        name?: string | undefined;
    }, {
        nomor_surat: string;
        tanggal_surat: string;
        tentang: string;
        name?: string | undefined;
    }>;
}, "strip", z.ZodTypeAny, {
    body: {
        nomor_surat: string;
        tanggal_surat: string;
        tentang: string;
        name?: string | undefined;
    };
}, {
    body: {
        nomor_surat: string;
        tanggal_surat: string;
        tentang: string;
        name?: string | undefined;
    };
}>;
export type UploadInovasiIndikatorFileRequest = z.infer<typeof uploadInovasiIndikatorFileSchema>['body'] & {
    dokumen?: Express.Multer.File;
};
export interface InovasiIndikatorFileParams extends BasePaginationRequest {
    q?: string;
}
export type ListInovasiIndikatorFiles = InovasiIndikatorFile[];
