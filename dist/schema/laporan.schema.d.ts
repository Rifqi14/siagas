import { LaporanIndeks } from '../entity/LaporanIndeks.entity';
import { BasePaginationRequest } from './base.schema';
export interface LaporanIndeksParams extends Partial<BasePaginationRequest> {
    q?: string;
    predikat?: 'semua' | 'belum mengisi data' | 'kurang inovatif' | 'inovatif' | 'terinovatif';
    pemda_id?: number;
    opd_yang_menangani?: 'Badan Litbang' | 'Bappeda Litbang' | 'Diluar';
}
export interface LaporanIndeksResponse {
    pemda_id: string;
    nama_pemda: string;
    opd_yang_menangani: string;
    skor: string;
    rangking: string;
    predikat: string;
}
export declare const toLaporanIndexResponse: (indeks: LaporanIndeks) => LaporanIndeksResponse;
export interface LaporanJenisInovasiParams extends Partial<BasePaginationRequest> {
    pemda_id?: number;
    q?: string;
}
export interface LaporanBentukInovasiParams extends Partial<BasePaginationRequest> {
    pemda_id?: number;
    q?: string;
}
export interface LaporanInisiatorInovasiParams extends Partial<BasePaginationRequest> {
    pemda_id?: number;
    q?: string;
}
export interface LaporanUrusanInovasiParams extends Partial<BasePaginationRequest> {
    pemda_id?: number;
    q?: string;
}
