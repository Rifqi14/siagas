"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.toReviewInovasiDaerahDownload = void 0;
const toReviewInovasiDaerahDownload = (data, no) => {
    var _a, _b, _c, _d, _e, _f, _g, _h, _j, _k, _l, _m;
    let skor = 0;
    switch (data.inovasi.innovation_phase) {
        case 'inisiatif':
            skor = 50;
            break;
        case 'uji coba':
            skor = 102;
            break;
        case 'penerapan':
            skor = 105;
            break;
        default:
            skor = 0;
            break;
    }
    let skor_verifikasi = skor;
    if (data.status == 'Rejected')
        skor_verifikasi = 10;
    if (data.status == 'Accept')
        skor_verifikasi += 50;
    return {
        no,
        nomor: (_b = (_a = data.random_number) === null || _a === void 0 ? void 0 : _a.toString()) !== null && _b !== void 0 ? _b : '',
        judul: (_d = (_c = data.inovasi) === null || _c === void 0 ? void 0 : _c.innovation_name) !== null && _d !== void 0 ? _d : '',
        pemda: (_g = (_f = (_e = data.inovasi) === null || _e === void 0 ? void 0 : _e.profilPemda) === null || _f === void 0 ? void 0 : _f.nama_daerah) !== null && _g !== void 0 ? _g : '',
        waktu_penerapan: (_j = (_h = data.inovasi) === null || _h === void 0 ? void 0 : _h.implementation_time) !== null && _j !== void 0 ? _j : '',
        skor: (_k = skor.toString()) !== null && _k !== void 0 ? _k : '',
        skor_verifikasi: (_l = skor_verifikasi.toString()) !== null && _l !== void 0 ? _l : '',
        qc: (_m = data.status) !== null && _m !== void 0 ? _m : '',
    };
};
exports.toReviewInovasiDaerahDownload = toReviewInovasiDaerahDownload;
