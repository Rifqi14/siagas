import { z } from 'zod';
import { BasePaginationRequest } from './base.schema';
export declare const createClusterDetailSchema: z.ZodObject<{
    body: z.ZodObject<{
        region_id: z.ZodNumber;
    }, "strip", z.ZodTypeAny, {
        region_id: number;
    }, {
        region_id: number;
    }>;
}, "strip", z.ZodTypeAny, {
    body: {
        region_id: number;
    };
}, {
    body: {
        region_id: number;
    };
}>;
export type CreateClusterDetailRequest = z.infer<typeof createClusterDetailSchema>['body'];
export interface GetClusterDetailRequest extends BasePaginationRequest {
    q?: string;
}
