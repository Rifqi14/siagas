"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.createTuxedoSchema = void 0;
const zod_1 = require("zod");
exports.createTuxedoSchema = (0, zod_1.object)({
    body: (0, zod_1.object)({
        title: (0, zod_1.string)(),
        slug: (0, zod_1.string)(),
        section: (0, zod_1.string)(),
        content: (0, zod_1.string)()
    })
});
