"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.loginSchema = void 0;
const zod_1 = require("zod");
exports.loginSchema = (0, zod_1.object)({
    body: (0, zod_1.object)({
        username: (0, zod_1.string)({ required_error: 'Username is required' }),
        password: (0, zod_1.string)({ required_error: 'Password is required' }),
        remember_me: (0, zod_1.boolean)().default(false),
    }),
});
