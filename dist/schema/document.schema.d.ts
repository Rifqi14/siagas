/// <reference types="multer" />
import { z } from 'zod';
import { BasePaginationRequest } from './base.schema';
export declare const createDocumentSchema: z.ZodObject<{
    body: z.ZodObject<{
        title: z.ZodString;
        content: z.ZodString;
    }, "strip", z.ZodTypeAny, {
        title: string;
        content: string;
    }, {
        title: string;
        content: string;
    }>;
}, "strip", z.ZodTypeAny, {
    body: {
        title: string;
        content: string;
    };
}, {
    body: {
        title: string;
        content: string;
    };
}>;
export type CreateDocumentRequest = z.infer<typeof createDocumentSchema>['body'] & {
    category_id?: number;
    document?: Express.Multer.File;
};
export interface GetDocumentRequest extends BasePaginationRequest {
    q?: string;
    category?: number;
}
