"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.createFaqSchema = void 0;
const zod_1 = require("zod");
exports.createFaqSchema = (0, zod_1.object)({
    body: (0, zod_1.object)({
        question: (0, zod_1.string)({ required_error: 'Question is required' }),
        answer: (0, zod_1.string)({ required_error: 'Answer is required' })
    })
});
