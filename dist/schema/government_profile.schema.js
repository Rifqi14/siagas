"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.createGovernmentProfileSchema = void 0;
const zod_1 = require("zod");
exports.createGovernmentProfileSchema = (0, zod_1.object)({
    body: (0, zod_1.object)({
        daerah: (0, zod_1.string)(),
        name: (0, zod_1.string)(),
        opd_menangani: (0, zod_1.string)(),
        alamat: (0, zod_1.string)(),
        email: (0, zod_1.string)().email(),
        nomor_telepon: (0, zod_1.string)(),
        nama_admin: (0, zod_1.string)()
    })
});
