"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.toLaporanIndexResponse = void 0;
const toLaporanIndexResponse = (indeks) => {
    var _a, _b, _c, _d, _e, _f, _g, _h, _j;
    return {
        pemda_id: (_b = (_a = indeks.pemda_id) === null || _a === void 0 ? void 0 : _a.toString()) !== null && _b !== void 0 ? _b : '',
        nama_pemda: (_c = indeks.nama_daerah) !== null && _c !== void 0 ? _c : '',
        opd_yang_menangani: (_d = indeks.opd_yang_menangani) !== null && _d !== void 0 ? _d : '',
        skor: (_f = (_e = indeks.nilai_indeks) === null || _e === void 0 ? void 0 : _e.toString()) !== null && _f !== void 0 ? _f : '',
        rangking: (_h = (_g = indeks.indeks) === null || _g === void 0 ? void 0 : _g.toString()) !== null && _h !== void 0 ? _h : '',
        predikat: (_j = indeks.predikat) !== null && _j !== void 0 ? _j : '',
    };
};
exports.toLaporanIndexResponse = toLaporanIndexResponse;
