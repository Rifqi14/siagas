import { BasePaginationRequest } from './base.schema';
export interface RekapIndeksAkhirParams extends Partial<BasePaginationRequest> {
    q?: string;
}
export interface UpdateNominatorRequest {
    nominator: 'Ya' | 'Tidak';
}
