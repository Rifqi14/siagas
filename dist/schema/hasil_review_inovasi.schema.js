"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.toDownloadResponse = void 0;
const toDownloadResponse = (data, no) => {
    var _a, _b, _c, _d, _e, _f, _g, _h, _j, _k, _l, _m, _o, _p, _q;
    let skor = 0;
    let kematangan = 0;
    switch ((_b = (_a = data.inovasi) === null || _a === void 0 ? void 0 : _a.innovation_phase) === null || _b === void 0 ? void 0 : _b.toLowerCase()) {
        case "inisiatif":
            skor = 50;
            kematangan = 3;
            break;
        case "uji coba":
            skor = 102;
            kematangan = 6;
            break;
        case "penerapan":
            skor = 105;
            kematangan = 9;
            break;
        default:
            skor = 0;
            break;
    }
    return {
        no,
        nomor: (_d = (_c = data.random_number) === null || _c === void 0 ? void 0 : _c.toString()) !== null && _d !== void 0 ? _d : "",
        judul: (_f = (_e = data.inovasi) === null || _e === void 0 ? void 0 : _e.innovation_name) !== null && _f !== void 0 ? _f : "",
        nama_pemda: (_j = (_h = (_g = data.inovasi) === null || _g === void 0 ? void 0 : _g.profilPemda) === null || _h === void 0 ? void 0 : _h.nama_daerah) !== null && _j !== void 0 ? _j : "",
        waktu_penerapan: (_m = (_l = (_k = data.inovasi) === null || _k === void 0 ? void 0 : _k.implementation_time) === null || _l === void 0 ? void 0 : _l.toString()) !== null && _m !== void 0 ? _m : "",
        kematangan: (_o = kematangan === null || kematangan === void 0 ? void 0 : kematangan.toString()) !== null && _o !== void 0 ? _o : "",
        skor_verifikasi: (_p = skor === null || skor === void 0 ? void 0 : skor.toString()) !== null && _p !== void 0 ? _p : "",
        QC: (_q = data.status) !== null && _q !== void 0 ? _q : "",
    };
};
exports.toDownloadResponse = toDownloadResponse;
