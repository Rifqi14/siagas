"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.uploadInovasiIndikatorFileSchema = void 0;
const zod_1 = require("zod");
exports.uploadInovasiIndikatorFileSchema = zod_1.z.object({
    body: zod_1.z.object({
        nomor_surat: zod_1.z.string(),
        tanggal_surat: zod_1.z.string(),
        tentang: zod_1.z.string(),
        name: zod_1.z.string().optional()
    })
});
