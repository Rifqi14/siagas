import { z } from 'zod';
import { BasePaginationRequest } from './base.schema';
export declare const createRegionsSchema: z.ZodObject<{
    body: z.ZodObject<{
        name: z.ZodString;
    }, "strip", z.ZodTypeAny, {
        name: string;
    }, {
        name: string;
    }>;
}, "strip", z.ZodTypeAny, {
    body: {
        name: string;
    };
}, {
    body: {
        name: string;
    };
}>;
export type CreateRegionRequest = z.infer<typeof createRegionsSchema>['body'];
export interface GetRegionRequest extends BasePaginationRequest {
    name?: string;
}
