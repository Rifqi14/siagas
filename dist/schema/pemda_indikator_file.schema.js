"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.uploadPemdaIndikatorFileSchema = void 0;
const zod_1 = require("zod");
exports.uploadPemdaIndikatorFileSchema = zod_1.z.object({
    body: zod_1.z.object({
        nomor_dokumen: zod_1.z.string(),
        tanggal_dokumen: zod_1.z.string(),
        tentang: zod_1.z.string(),
        name: zod_1.z.string().optional()
    })
});
