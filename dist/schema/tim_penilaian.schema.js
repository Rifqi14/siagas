"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.createTimPenilaianSchema = void 0;
const zod_1 = require("zod");
exports.createTimPenilaianSchema = zod_1.z.object({
    body: zod_1.z.object({
        asn_username: zod_1.z.string().optional(),
        nama: zod_1.z.string().optional(),
        instansi: zod_1.z.string().optional()
    })
});
