import { z } from 'zod';
import User from '../entity/User.entity';
import { BasePaginationRequest } from './base.schema';
import { OmitType } from '../types/lib';
export declare const createUserSchema: z.ZodObject<{
    body: z.ZodObject<{
        role_id: z.ZodNumber;
        nama_lengkap: z.ZodOptional<z.ZodString>;
        nama_pemda: z.ZodOptional<z.ZodString>;
        nama_panggilan: z.ZodOptional<z.ZodString>;
        email: z.ZodString;
        username: z.ZodEffects<z.ZodString, string, string>;
        password: z.ZodString;
    }, "strip", z.ZodTypeAny, {
        role_id: number;
        username: string;
        password: string;
        email: string;
        nama_lengkap?: string | undefined;
        nama_pemda?: string | undefined;
        nama_panggilan?: string | undefined;
    }, {
        role_id: number;
        username: string;
        password: string;
        email: string;
        nama_lengkap?: string | undefined;
        nama_pemda?: string | undefined;
        nama_panggilan?: string | undefined;
    }>;
}, "strip", z.ZodTypeAny, {
    body: {
        role_id: number;
        username: string;
        password: string;
        email: string;
        nama_lengkap?: string | undefined;
        nama_pemda?: string | undefined;
        nama_panggilan?: string | undefined;
    };
}, {
    body: {
        role_id: number;
        username: string;
        password: string;
        email: string;
        nama_lengkap?: string | undefined;
        nama_pemda?: string | undefined;
        nama_panggilan?: string | undefined;
    };
}>;
export type CreateUserRequest = z.infer<typeof createUserSchema>['body'];
export interface GetUserRequest extends Partial<BasePaginationRequest> {
    /**
     * Search user by username or email or full name
     */
    q?: string;
    role_id?: number;
}
export type UserResponse = OmitType<User, 'createPassword'>;
export type UserResponses = UserResponse[];
