import { z } from 'zod';
import Role from '../entity/Role.entity';
import User from '../entity/User.entity';
export declare const loginSchema: z.ZodObject<{
    body: z.ZodObject<{
        username: z.ZodString;
        password: z.ZodString;
        remember_me: z.ZodDefault<z.ZodBoolean>;
    }, "strip", z.ZodTypeAny, {
        username: string;
        password: string;
        remember_me: boolean;
    }, {
        username: string;
        password: string;
        remember_me?: boolean | undefined;
    }>;
}, "strip", z.ZodTypeAny, {
    body: {
        username: string;
        password: string;
        remember_me: boolean;
    };
}, {
    body: {
        username: string;
        password: string;
        remember_me?: boolean | undefined;
    };
}>;
export type LoginRequest = z.infer<typeof loginSchema>['body'];
export interface TokenPayload {
    user_id: number;
    username: string;
    role: Role;
}
export interface TokenResponse {
    token: string;
    role: Role;
    user: User;
    token_expired_at: number | null;
}
