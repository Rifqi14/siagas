import { z } from 'zod';
import { BasePaginationRequest } from './base.schema';
export declare const createFaqSchema: z.ZodObject<{
    body: z.ZodObject<{
        question: z.ZodString;
        answer: z.ZodString;
    }, "strip", z.ZodTypeAny, {
        question: string;
        answer: string;
    }, {
        question: string;
        answer: string;
    }>;
}, "strip", z.ZodTypeAny, {
    body: {
        question: string;
        answer: string;
    };
}, {
    body: {
        question: string;
        answer: string;
    };
}>;
export type CreateFaqRequest = z.infer<typeof createFaqSchema>['body'];
export interface GetFaqRequest extends BasePaginationRequest {
    q?: string;
}
