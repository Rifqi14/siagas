import { z } from 'zod';
import { BasePaginationRequest } from './base.schema';
export declare const createDocumentCategorySchema: z.ZodObject<{
    body: z.ZodObject<{
        name: z.ZodString;
        slug: z.ZodEffects<z.ZodEffects<z.ZodString, string, string>, string, string>;
    }, "strip", z.ZodTypeAny, {
        name: string;
        slug: string;
    }, {
        name: string;
        slug: string;
    }>;
}, "strip", z.ZodTypeAny, {
    body: {
        name: string;
        slug: string;
    };
}, {
    body: {
        name: string;
        slug: string;
    };
}>;
export type CreateDocumentCategoryRequest = z.infer<typeof createDocumentCategorySchema>['body'];
export interface GetDocumentCategoryRequest extends BasePaginationRequest {
    q?: string;
}
