import { DashboardArsip } from "../entity/DashboardArsip.entity";
import { BasePaginationRequest } from "./base.schema";
export interface DashboardArsipParams extends Partial<BasePaginationRequest> {
    q?: string;
    tahapan?: "semua" | "inisiatif" | "uji coba" | "penerapan";
    pemda_id?: string;
}
export interface DashboardArsipDownloadRequest {
    q?: string;
    tahapan?: "semua" | "inisiatif" | "uji coba" | "penerapan";
    pemda_id?: string;
}
export interface StatistikIndikatorInovasiParams {
    pemda_id: string;
}
export interface StatistikIndikatorInovasiResponse {
    id: string;
    nama_indikator: string;
    total_indikator: number;
    jumlah_upload: number;
}
export interface StatistikInovasiResponse {
    total_inovasi: number;
    total_inisiatif: number;
    total_uji_coba: number;
    total_penerapan: number;
}
export interface DashboardResponse {
    indeks_rata_rata: number;
    total_inovasi: number;
    total_uji_coba: number;
    total_penerapan: number;
    total_inisiatif: number;
    daerah_inovasi_tertinggi: string;
    daerah_inovasi_terendah: string;
}
export interface DashboardOPDResponse {
    total_badan_litbang: number;
    total_bappeda_litbang: number;
    total_diluar: number;
}
export interface DashboardArsipDownload {
    no: number;
    nama_pemda: string;
    nama_inovasi: string;
    tahapan_inovasi: string;
    waktu_uji_coba: string;
    waktu_penerapan: string;
    kematangan: number;
}
export declare const toDashboardArsipDownload: (data: DashboardArsip, idx: number) => DashboardArsipDownload;
