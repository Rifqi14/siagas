"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.createExportRequest = void 0;
const zod_1 = require("zod");
exports.createExportRequest = (0, zod_1.object)({
    body: (0, zod_1.object)({
        name: (0, zod_1.string)(),
        format: zod_1.z.enum(['xlsx', 'csv']),
        headers: (0, zod_1.object)({
            header: (0, zod_1.string)(),
            key: (0, zod_1.string)()
        }).array(),
        data: (0, zod_1.unknown)().array().array()
    })
});
