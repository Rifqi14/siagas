import { z } from 'zod';
export declare const createExportRequest: z.ZodObject<{
    body: z.ZodObject<{
        name: z.ZodString;
        format: z.ZodEnum<["xlsx", "csv"]>;
        headers: z.ZodArray<z.ZodObject<{
            header: z.ZodString;
            key: z.ZodString;
        }, "strip", z.ZodTypeAny, {
            header: string;
            key: string;
        }, {
            header: string;
            key: string;
        }>, "many">;
        data: z.ZodArray<z.ZodArray<z.ZodUnknown, "many">, "many">;
    }, "strip", z.ZodTypeAny, {
        data: unknown[][];
        name: string;
        format: "xlsx" | "csv";
        headers: {
            header: string;
            key: string;
        }[];
    }, {
        data: unknown[][];
        name: string;
        format: "xlsx" | "csv";
        headers: {
            header: string;
            key: string;
        }[];
    }>;
}, "strip", z.ZodTypeAny, {
    body: {
        data: unknown[][];
        name: string;
        format: "xlsx" | "csv";
        headers: {
            header: string;
            key: string;
        }[];
    };
}, {
    body: {
        data: unknown[][];
        name: string;
        format: "xlsx" | "csv";
        headers: {
            header: string;
            key: string;
        }[];
    };
}>;
export type CreateExportRequest = z.infer<typeof createExportRequest>['body'];
