"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const express_1 = require("express");
const export_controller_1 = __importDefault(require("../controller/export.controller"));
const base_routes_1 = __importDefault(require("./base.routes"));
class ExportRoutes extends base_routes_1.default {
    constructor(express) {
        super(express, new export_controller_1.default(), '/export');
    }
    routes() {
        let express = this.express;
        const route = (0, express_1.Router)();
        route.post('', this.controller.export);
        express.use(this.prefixRoutes, route);
        return express;
    }
}
exports.default = ExportRoutes;
