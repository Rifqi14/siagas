import { Application } from 'express';
import AuthenticationController from '../controller/authentication.controller';
import BaseRoutes from './base.routes';
declare class AuthenticationRoutes extends BaseRoutes<AuthenticationController> {
    constructor(express: Application);
    routes(): Application;
}
export default AuthenticationRoutes;
