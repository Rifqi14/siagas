import { Application } from 'express';
import UserController from '../controller/user.controller';
import BaseRoutes from './base.routes';
declare class UserRoutes extends BaseRoutes<UserController> {
    constructor(express: Application);
    routes(): Application;
}
export default UserRoutes;
