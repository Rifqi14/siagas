"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.RangkingIndexRoute = void 0;
const rangking_index_controller_1 = require("../controller/rangking_index.controller");
const base_routes_1 = __importDefault(require("./base.routes"));
const express_1 = require("express");
const jwt_middleware_1 = require("../middleware/jwt.middleware");
class RangkingIndexRoute extends base_routes_1.default {
    constructor(express) {
        super(express, new rangking_index_controller_1.RangkingIndexController(), "/ranking_index");
    }
    routes() {
        let express = this.express;
        const route = (0, express_1.Router)();
        route.use(jwt_middleware_1.authenticated);
        route.get("", this.controller.get);
        route.get("/download/:type", this.controller.getDownload);
        route.patch("/:id", this.controller.update);
        express.use(this.prefixRoutes, route);
        return express;
    }
}
exports.RangkingIndexRoute = RangkingIndexRoute;
