import { RangkingIndexController } from "../controller/rangking_index.controller";
import BaseRoutes from "./base.routes";
import { Application } from "express";
export declare class RangkingIndexRoute extends BaseRoutes<RangkingIndexController> {
    constructor(express: Application);
    routes(): Application;
}
