import { Application } from 'express';
import FileController from '../controller/file.controller';
import BaseRoutes from './base.routes';
declare class FileRoutes extends BaseRoutes<FileController> {
    constructor(express: Application);
    routes(): Application;
}
export default FileRoutes;
