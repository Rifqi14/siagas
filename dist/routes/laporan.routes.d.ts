import { LaporanController } from '../controller/laporan.controller';
import BaseRoutes from './base.routes';
import { Application } from 'express';
export declare class LaporanRoute extends BaseRoutes<LaporanController> {
    constructor(express: Application);
    routes(): Application;
}
