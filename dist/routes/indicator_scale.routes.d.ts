import { Application } from 'express';
import IndicatorScaleController from '../controller/indicator_scale.controller';
import BaseRoutes from './base.routes';
declare class IndicatorScaleRoutes extends BaseRoutes<IndicatorScaleController> {
    constructor(express: Application);
    routes(): Application;
}
export default IndicatorScaleRoutes;
