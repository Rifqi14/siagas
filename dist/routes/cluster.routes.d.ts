import { Application } from 'express';
import ClusterController from '../controller/cluster.controller';
import BaseRoutes from './base.routes';
declare class ClusterRoutes extends BaseRoutes<ClusterController> {
    constructor(express: Application);
    routes(): Application;
}
export default ClusterRoutes;
