"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const express_1 = require("express");
const indicator_scale_controller_1 = __importDefault(require("../controller/indicator_scale.controller"));
const base_routes_1 = __importDefault(require("./base.routes"));
class IndicatorScaleRoutes extends base_routes_1.default {
    constructor(express) {
        super(express, new indicator_scale_controller_1.default(), '/indikator');
    }
    routes() {
        let express = this.express;
        const route = (0, express_1.Router)();
        route.get('/:indikator_id/skala', this.controller.get);
        route.get('/skala/:id', this.controller.detail);
        route.post('/:indikator_id/skala', this.controller.create);
        route.patch('/skala/:id', this.controller.update);
        route.delete('/skala/:id', this.controller.delete);
        express.use(this.prefixRoutes, route);
        return express;
    }
}
exports.default = IndicatorScaleRoutes;
