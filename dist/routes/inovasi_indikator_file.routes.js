"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.InovasiIndikatorFileRoutes = void 0;
const inovasi_indikator_file_controller_1 = require("../controller/inovasi_indikator_file.controller");
const base_routes_1 = __importDefault(require("./base.routes"));
const express_1 = require("express");
const jwt_middleware_1 = require("../middleware/jwt.middleware");
const file_middleware_1 = require("../middleware/file.middleware");
const validation_middleware_1 = require("../middleware/validation.middleware");
const inovasi_indikator_file_schema_1 = require("../schema/inovasi_indikator_file.schema");
class InovasiIndikatorFileRoutes extends base_routes_1.default {
    constructor(express) {
        super(express, new inovasi_indikator_file_controller_1.InovasiIndikatorFileController(), '/inovasi_pemerintah_daerah/indikator');
    }
    routes() {
        let express = this.express;
        const route = (0, express_1.Router)();
        route.use(jwt_middleware_1.authenticated);
        route.get('/:inovasi_id/:indikator_id/files', this.controller.get);
        route.get('/:inovasi_id/:indikator_id/files/:file_id', this.controller.detail);
        route.post('/:inovasi_id/:indikator_id/upload', (0, file_middleware_1.upload)((0, file_middleware_1.storage)('public/upload/inovasi/dokumen/')).single('dokumen'), (0, validation_middleware_1.validate)(inovasi_indikator_file_schema_1.uploadInovasiIndikatorFileSchema), this.controller.upload);
        route.delete('/:inovasi_id/:indikator_id/delete/:file_id', this.controller.delete);
        express.use(this.prefixRoutes, route);
        return express;
    }
}
exports.InovasiIndikatorFileRoutes = InovasiIndikatorFileRoutes;
