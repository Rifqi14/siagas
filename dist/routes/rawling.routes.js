"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.RawlingRoute = void 0;
const rawling_controller_1 = require("../controller/rawling.controller");
const base_routes_1 = __importDefault(require("./base.routes"));
const express_1 = require("express");
const jwt_middleware_1 = require("../middleware/jwt.middleware");
class RawlingRoute extends base_routes_1.default {
    constructor(express) {
        super(express, new rawling_controller_1.RawlingController(), '/rawlog');
    }
    routes() {
        let express = this.express;
        const route = (0, express_1.Router)();
        route.use(jwt_middleware_1.authenticated);
        route.get('', this.controller.rawling);
        express.use(this.prefixRoutes, route);
        return express;
    }
}
exports.RawlingRoute = RawlingRoute;
