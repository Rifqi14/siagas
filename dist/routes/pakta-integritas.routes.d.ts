import PaktaIntegritasController from '../controller/pakta-integritas.controller';
import BaseRoutes from './base.routes';
import { Application } from 'express';
declare class PaktaIntegritasRoutes extends BaseRoutes<PaktaIntegritasController> {
    constructor(express: Application);
    routes(): Application;
}
export default PaktaIntegritasRoutes;
