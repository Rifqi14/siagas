import { DashboardController } from "../controller/dashboard.controller";
import BaseRoutes from "./base.routes";
import { Application } from "express";
export declare class DashboardRoute extends BaseRoutes<DashboardController> {
    constructor(express: Application);
    routes(): Application;
}
