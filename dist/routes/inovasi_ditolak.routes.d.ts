import { InovasiDitolakController } from '../controller/inovasi_ditolak.controller';
import BaseRoutes from './base.routes';
import { Application } from 'express';
export declare class InovasiDitolakRoute extends BaseRoutes<InovasiDitolakController> {
    constructor(express: Application);
    routes(): Application;
}
