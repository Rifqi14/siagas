"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.InnovativeGovernmentAwardRoute = void 0;
const innovative_government_award_controller_1 = require("../controller/innovative_government_award.controller");
const base_routes_1 = __importDefault(require("./base.routes"));
const express_1 = require("express");
const jwt_middleware_1 = require("../middleware/jwt.middleware");
class InnovativeGovernmentAwardRoute extends base_routes_1.default {
    constructor(express) {
        super(express, new innovative_government_award_controller_1.InnovativeGovernmentAwardController(), '/innovative_government_award');
    }
    routes() {
        let express = this.express;
        const route = (0, express_1.Router)();
        route.use(jwt_middleware_1.authenticated);
        route.get('/rangking', this.controller.rangking);
        route.get('/rangking/download/:type', this.controller.rangking_download);
        route.get('/prestasi', this.controller.prestasi);
        route.get('/prestasi/download/:type', this.controller.prestasi_download);
        route.get('/peringkat_hasil_review', this.controller.peringkat_hasil_review);
        route.get('/peringkat_hasil_review/download/:type', this.controller.peringkat_hasil_review_download);
        route.patch('/peringkat_hasil_review/:pemda_id', this.controller.nominator_peringkat_hasil_review);
        express.use(this.prefixRoutes, route);
        return express;
    }
}
exports.InnovativeGovernmentAwardRoute = InnovativeGovernmentAwardRoute;
