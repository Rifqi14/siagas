import { Application } from 'express';
import GovernmentProfileController from '../controller/government_profile.controller';
import BaseRoutes from './base.routes';
declare class GovernmentProfileRoutes extends BaseRoutes<GovernmentProfileController> {
    constructor(express: Application);
    routes(): Application;
}
export default GovernmentProfileRoutes;
