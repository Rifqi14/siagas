import { Application } from 'express';
import RegionalApparatusController from '../controller/regional_apparatus.controller';
import BaseRoutes from './base.routes';
declare class RegionalApparatusRoutes extends BaseRoutes<RegionalApparatusController> {
    constructor(express: Application);
    routes(): Application;
}
export default RegionalApparatusRoutes;
