import { Application } from 'express';
import DocumentCategoryController from '../controller/document_category.controller';
import BaseRoutes from './base.routes';
declare class DocumentCategoryRoutes extends BaseRoutes<DocumentCategoryController> {
    constructor(express: Application);
    routes(): Application;
}
export default DocumentCategoryRoutes;
