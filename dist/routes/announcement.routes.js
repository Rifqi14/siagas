"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const express_1 = require("express");
const announcement_controller_1 = __importDefault(require("../controller/announcement.controller"));
const file_middleware_1 = require("../middleware/file.middleware");
const jwt_middleware_1 = require("../middleware/jwt.middleware");
const validation_middleware_1 = require("../middleware/validation.middleware");
const announcement_schema_1 = require("../schema/announcement.schema");
const base_routes_1 = __importDefault(require("./base.routes"));
class AnnouncementRoutes extends base_routes_1.default {
    constructor(express) {
        super(express, new announcement_controller_1.default(), '/pengumuman');
    }
    routes() {
        let express = this.express;
        const route = (0, express_1.Router)();
        route.get('', this.controller.get);
        route.get('/:id', this.controller.detail);
        route.use(jwt_middleware_1.authenticated);
        route.post('', (0, file_middleware_1.upload)((0, file_middleware_1.storage)('public/upload/pengumuman/')).single('document'), (0, validation_middleware_1.validate)(announcement_schema_1.createAnnouncementSchema), this.controller.create);
        route.patch('/:id', (0, file_middleware_1.upload)((0, file_middleware_1.storage)('public/upload/pengumuman/')).single('document'), (0, validation_middleware_1.validate)(announcement_schema_1.createAnnouncementSchema), this.controller.update);
        route.delete('/:id', this.controller.delete);
        express.use(this.prefixRoutes, route);
        return express;
    }
}
exports.default = AnnouncementRoutes;
