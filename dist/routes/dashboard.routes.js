"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.DashboardRoute = void 0;
const dashboard_controller_1 = require("../controller/dashboard.controller");
const base_routes_1 = __importDefault(require("./base.routes"));
const express_1 = require("express");
class DashboardRoute extends base_routes_1.default {
    constructor(express) {
        super(express, new dashboard_controller_1.DashboardController(), "/dashboard");
    }
    routes() {
        let express = this.express;
        const route = (0, express_1.Router)();
        route.get("/arsip", this.controller.arsip);
        route.get("/arsip/download/:type", this.controller.arsip_download);
        route.get("/statistik_indikator", this.controller.statistik_indikator);
        route.get("/statistik_inovasi", this.controller.statistik_inovasi);
        route.get("/statistik_opd", this.controller.statistik_opd);
        express.use(this.prefixRoutes, route);
        return express;
    }
}
exports.DashboardRoute = DashboardRoute;
