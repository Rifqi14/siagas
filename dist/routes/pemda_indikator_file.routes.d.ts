import { Application } from 'express';
import BaseRoutes from './base.routes';
import { PemdaIndikatorFileController } from '../controller/pemda_indikator_file.controller';
export declare class PemdaIndikatorFileRoutes extends BaseRoutes<PemdaIndikatorFileController> {
    constructor(express: Application);
    routes(): Application;
}
