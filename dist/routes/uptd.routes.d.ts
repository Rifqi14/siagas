import { Application } from 'express';
import UptdController from '../controller/uptd.controller';
import BaseRoutes from './base.routes';
declare class UptdRoutes extends BaseRoutes<UptdController> {
    constructor(express: Application);
    routes(): Application;
}
export default UptdRoutes;
