"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const pemda_indikator_controller_1 = __importDefault(require("../controller/pemda_indikator.controller"));
const base_routes_1 = __importDefault(require("./base.routes"));
const express_1 = require("express");
const jwt_middleware_1 = require("../middleware/jwt.middleware");
class PemdaIndikatorRoutes extends base_routes_1.default {
    constructor(express) {
        super(express, new pemda_indikator_controller_1.default(), '/profil_pemda');
    }
    routes() {
        let express = this.express;
        const route = (0, express_1.Router)();
        route.use(jwt_middleware_1.authenticated);
        route.post('/:pemda_id/indikator', this.controller.create);
        route.get('/:pemda_id/indikator', this.controller.get);
        route.get('/:pemda_id/indikator/:indikator_id', this.controller.detail);
        route.patch('/:pemda_id/indikator/:pemda_indikator_id', this.controller.update);
        route.delete('/:pemda_id/indikator/:pemda_indikator_id', this.controller.delete);
        express.use(this.prefixRoutes, route);
        return express;
    }
}
exports.default = PemdaIndikatorRoutes;
