import { Application } from 'express';
import { RekapIndeksAkhirController } from '../controller/rekap_indeks_akhir.controller';
import BaseRoutes from './base.routes';
export declare class RekapIndeksAkhirRoute extends BaseRoutes<RekapIndeksAkhirController> {
    constructor(express: Application);
    routes(): Application;
}
