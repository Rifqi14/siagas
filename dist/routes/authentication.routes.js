"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const express_1 = require("express");
const authentication_controller_1 = __importDefault(require("../controller/authentication.controller"));
const base_routes_1 = __importDefault(require("./base.routes"));
const jwt_middleware_1 = require("../middleware/jwt.middleware");
class AuthenticationRoutes extends base_routes_1.default {
    constructor(express) {
        super(express, new authentication_controller_1.default(), '/authentication');
    }
    routes() {
        let express = this.express;
        const route = (0, express_1.Router)();
        route.get('/check_email', this.controller.checkEmail);
        route.get('/me', jwt_middleware_1.authenticated, this.controller.me);
        route.post('/login', this.controller.login);
        express.use(this.prefixRoutes, route);
        return express;
    }
}
exports.default = AuthenticationRoutes;
