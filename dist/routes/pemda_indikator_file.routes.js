"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.PemdaIndikatorFileRoutes = void 0;
const express_1 = require("express");
const base_routes_1 = __importDefault(require("./base.routes"));
const pemda_indikator_file_controller_1 = require("../controller/pemda_indikator_file.controller");
const jwt_middleware_1 = require("../middleware/jwt.middleware");
const file_middleware_1 = require("../middleware/file.middleware");
const validation_middleware_1 = require("../middleware/validation.middleware");
const pemda_indikator_file_schema_1 = require("../schema/pemda_indikator_file.schema");
class PemdaIndikatorFileRoutes extends base_routes_1.default {
    constructor(express) {
        super(express, new pemda_indikator_file_controller_1.PemdaIndikatorFileController(), '/profil_pemda/indikator');
    }
    routes() {
        let express = this.express;
        const route = (0, express_1.Router)();
        route.use(jwt_middleware_1.authenticated);
        route.get('/:pemda_id/:indikator_id/files', this.controller.get);
        route.get('/:pemda_id/:indikator_id/files/:file_id', this.controller.detail);
        route.post('/:pemda_id/:indikator_id/upload', (0, file_middleware_1.upload)((0, file_middleware_1.storage)('public/upload/pemda/dokumen/')).single('dokumen'), (0, validation_middleware_1.validate)(pemda_indikator_file_schema_1.uploadPemdaIndikatorFileSchema), this.controller.upload);
        route.delete('/:pemda_id/:indikator_id/delete/:file_id', this.controller.delete);
        express.use(this.prefixRoutes, route);
        return express;
    }
}
exports.PemdaIndikatorFileRoutes = PemdaIndikatorFileRoutes;
