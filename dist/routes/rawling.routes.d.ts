import { RawlingController } from '../controller/rawling.controller';
import BaseRoutes from './base.routes';
import { Application } from 'express';
export declare class RawlingRoute extends BaseRoutes<RawlingController> {
    constructor(express: Application);
    routes(): Application;
}
