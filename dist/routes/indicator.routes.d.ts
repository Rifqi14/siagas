import { Application } from 'express';
import IndicatorController from '../controller/indicator.controller';
import BaseRoutes from './base.routes';
declare class IndicatorRoutes extends BaseRoutes<IndicatorController> {
    constructor(express: Application);
    routes(): Application;
}
export default IndicatorRoutes;
