import { Application } from 'express';
import SettingController from '../controller/setting.controller';
import BaseRoutes from './base.routes';
declare class SettingRoutes extends BaseRoutes<SettingController> {
    constructor(express: Application);
    routes(): Application;
}
export default SettingRoutes;
