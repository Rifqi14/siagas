"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const express_1 = require("express");
const user_controller_1 = __importDefault(require("../controller/user.controller"));
const base_routes_1 = __importDefault(require("./base.routes"));
const jwt_middleware_1 = require("../middleware/jwt.middleware");
class UserRoutes extends base_routes_1.default {
    constructor(express) {
        super(express, new user_controller_1.default(), '/user');
    }
    routes() {
        let express = this.express;
        const route = (0, express_1.Router)();
        route.post('', this.controller.create);
        route.get('', this.controller.get);
        route.get('/:id', this.controller.detail);
        route.patch('/:id', this.controller.update);
        route.delete('/:id', this.controller.delete);
        express.use(this.prefixRoutes, jwt_middleware_1.authenticated, route);
        return express;
    }
}
exports.default = UserRoutes;
