import { InnovativeGovernmentAwardController } from '../controller/innovative_government_award.controller';
import BaseRoutes from './base.routes';
import { Application } from 'express';
export declare class InnovativeGovernmentAwardRoute extends BaseRoutes<InnovativeGovernmentAwardController> {
    constructor(express: Application);
    routes(): Application;
}
