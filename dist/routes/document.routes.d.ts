import { Application } from 'express';
import DocumentController from '../controller/document.controller';
import BaseRoutes from './base.routes';
declare class DocumentRoutes extends BaseRoutes<DocumentController> {
    constructor(express: Application);
    routes(): Application;
}
export default DocumentRoutes;
