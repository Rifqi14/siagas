"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.ReviewInovasiDaerahRoutes = void 0;
const review_inovasi_daerah_controller_1 = require("../controller/review_inovasi_daerah.controller");
const base_routes_1 = __importDefault(require("./base.routes"));
const express_1 = require("express");
const jwt_middleware_1 = require("../middleware/jwt.middleware");
class ReviewInovasiDaerahRoutes extends base_routes_1.default {
    constructor(express) {
        super(express, new review_inovasi_daerah_controller_1.ReviewInovasiDaerahController(), "/review_inovasi_daerah");
    }
    routes() {
        let express = this.express;
        const route = (0, express_1.Router)();
        route.use(jwt_middleware_1.authenticated);
        route.get("", this.controller.get);
        route.get("/download/:type", this.controller.getDownload);
        route.get("/:id", this.controller.detail);
        route.get("/:id/profil_inovasi", this.controller.profilInovasi);
        route.get("/:id/indikator", this.controller.indikator);
        route.get("/:id/indikator/:indikator_id", this.controller.indikatorDetail);
        route.get("/:id/indikator/:indikator_id/evaluasi", this.controller.getEvaluasi);
        route.post("", this.controller.create);
        route.patch("/:id", this.controller.update);
        route.patch("/:id/indikator/:indikator_id/evaluasi", this.controller.evaluasi);
        route.delete("/:id", this.controller.delete);
        express.use(this.prefixRoutes, route);
        return express;
    }
}
exports.ReviewInovasiDaerahRoutes = ReviewInovasiDaerahRoutes;
