"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const express_1 = require("express");
const government_innovation_controller_1 = __importDefault(require("../controller/government_innovation.controller"));
const file_middleware_1 = require("../middleware/file.middleware");
const jwt_middleware_1 = require("../middleware/jwt.middleware");
const base_routes_1 = __importDefault(require("./base.routes"));
class GovernmentInnovationRoutes extends base_routes_1.default {
    constructor(express) {
        super(express, new government_innovation_controller_1.default(), "/inovasi_pemerintah_daerah");
    }
    routes() {
        let express = this.express;
        const route = (0, express_1.Router)();
        route.use(jwt_middleware_1.authenticated);
        route.get("", this.controller.get);
        route.get("/download/:type", this.controller.download);
        route.get("/:id", this.controller.detail);
        route.get("/:id/download/:type", this.controller.downloadDetail);
        route.post("", (0, file_middleware_1.upload)((0, file_middleware_1.storage)("public/upload/inovasi_pemda/")).fields([
            { name: "anggaran_file", maxCount: 1 },
            { name: "profile_file", maxCount: 1 },
            { name: "foto", maxCount: 1 },
        ]), this.controller.create);
        route.patch("/:id", (0, file_middleware_1.upload)((0, file_middleware_1.storage)("public/upload/inovasi_pemda/")).fields([
            { name: "anggaran_file", maxCount: 1 },
            { name: "profile_file", maxCount: 1 },
            { name: "foto", maxCount: 1 },
        ]), this.controller.update);
        route.delete("/:id", this.controller.delete);
        express.use(this.prefixRoutes, route);
        return express;
    }
}
exports.default = GovernmentInnovationRoutes;
