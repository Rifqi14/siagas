import { Application } from 'express';
import GovernmentSectorController from '../controller/government_sector.controller';
import BaseRoutes from './base.routes';
declare class GovSectorRoutes extends BaseRoutes<GovernmentSectorController> {
    constructor(express: Application);
    routes(): Application;
}
export default GovSectorRoutes;
