import { Application } from 'express';
import TuxedoController from '../controller/tuxedo.controller';
import BaseRoutes from './base.routes';
declare class TuxedoRoutes extends BaseRoutes<TuxedoController> {
    constructor(express: Application);
    routes(): Application;
}
export default TuxedoRoutes;
