"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const health_controller_1 = __importDefault(require("../controller/health.controller"));
const base_routes_1 = __importDefault(require("./base.routes"));
class HealthRoutes extends base_routes_1.default {
    constructor(express) {
        const controller = new health_controller_1.default();
        const prefix = '/health';
        super(express, controller, prefix);
    }
    routes() {
        let express = this.express;
        express.get(this.prefixRoutes, this.controller.health);
        express.get('/template', this.controller.template);
        return express;
    }
}
exports.default = HealthRoutes;
