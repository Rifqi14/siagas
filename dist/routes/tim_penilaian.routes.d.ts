import TimPenilaianController from '../controller/tim_penilaian.controller';
import BaseRoutes from './base.routes';
import { Application } from 'express';
declare class TimPenilaianRoutes extends BaseRoutes<TimPenilaianController> {
    constructor(express: Application);
    routes(): Application;
}
export default TimPenilaianRoutes;
