"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const express_1 = require("express");
const tuxedo_controller_1 = __importDefault(require("../controller/tuxedo.controller"));
const jwt_middleware_1 = require("../middleware/jwt.middleware");
const validation_middleware_1 = require("../middleware/validation.middleware");
const tuxedo_schema_1 = require("../schema/tuxedo.schema");
const base_routes_1 = __importDefault(require("./base.routes"));
class TuxedoRoutes extends base_routes_1.default {
    constructor(express) {
        super(express, new tuxedo_controller_1.default(), '/tuxedo');
    }
    routes() {
        let express = this.express;
        const route = (0, express_1.Router)();
        route.use(jwt_middleware_1.authenticated);
        route.get('', this.controller.get);
        route.get('/:id', this.controller.detail);
        route.post('', (0, validation_middleware_1.validate)(tuxedo_schema_1.createTuxedoSchema), this.controller.create);
        route.patch('/:id', (0, validation_middleware_1.validate)(tuxedo_schema_1.createTuxedoSchema), this.controller.update);
        route.delete('/:id', this.controller.delete);
        express.use(this.prefixRoutes, route);
        return express;
    }
}
exports.default = TuxedoRoutes;
