import { PreviewReviewInovasiDaerahController } from '../controller/preview_inovasi_daerah.controller';
import BaseRoutes from './base.routes';
import { Application } from 'express';
export declare class PreviewReviewInovasiDaerahRoutes extends BaseRoutes<PreviewReviewInovasiDaerahController> {
    constructor(express: Application);
    routes(): Application;
}
