import ProfilPemdaController from '../controller/profil_pemda.controller';
import BaseRoutes from './base.routes';
import { Application } from 'express';
declare class ProfilPemdaRoutes extends BaseRoutes<ProfilPemdaController> {
    constructor(express: Application);
    routes(): Application;
}
export default ProfilPemdaRoutes;
