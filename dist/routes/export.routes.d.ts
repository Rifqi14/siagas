import { Application } from 'express';
import ExportController from '../controller/export.controller';
import BaseRoutes from './base.routes';
declare class ExportRoutes extends BaseRoutes<ExportController> {
    constructor(express: Application);
    routes(): Application;
}
export default ExportRoutes;
