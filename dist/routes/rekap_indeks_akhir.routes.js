"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.RekapIndeksAkhirRoute = void 0;
const express_1 = require("express");
const rekap_indeks_akhir_controller_1 = require("../controller/rekap_indeks_akhir.controller");
const jwt_middleware_1 = require("../middleware/jwt.middleware");
const base_routes_1 = __importDefault(require("./base.routes"));
class RekapIndeksAkhirRoute extends base_routes_1.default {
    constructor(express) {
        super(express, new rekap_indeks_akhir_controller_1.RekapIndeksAkhirController(), '/rekap_indeks_akhir');
    }
    routes() {
        let express = this.express;
        const route = (0, express_1.Router)();
        route.use(jwt_middleware_1.authenticated);
        route.get('', this.controller.get);
        route.patch('/:id', this.controller.update);
        express.use(this.prefixRoutes, route);
        return express;
    }
}
exports.RekapIndeksAkhirRoute = RekapIndeksAkhirRoute;
