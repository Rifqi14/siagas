import { InovasiIndikatorFileController } from '../controller/inovasi_indikator_file.controller';
import BaseRoutes from './base.routes';
import { Application } from 'express';
export declare class InovasiIndikatorFileRoutes extends BaseRoutes<InovasiIndikatorFileController> {
    constructor(express: Application);
    routes(): Application;
}
