"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const profil_pemda_controller_1 = __importDefault(require("../controller/profil_pemda.controller"));
const base_routes_1 = __importDefault(require("./base.routes"));
const express_1 = require("express");
const jwt_middleware_1 = require("../middleware/jwt.middleware");
const file_middleware_1 = require("../middleware/file.middleware");
class ProfilPemdaRoutes extends base_routes_1.default {
    constructor(express) {
        super(express, new profil_pemda_controller_1.default(), '/profil_pemda');
    }
    routes() {
        let express = this.express;
        const route = (0, express_1.Router)();
        route.use(jwt_middleware_1.authenticated);
        route.get('', this.controller.get);
        route.get('/download/:type', this.controller.getDownload);
        route.get('/:id', this.controller.detail);
        route.get('/:id/download/:type', this.controller.downloadDetail);
        route.post('', (0, file_middleware_1.upload)((0, file_middleware_1.storage)('public/upload/profil_pemda/')).single('file'), this.controller.create);
        route.patch('/:id', (0, file_middleware_1.upload)((0, file_middleware_1.storage)('public/upload/profil_pemda/')).single('file'), this.controller.update);
        route.delete('/:id', this.controller.delete);
        express.use(this.prefixRoutes, route);
        return express;
    }
}
exports.default = ProfilPemdaRoutes;
