"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.LaporanRoute = void 0;
const laporan_controller_1 = require("../controller/laporan.controller");
const base_routes_1 = __importDefault(require("./base.routes"));
const express_1 = require("express");
const jwt_middleware_1 = require("../middleware/jwt.middleware");
class LaporanRoute extends base_routes_1.default {
    constructor(express) {
        super(express, new laporan_controller_1.LaporanController(), '/laporan');
    }
    routes() {
        let express = this.express;
        const route = (0, express_1.Router)();
        route.use(jwt_middleware_1.authenticated);
        route.get('/indeks', this.controller.laporanIndeks);
        route.get('/indeks/download/:type', this.controller.laporanIndeksDownload);
        route.get('/jenis_inovasi', this.controller.laporanJenisInovasi);
        route.get('/bentuk_inovasi', this.controller.laporanBentuk);
        route.get('/inisiator_inovasi', this.controller.laporanInisiator);
        route.get('/inisiator_inovasi/download/:type', this.controller.laporanInisiatorInovasiDownload);
        route.get('/urusan_inovasi', this.controller.laporanUrusan);
        route.get('/urusan_inovasi/download/:type', this.controller.laporanUrusanInovasiDownload);
        express.use(this.prefixRoutes, route);
        return express;
    }
}
exports.LaporanRoute = LaporanRoute;
