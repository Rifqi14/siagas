"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const express_1 = require("express");
const uptd_controller_1 = __importDefault(require("../controller/uptd.controller"));
const jwt_middleware_1 = require("../middleware/jwt.middleware");
const base_routes_1 = __importDefault(require("./base.routes"));
class UptdRoutes extends base_routes_1.default {
    constructor(express) {
        super(express, new uptd_controller_1.default(), '/uptd');
    }
    routes() {
        let express = this.express;
        const route = (0, express_1.Router)();
        route.use(jwt_middleware_1.authenticated);
        route.get('', this.controller.get);
        route.get('/:id', this.controller.detail);
        route.post('', this.controller.create);
        route.patch('/:id', this.controller.update);
        route.delete('/:id', this.controller.delete);
        express.use(this.prefixRoutes, route);
        return express;
    }
}
exports.default = UptdRoutes;
