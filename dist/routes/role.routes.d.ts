import { Application } from 'express';
import RoleController from '../controller/role.controller';
import BaseRoutes from './base.routes';
declare class RoleRoutes extends BaseRoutes<RoleController> {
    constructor(express: Application);
    routes(): Application;
}
export default RoleRoutes;
