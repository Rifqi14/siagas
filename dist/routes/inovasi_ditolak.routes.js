"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.InovasiDitolakRoute = void 0;
const inovasi_ditolak_controller_1 = require("../controller/inovasi_ditolak.controller");
const base_routes_1 = __importDefault(require("./base.routes"));
const express_1 = require("express");
const jwt_middleware_1 = require("../middleware/jwt.middleware");
class InovasiDitolakRoute extends base_routes_1.default {
    constructor(express) {
        super(express, new inovasi_ditolak_controller_1.InovasiDitolakController(), '/inovasi_ditolak');
    }
    routes() {
        let express = this.express;
        const route = (0, express_1.Router)();
        route.use(jwt_middleware_1.authenticated);
        route.get('', this.controller.get);
        route.get('/download/:type', this.controller.getDownload);
        route.get('/:review_inovasi_id', this.controller.detail);
        route.patch('/:review_inovasi_id', this.controller.update);
        express.use(this.prefixRoutes, route);
        return express;
    }
}
exports.InovasiDitolakRoute = InovasiDitolakRoute;
