import { ReviewInovasiDaerahController } from "../controller/review_inovasi_daerah.controller";
import BaseRoutes from "./base.routes";
import { Application } from "express";
export declare class ReviewInovasiDaerahRoutes extends BaseRoutes<ReviewInovasiDaerahController> {
    constructor(express: Application);
    routes(): Application;
}
