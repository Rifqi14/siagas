"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const express_1 = require("express");
const file_controller_1 = __importDefault(require("../controller/file.controller"));
const file_middleware_1 = require("../middleware/file.middleware");
const jwt_middleware_1 = require("../middleware/jwt.middleware");
const validation_middleware_1 = require("../middleware/validation.middleware");
const file_schema_1 = require("../schema/file.schema");
const base_routes_1 = __importDefault(require("./base.routes"));
class FileRoutes extends base_routes_1.default {
    constructor(express) {
        super(express, new file_controller_1.default(), '/file');
    }
    routes() {
        let express = this.express;
        const route = (0, express_1.Router)();
        route.use(jwt_middleware_1.authenticated);
        route.post('', (0, file_middleware_1.upload)((0, file_middleware_1.storage)()).single('file'), (0, validation_middleware_1.validate)(file_schema_1.uploadFileSchema), this.controller.upload);
        express.use(this.prefixRoutes, route);
        return express;
    }
}
exports.default = FileRoutes;
