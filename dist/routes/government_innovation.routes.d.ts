import { Application } from "express";
import GovernmentInnovationController from "../controller/government_innovation.controller";
import BaseRoutes from "./base.routes";
declare class GovernmentInnovationRoutes extends BaseRoutes<GovernmentInnovationController> {
    constructor(express: Application);
    routes(): Application;
}
export default GovernmentInnovationRoutes;
