import DistrikController from "../controller/distrik.controller";
import BaseRoutes from "./base.routes";
import { Application } from "express";
declare class DistrikRoutes extends BaseRoutes<DistrikController> {
    constructor(express: Application);
    routes(): Application;
}
export default DistrikRoutes;
