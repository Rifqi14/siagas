import { Application } from 'express';
export default class BaseRoutes<C> {
    controller: C;
    express: Application;
    prefixRoutes: string;
    constructor(express: Application, controller: C, prefix: string);
}
