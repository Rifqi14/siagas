import PemdaIndikatorController from '../controller/pemda_indikator.controller';
import BaseRoutes from './base.routes';
import { Application } from 'express';
declare class PemdaIndikatorRoutes extends BaseRoutes<PemdaIndikatorController> {
    constructor(express: Application);
    routes(): Application;
}
export default PemdaIndikatorRoutes;
