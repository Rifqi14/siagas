import { Application } from 'express';
import RegionController from '../controller/region.controller';
import BaseRoutes from './base.routes';
declare class RegionRoutes extends BaseRoutes<RegionController> {
    constructor(express: Application);
    routes(): Application;
}
export default RegionRoutes;
