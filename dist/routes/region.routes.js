"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const region_controller_1 = __importDefault(require("../controller/region.controller"));
const base_routes_1 = __importDefault(require("./base.routes"));
class RegionRoutes extends base_routes_1.default {
    constructor(express) {
        super(express, new region_controller_1.default(), '/daerah');
    }
    routes() {
        let express = this.express;
        express.get(this.prefixRoutes + '', this.controller.get);
        express.get(this.prefixRoutes + '/:id', this.controller.detail);
        express.post(this.prefixRoutes + '', this.controller.create);
        express.patch(this.prefixRoutes + '/:id', this.controller.update);
        express.delete(this.prefixRoutes + '/:id', this.controller.delete);
        return express;
    }
}
exports.default = RegionRoutes;
