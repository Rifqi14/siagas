"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const express_1 = require("express");
const validation_middleware_1 = require("../middleware/validation.middleware");
const government_sector_controller_1 = __importDefault(require("../controller/government_sector.controller"));
const base_routes_1 = __importDefault(require("./base.routes"));
const government_sector_schema_1 = require("../schema/government_sector.schema");
const jwt_middleware_1 = require("../middleware/jwt.middleware");
class GovSectorRoutes extends base_routes_1.default {
    constructor(express) {
        super(express, new government_sector_controller_1.default(), '/urusan_pemerintahan');
    }
    routes() {
        let express = this.express;
        const route = (0, express_1.Router)();
        route.use(jwt_middleware_1.authenticated);
        route.get('', this.controller.get);
        route.get('/:id', this.controller.detail);
        route.post('', (0, validation_middleware_1.validate)(government_sector_schema_1.createGovernmentSectorSchema), this.controller.create);
        route.patch('/:id', (0, validation_middleware_1.validate)(government_sector_schema_1.createGovernmentSectorSchema), this.controller.update);
        route.delete('/:id', this.controller.delete);
        express.use(this.prefixRoutes, route);
        return express;
    }
}
exports.default = GovSectorRoutes;
