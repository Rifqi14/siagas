"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const express_1 = require("express");
const role_controller_1 = __importDefault(require("../controller/role.controller"));
const base_routes_1 = __importDefault(require("./base.routes"));
class RoleRoutes extends base_routes_1.default {
    constructor(express) {
        super(express, new role_controller_1.default(), '/role');
    }
    routes() {
        let express = this.express;
        const route = (0, express_1.Router)();
        route.get('', this.controller.get);
        route.get('/:id', this.controller.detail);
        route.post('', this.controller.create);
        route.patch('/:id', this.controller.update);
        route.delete('/:id', this.controller.delete);
        express.use(this.prefixRoutes, route);
        return express;
    }
}
exports.default = RoleRoutes;
