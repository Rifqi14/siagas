import { Application } from 'express';
import HealthController from '../controller/health.controller';
import BaseRoutes from './base.routes';
declare class HealthRoutes extends BaseRoutes<HealthController> {
    constructor(express: Application);
    routes(): Application;
}
export default HealthRoutes;
