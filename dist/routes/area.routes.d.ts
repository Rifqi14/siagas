import { Application } from 'express';
import AreaController from '../controller/area.controller';
import BaseRoutes from './base.routes';
declare class AreaRoutes extends BaseRoutes<AreaController> {
    constructor(express: Application);
    routes(): Application;
}
export default AreaRoutes;
