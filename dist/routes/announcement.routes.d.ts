import { Application } from 'express';
import AnnouncementController from '../controller/announcement.controller';
import BaseRoutes from './base.routes';
declare class AnnouncementRoutes extends BaseRoutes<AnnouncementController> {
    constructor(express: Application);
    routes(): Application;
}
export default AnnouncementRoutes;
