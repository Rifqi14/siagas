"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const express_1 = require("express");
const cluster_controller_1 = __importDefault(require("../controller/cluster.controller"));
const jwt_middleware_1 = require("../middleware/jwt.middleware");
const validation_middleware_1 = require("../middleware/validation.middleware");
const cluster_schema_1 = require("../schema/cluster.schema");
const clusterDetail_schema_1 = require("../schema/clusterDetail.schema");
const base_routes_1 = __importDefault(require("./base.routes"));
class ClusterRoutes extends base_routes_1.default {
    constructor(express) {
        super(express, new cluster_controller_1.default(), '/cluster');
    }
    routes() {
        let express = this.express;
        const route = (0, express_1.Router)();
        route.use(jwt_middleware_1.authenticated);
        route.get('', this.controller.get);
        route.get('/:id', this.controller.detail);
        route.post('', (0, validation_middleware_1.validate)(cluster_schema_1.createClusterSchema), this.controller.create);
        route.patch('/:id', (0, validation_middleware_1.validate)(cluster_schema_1.createClusterSchema), this.controller.update);
        route.delete('/:id', this.controller.delete);
        // Detail route
        route.get('/:id/detail', this.controller.getDetails);
        route.get('/:id/detail/:region_id', this.controller.getDetail);
        route.post('/:id/detail', (0, validation_middleware_1.validate)(clusterDetail_schema_1.createClusterDetailSchema), this.controller.createDetail);
        route.patch('/:id/detail/:region_id', (0, validation_middleware_1.validate)(clusterDetail_schema_1.createClusterDetailSchema), this.controller.updateDetail);
        route.delete('/:id/detail/:region_id', this.controller.deleteDetail);
        express.use(this.prefixRoutes, route);
        return express;
    }
}
exports.default = ClusterRoutes;
