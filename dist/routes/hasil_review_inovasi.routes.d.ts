import { HasilReviewInovasiController } from "../controller/hasil_review_inovasi.controller";
import BaseRoutes from "./base.routes";
import { Application } from "express";
export declare class HasilReviewInovasiRoute extends BaseRoutes<HasilReviewInovasiController> {
    constructor(express: Application);
    routes(): Application;
}
