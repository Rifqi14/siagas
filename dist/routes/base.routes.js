"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
class BaseRoutes {
    constructor(express, controller, prefix) {
        this.controller = controller;
        this.express = express;
        this.prefixRoutes = prefix;
    }
}
exports.default = BaseRoutes;
