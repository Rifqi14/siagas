"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const distrik_controller_1 = __importDefault(require("../controller/distrik.controller"));
const base_routes_1 = __importDefault(require("./base.routes"));
const jwt_middleware_1 = require("../middleware/jwt.middleware");
class DistrikRoutes extends base_routes_1.default {
    constructor(express) {
        super(express, new distrik_controller_1.default(), "/distrik");
    }
    routes() {
        let express = this.express;
        express.use(jwt_middleware_1.authenticated);
        express.get(this.prefixRoutes + "", this.controller.get);
        express.get(this.prefixRoutes + "/:id", this.controller.detail);
        express.post(this.prefixRoutes + "", this.controller.create);
        express.patch(this.prefixRoutes + "/:id", this.controller.update);
        express.delete(this.prefixRoutes + "/:id", this.controller.delete);
        return express;
    }
}
exports.default = DistrikRoutes;
