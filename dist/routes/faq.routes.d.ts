import { Application } from 'express';
import FaqController from '../controller/faq.controller';
import BaseRoutes from './base.routes';
declare class FaqRoutes extends BaseRoutes<FaqController> {
    constructor(express: Application);
    routes(): Application;
}
export default FaqRoutes;
