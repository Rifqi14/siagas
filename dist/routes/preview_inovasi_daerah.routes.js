"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.PreviewReviewInovasiDaerahRoutes = void 0;
const preview_inovasi_daerah_controller_1 = require("../controller/preview_inovasi_daerah.controller");
const base_routes_1 = __importDefault(require("./base.routes"));
const express_1 = require("express");
const jwt_middleware_1 = require("../middleware/jwt.middleware");
class PreviewReviewInovasiDaerahRoutes extends base_routes_1.default {
    constructor(express) {
        super(express, new preview_inovasi_daerah_controller_1.PreviewReviewInovasiDaerahController(), '/review_inovasi_daerah/:review_id/preview');
    }
    routes() {
        let express = this.express;
        const route = (0, express_1.Router)();
        route.use(jwt_middleware_1.authenticated);
        route.get('', this.controller.get);
        route.get('/:id', this.controller.detail);
        route.post('', this.controller.create);
        route.patch('/:id', this.controller.update);
        route.delete('/:id', this.controller.delete);
        express.use(this.prefixRoutes, route);
        return express;
    }
}
exports.PreviewReviewInovasiDaerahRoutes = PreviewReviewInovasiDaerahRoutes;
