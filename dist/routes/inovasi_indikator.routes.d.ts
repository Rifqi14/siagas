import InovasiIndikatorController from '../controller/inovasi_indikator.controller';
import BaseRoutes from './base.routes';
import { Application } from 'express';
declare class InovasiIndikatorRoutes extends BaseRoutes<InovasiIndikatorController> {
    constructor(express: Application);
    routes(): Application;
}
export default InovasiIndikatorRoutes;
