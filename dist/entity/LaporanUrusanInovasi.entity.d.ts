export declare class LaporanUrusanInovasi {
    pemda_id: number;
    urusan_pemerintahan: string;
    total_disetujui: number;
    total_ditolak: number;
    total_keseluruhan: number;
}
