"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.PemdaIndikatorFile = void 0;
const lib_1 = require("../types/lib");
const typeorm_1 = require("typeorm");
const PemdaIndikator_entity_1 = require("./PemdaIndikator.entity");
const File_entity_1 = __importDefault(require("./File.entity"));
const ProfilePemda_entity_1 = require("./ProfilePemda.entity");
const Indicator_entity_1 = __importDefault(require("./Indicator.entity"));
let PemdaIndikatorFile = class PemdaIndikatorFile extends lib_1.AppBaseEntity {
};
__decorate([
    (0, typeorm_1.PrimaryGeneratedColumn)(),
    __metadata("design:type", Number)
], PemdaIndikatorFile.prototype, "id", void 0);
__decorate([
    (0, typeorm_1.Column)(),
    __metadata("design:type", Number)
], PemdaIndikatorFile.prototype, "pemda_indikator_id", void 0);
__decorate([
    (0, typeorm_1.Column)(),
    __metadata("design:type", Number)
], PemdaIndikatorFile.prototype, "pemda_id", void 0);
__decorate([
    (0, typeorm_1.Column)(),
    __metadata("design:type", Number)
], PemdaIndikatorFile.prototype, "indikator_id", void 0);
__decorate([
    (0, typeorm_1.Column)(),
    __metadata("design:type", Number)
], PemdaIndikatorFile.prototype, "file_id", void 0);
__decorate([
    (0, typeorm_1.Column)(),
    __metadata("design:type", String)
], PemdaIndikatorFile.prototype, "nomor_surat", void 0);
__decorate([
    (0, typeorm_1.Column)(),
    __metadata("design:type", String)
], PemdaIndikatorFile.prototype, "tanggal_surat", void 0);
__decorate([
    (0, typeorm_1.Column)(),
    __metadata("design:type", String)
], PemdaIndikatorFile.prototype, "nama_surat", void 0);
__decorate([
    (0, typeorm_1.ManyToOne)(() => ProfilePemda_entity_1.ProfilPemda),
    (0, typeorm_1.JoinColumn)({ name: 'pemda_id' }),
    __metadata("design:type", ProfilePemda_entity_1.ProfilPemda)
], PemdaIndikatorFile.prototype, "pemda", void 0);
__decorate([
    (0, typeorm_1.ManyToOne)(() => Indicator_entity_1.default, { eager: true }),
    (0, typeorm_1.JoinColumn)({ name: 'indikator_id' }),
    __metadata("design:type", Indicator_entity_1.default)
], PemdaIndikatorFile.prototype, "indicator", void 0);
__decorate([
    (0, typeorm_1.ManyToOne)(() => PemdaIndikator_entity_1.PemdaIndikator),
    (0, typeorm_1.JoinColumn)({ name: 'pemda_indikator_id' }),
    __metadata("design:type", PemdaIndikator_entity_1.PemdaIndikator)
], PemdaIndikatorFile.prototype, "pemda_indikator", void 0);
__decorate([
    (0, typeorm_1.ManyToOne)(() => File_entity_1.default, { eager: true }),
    (0, typeorm_1.JoinColumn)({ name: 'file_id' }),
    __metadata("design:type", File_entity_1.default)
], PemdaIndikatorFile.prototype, "file", void 0);
PemdaIndikatorFile = __decorate([
    (0, typeorm_1.Entity)({ name: 'pemda_indikator_file' })
], PemdaIndikatorFile);
exports.PemdaIndikatorFile = PemdaIndikatorFile;
