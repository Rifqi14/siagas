import { BaseEntity } from 'typeorm';
export default class Setting extends BaseEntity {
    id: number;
    key: string;
    value: string;
    helper: string;
    created_at: Date;
    updated_at: Date;
}
