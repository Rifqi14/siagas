export declare class LaporanIndeks {
    pemda_id: number;
    nama_daerah: string;
    opd_yang_menangani: string;
    jumlah_inovasi: number;
    total_skor_mandiri: number;
    nilai_indeks: number;
    total_file: number;
    predikat: string;
    indeks: number;
    nominator: string;
    created_by: string;
}
