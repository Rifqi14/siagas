import { BaseEntity } from 'typeorm';
import Regions from './Region.entity';
export default class Area extends BaseEntity {
    id: number;
    region_id: number;
    name: string;
    created_at: Date;
    updated_at: Date;
    region: Regions;
}
