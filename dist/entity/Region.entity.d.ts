import { BaseEntity } from 'typeorm';
import Area from './Area.entity';
import ClusterDetail from './ClusterDetail.entity';
export default class Regions extends BaseEntity {
    id: number;
    name: string;
    created_at: Date;
    updated_at: Date;
    areas: Area[];
    clusters: ClusterDetail[];
}
