import { AppBaseEntity } from '../types/lib';
import { InovasiIndikator } from './InovasiIndikator.entity';
export declare class EvaluasiInovasiDaerah extends AppBaseEntity {
    id: number;
    data_saat_ini: string;
    catatan: string;
    keterangan: string;
    tentang: string;
    kategori: string;
    inovasi_indikator_id: number;
    inovasi_indikator: InovasiIndikator;
}
