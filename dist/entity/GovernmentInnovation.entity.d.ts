import { CustomBaseEntity, OmitType } from "../types/lib";
import File from "./File.entity";
import GovernmentSector from "./GovernmentSector";
import User from "./User.entity";
import { InovasiIndikator } from "./InovasiIndikator.entity";
import { ProfilPemda } from "./ProfilePemda.entity";
export default class GovernmentInnovation extends CustomBaseEntity {
    id: number;
    government_name: string;
    innovation_name: string;
    innovation_phase: string;
    innovation_initiator: string;
    innovation_type: string;
    innovation_form: string;
    thematic: string;
    first_field: string;
    other_fields: string;
    trial_time: string;
    implementation_time: string;
    design: string;
    purpose: string;
    benefit: string;
    result: string;
    budget_file_id: number;
    profile_file_id: number;
    foto_id?: number;
    thematic_detail: string;
    government_sector_id: number;
    pemda_id: number;
    created_at: Date;
    updated_at: Date;
    urusan: GovernmentSector;
    pemda: OmitType<User, "role">;
    indikator: InovasiIndikator[];
    budgetFile: Omit<File, "announcements">;
    profileFile: Omit<File, "announcements">;
    fotoFile: Omit<File, "announcements">;
    profilPemda: ProfilPemda;
}
