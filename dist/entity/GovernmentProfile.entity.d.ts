import { BaseEntity } from 'typeorm';
import File from './File.entity';
import Regions from './Region.entity';
export default class GovernmentProfile extends BaseEntity {
    id: number;
    region_id: number;
    name: string;
    pic: string;
    address: string;
    email: string;
    phone: string;
    admin_name: string;
    file_id: number;
    created_at: Date;
    updated_at: Date;
    region: Omit<Regions, 'areas' | 'clusters'>;
    file: Omit<File, 'announcement'>;
}
