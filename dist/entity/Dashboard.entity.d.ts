export declare class Dashboard {
    total_pemda: number;
    total_inovasi: number;
    total_uji_coba: number;
    total_penerapan: number;
    total_inisiatif: number;
    daerah_tertinggi: string;
    daerah_terendah: string;
}
