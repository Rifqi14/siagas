import { AppBaseEntity } from '../types/lib';
import { PemdaIndikator } from './PemdaIndikator.entity';
import File from './File.entity';
import { ProfilPemda } from './ProfilePemda.entity';
import Indicator from './Indicator.entity';
export declare class PemdaIndikatorFile extends AppBaseEntity {
    id: number;
    pemda_indikator_id: number;
    pemda_id: number;
    indikator_id: number;
    file_id: number;
    nomor_surat: string;
    tanggal_surat: string;
    nama_surat: string;
    pemda: ProfilPemda;
    indicator: Indicator;
    pemda_indikator: PemdaIndikator;
    file: File;
}
