import { BaseEntity } from 'typeorm';
import Role from './Role.entity';
export default class User extends BaseEntity {
    id: number;
    role_id: number;
    username: string;
    password: string;
    created_at: Date;
    updated_at: Date;
    email: string;
    full_name: string;
    nickname: string;
    nama_pemda: string;
    created_by: number;
    createdBy: User;
    role: Role;
    createPassword(): void;
}
