"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const lib_1 = require("../types/lib");
const typeorm_1 = require("typeorm");
const File_entity_1 = __importDefault(require("./File.entity"));
const GovernmentSector_1 = __importDefault(require("./GovernmentSector"));
const User_entity_1 = __importDefault(require("./User.entity"));
const InovasiIndikator_entity_1 = require("./InovasiIndikator.entity");
const ProfilePemda_entity_1 = require("./ProfilePemda.entity");
let GovernmentInnovation = class GovernmentInnovation extends lib_1.CustomBaseEntity {
};
__decorate([
    (0, typeorm_1.PrimaryGeneratedColumn)(),
    __metadata("design:type", Number)
], GovernmentInnovation.prototype, "id", void 0);
__decorate([
    (0, typeorm_1.Column)(),
    __metadata("design:type", String)
], GovernmentInnovation.prototype, "government_name", void 0);
__decorate([
    (0, typeorm_1.Column)(),
    __metadata("design:type", String)
], GovernmentInnovation.prototype, "innovation_name", void 0);
__decorate([
    (0, typeorm_1.Column)(),
    __metadata("design:type", String)
], GovernmentInnovation.prototype, "innovation_phase", void 0);
__decorate([
    (0, typeorm_1.Column)(),
    __metadata("design:type", String)
], GovernmentInnovation.prototype, "innovation_initiator", void 0);
__decorate([
    (0, typeorm_1.Column)(),
    __metadata("design:type", String)
], GovernmentInnovation.prototype, "innovation_type", void 0);
__decorate([
    (0, typeorm_1.Column)(),
    __metadata("design:type", String)
], GovernmentInnovation.prototype, "innovation_form", void 0);
__decorate([
    (0, typeorm_1.Column)(),
    __metadata("design:type", String)
], GovernmentInnovation.prototype, "thematic", void 0);
__decorate([
    (0, typeorm_1.Column)(),
    __metadata("design:type", String)
], GovernmentInnovation.prototype, "first_field", void 0);
__decorate([
    (0, typeorm_1.Column)(),
    __metadata("design:type", String)
], GovernmentInnovation.prototype, "other_fields", void 0);
__decorate([
    (0, typeorm_1.Column)({ type: "date" }),
    __metadata("design:type", String)
], GovernmentInnovation.prototype, "trial_time", void 0);
__decorate([
    (0, typeorm_1.Column)({ type: "date" }),
    __metadata("design:type", String)
], GovernmentInnovation.prototype, "implementation_time", void 0);
__decorate([
    (0, typeorm_1.Column)(),
    __metadata("design:type", String)
], GovernmentInnovation.prototype, "design", void 0);
__decorate([
    (0, typeorm_1.Column)(),
    __metadata("design:type", String)
], GovernmentInnovation.prototype, "purpose", void 0);
__decorate([
    (0, typeorm_1.Column)(),
    __metadata("design:type", String)
], GovernmentInnovation.prototype, "benefit", void 0);
__decorate([
    (0, typeorm_1.Column)(),
    __metadata("design:type", String)
], GovernmentInnovation.prototype, "result", void 0);
__decorate([
    (0, typeorm_1.Column)(),
    __metadata("design:type", Number)
], GovernmentInnovation.prototype, "budget_file_id", void 0);
__decorate([
    (0, typeorm_1.Column)(),
    __metadata("design:type", Number)
], GovernmentInnovation.prototype, "profile_file_id", void 0);
__decorate([
    (0, typeorm_1.Column)(),
    __metadata("design:type", Number)
], GovernmentInnovation.prototype, "foto_id", void 0);
__decorate([
    (0, typeorm_1.Column)(),
    __metadata("design:type", String)
], GovernmentInnovation.prototype, "thematic_detail", void 0);
__decorate([
    (0, typeorm_1.Column)(),
    __metadata("design:type", Number)
], GovernmentInnovation.prototype, "government_sector_id", void 0);
__decorate([
    (0, typeorm_1.Column)(),
    __metadata("design:type", Number)
], GovernmentInnovation.prototype, "pemda_id", void 0);
__decorate([
    (0, typeorm_1.CreateDateColumn)(),
    __metadata("design:type", Date)
], GovernmentInnovation.prototype, "created_at", void 0);
__decorate([
    (0, typeorm_1.UpdateDateColumn)(),
    __metadata("design:type", Date)
], GovernmentInnovation.prototype, "updated_at", void 0);
__decorate([
    (0, typeorm_1.ManyToOne)(() => GovernmentSector_1.default, { cascade: true }),
    (0, typeorm_1.JoinColumn)({ name: "government_sector_id" }),
    __metadata("design:type", GovernmentSector_1.default)
], GovernmentInnovation.prototype, "urusan", void 0);
__decorate([
    (0, typeorm_1.ManyToOne)(() => User_entity_1.default, { eager: true }),
    (0, typeorm_1.JoinColumn)({ name: "government_name", referencedColumnName: "username" }),
    __metadata("design:type", Object)
], GovernmentInnovation.prototype, "pemda", void 0);
__decorate([
    (0, typeorm_1.OneToMany)(() => InovasiIndikator_entity_1.InovasiIndikator, indikator => indikator.inovasi, {
        cascade: true,
        eager: true,
    }),
    __metadata("design:type", Array)
], GovernmentInnovation.prototype, "indikator", void 0);
__decorate([
    (0, typeorm_1.ManyToOne)(() => File_entity_1.default),
    (0, typeorm_1.JoinColumn)({ name: "budget_file_id" }),
    __metadata("design:type", Object)
], GovernmentInnovation.prototype, "budgetFile", void 0);
__decorate([
    (0, typeorm_1.ManyToOne)(() => File_entity_1.default),
    (0, typeorm_1.JoinColumn)({ name: "profile_file_id" }),
    __metadata("design:type", Object)
], GovernmentInnovation.prototype, "profileFile", void 0);
__decorate([
    (0, typeorm_1.ManyToOne)(() => File_entity_1.default),
    (0, typeorm_1.JoinColumn)({ name: "foto_id" }),
    __metadata("design:type", Object)
], GovernmentInnovation.prototype, "fotoFile", void 0);
__decorate([
    (0, typeorm_1.ManyToOne)(() => ProfilePemda_entity_1.ProfilPemda),
    (0, typeorm_1.JoinColumn)({ name: "pemda_id" }),
    __metadata("design:type", ProfilePemda_entity_1.ProfilPemda)
], GovernmentInnovation.prototype, "profilPemda", void 0);
GovernmentInnovation = __decorate([
    (0, typeorm_1.Entity)({ name: "government_innovations" })
], GovernmentInnovation);
exports.default = GovernmentInnovation;
