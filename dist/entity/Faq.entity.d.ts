import { BaseEntity } from 'typeorm';
export default class Faq extends BaseEntity {
    id: number;
    question: string;
    answer: string;
    created_at: Date;
    updated_at: Date;
}
