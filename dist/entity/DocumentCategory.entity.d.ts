import { BaseEntity } from 'typeorm';
export default class DocumentCategory extends BaseEntity {
    id: number;
    name: string;
    slug: string;
    created_at: Date;
    updated_at: Date;
}
