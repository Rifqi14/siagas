import { BaseEntity } from 'typeorm';
import Indicator from './Indicator.entity';
import GovernmentProfile from './GovernmentProfile.entity';
import IndicatorScale from './IndicatorScale.entity';
export declare class GovernmentIndicators extends BaseEntity {
    id: number;
    value: string;
    value_before: string;
    value_after: string;
    other_value: string;
    score: string;
    created_at: Date;
    updated_at: Date;
    deleted_at: Date;
    scale: IndicatorScale;
    indicator: Indicator;
    profile: GovernmentProfile;
}
