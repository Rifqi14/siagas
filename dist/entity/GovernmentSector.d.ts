import { BaseEntity } from "typeorm";
import GovernmentInnovation from "./GovernmentInnovation.entity";
export default class GovernmentSector extends BaseEntity {
    id: number;
    name: string;
    deadline: string;
    created_at: Date;
    updated_at: Date;
    innovations: GovernmentInnovation[];
    deadline_in_days: number;
    setDeadline(): void;
}
