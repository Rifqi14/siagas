import { BaseEntity } from 'typeorm';
import Cluster from './Cluster.entity';
import Regions from './Region.entity';
export default class ClusterDetail extends BaseEntity {
    cluster_id: number;
    region_id: number;
    created_at: Date;
    updated_at: Date;
    cluster: Cluster;
    region: Regions;
}
