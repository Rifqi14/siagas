import { AppBaseEntity } from '../types/lib';
import { ReviewInovasiDaerah } from './ReviewInovasiDaerah.entity';
import { EvaluasiReviewInovasiDaerah } from './EvaluasiReviewInovasiDaerah.entity';
export declare class PreviewReviewInovasiDaerah extends AppBaseEntity {
    id: number;
    komentar?: string;
    review_inovasi_daerah_id: number;
    review_inovasi_daerah: ReviewInovasiDaerah;
    evaluasi: EvaluasiReviewInovasiDaerah[];
}
