import { BaseEntity } from 'typeorm';
import User from './User.entity';
export default class Role extends BaseEntity {
    id: number;
    name: string;
    is_creator: string;
    is_super_admin: string;
    label: string;
    created_at: Date;
    updated_at: Date;
    users: User[];
}
