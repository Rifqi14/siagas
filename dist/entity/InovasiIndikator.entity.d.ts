import { AppBaseEntity } from '../types/lib';
import GovernmentInnovation from './GovernmentInnovation.entity';
import Indicator from './Indicator.entity';
import { InovasiIndikatorFile } from './InovasiIndikatorFile.entity';
import { EvaluasiInovasiDaerah } from './EvaluasiInovasiDaerah.entity';
export declare class InovasiIndikator extends AppBaseEntity {
    id: number;
    inovasi_id: number;
    indikator_id: number;
    informasi: string;
    nilai: number;
    nilai_sebelum: number;
    nilai_sesudah: number;
    inovasi: GovernmentInnovation;
    indikator: Indicator;
    evaluasi: EvaluasiInovasiDaerah[];
    files: InovasiIndikatorFile[];
}
