"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.GovernmentIndicators = void 0;
const typeorm_1 = require("typeorm");
const Indicator_entity_1 = __importDefault(require("./Indicator.entity"));
const GovernmentProfile_entity_1 = __importDefault(require("./GovernmentProfile.entity"));
const IndicatorScale_entity_1 = __importDefault(require("./IndicatorScale.entity"));
let GovernmentIndicators = class GovernmentIndicators extends typeorm_1.BaseEntity {
};
__decorate([
    (0, typeorm_1.PrimaryGeneratedColumn)(),
    __metadata("design:type", Number)
], GovernmentIndicators.prototype, "id", void 0);
__decorate([
    (0, typeorm_1.Column)(),
    __metadata("design:type", String)
], GovernmentIndicators.prototype, "value", void 0);
__decorate([
    (0, typeorm_1.Column)(),
    __metadata("design:type", String)
], GovernmentIndicators.prototype, "value_before", void 0);
__decorate([
    (0, typeorm_1.Column)(),
    __metadata("design:type", String)
], GovernmentIndicators.prototype, "value_after", void 0);
__decorate([
    (0, typeorm_1.Column)(),
    __metadata("design:type", String)
], GovernmentIndicators.prototype, "other_value", void 0);
__decorate([
    (0, typeorm_1.Column)(),
    __metadata("design:type", String)
], GovernmentIndicators.prototype, "score", void 0);
__decorate([
    (0, typeorm_1.CreateDateColumn)(),
    __metadata("design:type", Date)
], GovernmentIndicators.prototype, "created_at", void 0);
__decorate([
    (0, typeorm_1.UpdateDateColumn)(),
    __metadata("design:type", Date)
], GovernmentIndicators.prototype, "updated_at", void 0);
__decorate([
    (0, typeorm_1.DeleteDateColumn)(),
    __metadata("design:type", Date)
], GovernmentIndicators.prototype, "deleted_at", void 0);
__decorate([
    (0, typeorm_1.ManyToOne)(() => IndicatorScale_entity_1.default),
    (0, typeorm_1.JoinColumn)({ name: 'scale_id' }),
    __metadata("design:type", IndicatorScale_entity_1.default)
], GovernmentIndicators.prototype, "scale", void 0);
__decorate([
    (0, typeorm_1.ManyToOne)(() => Indicator_entity_1.default),
    (0, typeorm_1.JoinColumn)({ name: 'indicator_id' }),
    __metadata("design:type", Indicator_entity_1.default)
], GovernmentIndicators.prototype, "indicator", void 0);
__decorate([
    (0, typeorm_1.ManyToOne)(() => GovernmentProfile_entity_1.default),
    (0, typeorm_1.JoinColumn)({ name: 'gov_profile_id' }),
    __metadata("design:type", GovernmentProfile_entity_1.default)
], GovernmentIndicators.prototype, "profile", void 0);
GovernmentIndicators = __decorate([
    (0, typeorm_1.Entity)({ name: 'government_indicators', orderBy: { created_at: 'DESC' } })
], GovernmentIndicators);
exports.GovernmentIndicators = GovernmentIndicators;
