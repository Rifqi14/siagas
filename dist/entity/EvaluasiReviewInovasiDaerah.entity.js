"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.EvaluasiReviewInovasiDaerah = void 0;
const lib_1 = require("../types/lib");
const typeorm_1 = require("typeorm");
const PreviewReviewInovasiDaerah_entity_1 = require("./PreviewReviewInovasiDaerah.entity");
const File_entity_1 = __importDefault(require("./File.entity"));
let EvaluasiReviewInovasiDaerah = class EvaluasiReviewInovasiDaerah extends lib_1.AppBaseEntity {
};
__decorate([
    (0, typeorm_1.PrimaryGeneratedColumn)(),
    __metadata("design:type", Number)
], EvaluasiReviewInovasiDaerah.prototype, "id", void 0);
__decorate([
    (0, typeorm_1.Column)('varchar'),
    __metadata("design:type", String)
], EvaluasiReviewInovasiDaerah.prototype, "data_saat_ini", void 0);
__decorate([
    (0, typeorm_1.Column)('varchar'),
    __metadata("design:type", String)
], EvaluasiReviewInovasiDaerah.prototype, "catatan", void 0);
__decorate([
    (0, typeorm_1.Column)('varchar'),
    __metadata("design:type", String)
], EvaluasiReviewInovasiDaerah.prototype, "keterangan", void 0);
__decorate([
    (0, typeorm_1.Column)('bigint'),
    __metadata("design:type", Number)
], EvaluasiReviewInovasiDaerah.prototype, "preview_review_inovasi_daerah_id", void 0);
__decorate([
    (0, typeorm_1.Column)('bigint'),
    __metadata("design:type", Number)
], EvaluasiReviewInovasiDaerah.prototype, "document_id", void 0);
__decorate([
    (0, typeorm_1.ManyToOne)(() => PreviewReviewInovasiDaerah_entity_1.PreviewReviewInovasiDaerah),
    (0, typeorm_1.JoinColumn)({ name: 'preview_review_inovasi_daerah_id' }),
    __metadata("design:type", PreviewReviewInovasiDaerah_entity_1.PreviewReviewInovasiDaerah)
], EvaluasiReviewInovasiDaerah.prototype, "preview", void 0);
__decorate([
    (0, typeorm_1.ManyToOne)(() => File_entity_1.default),
    (0, typeorm_1.JoinColumn)({ name: 'document_id' }),
    __metadata("design:type", File_entity_1.default)
], EvaluasiReviewInovasiDaerah.prototype, "document", void 0);
EvaluasiReviewInovasiDaerah = __decorate([
    (0, typeorm_1.Entity)({ name: 'evaluasi_review_inovasi_daerah' })
], EvaluasiReviewInovasiDaerah);
exports.EvaluasiReviewInovasiDaerah = EvaluasiReviewInovasiDaerah;
