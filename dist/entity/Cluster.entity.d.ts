import { BaseEntity } from 'typeorm';
import ClusterDetail from './ClusterDetail.entity';
export default class Cluster extends BaseEntity {
    id: number;
    name: string;
    created_at: Date;
    updated_at: Date;
    details: ClusterDetail[];
}
