"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.PeringkatHasilReview = void 0;
const typeorm_1 = require("typeorm");
let PeringkatHasilReview = class PeringkatHasilReview {
};
__decorate([
    (0, typeorm_1.ViewColumn)(),
    __metadata("design:type", Number)
], PeringkatHasilReview.prototype, "id", void 0);
__decorate([
    (0, typeorm_1.ViewColumn)(),
    __metadata("design:type", String)
], PeringkatHasilReview.prototype, "nama_daerah", void 0);
__decorate([
    (0, typeorm_1.ViewColumn)(),
    __metadata("design:type", Number)
], PeringkatHasilReview.prototype, "jumlah_inovasi", void 0);
__decorate([
    (0, typeorm_1.ViewColumn)(),
    __metadata("design:type", Number)
], PeringkatHasilReview.prototype, "isp", void 0);
__decorate([
    (0, typeorm_1.ViewColumn)(),
    __metadata("design:type", Number)
], PeringkatHasilReview.prototype, "total_pemda", void 0);
__decorate([
    (0, typeorm_1.ViewColumn)(),
    __metadata("design:type", Number)
], PeringkatHasilReview.prototype, "total_skor_verifikasi", void 0);
__decorate([
    (0, typeorm_1.ViewColumn)(),
    __metadata("design:type", String)
], PeringkatHasilReview.prototype, "predikat", void 0);
__decorate([
    (0, typeorm_1.ViewColumn)(),
    __metadata("design:type", String)
], PeringkatHasilReview.prototype, "nominator", void 0);
__decorate([
    (0, typeorm_1.ViewColumn)(),
    __metadata("design:type", Number)
], PeringkatHasilReview.prototype, "skor", void 0);
__decorate([
    (0, typeorm_1.ViewColumn)(),
    __metadata("design:type", Number)
], PeringkatHasilReview.prototype, "skor_evaluasi", void 0);
__decorate([
    (0, typeorm_1.ViewColumn)(),
    __metadata("design:type", Number)
], PeringkatHasilReview.prototype, "total_skor_mandiri", void 0);
__decorate([
    (0, typeorm_1.ViewColumn)(),
    __metadata("design:type", String)
], PeringkatHasilReview.prototype, "created_by", void 0);
PeringkatHasilReview = __decorate([
    (0, typeorm_1.ViewEntity)({
        expression: `SELECT
  pp.id,
  pp.nama_daerah,
  count(gi.id) AS jumlah_inovasi,
  CASE
    WHEN lower(pp.nominator::text) = 'ya'::text THEN '1.00'::text
    ELSE '0.00'::text
  END AS isp,
  (
    SELECT
      count(pp2.id) AS count
    FROM
      profil_pemda pp2
  ) AS total_pemda,
  CASE
    WHEN (
      (
        SELECT
          sum(pi.skor_verifikasi) AS sum
        FROM
          pemda_indikator pi
        WHERE
          pi.pemda_id = pp.id
      )
    ) > 0::numeric THEN (
      SELECT
        sum(pi.skor_verifikasi) AS sum
      FROM
        pemda_indikator pi
      WHERE
        pi.pemda_id = pp.id
    )
    ELSE 0::numeric
  END AS total_skor_verifikasi,
  (
    SELECT
      ri.predikat
    FROM
      rangking_index ri
    WHERE
      ri.id = pp.id
  ) AS predikat,
  pp.nominator,
  (
    SELECT
      sum(pi.skor) AS sum
    FROM
      pemda_indikator pi
    WHERE
      pi.pemda_id = pp.id
  ) AS skor,
  (
    SELECT
      sum(pi.skor_verifikasi) AS sum
    FROM
      pemda_indikator pi
    WHERE
      pi.pemda_id = pp.id
  ) AS skor_evaluasi,
  sum(
    CASE
      WHEN lower(gi.innovation_phase::text) = 'inisiatif'::text THEN 50
      WHEN lower(gi.innovation_phase::text) = 'uji coba'::text THEN 102
      WHEN lower(gi.innovation_phase::text) = 'penerapan'::text THEN 105
      ELSE 0
    END
  ) AS total_skor_mandiri,
  pp.created_by
FROM
  profil_pemda pp
  LEFT JOIN government_innovations gi ON gi.pemda_id = pp.id
GROUP BY
  pp.id,
  pp.nama_daerah`,
    })
], PeringkatHasilReview);
exports.PeringkatHasilReview = PeringkatHasilReview;
