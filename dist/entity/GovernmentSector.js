"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const typeorm_1 = require("typeorm");
const GovernmentInnovation_entity_1 = __importDefault(require("./GovernmentInnovation.entity"));
let GovernmentSector = class GovernmentSector extends typeorm_1.BaseEntity {
    setDeadline() {
        const now = new Date();
        const deadline = new Date(new Date(this.deadline).getTime() - new Date(this.deadline).getTimezoneOffset() * 60000);
        const diff = deadline.getTime() - now.getTime();
        const dayDiff = Math.round(diff / (1000 * 60 * 60 * 24));
        this.deadline_in_days = dayDiff > 0 ? dayDiff : 0;
        this.deadline = deadline.toISOString().split("T")[0];
    }
};
__decorate([
    (0, typeorm_1.PrimaryGeneratedColumn)(),
    __metadata("design:type", Number)
], GovernmentSector.prototype, "id", void 0);
__decorate([
    (0, typeorm_1.Column)(),
    __metadata("design:type", String)
], GovernmentSector.prototype, "name", void 0);
__decorate([
    (0, typeorm_1.Column)(),
    __metadata("design:type", String)
], GovernmentSector.prototype, "deadline", void 0);
__decorate([
    (0, typeorm_1.CreateDateColumn)(),
    __metadata("design:type", Date)
], GovernmentSector.prototype, "created_at", void 0);
__decorate([
    (0, typeorm_1.UpdateDateColumn)(),
    __metadata("design:type", Date)
], GovernmentSector.prototype, "updated_at", void 0);
__decorate([
    (0, typeorm_1.OneToMany)(() => GovernmentInnovation_entity_1.default, innovations => innovations.urusan),
    __metadata("design:type", Array)
], GovernmentSector.prototype, "innovations", void 0);
__decorate([
    (0, typeorm_1.AfterLoad)(),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", []),
    __metadata("design:returntype", void 0)
], GovernmentSector.prototype, "setDeadline", null);
GovernmentSector = __decorate([
    (0, typeorm_1.Entity)({ name: "government_sectors" })
], GovernmentSector);
exports.default = GovernmentSector;
