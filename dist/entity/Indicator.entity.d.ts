import { AppBaseEntity } from '../types/lib';
import { PemdaIndikator } from './PemdaIndikator.entity';
export default class Indicator extends AppBaseEntity {
    id: number;
    no_urut: number;
    jenis_indikator: string;
    nama_indikator: string;
    keterangan: string;
    nama_dokumen_pendukung: string;
    bobot: number;
    jenis_file: string;
    parent: string;
    mandatory: string;
    pemdaIndikator: PemdaIndikator[];
}
