"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.LaporanInisiatorInovasi = void 0;
const typeorm_1 = require("typeorm");
let LaporanInisiatorInovasi = class LaporanInisiatorInovasi {
};
__decorate([
    (0, typeorm_1.ViewColumn)(),
    __metadata("design:type", Number)
], LaporanInisiatorInovasi.prototype, "pemda_id", void 0);
__decorate([
    (0, typeorm_1.ViewColumn)(),
    __metadata("design:type", String)
], LaporanInisiatorInovasi.prototype, "inisiator_inovasi", void 0);
__decorate([
    (0, typeorm_1.ViewColumn)(),
    __metadata("design:type", Number)
], LaporanInisiatorInovasi.prototype, "total_disetujui", void 0);
__decorate([
    (0, typeorm_1.ViewColumn)(),
    __metadata("design:type", Number)
], LaporanInisiatorInovasi.prototype, "total_ditolak", void 0);
__decorate([
    (0, typeorm_1.ViewColumn)(),
    __metadata("design:type", Number)
], LaporanInisiatorInovasi.prototype, "total_keseluruhan", void 0);
LaporanInisiatorInovasi = __decorate([
    (0, typeorm_1.ViewEntity)({
        expression: `select
    gi.pemda_id,
    gi.innovation_initiator as inisiator_inovasi,
    (select count(rid.id) from review_inovasi_daerah rid left join government_innovations gi2 on gi2.id = rid.inovasi_id where lower(gi2.innovation_initiator) = lower(gi.innovation_initiator) and lower(rid.status) = 'accept' and gi2.pemda_id = gi.pemda_id) total_disetujui,
    (select count(rid.id) from review_inovasi_daerah rid left join government_innovations gi2 on gi2.id = rid.inovasi_id where lower(gi2.innovation_initiator) = lower(gi.innovation_initiator) and (lower(rid.status) = 'pending' or lower(rid.status) = 'rejected') and gi2.pemda_id = gi.pemda_id) total_ditolak,
    (select count(rid.id) from review_inovasi_daerah rid left join government_innovations gi2 on gi2.id = rid.inovasi_id where lower(gi2.innovation_initiator) = lower(gi.innovation_initiator) and gi2.pemda_id = gi.pemda_id) total_keseluruhan
  from profil_pemda pp
  right join government_innovations gi on gi.pemda_id = pp.id
  group by gi.innovation_initiator, gi.pemda_id`
    })
], LaporanInisiatorInovasi);
exports.LaporanInisiatorInovasi = LaporanInisiatorInovasi;
