export declare class PeringkatHasilReview {
    id: number;
    nama_daerah: string;
    jumlah_inovasi: number;
    isp: number;
    total_pemda: number;
    total_skor_verifikasi: number;
    predikat: string;
    nominator: string;
    skor: number;
    skor_evaluasi: number;
    total_skor_mandiri: number;
    created_by: string;
}
