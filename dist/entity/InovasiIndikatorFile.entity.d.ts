import { AppBaseEntity } from '../types/lib';
import { InovasiIndikator } from './InovasiIndikator.entity';
import File from './File.entity';
import GovernmentInnovation from './GovernmentInnovation.entity';
import Indicator from './Indicator.entity';
export declare class InovasiIndikatorFile extends AppBaseEntity {
    id: number;
    inovasi_indikator_id: number;
    inovasi_id: number;
    indikator_id: number;
    file_id: number;
    nomor_surat: string;
    tanggal_surat: string;
    tentang: string;
    inovasi: GovernmentInnovation;
    indikator: Indicator;
    inovasi_indikator: InovasiIndikator;
    file: File;
}
