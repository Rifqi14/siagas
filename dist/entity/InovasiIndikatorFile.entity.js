"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.InovasiIndikatorFile = void 0;
const lib_1 = require("../types/lib");
const typeorm_1 = require("typeorm");
const InovasiIndikator_entity_1 = require("./InovasiIndikator.entity");
const File_entity_1 = __importDefault(require("./File.entity"));
const GovernmentInnovation_entity_1 = __importDefault(require("./GovernmentInnovation.entity"));
const Indicator_entity_1 = __importDefault(require("./Indicator.entity"));
let InovasiIndikatorFile = class InovasiIndikatorFile extends lib_1.AppBaseEntity {
};
__decorate([
    (0, typeorm_1.PrimaryGeneratedColumn)(),
    __metadata("design:type", Number)
], InovasiIndikatorFile.prototype, "id", void 0);
__decorate([
    (0, typeorm_1.Column)(),
    __metadata("design:type", Number)
], InovasiIndikatorFile.prototype, "inovasi_indikator_id", void 0);
__decorate([
    (0, typeorm_1.Column)(),
    __metadata("design:type", Number)
], InovasiIndikatorFile.prototype, "inovasi_id", void 0);
__decorate([
    (0, typeorm_1.Column)(),
    __metadata("design:type", Number)
], InovasiIndikatorFile.prototype, "indikator_id", void 0);
__decorate([
    (0, typeorm_1.Column)(),
    __metadata("design:type", Number)
], InovasiIndikatorFile.prototype, "file_id", void 0);
__decorate([
    (0, typeorm_1.Column)(),
    __metadata("design:type", String)
], InovasiIndikatorFile.prototype, "nomor_surat", void 0);
__decorate([
    (0, typeorm_1.Column)(),
    __metadata("design:type", String)
], InovasiIndikatorFile.prototype, "tanggal_surat", void 0);
__decorate([
    (0, typeorm_1.Column)(),
    __metadata("design:type", String)
], InovasiIndikatorFile.prototype, "tentang", void 0);
__decorate([
    (0, typeorm_1.ManyToOne)(() => GovernmentInnovation_entity_1.default),
    (0, typeorm_1.JoinColumn)({ name: 'inovasi_id' }),
    __metadata("design:type", GovernmentInnovation_entity_1.default)
], InovasiIndikatorFile.prototype, "inovasi", void 0);
__decorate([
    (0, typeorm_1.ManyToOne)(() => Indicator_entity_1.default, { eager: true }),
    (0, typeorm_1.JoinColumn)({ name: 'indikator_id' }),
    __metadata("design:type", Indicator_entity_1.default)
], InovasiIndikatorFile.prototype, "indikator", void 0);
__decorate([
    (0, typeorm_1.ManyToOne)(() => InovasiIndikator_entity_1.InovasiIndikator),
    (0, typeorm_1.JoinColumn)({ name: 'inovasi_indikator_id' }),
    __metadata("design:type", InovasiIndikator_entity_1.InovasiIndikator)
], InovasiIndikatorFile.prototype, "inovasi_indikator", void 0);
__decorate([
    (0, typeorm_1.ManyToOne)(() => File_entity_1.default, { eager: true }),
    (0, typeorm_1.JoinColumn)({ name: 'file_id' }),
    __metadata("design:type", File_entity_1.default)
], InovasiIndikatorFile.prototype, "file", void 0);
InovasiIndikatorFile = __decorate([
    (0, typeorm_1.Entity)({ name: 'inovasi_indikator_file' })
], InovasiIndikatorFile);
exports.InovasiIndikatorFile = InovasiIndikatorFile;
