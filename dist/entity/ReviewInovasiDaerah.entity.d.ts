import { AppBaseEntity } from '../types/lib';
import GovernmentInnovation from './GovernmentInnovation.entity';
import { PreviewReviewInovasiDaerah } from './PreviewReviewInovasiDaerah.entity';
export declare class ReviewInovasiDaerah extends AppBaseEntity {
    id: number;
    random_number: number;
    skor: number;
    status?: string;
    inovasi_id?: number;
    inovasi: GovernmentInnovation;
    previews: PreviewReviewInovasiDaerah[];
}
