import { AppBaseEntity } from '../types/lib';
import Indicator from './Indicator.entity';
import { PemdaIndikatorFile } from './PemdaIndikatorFile.entity';
import { ProfilPemda } from './ProfilePemda.entity';
export declare class PemdaIndikator extends AppBaseEntity {
    id: number;
    pemda_id: number;
    indikator_id: number;
    informasi: string;
    keterangan: string;
    catatan: string;
    skor: number;
    nilai_sebelum: number;
    nilai_sesudah: number;
    skor_verifikasi: string;
    data_saat_ini: string;
    created_at: Date;
    updated_at: Date;
    pemda: ProfilPemda;
    indicator: Indicator;
    files: PemdaIndikatorFile[];
}
