import { AppBaseEntity, OmitType } from '../types/lib';
import File from './File.entity';
import User from './User.entity';
import { PemdaIndikator } from './PemdaIndikator.entity';
import GovernmentInnovation from './GovernmentInnovation.entity';
export declare class ProfilPemda extends AppBaseEntity {
    id: number;
    user_id: string;
    nama_daerah: string;
    opd_yang_menangani: string;
    alamat_pemda: string;
    email: string;
    no_telpon: string;
    nama_admin: string;
    document_id: string;
    nama_pemda: string;
    nominator: string;
    nominator_rekap_indeks_akhir: string;
    document: OmitType<File, 'announcements'>;
    user: User;
    indicators: PemdaIndikator[];
    innovations: GovernmentInnovation[];
}
