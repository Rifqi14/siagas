import { BaseEntity } from 'typeorm';
export default class IndicatorScale extends BaseEntity {
    id: number;
    indicator_id: number;
    value: number;
    start_at: number;
    finish_at: number;
    description: string;
    created_at: Date;
    updated_at: Date;
}
