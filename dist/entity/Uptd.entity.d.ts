import { BaseEntity } from 'typeorm';
import RegionalApparatus from './RegionApparatus.entity';
export default class Uptd extends BaseEntity {
    id: number;
    regional_apparatus_id: number;
    name: string;
    created_at: Date;
    created_by: string;
    updated_at: Date;
    updated_by: string;
    regionalApparatus: RegionalApparatus;
}
