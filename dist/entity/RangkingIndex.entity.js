"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.RangkingIndex = void 0;
const typeorm_1 = require("typeorm");
let RangkingIndex = class RangkingIndex {
};
__decorate([
    (0, typeorm_1.ViewColumn)(),
    __metadata("design:type", Number)
], RangkingIndex.prototype, "id", void 0);
__decorate([
    (0, typeorm_1.ViewColumn)(),
    __metadata("design:type", String)
], RangkingIndex.prototype, "nama_daerah", void 0);
__decorate([
    (0, typeorm_1.ViewColumn)(),
    __metadata("design:type", Number)
], RangkingIndex.prototype, "jumlah_inovasi", void 0);
__decorate([
    (0, typeorm_1.ViewColumn)(),
    __metadata("design:type", Number)
], RangkingIndex.prototype, "total_skor_mandiri", void 0);
__decorate([
    (0, typeorm_1.ViewColumn)(),
    __metadata("design:type", Number)
], RangkingIndex.prototype, "nilai_indeks", void 0);
__decorate([
    (0, typeorm_1.ViewColumn)(),
    __metadata("design:type", Number)
], RangkingIndex.prototype, "total_file", void 0);
__decorate([
    (0, typeorm_1.ViewColumn)(),
    __metadata("design:type", String)
], RangkingIndex.prototype, "predikat", void 0);
__decorate([
    (0, typeorm_1.ViewColumn)(),
    __metadata("design:type", Number)
], RangkingIndex.prototype, "indeks", void 0);
__decorate([
    (0, typeorm_1.ViewColumn)(),
    __metadata("design:type", String)
], RangkingIndex.prototype, "nominator", void 0);
__decorate([
    (0, typeorm_1.ViewColumn)(),
    __metadata("design:type", String)
], RangkingIndex.prototype, "created_by", void 0);
RangkingIndex = __decorate([
    (0, typeorm_1.ViewEntity)({
        expression: `select
      pp.id,
      pp.nama_daerah,
      (select count(gi.id) from government_innovations gi where gi.pemda_id = pp.id) as jumlah_inovasi,
      sum(
        case
          when lower(gi.innovation_phase) = 'inisiatif' then 50
          when lower(gi.innovation_phase) = 'uji coba' then 102
          when lower(gi.innovation_phase) = 'penerapan' then 105
          else 0
        end
      ) as total_skor_mandiri,
      case
        when count(gi.id) > 0 then sum(
          case
            when lower(gi.innovation_phase) = 'inisiatif' then 50
            when lower(gi.innovation_phase) = 'uji coba' then 102
            when lower(gi.innovation_phase) = 'penerapan' then 105
            else 0
          end
        ) / (select count(gi.id) from government_innovations gi where gi.pemda_id = pp.id)::float
        else 0::float
      end as nilai_indeks,
      count(iif.id) as total_file,
      case
        when count(iif.id) > 10 then 'Sangat Inovatif'
        when count(iif.id) <= 10 and count(iif.id) >= 6 then 'Inovatif'
        when count(iif.id) <= 5 and count(iif.id) > 0 then 'Kurang Inovatif'
        else 'Tidak Inovatif'
      end as predikat,
      row_number () over (
        order by case
          when count(gi.id) > 0 then sum(
            case
              when lower(gi.innovation_phase) = 'inisiatif' then 50
              when lower(gi.innovation_phase) = 'uji coba' then 102
              when lower(gi.innovation_phase) = 'penerapan' then 105
              else 0
            end
          ) / (select count(gi.id) from government_innovations gi where gi.pemda_id = pp.id)::float
          else 0::float
        end desc
      ) as indeks,
      pp.nominator as nominator
    from profil_pemda pp
    left join government_innovations gi on gi.pemda_id = pp.id
    left join inovasi_indikator ii on ii.inovasi_id = gi.id
    left join inovasi_indikator_file iif on iif.inovasi_indikator_id = ii.id
    group by pp.nama_daerah, pp.id`
    })
], RangkingIndex);
exports.RangkingIndex = RangkingIndex;
