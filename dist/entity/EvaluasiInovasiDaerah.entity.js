"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.EvaluasiInovasiDaerah = void 0;
const lib_1 = require("../types/lib");
const typeorm_1 = require("typeorm");
const InovasiIndikator_entity_1 = require("./InovasiIndikator.entity");
let EvaluasiInovasiDaerah = class EvaluasiInovasiDaerah extends lib_1.AppBaseEntity {
};
__decorate([
    (0, typeorm_1.PrimaryGeneratedColumn)(),
    __metadata("design:type", Number)
], EvaluasiInovasiDaerah.prototype, "id", void 0);
__decorate([
    (0, typeorm_1.Column)('varchar'),
    __metadata("design:type", String)
], EvaluasiInovasiDaerah.prototype, "data_saat_ini", void 0);
__decorate([
    (0, typeorm_1.Column)('varchar'),
    __metadata("design:type", String)
], EvaluasiInovasiDaerah.prototype, "catatan", void 0);
__decorate([
    (0, typeorm_1.Column)('varchar'),
    __metadata("design:type", String)
], EvaluasiInovasiDaerah.prototype, "keterangan", void 0);
__decorate([
    (0, typeorm_1.Column)('varchar'),
    __metadata("design:type", String)
], EvaluasiInovasiDaerah.prototype, "tentang", void 0);
__decorate([
    (0, typeorm_1.Column)('varchar'),
    __metadata("design:type", String)
], EvaluasiInovasiDaerah.prototype, "kategori", void 0);
__decorate([
    (0, typeorm_1.Column)('bigint'),
    __metadata("design:type", Number)
], EvaluasiInovasiDaerah.prototype, "inovasi_indikator_id", void 0);
__decorate([
    (0, typeorm_1.ManyToOne)(() => InovasiIndikator_entity_1.InovasiIndikator),
    (0, typeorm_1.JoinColumn)({ name: 'inovasi_indikator_id' }),
    __metadata("design:type", InovasiIndikator_entity_1.InovasiIndikator)
], EvaluasiInovasiDaerah.prototype, "inovasi_indikator", void 0);
EvaluasiInovasiDaerah = __decorate([
    (0, typeorm_1.Entity)({ name: 'evaluasi_inovasi_daerah' })
], EvaluasiInovasiDaerah);
exports.EvaluasiInovasiDaerah = EvaluasiInovasiDaerah;
