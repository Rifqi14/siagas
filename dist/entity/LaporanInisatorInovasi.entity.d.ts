export declare class LaporanInisiatorInovasi {
    pemda_id: number;
    inisiator_inovasi: string;
    total_disetujui: number;
    total_ditolak: number;
    total_keseluruhan: number;
}
