"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const typeorm_1 = require("typeorm");
const File_entity_1 = __importDefault(require("./File.entity"));
const Region_entity_1 = __importDefault(require("./Region.entity"));
let GovernmentProfile = class GovernmentProfile extends typeorm_1.BaseEntity {
};
__decorate([
    (0, typeorm_1.PrimaryGeneratedColumn)(),
    __metadata("design:type", Number)
], GovernmentProfile.prototype, "id", void 0);
__decorate([
    (0, typeorm_1.Column)(),
    __metadata("design:type", Number)
], GovernmentProfile.prototype, "region_id", void 0);
__decorate([
    (0, typeorm_1.Column)(),
    __metadata("design:type", String)
], GovernmentProfile.prototype, "name", void 0);
__decorate([
    (0, typeorm_1.Column)(),
    __metadata("design:type", String)
], GovernmentProfile.prototype, "pic", void 0);
__decorate([
    (0, typeorm_1.Column)(),
    __metadata("design:type", String)
], GovernmentProfile.prototype, "address", void 0);
__decorate([
    (0, typeorm_1.Column)(),
    __metadata("design:type", String)
], GovernmentProfile.prototype, "email", void 0);
__decorate([
    (0, typeorm_1.Column)(),
    __metadata("design:type", String)
], GovernmentProfile.prototype, "phone", void 0);
__decorate([
    (0, typeorm_1.Column)(),
    __metadata("design:type", String)
], GovernmentProfile.prototype, "admin_name", void 0);
__decorate([
    (0, typeorm_1.Column)({ nullable: true }),
    __metadata("design:type", Number)
], GovernmentProfile.prototype, "file_id", void 0);
__decorate([
    (0, typeorm_1.CreateDateColumn)(),
    __metadata("design:type", Date)
], GovernmentProfile.prototype, "created_at", void 0);
__decorate([
    (0, typeorm_1.UpdateDateColumn)(),
    __metadata("design:type", Date)
], GovernmentProfile.prototype, "updated_at", void 0);
__decorate([
    (0, typeorm_1.ManyToOne)(() => Region_entity_1.default, { eager: true }),
    (0, typeorm_1.JoinColumn)({ name: 'region_id' }),
    __metadata("design:type", Object)
], GovernmentProfile.prototype, "region", void 0);
__decorate([
    (0, typeorm_1.ManyToOne)(() => File_entity_1.default, { eager: true }),
    (0, typeorm_1.JoinColumn)({ name: 'file_id' }),
    __metadata("design:type", Object)
], GovernmentProfile.prototype, "file", void 0);
GovernmentProfile = __decorate([
    (0, typeorm_1.Entity)({ name: 'government_profiles', orderBy: { created_at: 'DESC' } })
], GovernmentProfile);
exports.default = GovernmentProfile;
