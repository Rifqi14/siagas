"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.PreviewReviewInovasiDaerah = void 0;
const lib_1 = require("../types/lib");
const typeorm_1 = require("typeorm");
const ReviewInovasiDaerah_entity_1 = require("./ReviewInovasiDaerah.entity");
const EvaluasiReviewInovasiDaerah_entity_1 = require("./EvaluasiReviewInovasiDaerah.entity");
let PreviewReviewInovasiDaerah = class PreviewReviewInovasiDaerah extends lib_1.AppBaseEntity {
};
__decorate([
    (0, typeorm_1.PrimaryGeneratedColumn)(),
    __metadata("design:type", Number)
], PreviewReviewInovasiDaerah.prototype, "id", void 0);
__decorate([
    (0, typeorm_1.Column)('varchar'),
    __metadata("design:type", String)
], PreviewReviewInovasiDaerah.prototype, "komentar", void 0);
__decorate([
    (0, typeorm_1.Column)('bigint'),
    __metadata("design:type", Number)
], PreviewReviewInovasiDaerah.prototype, "review_inovasi_daerah_id", void 0);
__decorate([
    (0, typeorm_1.ManyToOne)(() => ReviewInovasiDaerah_entity_1.ReviewInovasiDaerah),
    (0, typeorm_1.JoinColumn)({ name: 'review_inovasi_daerah_id' }),
    __metadata("design:type", ReviewInovasiDaerah_entity_1.ReviewInovasiDaerah)
], PreviewReviewInovasiDaerah.prototype, "review_inovasi_daerah", void 0);
__decorate([
    (0, typeorm_1.OneToMany)(() => EvaluasiReviewInovasiDaerah_entity_1.EvaluasiReviewInovasiDaerah, evaluasi => evaluasi.preview),
    __metadata("design:type", Array)
], PreviewReviewInovasiDaerah.prototype, "evaluasi", void 0);
PreviewReviewInovasiDaerah = __decorate([
    (0, typeorm_1.Entity)({ name: 'preview_review_inovasi_daerah' })
], PreviewReviewInovasiDaerah);
exports.PreviewReviewInovasiDaerah = PreviewReviewInovasiDaerah;
