import { AppBaseEntity } from '../types/lib';
import { PreviewReviewInovasiDaerah } from './PreviewReviewInovasiDaerah.entity';
import File from './File.entity';
export declare class EvaluasiReviewInovasiDaerah extends AppBaseEntity {
    id: number;
    data_saat_ini: string;
    catatan: string;
    keterangan: string;
    preview_review_inovasi_daerah_id: number;
    document_id: number;
    preview: PreviewReviewInovasiDaerah;
    document: File;
}
