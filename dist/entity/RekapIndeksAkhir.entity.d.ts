export declare class RekapIndeksAkhir {
    id: number;
    nama_daerah: string;
    jumlah_inovasi: number;
    total_skor_mandiri: number;
    nilai_indeks: number;
    total_file: number;
    nilai_indeks_verifikasi: string;
    predikat: string;
    indeks: number;
    nominator: string;
    created_by: string;
}
