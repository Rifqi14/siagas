export declare class DashboardArsip {
    id: number;
    pemda_id: number;
    nama_daerah: string;
    innovation_name: string;
    innovation_phase: string;
    trial_time: string;
    implementation_time: string;
    skor: number;
    created_by: string;
}
