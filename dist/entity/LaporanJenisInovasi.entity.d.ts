export declare class LaporanJenisInovasi {
    pemda_id: number;
    jenis_inovasi: string;
    total_disetujui: number;
    total_ditolak: number;
    total_keseluruhan: number;
}
