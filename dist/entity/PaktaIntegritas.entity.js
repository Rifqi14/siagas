"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.PaktaIntegritas = void 0;
const lib_1 = require("../types/lib");
const typeorm_1 = require("typeorm");
const User_entity_1 = __importDefault(require("./User.entity"));
const File_entity_1 = __importDefault(require("./File.entity"));
let PaktaIntegritas = class PaktaIntegritas extends lib_1.CustomBaseEntity {
};
__decorate([
    (0, typeorm_1.PrimaryGeneratedColumn)(),
    __metadata("design:type", Number)
], PaktaIntegritas.prototype, "id", void 0);
__decorate([
    (0, typeorm_1.Column)(),
    __metadata("design:type", String)
], PaktaIntegritas.prototype, "user_id", void 0);
__decorate([
    (0, typeorm_1.Column)(),
    __metadata("design:type", String)
], PaktaIntegritas.prototype, "upload_id", void 0);
__decorate([
    (0, typeorm_1.CreateDateColumn)(),
    __metadata("design:type", Date)
], PaktaIntegritas.prototype, "created_at", void 0);
__decorate([
    (0, typeorm_1.UpdateDateColumn)(),
    __metadata("design:type", Date)
], PaktaIntegritas.prototype, "updated_at", void 0);
__decorate([
    (0, typeorm_1.ManyToOne)(() => File_entity_1.default, {}),
    (0, typeorm_1.JoinColumn)({ name: 'upload_id' }),
    __metadata("design:type", Object)
], PaktaIntegritas.prototype, "upload", void 0);
__decorate([
    (0, typeorm_1.ManyToOne)(() => User_entity_1.default, {}),
    (0, typeorm_1.JoinColumn)({ name: 'user_id' }),
    __metadata("design:type", User_entity_1.default)
], PaktaIntegritas.prototype, "user", void 0);
PaktaIntegritas = __decorate([
    (0, typeorm_1.Entity)({ name: 'pakta_integritas' })
], PaktaIntegritas);
exports.PaktaIntegritas = PaktaIntegritas;
