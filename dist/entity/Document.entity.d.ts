import { BaseEntity } from 'typeorm';
import DocumentCategory from './DocumentCategory.entity';
import File from './File.entity';
export default class Document extends BaseEntity {
    id: number;
    category_id: number | null;
    title: string;
    content: string;
    document_id: number | null;
    created_at: number;
    updated_at: number;
    category: DocumentCategory;
    document: File;
}
