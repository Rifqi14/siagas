import { CustomBaseEntity } from '../types/lib';
import Uptd from './Uptd.entity';
export default class RegionalApparatus extends CustomBaseEntity {
    id: number;
    name: string;
    created_at: Date;
    updated_at: Date;
    uptds: Uptd[];
}
