"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.LaporanIndeks = void 0;
const typeorm_1 = require("typeorm");
let LaporanIndeks = class LaporanIndeks {
};
__decorate([
    (0, typeorm_1.ViewColumn)(),
    __metadata("design:type", Number)
], LaporanIndeks.prototype, "pemda_id", void 0);
__decorate([
    (0, typeorm_1.ViewColumn)(),
    __metadata("design:type", String)
], LaporanIndeks.prototype, "nama_daerah", void 0);
__decorate([
    (0, typeorm_1.ViewColumn)(),
    __metadata("design:type", String)
], LaporanIndeks.prototype, "opd_yang_menangani", void 0);
__decorate([
    (0, typeorm_1.ViewColumn)(),
    __metadata("design:type", Number)
], LaporanIndeks.prototype, "jumlah_inovasi", void 0);
__decorate([
    (0, typeorm_1.ViewColumn)(),
    __metadata("design:type", Number)
], LaporanIndeks.prototype, "total_skor_mandiri", void 0);
__decorate([
    (0, typeorm_1.ViewColumn)(),
    __metadata("design:type", Number)
], LaporanIndeks.prototype, "nilai_indeks", void 0);
__decorate([
    (0, typeorm_1.ViewColumn)(),
    __metadata("design:type", Number)
], LaporanIndeks.prototype, "total_file", void 0);
__decorate([
    (0, typeorm_1.ViewColumn)(),
    __metadata("design:type", String)
], LaporanIndeks.prototype, "predikat", void 0);
__decorate([
    (0, typeorm_1.ViewColumn)(),
    __metadata("design:type", Number)
], LaporanIndeks.prototype, "indeks", void 0);
__decorate([
    (0, typeorm_1.ViewColumn)(),
    __metadata("design:type", String)
], LaporanIndeks.prototype, "nominator", void 0);
__decorate([
    (0, typeorm_1.ViewColumn)(),
    __metadata("design:type", String)
], LaporanIndeks.prototype, "created_by", void 0);
LaporanIndeks = __decorate([
    (0, typeorm_1.ViewEntity)({
        expression: `SELECT
                pp.id as pemda_id,
                pp.nama_daerah,
                pp.opd_yang_menangani,
                (
                  SELECT
                    count(gi_1.id) AS count
                  FROM
                    government_innovations gi_1
                  WHERE
                    gi_1.pemda_id = pp.id
                ) AS jumlah_inovasi,
                sum(
                  CASE
                    WHEN lower(gi.innovation_phase::text) = 'inisiatif'::text THEN 50
                    WHEN lower(gi.innovation_phase::text) = 'uji coba'::text THEN 102
                    WHEN lower(gi.innovation_phase::text) = 'penerapan'::text THEN 105
                    ELSE 0
                  END
                ) AS total_skor_mandiri,
                CASE
                  WHEN count(gi.id) > 0 THEN sum(
                    CASE
                      WHEN lower(gi.innovation_phase::text) = 'inisiatif'::text THEN 50
                      WHEN lower(gi.innovation_phase::text) = 'uji coba'::text THEN 102
                      WHEN lower(gi.innovation_phase::text) = 'penerapan'::text THEN 105
                      ELSE 0
                    END
                  )::double precision / (
                    (
                      SELECT
                        count(gi_1.id) AS count
                      FROM
                        government_innovations gi_1
                      WHERE
                        gi_1.pemda_id = pp.id
                    )
                  )::double precision
                  ELSE 0::double precision
                END AS nilai_indeks,
                count(iif.id) AS total_file,
                CASE
                  WHEN count(iif.id) > 10 THEN 'Sangat Inovatif'::text
                  WHEN count(iif.id) <= 10
                  AND count(iif.id) >= 6 THEN 'Inovatif'::text
                  WHEN count(iif.id) <= 5
                  AND count(iif.id) > 0 THEN 'Kurang Inovatif'::text
                  ELSE 'Tidak Inovatif'::text
                END AS predikat,
                row_number() OVER (
                  ORDER BY
                    (
                      CASE
                        WHEN count(gi.id) > 0 THEN sum(
                          CASE
                            WHEN lower(gi.innovation_phase::text) = 'inisiatif'::text THEN 50
                            WHEN lower(gi.innovation_phase::text) = 'uji coba'::text THEN 102
                            WHEN lower(gi.innovation_phase::text) = 'penerapan'::text THEN 105
                            ELSE 0
                          END
                        )::double precision / (
                          (
                            SELECT
                              count(gi_1.id) AS count
                            FROM
                              government_innovations gi_1
                            WHERE
                              gi_1.pemda_id = pp.id
                          )
                        )::double precision
                        ELSE 0::double precision
                      END
                    ) DESC
                ) AS indeks,
                pp.nominator,
                pp.created_by
              FROM
                profil_pemda pp
                LEFT JOIN government_innovations gi ON gi.pemda_id = pp.id
                LEFT JOIN inovasi_indikator ii ON ii.inovasi_id = gi.id
                LEFT JOIN inovasi_indikator_file iif ON iif.inovasi_indikator_id = ii.id
              GROUP BY
                pp.nama_daerah,
                pp.id;`
    })
], LaporanIndeks);
exports.LaporanIndeks = LaporanIndeks;
