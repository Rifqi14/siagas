import { BaseEntity } from 'typeorm';
import Announcement from './Announcement.entity';
export default class File extends BaseEntity {
    id: number;
    name: string;
    path: string;
    extension: string;
    size: string;
    uploaded_by: string;
    created_at: Date;
    updated_at: Date;
    full_path: string;
    announcements: Announcement[];
    setFullPath(): void;
}
