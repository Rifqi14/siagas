import { BaseEntity } from "typeorm";
export declare class Distrik extends BaseEntity {
    id: number;
    nama_distrik: string;
    created_at: Date;
    updated_at: Date;
}
