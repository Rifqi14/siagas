import { CustomBaseEntity, OmitType } from '../types/lib';
import User from './User.entity';
import File from './File.entity';
export declare class PaktaIntegritas extends CustomBaseEntity {
    id: number;
    user_id: string;
    upload_id: string;
    created_at: Date;
    updated_at: Date;
    upload: OmitType<File, 'announcements'>;
    user: User;
}
