import { BaseEntity } from 'typeorm';
export default class Tuxedo extends BaseEntity {
    id: number;
    title: string;
    slug: string;
    section: string;
    content: string;
    created_at: Date;
    updated_at: Date;
}
