"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const lib_1 = require("../types/lib");
const typeorm_1 = require("typeorm");
const Uptd_entity_1 = __importDefault(require("./Uptd.entity"));
let RegionalApparatus = class RegionalApparatus extends lib_1.CustomBaseEntity {
};
__decorate([
    (0, typeorm_1.PrimaryGeneratedColumn)(),
    __metadata("design:type", Number)
], RegionalApparatus.prototype, "id", void 0);
__decorate([
    (0, typeorm_1.Column)(),
    __metadata("design:type", String)
], RegionalApparatus.prototype, "name", void 0);
__decorate([
    (0, typeorm_1.CreateDateColumn)(),
    __metadata("design:type", Date)
], RegionalApparatus.prototype, "created_at", void 0);
__decorate([
    (0, typeorm_1.UpdateDateColumn)(),
    __metadata("design:type", Date)
], RegionalApparatus.prototype, "updated_at", void 0);
__decorate([
    (0, typeorm_1.OneToMany)(() => Uptd_entity_1.default, uptd => uptd.regionalApparatus),
    __metadata("design:type", Array)
], RegionalApparatus.prototype, "uptds", void 0);
RegionalApparatus = __decorate([
    (0, typeorm_1.Entity)({ name: 'regional_apparatus' })
], RegionalApparatus);
exports.default = RegionalApparatus;
