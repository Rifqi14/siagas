import { BaseEntity } from 'typeorm';
import File from './File.entity';
export default class Announcement extends BaseEntity {
    id: number;
    title: string;
    slug: string;
    content: string;
    file_id: number | null;
    created_at: Date;
    updated_at: Date;
    file: File;
}
