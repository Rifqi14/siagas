"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.InovasiIndikator = void 0;
const lib_1 = require("../types/lib");
const typeorm_1 = require("typeorm");
const GovernmentInnovation_entity_1 = __importDefault(require("./GovernmentInnovation.entity"));
const Indicator_entity_1 = __importDefault(require("./Indicator.entity"));
const InovasiIndikatorFile_entity_1 = require("./InovasiIndikatorFile.entity");
const EvaluasiInovasiDaerah_entity_1 = require("./EvaluasiInovasiDaerah.entity");
let InovasiIndikator = class InovasiIndikator extends lib_1.AppBaseEntity {
};
__decorate([
    (0, typeorm_1.PrimaryGeneratedColumn)(),
    __metadata("design:type", Number)
], InovasiIndikator.prototype, "id", void 0);
__decorate([
    (0, typeorm_1.Column)(),
    __metadata("design:type", Number)
], InovasiIndikator.prototype, "inovasi_id", void 0);
__decorate([
    (0, typeorm_1.Column)(),
    __metadata("design:type", Number)
], InovasiIndikator.prototype, "indikator_id", void 0);
__decorate([
    (0, typeorm_1.Column)(),
    __metadata("design:type", String)
], InovasiIndikator.prototype, "informasi", void 0);
__decorate([
    (0, typeorm_1.Column)(),
    __metadata("design:type", Number)
], InovasiIndikator.prototype, "nilai", void 0);
__decorate([
    (0, typeorm_1.Column)(),
    __metadata("design:type", Number)
], InovasiIndikator.prototype, "nilai_sebelum", void 0);
__decorate([
    (0, typeorm_1.Column)(),
    __metadata("design:type", Number)
], InovasiIndikator.prototype, "nilai_sesudah", void 0);
__decorate([
    (0, typeorm_1.ManyToOne)(() => GovernmentInnovation_entity_1.default),
    (0, typeorm_1.JoinColumn)({ name: 'inovasi_id' }),
    __metadata("design:type", GovernmentInnovation_entity_1.default)
], InovasiIndikator.prototype, "inovasi", void 0);
__decorate([
    (0, typeorm_1.ManyToOne)(() => Indicator_entity_1.default, { eager: true }),
    (0, typeorm_1.JoinColumn)({ name: 'indikator_id' }),
    __metadata("design:type", Indicator_entity_1.default)
], InovasiIndikator.prototype, "indikator", void 0);
__decorate([
    (0, typeorm_1.OneToMany)(() => EvaluasiInovasiDaerah_entity_1.EvaluasiInovasiDaerah, evaluasi => evaluasi.inovasi_indikator),
    __metadata("design:type", Array)
], InovasiIndikator.prototype, "evaluasi", void 0);
__decorate([
    (0, typeorm_1.OneToMany)(() => InovasiIndikatorFile_entity_1.InovasiIndikatorFile, file => file.inovasi_indikator, {
        eager: true
    }),
    __metadata("design:type", Array)
], InovasiIndikator.prototype, "files", void 0);
InovasiIndikator = __decorate([
    (0, typeorm_1.Entity)({ name: 'inovasi_indikator' })
], InovasiIndikator);
exports.InovasiIndikator = InovasiIndikator;
