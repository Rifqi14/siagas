"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.Dashboard = void 0;
const typeorm_1 = require("typeorm");
let Dashboard = class Dashboard {
};
__decorate([
    (0, typeorm_1.ViewColumn)(),
    __metadata("design:type", Number)
], Dashboard.prototype, "total_pemda", void 0);
__decorate([
    (0, typeorm_1.ViewColumn)(),
    __metadata("design:type", Number)
], Dashboard.prototype, "total_inovasi", void 0);
__decorate([
    (0, typeorm_1.ViewColumn)(),
    __metadata("design:type", Number)
], Dashboard.prototype, "total_uji_coba", void 0);
__decorate([
    (0, typeorm_1.ViewColumn)(),
    __metadata("design:type", Number)
], Dashboard.prototype, "total_penerapan", void 0);
__decorate([
    (0, typeorm_1.ViewColumn)(),
    __metadata("design:type", Number)
], Dashboard.prototype, "total_inisiatif", void 0);
__decorate([
    (0, typeorm_1.ViewColumn)(),
    __metadata("design:type", String)
], Dashboard.prototype, "daerah_tertinggi", void 0);
__decorate([
    (0, typeorm_1.ViewColumn)(),
    __metadata("design:type", String)
], Dashboard.prototype, "daerah_terendah", void 0);
Dashboard = __decorate([
    (0, typeorm_1.ViewEntity)({
        expression: `select
                (select count(id) from profil_pemda) as total_pemda,
                (select count(id) from government_innovations) as total_inovasi,
                (select count(id) from government_innovations gi where lower(gi.innovation_phase) = 'uji coba') as total_uji_coba,
                (select count(id) from government_innovations gi where lower(gi.innovation_phase) = 'penerapan') as total_penerapan,
                (select count(id) from government_innovations gi where lower(gi.innovation_phase) = 'inisiatif') as total_inisiatif,
                (select nama_daerah from (select pp.nama_daerah, count(gi.id) total_innovation
                    from government_innovations gi
                    left join profil_pemda pp on pp.id = gi.pemda_id
                    group by pp.nama_daerah
                    order by count(gi.id) desc
                    limit 1) as lowest) as daerah_tertinggi,
                (select nama_daerah from (select pp.nama_daerah, count(gi.id) total_innovation
                    from government_innovations gi
                    left join profil_pemda pp on pp.id = gi.pemda_id
                    group by pp.nama_daerah
                    order by count(gi.id) asc
                    limit 1) as lowest) as daerah_terendah`
    })
], Dashboard);
exports.Dashboard = Dashboard;
