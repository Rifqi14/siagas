"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.PemdaIndikator = void 0;
const lib_1 = require("../types/lib");
const typeorm_1 = require("typeorm");
const Indicator_entity_1 = __importDefault(require("./Indicator.entity"));
const PemdaIndikatorFile_entity_1 = require("./PemdaIndikatorFile.entity");
const ProfilePemda_entity_1 = require("./ProfilePemda.entity");
let PemdaIndikator = class PemdaIndikator extends lib_1.AppBaseEntity {
};
__decorate([
    (0, typeorm_1.PrimaryGeneratedColumn)(),
    __metadata("design:type", Number)
], PemdaIndikator.prototype, "id", void 0);
__decorate([
    (0, typeorm_1.Column)(),
    __metadata("design:type", Number)
], PemdaIndikator.prototype, "pemda_id", void 0);
__decorate([
    (0, typeorm_1.Column)(),
    __metadata("design:type", Number)
], PemdaIndikator.prototype, "indikator_id", void 0);
__decorate([
    (0, typeorm_1.Column)(),
    __metadata("design:type", String)
], PemdaIndikator.prototype, "informasi", void 0);
__decorate([
    (0, typeorm_1.Column)(),
    __metadata("design:type", String)
], PemdaIndikator.prototype, "keterangan", void 0);
__decorate([
    (0, typeorm_1.Column)(),
    __metadata("design:type", String)
], PemdaIndikator.prototype, "catatan", void 0);
__decorate([
    (0, typeorm_1.Column)(),
    __metadata("design:type", Number)
], PemdaIndikator.prototype, "skor", void 0);
__decorate([
    (0, typeorm_1.Column)(),
    __metadata("design:type", Number)
], PemdaIndikator.prototype, "nilai_sebelum", void 0);
__decorate([
    (0, typeorm_1.Column)(),
    __metadata("design:type", Number)
], PemdaIndikator.prototype, "nilai_sesudah", void 0);
__decorate([
    (0, typeorm_1.Column)(),
    __metadata("design:type", String)
], PemdaIndikator.prototype, "skor_verifikasi", void 0);
__decorate([
    (0, typeorm_1.Column)(),
    __metadata("design:type", String)
], PemdaIndikator.prototype, "data_saat_ini", void 0);
__decorate([
    (0, typeorm_1.CreateDateColumn)(),
    __metadata("design:type", Date)
], PemdaIndikator.prototype, "created_at", void 0);
__decorate([
    (0, typeorm_1.UpdateDateColumn)(),
    __metadata("design:type", Date)
], PemdaIndikator.prototype, "updated_at", void 0);
__decorate([
    (0, typeorm_1.ManyToOne)(() => ProfilePemda_entity_1.ProfilPemda),
    (0, typeorm_1.JoinColumn)({ name: 'pemda_id' }),
    __metadata("design:type", ProfilePemda_entity_1.ProfilPemda)
], PemdaIndikator.prototype, "pemda", void 0);
__decorate([
    (0, typeorm_1.ManyToOne)(() => Indicator_entity_1.default, { eager: true }),
    (0, typeorm_1.JoinColumn)({ name: 'indikator_id' }),
    __metadata("design:type", Indicator_entity_1.default)
], PemdaIndikator.prototype, "indicator", void 0);
__decorate([
    (0, typeorm_1.OneToMany)(() => PemdaIndikatorFile_entity_1.PemdaIndikatorFile, file => file.pemda_indikator, {
        eager: true
    }),
    __metadata("design:type", Array)
], PemdaIndikator.prototype, "files", void 0);
PemdaIndikator = __decorate([
    (0, typeorm_1.Entity)({ name: 'pemda_indikator' })
], PemdaIndikator);
exports.PemdaIndikator = PemdaIndikator;
