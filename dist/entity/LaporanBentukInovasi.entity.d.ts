export declare class LaporanBentukInovasi {
    pemda_id: number;
    bentuk_inovasi: string;
    total_disetujui: number;
    total_ditolak: number;
    total_keseluruhan: number;
}
