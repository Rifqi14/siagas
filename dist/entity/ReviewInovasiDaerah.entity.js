"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.ReviewInovasiDaerah = void 0;
const lib_1 = require("../types/lib");
const typeorm_1 = require("typeorm");
const GovernmentInnovation_entity_1 = __importDefault(require("./GovernmentInnovation.entity"));
const PreviewReviewInovasiDaerah_entity_1 = require("./PreviewReviewInovasiDaerah.entity");
let ReviewInovasiDaerah = class ReviewInovasiDaerah extends lib_1.AppBaseEntity {
};
__decorate([
    (0, typeorm_1.PrimaryGeneratedColumn)(),
    __metadata("design:type", Number)
], ReviewInovasiDaerah.prototype, "id", void 0);
__decorate([
    (0, typeorm_1.Column)('bigint'),
    __metadata("design:type", Number)
], ReviewInovasiDaerah.prototype, "random_number", void 0);
__decorate([
    (0, typeorm_1.Column)('float'),
    __metadata("design:type", Number)
], ReviewInovasiDaerah.prototype, "skor", void 0);
__decorate([
    (0, typeorm_1.Column)('varchar'),
    __metadata("design:type", String)
], ReviewInovasiDaerah.prototype, "status", void 0);
__decorate([
    (0, typeorm_1.Column)('bigint'),
    __metadata("design:type", Number)
], ReviewInovasiDaerah.prototype, "inovasi_id", void 0);
__decorate([
    (0, typeorm_1.ManyToOne)(() => GovernmentInnovation_entity_1.default),
    (0, typeorm_1.JoinColumn)({ name: 'inovasi_id' }),
    __metadata("design:type", GovernmentInnovation_entity_1.default)
], ReviewInovasiDaerah.prototype, "inovasi", void 0);
__decorate([
    (0, typeorm_1.OneToMany)(() => PreviewReviewInovasiDaerah_entity_1.PreviewReviewInovasiDaerah, preview => preview.review_inovasi_daerah),
    __metadata("design:type", Array)
], ReviewInovasiDaerah.prototype, "previews", void 0);
ReviewInovasiDaerah = __decorate([
    (0, typeorm_1.Entity)({ name: 'review_inovasi_daerah' })
], ReviewInovasiDaerah);
exports.ReviewInovasiDaerah = ReviewInovasiDaerah;
