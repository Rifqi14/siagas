"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.ProfilPemda = void 0;
const lib_1 = require("../types/lib");
const typeorm_1 = require("typeorm");
const File_entity_1 = __importDefault(require("./File.entity"));
const User_entity_1 = __importDefault(require("./User.entity"));
const PemdaIndikator_entity_1 = require("./PemdaIndikator.entity");
const GovernmentInnovation_entity_1 = __importDefault(require("./GovernmentInnovation.entity"));
let ProfilPemda = class ProfilPemda extends lib_1.AppBaseEntity {
};
__decorate([
    (0, typeorm_1.PrimaryGeneratedColumn)(),
    __metadata("design:type", Number)
], ProfilPemda.prototype, "id", void 0);
__decorate([
    (0, typeorm_1.Column)(),
    __metadata("design:type", String)
], ProfilPemda.prototype, "user_id", void 0);
__decorate([
    (0, typeorm_1.Column)(),
    __metadata("design:type", String)
], ProfilPemda.prototype, "nama_daerah", void 0);
__decorate([
    (0, typeorm_1.Column)(),
    __metadata("design:type", String)
], ProfilPemda.prototype, "opd_yang_menangani", void 0);
__decorate([
    (0, typeorm_1.Column)(),
    __metadata("design:type", String)
], ProfilPemda.prototype, "alamat_pemda", void 0);
__decorate([
    (0, typeorm_1.Column)(),
    __metadata("design:type", String)
], ProfilPemda.prototype, "email", void 0);
__decorate([
    (0, typeorm_1.Column)(),
    __metadata("design:type", String)
], ProfilPemda.prototype, "no_telpon", void 0);
__decorate([
    (0, typeorm_1.Column)(),
    __metadata("design:type", String)
], ProfilPemda.prototype, "nama_admin", void 0);
__decorate([
    (0, typeorm_1.Column)(),
    __metadata("design:type", String)
], ProfilPemda.prototype, "document_id", void 0);
__decorate([
    (0, typeorm_1.Column)(),
    __metadata("design:type", String)
], ProfilPemda.prototype, "nama_pemda", void 0);
__decorate([
    (0, typeorm_1.Column)(),
    __metadata("design:type", String)
], ProfilPemda.prototype, "nominator", void 0);
__decorate([
    (0, typeorm_1.Column)(),
    __metadata("design:type", String)
], ProfilPemda.prototype, "nominator_rekap_indeks_akhir", void 0);
__decorate([
    (0, typeorm_1.ManyToOne)(() => File_entity_1.default, { eager: true }),
    (0, typeorm_1.JoinColumn)({ name: 'document_id' }),
    __metadata("design:type", Object)
], ProfilPemda.prototype, "document", void 0);
__decorate([
    (0, typeorm_1.ManyToOne)(() => User_entity_1.default, { eager: true }),
    (0, typeorm_1.JoinColumn)({ name: 'user_id' }),
    __metadata("design:type", User_entity_1.default)
], ProfilPemda.prototype, "user", void 0);
__decorate([
    (0, typeorm_1.OneToMany)(() => PemdaIndikator_entity_1.PemdaIndikator, pemdaIndikator => pemdaIndikator.pemda, {
        eager: true,
        cascade: ['insert']
    }),
    __metadata("design:type", Array)
], ProfilPemda.prototype, "indicators", void 0);
__decorate([
    (0, typeorm_1.OneToMany)(() => GovernmentInnovation_entity_1.default, pemdaInovasi => pemdaInovasi.profilPemda),
    __metadata("design:type", Array)
], ProfilPemda.prototype, "innovations", void 0);
ProfilPemda = __decorate([
    (0, typeorm_1.Entity)({ name: 'profil_pemda' })
], ProfilPemda);
exports.ProfilPemda = ProfilPemda;
