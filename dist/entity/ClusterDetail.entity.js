"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const typeorm_1 = require("typeorm");
const Cluster_entity_1 = __importDefault(require("./Cluster.entity"));
const Region_entity_1 = __importDefault(require("./Region.entity"));
let ClusterDetail = class ClusterDetail extends typeorm_1.BaseEntity {
};
__decorate([
    (0, typeorm_1.PrimaryColumn)(),
    __metadata("design:type", Number)
], ClusterDetail.prototype, "cluster_id", void 0);
__decorate([
    (0, typeorm_1.PrimaryColumn)(),
    __metadata("design:type", Number)
], ClusterDetail.prototype, "region_id", void 0);
__decorate([
    (0, typeorm_1.CreateDateColumn)(),
    __metadata("design:type", Date)
], ClusterDetail.prototype, "created_at", void 0);
__decorate([
    (0, typeorm_1.UpdateDateColumn)(),
    __metadata("design:type", Date)
], ClusterDetail.prototype, "updated_at", void 0);
__decorate([
    (0, typeorm_1.ManyToOne)(() => Cluster_entity_1.default, cluster => cluster.details),
    (0, typeorm_1.JoinColumn)({ name: 'cluster_id' }),
    __metadata("design:type", Cluster_entity_1.default)
], ClusterDetail.prototype, "cluster", void 0);
__decorate([
    (0, typeorm_1.ManyToOne)(() => Region_entity_1.default, region => region.clusters),
    (0, typeorm_1.JoinColumn)({ name: 'region_id' }),
    __metadata("design:type", Region_entity_1.default)
], ClusterDetail.prototype, "region", void 0);
ClusterDetail = __decorate([
    (0, typeorm_1.Entity)({ name: 'cluster_details' })
], ClusterDetail);
exports.default = ClusterDetail;
