"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.DashboardArsip = void 0;
const typeorm_1 = require("typeorm");
let DashboardArsip = class DashboardArsip {
};
__decorate([
    (0, typeorm_1.ViewColumn)(),
    __metadata("design:type", Number)
], DashboardArsip.prototype, "id", void 0);
__decorate([
    (0, typeorm_1.ViewColumn)(),
    __metadata("design:type", Number)
], DashboardArsip.prototype, "pemda_id", void 0);
__decorate([
    (0, typeorm_1.ViewColumn)(),
    __metadata("design:type", String)
], DashboardArsip.prototype, "nama_daerah", void 0);
__decorate([
    (0, typeorm_1.ViewColumn)(),
    __metadata("design:type", String)
], DashboardArsip.prototype, "innovation_name", void 0);
__decorate([
    (0, typeorm_1.ViewColumn)(),
    __metadata("design:type", String)
], DashboardArsip.prototype, "innovation_phase", void 0);
__decorate([
    (0, typeorm_1.ViewColumn)(),
    __metadata("design:type", String)
], DashboardArsip.prototype, "trial_time", void 0);
__decorate([
    (0, typeorm_1.ViewColumn)(),
    __metadata("design:type", String)
], DashboardArsip.prototype, "implementation_time", void 0);
__decorate([
    (0, typeorm_1.ViewColumn)(),
    __metadata("design:type", Number)
], DashboardArsip.prototype, "skor", void 0);
__decorate([
    (0, typeorm_1.ViewColumn)(),
    __metadata("design:type", String)
], DashboardArsip.prototype, "created_by", void 0);
DashboardArsip = __decorate([
    (0, typeorm_1.ViewEntity)({
        expression: `select 
	gi.id,
  pp.nama_daerah,
  gi.innovation_name,
  gi.innovation_phase,
  gi.trial_time,
  gi.implementation_time,
  case
  	when lower(gi.innovation_phase) = 'inisiatif' then 3
    when lower(gi.innovation_phase) = 'uji coba' then 6
    when lower(gi.innovation_phase) = 'penerapan' then 9
  	else 0
  end as skor,
  gi.created_by
from government_innovations gi
left join profil_pemda pp on gi.pemda_id = pp.id`
    })
], DashboardArsip);
exports.DashboardArsip = DashboardArsip;
