import { NextFunction, Request, Response } from 'express';
export declare const moduleAccessUser: (req: Request, res: Response, next: NextFunction) => Promise<void>;
export declare const moduleAccessSuperAdmin: (req: Request, res: Response, next: NextFunction) => Promise<void>;
