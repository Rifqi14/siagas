import { NextFunction, Request, Response } from 'express';
import { TokenPayload } from '../schema/authentication.schema';
export declare const signJwt: (payload: TokenPayload, rememberMe: boolean) => string;
export declare const verifyJwt: (token: string) => TokenPayload | null;
export declare const authenticated: (req: Request, res: Response, next: NextFunction) => Promise<void>;
