"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.upload = exports.storage = exports.validateFile = void 0;
const multer_1 = __importDefault(require("multer"));
const api_error_1 = __importDefault(require("../providers/exceptions/api.error"));
const setting_repository_1 = __importDefault(require("../repository/setting.repository"));
const http_code_enum_1 = require("../types/http_code.enum");
const path_1 = require("path");
const validateFile = async (req, res, next) => {
    try {
        const file = req.file;
        if (!file) {
            throw new api_error_1.default({
                httpCode: http_code_enum_1.HttpCode.BAD_REQUEST,
                message: 'File not valid'
            });
        }
        const repo = new setting_repository_1.default();
        const ext = await repo.findOneByOrFail({ key: 'whitelisted_file' });
        const size = await repo.findOneByOrFail({ key: 'file_max_size' });
        const array_of_allowed_ext = ext.value.split(',');
        const file_extension = file.originalname.slice(((file.originalname.lastIndexOf('.') - 1) >>> 0) + 2);
        if (!array_of_allowed_ext.includes(file_extension)) {
            throw new api_error_1.default({
                httpCode: http_code_enum_1.HttpCode.BAD_REQUEST,
                message: 'Extension not valid'
            });
        }
        if (file.size / (1024 * 1024) > parseInt(size.value)) {
            throw new api_error_1.default({
                httpCode: http_code_enum_1.HttpCode.BAD_REQUEST,
                message: 'File too large'
            });
        }
        next();
    }
    catch (error) {
        next(error);
    }
};
exports.validateFile = validateFile;
const storage = (dest = 'public/tmp/') => {
    return multer_1.default.diskStorage({
        destination: dest,
        filename(req, file, callback) {
            callback(null, `${new Date().getTime().toString()}_${req.body.name
                ? `${req.body.name}${(0, path_1.extname)(file.originalname)}`
                : file.originalname.toLowerCase().replace(' ', '_')}`);
        }
    });
};
exports.storage = storage;
const upload = (dest) => {
    return (0, multer_1.default)({
        storage: dest,
        fileFilter: async (req, file, callback) => {
            const repo = new setting_repository_1.default();
            const ext = await repo.findOneByOrFail({ key: 'whitelisted_file' });
            const size = await repo.findOneByOrFail({ key: 'file_max_size' });
            const array_of_allowed_ext = ext.value.split(',');
            const file_extension = file.originalname.slice(((file.originalname.lastIndexOf('.') - 1) >>> 0) + 2);
            if (!array_of_allowed_ext.includes(`.${file_extension}`)) {
                return callback(new api_error_1.default({
                    httpCode: http_code_enum_1.HttpCode.BAD_REQUEST,
                    message: 'Extension not valid'
                }));
            }
            callback(null, true);
        }
    });
};
exports.upload = upload;
