"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.authenticated = exports.verifyJwt = exports.signJwt = void 0;
const fs_1 = require("fs");
const jsonwebtoken_1 = require("jsonwebtoken");
const config_1 = __importDefault(require("../providers/config"));
const api_error_1 = __importDefault(require("../providers/exceptions/api.error"));
const user_service_1 = __importDefault(require("../service/user.service"));
const http_code_enum_1 = require("../types/http_code.enum");
const signJwt = (payload, rememberMe) => {
    const privateKey = (0, fs_1.readFileSync)(config_1.default.load().private_key_location);
    const options = {
        algorithm: 'RS256'
    };
    if (!rememberMe) {
        options.expiresIn = `${config_1.default.load().token_expired_in_day}d`;
    }
    const jwt = (0, jsonwebtoken_1.sign)(payload, privateKey, options);
    return jwt;
};
exports.signJwt = signJwt;
const verifyJwt = (token) => {
    try {
        const publicKey = (0, fs_1.readFileSync)(config_1.default.load().public_key_location).toString('ascii');
        return (0, jsonwebtoken_1.verify)(token, publicKey);
    }
    catch (error) {
        return null;
    }
};
exports.verifyJwt = verifyJwt;
const authenticated = async (req, res, next) => {
    try {
        if (req.headers.authorization &&
            req.headers.authorization.includes('creator')) {
            const userService = new user_service_1.default();
            const user = (await userService.detail(1));
            if (!user) {
                return next(new api_error_1.default({
                    httpCode: http_code_enum_1.HttpCode.UNAUTHORIZED,
                    message: 'Unauthorized'
                }));
            }
            res.locals.user = user;
            return next();
        }
        let access_token;
        if (req.headers.authorization &&
            req.headers.authorization.startsWith('Bearer')) {
            access_token = req.headers.authorization.split(' ')[1];
        }
        if (!access_token) {
            return next(new api_error_1.default({
                httpCode: http_code_enum_1.HttpCode.UNAUTHORIZED,
                message: 'Unauthorized'
            }));
        }
        const decoded = (0, exports.verifyJwt)(access_token);
        if (!decoded) {
            console.log('Invalid');
            return next(new api_error_1.default({
                httpCode: http_code_enum_1.HttpCode.UNAUTHORIZED,
                message: 'Unauthorized'
            }));
        }
        const userService = new user_service_1.default();
        const user = (await userService.detail(decoded.user_id));
        if (!user) {
            return next(new api_error_1.default({
                httpCode: http_code_enum_1.HttpCode.UNAUTHORIZED,
                message: 'Unauthorized'
            }));
        }
        res.locals.user = user;
        next();
    }
    catch (error) {
        next(error);
    }
};
exports.authenticated = authenticated;
