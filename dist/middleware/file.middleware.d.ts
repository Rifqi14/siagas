import { NextFunction, Request, Response } from 'express';
import multer from 'multer';
export declare const validateFile: (req: Request, res: Response, next: NextFunction) => Promise<void>;
export declare const storage: (dest?: string) => multer.StorageEngine;
export declare const upload: (dest: multer.StorageEngine) => multer.Multer;
