"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.moduleAccessSuperAdmin = exports.moduleAccessUser = void 0;
const api_error_1 = __importDefault(require("../providers/exceptions/api.error"));
const http_code_enum_1 = require("../types/http_code.enum");
const moduleAccessUser = async (req, res, next) => {
    try {
        const { role } = res.locals.user;
        if (role.is_super_admin != 't') {
            return next(new api_error_1.default({
                httpCode: http_code_enum_1.HttpCode.UNAUTHORIZED,
                message: "Your role don't have permission to access this endpoint."
            }));
        }
        next();
    }
    catch (error) {
        next(error);
    }
};
exports.moduleAccessUser = moduleAccessUser;
const moduleAccessSuperAdmin = async (req, res, next) => {
    try {
        const { role } = res.locals.user;
        if (role.is_super_admin != 'y') {
            return next(new api_error_1.default({
                httpCode: http_code_enum_1.HttpCode.UNAUTHORIZED,
                message: "Your role don't have permission to access this endpoint."
            }));
        }
        next();
    }
    catch (error) {
        next(error);
    }
};
exports.moduleAccessSuperAdmin = moduleAccessSuperAdmin;
