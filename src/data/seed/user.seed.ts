import { DataSource } from 'typeorm';
import Role from '../../entity/Role.entity';
import User from '../../entity/User.entity';
import users from '../seeder/user.json';

export const userSeed = async (ds: DataSource): Promise<void> => {
  const repo = ds.getRepository(User);
  const roleRepo = ds.getRepository(Role);
  const entities: User[] = [];

  for (let i = 0; i < users.length; i++) {
    const { email, nama_lengkap, nama_panggilan, password, role, username } =
      users[i];

    const entity = await repo.findOneBy({ username });
    if (!entity) {
      const data = repo.create({
        email,
        full_name: nama_lengkap,
        nickname: nama_panggilan,
        password,
        username
      });
      const roleEntity = await roleRepo.findOneBy({ name: role });
      if (roleEntity) {
        data.role = roleEntity;
      }
      entities.push(data);
    }
  }

  await repo.insert(entities).then(
    () => console.log(`Upsert user data`),
    err => console.log(`Error upsert data: ${err}`)
  );
};
