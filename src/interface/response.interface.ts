import { Response } from 'express';
import { ZodError } from 'zod';

export interface IResponse<T, P> {
  success(data: T, code: number, message: string): this;
  pagination(data: T, pagination: P, code: number, message: string): this;
  error(error: Error, code: number, message: string): this;
  validation(error: ZodError, code: number, message: string): this;
  create(res: Response): void;
}
