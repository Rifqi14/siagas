import { NextFunction, Request, Response } from 'express';
import { ParamsDictionary } from 'express-serve-static-core';
import { ParsedQs } from 'qs';
import TimPenilaianService from 'src/service/tim_penilaian.service';
import BaseController from './base.controller';
import { IBaseController } from './interface/ibase.controller';
import { SuccessResponse } from 'src/types/response.type';
import { TimPenilaian } from 'src/entity/TimPenilaian.entity';
import {
  CreateTimPenilaianRequest,
  ListTimPenilaian,
  ListTimPenilaianParams
} from 'src/schema/tim_penilaian.schema';
import response from 'src/providers/response';
import { PaginationData } from 'src/schema/base.schema';
import ApiError from 'src/providers/exceptions/api.error';
import { HttpCode } from 'src/types/http_code.enum';

class TimPenilaianController
  extends BaseController<TimPenilaianService>
  implements IBaseController
{
  constructor() {
    super(new TimPenilaianService());
  }

  create = async (
    req: Request<
      ParamsDictionary,
      any,
      CreateTimPenilaianRequest,
      ParsedQs,
      Record<string, any>
    >,
    res: Response<SuccessResponse<TimPenilaian>, Record<string, any>>,
    next: NextFunction
  ): Promise<void> => {
    try {
      const { username } = res.locals.user;
      const result = await this.service.create(req.body, username);
      response.success(result).create(res);
    } catch (error) {
      next(error);
    }
  };

  get = async (
    req: Request<
      ParamsDictionary,
      any,
      any,
      ParsedQs & ListTimPenilaianParams,
      Record<string, any>
    >,
    res: Response<any, Record<string, any>>,
    next: NextFunction
  ): Promise<void> => {
    try {
      const { username } = res.locals.user;
      const url = req.protocol + '://' + req.get('host') + req.originalUrl;
      const result = (await this.service.get(
        url,
        username,
        req.query
      )) as PaginationData<ListTimPenilaian>;
      response.pagination(result.res, result.paging).create(res);
    } catch (error) {
      next(error);
    }
  };

  detail = async (
    req: Request<
      ParamsDictionary & { id: number },
      any,
      any,
      ParsedQs,
      Record<string, any>
    >,
    res: Response<any, Record<string, any>>,
    next: NextFunction
  ): Promise<void> => {
    try {
      const { id } = req.params;

      const result = await this.service.detail(id);
      response.success(result).create(res);
    } catch (error) {
      next(error);
    }
  };

  update = async (
    req: Request<
      ParamsDictionary & { id: number },
      any,
      CreateTimPenilaianRequest,
      ParsedQs,
      Record<string, any>
    >,
    res: Response<any, Record<string, any>>,
    next: NextFunction
  ): Promise<void> => {
    try {
      const { id } = req.params;
      const request = req.body;
      const { username } = res.locals.user;

      const result = await this.service.update(id, request, username);
      if (!result) {
        throw new ApiError({
          httpCode: HttpCode.BAD_REQUEST,
          message: 'Gagal ubah data'
        });
      }
      response.success('Berhasil ubah data').create(res);
    } catch (error) {
      next(error);
    }
  };

  delete = async (
    req: Request<
      ParamsDictionary & { id: number },
      any,
      any,
      ParsedQs,
      Record<string, any>
    >,
    res: Response<any, Record<string, any>>,
    next: NextFunction
  ): Promise<void> => {
    try {
      const { id } = req.params;

      const result = await this.service.delete(id);
      if (!result) {
        throw new ApiError({
          httpCode: HttpCode.BAD_REQUEST,
          message: 'Gagal hapus data'
        });
      }
      response.success('Berhasil hapus data').create(res);
    } catch (error) {
      next(error);
    }
  };
}

export default TimPenilaianController;
