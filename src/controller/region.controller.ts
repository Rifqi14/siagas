import { Request, Response, NextFunction } from 'express';
import { ParamsDictionary } from 'express-serve-static-core';
import { ParsedQs } from 'qs';
import Regions from '../entity/Region.entity';
import { CreateRegionRequest, GetRegionRequest } from '../schema/region.schema';
import { SuccessResponse } from '../types/response.type';
import RegionService from '../service/region.service';
import BaseController from './base.controller';
import { IBaseController } from './interface/ibase.controller';
import response from '../providers/response';
import { PaginationResponse } from '../interface/pagination.interface';
import ApiError from '../providers/exceptions/api.error';
import { HttpCode } from '../types/http_code.enum';

class RegionController
  extends BaseController<RegionService>
  implements IBaseController
{
  constructor() {
    const service = new RegionService();
    super(service);
  }

  create = async (
    req: Request<
      ParamsDictionary,
      SuccessResponse<Regions>,
      CreateRegionRequest,
      ParsedQs,
      Record<string, any>
    >,
    res: Response<any, Record<string, any>>,
    next: NextFunction
  ): Promise<void> => {
    try {
      const result = await this.service.create(req.body);
      response.success(result).create(res);
    } catch (error) {
      next(error);
    }
  };
  get = async (
    req: Request<
      ParamsDictionary,
      any,
      any,
      ParsedQs & GetRegionRequest,
      Record<string, any>
    >,
    res: Response<any, Record<string, any>>,
    next: NextFunction
  ): Promise<void> => {
    try {
      const url = req.protocol + '://' + req.get('host') + req.originalUrl;
      const result = (await this.service.get(url, req.query)) as {
        paging: PaginationResponse;
        res: Regions[];
      };
      response.pagination(result.res, result.paging).create(res);
    } catch (error) {
      next(error);
    }
  };
  detail = async (
    req: Request<
      ParamsDictionary & { id: number },
      any,
      any,
      ParsedQs,
      Record<string, any>
    >,
    res: Response<any, Record<string, any>>,
    next: NextFunction
  ): Promise<void> => {
    try {
      const { id } = req.params;

      const result = await this.service.detail(id);
      response.success(result).create(res);
    } catch (error) {
      next(error);
    }
  };
  update = async (
    req: Request<
      ParamsDictionary & { id: number },
      any,
      CreateRegionRequest,
      ParsedQs,
      Record<string, any>
    >,
    res: Response<any, Record<string, any>>,
    next: NextFunction
  ): Promise<void> => {
    try {
      const { id } = req.params;
      const request = req.body;

      const result = await this.service.update(id, request);
      if (!result) {
        throw new ApiError({
          httpCode: HttpCode.BAD_REQUEST,
          message: 'Gagal ubah data'
        });
      }
      response.success('Berhasil ubah data').create(res);
    } catch (error) {
      next(error);
    }
  };
  delete = async (
    req: Request<
      ParamsDictionary & { id: number },
      any,
      any,
      ParsedQs,
      Record<string, any>
    >,
    res: Response<any, Record<string, any>>,
    next: NextFunction
  ): Promise<void> => {
    try {
      const { id } = req.params;

      const result = await this.service.delete(id);
      if (!result) {
        throw new ApiError({
          httpCode: HttpCode.BAD_REQUEST,
          message: 'Gagal hapus data'
        });
      }
      response.success('Berhasil hapus data').create(res);
    } catch (error) {
      next(error);
    }
  };
}

export default RegionController;
