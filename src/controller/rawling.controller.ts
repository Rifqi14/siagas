import RawlingService from 'src/service/rawling.service';
import BaseController from './base.controller';
import { Request, Response, NextFunction } from 'express';
import { ParamsDictionary } from 'express-serve-static-core';
import { ParsedQs } from 'qs';
import { RawlingParams } from 'src/schema/rawling.schema';
import response from 'src/providers/response';

export class RawlingController extends BaseController<RawlingService> {
  constructor() {
    super(new RawlingService());
  }

  rawling = async (
    req: Request<
      ParamsDictionary,
      any,
      any,
      ParsedQs & RawlingParams,
      Record<string, any>
    >,
    res: Response<any, Record<string, any>>,
    next: NextFunction
  ): Promise<void> => {
    try {
      const result = await this.service.rawling(req.query);
      response.success(result).create(res);
    } catch (error) {
      next(error);
    }
  };
}
