import { NextFunction, Request, Response } from 'express';
import { ParamsDictionary } from 'express-serve-static-core';
import { ParsedQs } from 'qs';
import RegionalApparatus from '../entity/RegionApparatus.entity';
import ApiError from '../providers/exceptions/api.error';
import response from '../providers/response';
import { PaginationData } from '../schema/base.schema';
import {
  CreateRegionApparatusRequest,
  GetRegionApparatusRequest
} from '../schema/region_apparatus.scheme';
import RegionalApparatusService from '../service/regional_apparatus.service';
import { HttpCode } from '../types/http_code.enum';
import { SuccessResponse } from '../types/response.type';
import BaseController from './base.controller';
import { IBaseController } from './interface/ibase.controller';

class RegionalApparatusController
  extends BaseController<RegionalApparatusService>
  implements IBaseController
{
  constructor() {
    super(new RegionalApparatusService());
  }

  create = async (
    req: Request<
      ParamsDictionary,
      any,
      CreateRegionApparatusRequest,
      ParsedQs,
      Record<string, any>
    >,
    res: Response<SuccessResponse<RegionalApparatus>, Record<string, any>>,
    next: NextFunction
  ): Promise<void> => {
    try {
      const { username } = res.locals.user;
      const result = await this.service.create(req.body, username);
      response.success(result).create(res);
    } catch (error) {
      next(error);
    }
  };

  get = async (
    req: Request<
      ParamsDictionary,
      any,
      any,
      ParsedQs & GetRegionApparatusRequest,
      Record<string, any>
    >,
    res: Response<any, Record<string, any>>,
    next: NextFunction
  ): Promise<void> => {
    try {
      const url = req.protocol + '://' + req.get('host') + req.originalUrl;
      const result = (await this.service.get(url, req.query)) as PaginationData<
        RegionalApparatus[]
      >;
      response.pagination(result.res, result.paging).create(res);
    } catch (error) {
      next(error);
    }
  };

  detail = async (
    req: Request<
      ParamsDictionary & { id: number },
      any,
      any,
      ParsedQs,
      Record<string, any>
    >,
    res: Response<any, Record<string, any>>,
    next: NextFunction
  ): Promise<void> => {
    try {
      const { id } = req.params;

      const result = await this.service.detail(id);
      response.success(result).create(res);
    } catch (error) {
      next(error);
    }
  };

  update = async (
    req: Request<
      ParamsDictionary & { id: number },
      any,
      CreateRegionApparatusRequest,
      ParsedQs,
      Record<string, any>
    >,
    res: Response<any, Record<string, any>>,
    next: NextFunction
  ): Promise<void> => {
    try {
      const { id } = req.params;
      const request = req.body;
      const { username } = res.locals.user;

      const result = await this.service.update(id, request, username);
      if (!result) {
        throw new ApiError({
          httpCode: HttpCode.BAD_REQUEST,
          message: 'Gagal ubah data'
        });
      }
      response.success('Berhasil ubah data').create(res);
    } catch (error) {
      next(error);
    }
  };

  delete = async (
    req: Request<
      ParamsDictionary & { id: number },
      any,
      any,
      ParsedQs,
      Record<string, any>
    >,
    res: Response<any, Record<string, any>>,
    next: NextFunction
  ): Promise<void> => {
    try {
      const { id } = req.params;

      const result = await this.service.delete(id);
      if (!result) {
        throw new ApiError({
          httpCode: HttpCode.BAD_REQUEST,
          message: 'Gagal hapus data'
        });
      }
      response.success('Berhasil hapus data').create(res);
    } catch (error) {
      next(error);
    }
  };
}

export default RegionalApparatusController;
