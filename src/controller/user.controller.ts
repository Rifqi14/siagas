import { Request, Response, NextFunction } from 'express';
import { ParamsDictionary } from 'express-serve-static-core';
import { ParsedQs } from 'qs';
import User from '../entity/User.entity';
import response from '../providers/response';
import { CreateUserRequest } from '../schema/user.schema';
import UserService from '../service/user.service';
import { SuccessResponse } from '../types/response.type';
import BaseController from './base.controller';
import { IBaseController } from './interface/ibase.controller';
import { PaginationData } from '../schema/base.schema';

class UserController extends BaseController<UserService> implements IBaseController {
	constructor() {
		super(new UserService());
	}

	create = async (
		req: Request<ParamsDictionary, any, CreateUserRequest, ParsedQs, Record<string, any>>,
		res: Response<SuccessResponse<User>, Record<string, any>>,
		next: NextFunction
	): Promise<void> => {
		try {
			const result = await this.service.create(req.body);
			response.success(result).create(res);
		} catch (error) {
			next(error);
		}
	};

	get = async (
		req: Request<ParamsDictionary, any, any, ParsedQs, Record<string, any>>,
		res: Response<any, Record<string, any>>,
		next: NextFunction
	): Promise<void> => {
		try {
			const { username } = res.locals.user;
			const url = req.protocol + '://' + req.get('host') + req.originalUrl;
			const result = (await this.service.findAll(req.query, url, username)) as PaginationData<User[]>;
			response.pagination(result.res, result.paging).create(res);
		} catch (error) {
			next(error);
		}
	};

	detail = async (
		req: Request<ParamsDictionary & { id: number }, any, any, ParsedQs, Record<string, any>>,
		res: Response<SuccessResponse<User>, Record<string, any>>,
		next: NextFunction
	): Promise<void> => {
		try {
			const result = await this.service.detail(req.params.id);
			response.success(result).create(res);
		} catch (error) {
			next(error);
		}
	};

	update = async (
		req: Request<ParamsDictionary & { id: string }, any, CreateUserRequest, ParsedQs, Record<string, any>>,
		res: Response<any, Record<string, any>>,
		next: NextFunction
	): Promise<void> => {
		try {
			const { id } = req.params;
			const result = await this.service.update(+id, req.body);
			response.success(result).create(res);
		} catch (error) {
			next(error);
		}
	};

	delete = async (
		req: Request<ParamsDictionary & { id: string }, any, any, ParsedQs, Record<string, any>>,
		res: Response<any, Record<string, any>>,
		next: NextFunction
	): Promise<void> => {
		try {
			const result = await this.service.delete(+req.params.id);
			response.success(result).create(res);
		} catch (error) {
			next(error);
		}
	};
}

export default UserController;
