import { NextFunction, Request, Response } from 'express';
import { ParamsDictionary } from 'express-serve-static-core';
import { ParsedQs } from 'qs';
import { ReviewInovasiDaerah } from 'src/entity/ReviewInovasiDaerah.entity';
import ApiError from 'src/providers/exceptions/api.error';
import response from 'src/providers/response';
import { PaginationData } from 'src/schema/base.schema';
import {
	CreateReviewInovasiDaerahRequest,
	GetReviewInovasiRequest,
	ReviewInovasiDaerahResponse,
	UpdateEvaluasiRequest,
} from 'src/schema/review_inovasi_daerah.schema';
import ReviewInovasiDaerahService from 'src/service/review_inovasi_daerah.service';
import { HttpCode } from 'src/types/http_code.enum';
import { SuccessResponse } from 'src/types/response.type';
import BaseController from './base.controller';
import { IBaseController } from './interface/ibase.controller';
import { DownloadType } from 'src/types/lib';
import { unlinkSync } from 'fs';

export class ReviewInovasiDaerahController extends BaseController<ReviewInovasiDaerahService> implements IBaseController {
	constructor() {
		super(new ReviewInovasiDaerahService());
	}

	create = async (
		req: Request<ParamsDictionary, any, CreateReviewInovasiDaerahRequest, ParsedQs, Record<string, any>>,
		res: Response<SuccessResponse<ReviewInovasiDaerah>, Record<string, any>>,
		next: NextFunction
	): Promise<void> => {
		try {
			const { username } = res.locals.user;
			const result = await this.service.create(username, req.body);
			response.success(result).create(res);
		} catch (error) {
			next(error);
		}
	};

	get = async (
		req: Request<ParamsDictionary, any, any, ParsedQs & GetReviewInovasiRequest, Record<string, any>>,
		res: Response<any, Record<string, any>>,
		next: NextFunction
	): Promise<void> => {
		try {
			const { username } = res.locals.user;
			const url = req.protocol + '://' + req.get('host') + req.originalUrl;
			const result = (await this.service.get(url, username, req.query)) as PaginationData<ReviewInovasiDaerahResponse[]>;
			response.pagination(result.res, result.paging).create(res);
		} catch (error) {
			next(error);
		}
	};

	getDownload = async (
		req: Request<ParamsDictionary & { type: DownloadType }, any, any, ParsedQs & GetReviewInovasiRequest, Record<string, any>>,
		res: Response<any, Record<string, any>>,
		next: NextFunction
	): Promise<void> => {
		try {
			const { username } = res.locals.user;
			const result = await this.service.download(req.params.type, username, req.query);
			res.download(result, err => !err && unlinkSync(result));
		} catch (error) {
			next(error);
		}
	};

	detail = async (
		req: Request<ParamsDictionary & { id: number }, any, any, ParsedQs, Record<string, any>>,
		res: Response<any, Record<string, any>>,
		next: NextFunction
	): Promise<void> => {
		try {
			const { id } = req.params;

			const result = await this.service.detail(id);
			response.success(result).create(res);
		} catch (error) {
			next(error);
		}
	};

	profilInovasi = async (
		req: Request<ParamsDictionary & { id: number }, any, any, ParsedQs, Record<string, any>>,
		res: Response<any, Record<string, any>>,
		next: NextFunction
	): Promise<void> => {
		try {
			const { id } = req.params;

			const result = await this.service.getProfilInovasi(id);
			response.success(result).create(res);
		} catch (error) {
			next(error);
		}
	};

	indikator = async (
		req: Request<ParamsDictionary & { id: number }, any, any, ParsedQs, Record<string, any>>,
		res: Response<any, Record<string, any>>,
		next: NextFunction
	): Promise<void> => {
		try {
			const { id } = req.params;

			const result = await this.service.getIndikator(id);
			response.success(result).create(res);
		} catch (error) {
			next(error);
		}
	};

	indikatorDetail = async (
		req: Request<ParamsDictionary & { id: number; indikator_id: string }, any, any, ParsedQs, Record<string, any>>,
		res: Response<any, Record<string, any>>,
		next: NextFunction
	): Promise<void> => {
		try {
			const { id, indikator_id } = req.params;

			const result = await this.service.getIndikatorDetail(id, indikator_id);
			response.success(result).create(res);
		} catch (error) {
			next(error);
		}
	};

	evaluasi = async (
		req: Request<ParamsDictionary & { id: number; indikator_id: string }, any, UpdateEvaluasiRequest, ParsedQs, Record<string, any>>,
		res: Response<any, Record<string, any>>,
		next: NextFunction
	): Promise<void> => {
		try {
			const { id, indikator_id } = req.params;

			const result = await this.service.updateEvaluasi(id, indikator_id, res.locals.user.username, req.body);
			response.success(result).create(res);
		} catch (error) {
			next(error);
		}
	};

	getEvaluasi = async (
		req: Request<ParamsDictionary & { id: number; indikator_id: number }, any, UpdateEvaluasiRequest, ParsedQs, Record<string, any>>,
		res: Response<any, Record<string, any>>,
		next: NextFunction
	): Promise<void> => {
		try {
			const { id, indikator_id } = req.params;

			const result = await this.service.getEvaluasi(id, indikator_id, res.locals.user.username);
			response.success(result).create(res);
		} catch (error) {
			next(error);
		}
	};

	update = async (
		req: Request<ParamsDictionary & { id: number }, any, CreateReviewInovasiDaerahRequest, ParsedQs, Record<string, any>>,
		res: Response<any, Record<string, any>>,
		next: NextFunction
	): Promise<void> => {
		try {
			const { id } = req.params;
			const { username } = res.locals.user;
			const result = await this.service.update(id, username, req.body);
			if (!result) {
				throw new ApiError({
					httpCode: HttpCode.BAD_REQUEST,
					message: 'Gagal ubah data',
				});
			}
			response.success('Berhasil ubah data').create(res);
		} catch (error) {
			next(error);
		}
	};

	delete = async (
		req: Request<ParamsDictionary & { id: number }, any, any, ParsedQs, Record<string, any>>,
		res: Response<any, Record<string, any>>,
		next: NextFunction
	): Promise<void> => {
		try {
			const { id } = req.params;

			const result = await this.service.delete(id);
			if (!result) {
				throw new ApiError({
					httpCode: HttpCode.BAD_REQUEST,
					message: 'Gagal hapus data',
				});
			}
			response.success('Berhasil hapus data').create(res);
		} catch (error) {
			next(error);
		}
	};
}
