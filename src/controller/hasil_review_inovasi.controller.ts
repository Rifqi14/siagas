import HasilReviewInovasiService from "src/service/hasil_review_inovasi.service";
import BaseController from "./base.controller";
import { IBaseController } from "./interface/ibase.controller";
import { Request, Response, NextFunction } from "express";
import { ParamsDictionary } from "express-serve-static-core";
import { ParsedQs } from "qs";
import ApiError from "src/providers/exceptions/api.error";
import { HttpCode } from "src/types/http_code.enum";
import response from "src/providers/response";
import { HasilReviewParams } from "src/schema/hasil_review_inovasi.schema";
import { PaginationData } from "src/schema/base.schema";
import { DownloadType } from "src/types/lib";
import { unlinkSync } from "fs";

export class HasilReviewInovasiController extends BaseController<HasilReviewInovasiService> implements IBaseController {
	constructor() {
		super(new HasilReviewInovasiService());
	}

	create = async (
		req: Request<ParamsDictionary, any, any, ParsedQs, Record<string, any>>,
		res: Response<any, Record<string, any>>,
		next: NextFunction
	): Promise<void> => {
		throw new Error("Method not implemented.");
	};

	get = async (
		req: Request<ParamsDictionary, any, any, ParsedQs & HasilReviewParams, Record<string, any>>,
		res: Response<any, Record<string, any>>,
		next: NextFunction
	): Promise<void> => {
		try {
			const { username } = res.locals.user;
			const url = req.protocol + "://" + req.get("host") + req.originalUrl;
			const result = (await this.service.get(url, username, req.query)) as PaginationData<HasilReviewParams[]>;
			response.pagination(result.res, result.paging).create(res);
		} catch (error) {
			next(error);
		}
	};
	detail = async (
		req: Request<ParamsDictionary & { review_inovasi_id: number }, any, any, ParsedQs, Record<string, any>>,
		res: Response<any, Record<string, any>>,
		next: NextFunction
	): Promise<void> => {
		try {
			const { review_inovasi_id } = req.params;

			const result = await this.service.detail(review_inovasi_id);
			response.success(result).create(res);
		} catch (error) {
			next(error);
		}
	};
	update = async (
		req: Request<ParamsDictionary, any, any, ParsedQs, Record<string, any>>,
		res: Response<any, Record<string, any>>,
		next: NextFunction
	): Promise<void> => {
		throw new Error("Method not implemented.");
	};
	delete = async (
		req: Request<ParamsDictionary & { review_inovasi_id: number }, any, any, ParsedQs, Record<string, any>>,
		res: Response<any, Record<string, any>>,
		next: NextFunction
	): Promise<void> => {
		try {
			const { review_inovasi_id } = req.params;

			const result = await this.service.delete(review_inovasi_id);
			if (!result) {
				throw new ApiError({
					httpCode: HttpCode.BAD_REQUEST,
					message: "Gagal hapus data",
				});
			}
			response.success("Berhasil hapus data").create(res);
		} catch (error) {
			next(error);
		}
	};

	getDownload = async (
		req: Request<ParamsDictionary & { type: DownloadType }, any, any, ParsedQs & HasilReviewParams, Record<string, any>>,
		res: Response<any, Record<string, any>>,
		next: NextFunction
	): Promise<void> => {
		try {
			const { username } = res.locals.user;
			const result = await this.service.download(req.params.type, username, req.query);
			res.download(result, err => !err && unlinkSync(result));
		} catch (error) {
			next(error);
		}
	};
}
