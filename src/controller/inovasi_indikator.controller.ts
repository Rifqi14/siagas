import { NextFunction, Request, Response } from 'express';
import { ParamsDictionary } from 'express-serve-static-core';
import { ParsedQs } from 'qs';
import InovasiIndikatorService from 'src/service/inovasi_indikator.service';
import BaseController from './base.controller';
import { IBaseController } from './interface/ibase.controller';
import ApiError from 'src/providers/exceptions/api.error';
import { HttpCode } from 'src/types/http_code.enum';
import {
  InovasiIndikatorParams,
  ListInovasiIndikator
} from 'src/schema/inovasi_indikator.schema';
import { PaginationData } from 'src/schema/base.schema';
import response from 'src/providers/response';

class InovasiIndikatorController
  extends BaseController<InovasiIndikatorService>
  implements IBaseController
{
  constructor() {
    super(new InovasiIndikatorService());
  }

  create = async (
    req: Request<ParamsDictionary, any, any, ParsedQs, Record<string, any>>,
    res: Response<any, Record<string, any>>,
    next: NextFunction
  ): Promise<void> => {
    throw new ApiError({
      httpCode: HttpCode.NOT_FOUND,
      message: 'Method not implemented'
    });
  };

  get = async (
    req: Request<
      ParamsDictionary & { pemda_id: string },
      any,
      any,
      ParsedQs & InovasiIndikatorParams,
      Record<string, any>
    >,
    res: Response<any, Record<string, any>>,
    next: NextFunction
  ): Promise<void> => {
    try {
      const { username } = res.locals.user;
      const url = req.protocol + '://' + req.get('host') + req.originalUrl;
      const result = (await this.service.get(
        req.params.pemda_id,
        url,
        username,
        req.query
      )) as PaginationData<ListInovasiIndikator>;
      response.pagination(result.res, result.paging).create(res);
    } catch (error) {
      next(error);
    }
  };

  detail = async (
    req: Request<
      ParamsDictionary & { inovasi_id: number; indikator_id: number },
      any,
      any,
      ParsedQs,
      Record<string, any>
    >,
    res: Response<any, Record<string, any>>,
    next: NextFunction
  ): Promise<void> => {
    try {
      const { inovasi_id, indikator_id } = req.params;

      const result = await this.service.detail(inovasi_id, indikator_id);
      response.success(result).create(res);
    } catch (error) {
      next(error);
    }
  };

  update = async (
    req: Request<
      ParamsDictionary & { id: number },
      any,
      any,
      ParsedQs,
      Record<string, any>
    >,
    res: Response<any, Record<string, any>>,
    next: NextFunction
  ): Promise<void> => {
    throw new ApiError({
      httpCode: HttpCode.NOT_FOUND,
      message: 'Method not implemented'
    });
  };

  delete = async (
    req: Request<
      ParamsDictionary & { id: number },
      any,
      any,
      ParsedQs,
      Record<string, any>
    >,
    res: Response<any, Record<string, any>>,
    next: NextFunction
  ): Promise<void> => {
    throw new ApiError({
      httpCode: HttpCode.NOT_FOUND,
      message: 'Method not implemented'
    });
  };
}

export default InovasiIndikatorController;
