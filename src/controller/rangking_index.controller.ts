import RangkingIndexService from "src/service/rangking_index.service";
import BaseController from "./base.controller";
import { Request, Response, NextFunction } from "express";
import { ParamsDictionary } from "express-serve-static-core";
import { ParsedQs } from "qs";
import { RangkingIndexParams, RangkingIndexParamsDownload, UpdateNominatorRequest } from "src/schema/rangking_index.schema";
import { RangkingIndex } from "src/entity/RangkingIndex.entity";
import { PaginationData } from "src/schema/base.schema";
import response from "src/providers/response";
import ApiError from "src/providers/exceptions/api.error";
import { HttpCode } from "src/types/http_code.enum";
import { DownloadType } from "src/types/lib";
import { unlinkSync } from "fs";

export class RangkingIndexController extends BaseController<RangkingIndexService> {
	constructor() {
		super(new RangkingIndexService());
	}

	get = async (
		req: Request<ParamsDictionary, any, any, ParsedQs & RangkingIndexParams, Record<string, any>>,
		res: Response<any, Record<string, any>>,
		next: NextFunction
	): Promise<void> => {
		try {
			const { username } = res.locals.user;
			const url = req.protocol + "://" + req.get("host") + req.originalUrl;
			const result = (await this.service.get(url, username, req.query)) as PaginationData<RangkingIndex[]>;
			response.pagination(result.res, result.paging).create(res);
		} catch (error) {
			next(error);
		}
	};

	update = async (
		req: Request<ParamsDictionary & { id: number }, UpdateNominatorRequest, any, ParsedQs, Record<string, any>>,
		res: Response<any, Record<string, any>>,
		next: NextFunction
	): Promise<void> => {
		try {
			const { id } = req.params;

			const result = await this.service.nominator(id, req.body);
			if (!result) {
				throw new ApiError({
					httpCode: HttpCode.BAD_REQUEST,
					message: "Gagal ubah data",
				});
			}
			response.success("Berhasil ubah data").create(res);
		} catch (error) {
			next(error);
		}
	};

	getDownload = async (
		req: Request<ParamsDictionary & { type: DownloadType }, any, any, ParsedQs & RangkingIndexParamsDownload, Record<string, any>>,
		res: Response<any, Record<string, any>>,
		next: NextFunction
	): Promise<void> => {
		try {
			const { username } = res.locals.user;
			const result = await this.service.download(req.params.type, username, req.query);
			res.download(result, err => !err && unlinkSync(result));
		} catch (error) {
			next(error);
		}
	};
}
