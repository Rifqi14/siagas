import { NextFunction, Request, Response } from 'express';
import { ParamsDictionary } from 'express-serve-static-core';
import { ParsedQs } from 'qs';
import response from '../providers/response';
import {
  LoginRequest,
  TokenPayload,
  TokenResponse
} from '../schema/authentication.schema';
import AuthenticationService from '../service/authentication.service';
import { HttpCode } from '../types/http_code.enum';
import { SuccessResponse } from '../types/response.type';

class AuthenticationController {
  private service;

  constructor() {
    this.service = new AuthenticationService();
  }

  login = async (
    req: Request<
      ParamsDictionary,
      any,
      LoginRequest,
      ParsedQs,
      Record<string, any>
    >,
    res: Response<SuccessResponse<TokenResponse>, Record<string, any>>,
    next: NextFunction
  ): Promise<void> => {
    try {
      const result = await this.service.login(req.body);
      response.success(result).create(res);
    } catch (error) {
      next(error);
    }
  };

  checkEmail = async (
    req: Request<
      ParamsDictionary,
      any,
      any,
      ParsedQs & { email: string },
      Record<string, any>
    >,
    res: Response<SuccessResponse<boolean>, Record<string, any>>,
    next: NextFunction
  ): Promise<void> => {
    try {
      const result = await this.service.checkEmail(req.query.email);
      let message = 'Email yang anda masukkan sudah digunakan';
      if (!result) {
        message = 'Email yang anda masukkan belum digunakan';
      }
      response.success(result, HttpCode.OK, message).create(res);
    } catch (error) {
      next(error);
    }
  };

  me = async (
    req: Request<ParamsDictionary, any, any, ParsedQs, Record<string, any>>,
    res: Response<SuccessResponse<boolean>, Record<string, any>>,
    next: NextFunction
  ): Promise<void> => {
    try {
      const me = res.locals.user as TokenPayload;
      response.success(me).create(res);
    } catch (error) {
      next(error);
    }
  };
}

export default AuthenticationController;
