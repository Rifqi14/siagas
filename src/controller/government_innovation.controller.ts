import { NextFunction, Request, Response } from 'express';
import { ParamsDictionary } from 'express-serve-static-core';
import { unlink, unlinkSync } from 'fs';
import { join } from 'path';
import { ParsedQs } from 'qs';
import GovernmentInnovation from '../entity/GovernmentInnovation.entity';
import ApiError from '../providers/exceptions/api.error';
import response from '../providers/response';
import { PaginationData } from '../schema/base.schema';
import {
	CreateGovernmentInnovationRequest,
	GetGovernmentInnovationDownloadRequest,
	GetGovernmentInnovationRequest,
} from '../schema/government_innovation.schema';
import GovernmentInnovationService from '../service/government_innovation.service';
import { HttpCode } from '../types/http_code.enum';
import BaseController from './base.controller';
import { IBaseController } from './interface/ibase.controller';
import { DownloadType } from 'src/types/lib';

class GovernmentInnovationController extends BaseController<GovernmentInnovationService> implements IBaseController {
	constructor() {
		super(new GovernmentInnovationService());
	}

	create = async (
		req: Request<ParamsDictionary, any, CreateGovernmentInnovationRequest, ParsedQs, Record<string, any>>,
		res: Response<any, Record<string, any>>,
		next: NextFunction
	): Promise<void> => {
		const uploads = req.files as {
			[fieldname: string]: Express.Multer.File[];
		};
		try {
			const {
				bentuk_inovasi,
				hasil_inovasi,
				inisiator_inovasi,
				jenis_inovasi,
				manfaat,
				nama_inovasi,
				nama_pemda,
				rancang_bangun,
				tahapan_inovasi,
				tematik,
				tujuan,
				urusan_lain,
				urusan_pertama,
				waktu_penerapan,
				waktu_uji_coba,
				urusan_pemerintah,
			} = req.body;
			let anggaran_file: Express.Multer.File | undefined = undefined;
			let profile_file: Express.Multer.File | undefined = undefined;
			let foto: Express.Multer.File | undefined = undefined;
			if (uploads && uploads.anggaran_file) anggaran_file = uploads.anggaran_file[0];
			if (uploads && uploads.profile_file) profile_file = uploads.profile_file[0];
			if (uploads && uploads.foto) foto = uploads.foto[0];
			const result = await this.service.create(
				nama_pemda,
				nama_inovasi,
				tahapan_inovasi,
				inisiator_inovasi,
				jenis_inovasi,
				bentuk_inovasi,
				tematik,
				urusan_pertama,
				urusan_lain,
				waktu_uji_coba,
				waktu_penerapan,
				rancang_bangun,
				tujuan,
				manfaat,
				hasil_inovasi,
				urusan_pemerintah,
				anggaran_file,
				profile_file,
				foto,
				res.locals.user.username
			);
			response.success(result).create(res);
		} catch (error) {
			if (uploads && uploads.anggaran_file) {
				unlink(join(__dirname, '..', '..', 'public', 'upload', 'inovasi_pemda', (uploads.anggaran_file[0] as Express.Multer.File).filename), err => {
					if (err) {
						console.log(`${(uploads.anggaran_file[0] as Express.Multer.File).filename} was deleted`);
					}
				});
			}
			if (uploads && uploads.profile_file) {
				unlink(join(__dirname, '..', '..', 'public', 'upload', 'inovasi_pemda', (uploads.profile_file[0] as Express.Multer.File).filename), err => {
					if (err) {
						console.log(`${(uploads.profile_file[0] as Express.Multer.File).filename} was deleted`);
					}
				});
			}
			next(error);
		}
	};

	download = async (
		req: Request<ParamsDictionary & { type: DownloadType }, any, any, ParsedQs & GetGovernmentInnovationDownloadRequest, Record<string, any>>,
		res: Response<any, Record<string, any>>,
		next: NextFunction
	): Promise<void> => {
		try {
			const { username } = res.locals.user;
			const result = await this.service.download(req.params.type, username, req.query);
			res.download(result, err => {
				if (!err) unlinkSync(result);
			});
		} catch (error) {
			next(error);
		}
	};

	downloadDetail = async (
		req: Request<
			ParamsDictionary & { type: DownloadType; id: number },
			any,
			any,
			ParsedQs & GetGovernmentInnovationDownloadRequest,
			Record<string, any>
		>,
		res: Response<any, Record<string, any>>,
		next: NextFunction
	): Promise<void> => {
		try {
			const { username } = res.locals.user;
			const result = await this.service.downloadProfil(req.params.id, req.params.type);
			res.download(result, err => {
				if (!err) unlinkSync(result);
			});
		} catch (error) {
			next(error);
		}
	};

	get = async (
		req: Request<ParamsDictionary, any, any, ParsedQs & GetGovernmentInnovationRequest, Record<string, any>>,
		res: Response<any, Record<string, any>>,
		next: NextFunction
	): Promise<void> => {
		try {
			const { username } = res.locals.user;
			const url = req.protocol + '://' + req.get('host') + req.originalUrl;
			const result = (await this.service.get(req.query, url, username)) as PaginationData<GovernmentInnovation[]>;
			response.pagination(result.res, result.paging).create(res);
		} catch (error) {
			next(error);
		}
	};

	detail = async (
		req: Request<ParamsDictionary & { id: number }, any, any, ParsedQs, Record<string, any>>,
		res: Response<any, Record<string, any>>,
		next: NextFunction
	): Promise<void> => {
		try {
			const result = await this.service.detail(req.params.id);
			response.success(result).create(res);
		} catch (error) {
			next(error);
		}
	};

	update = async (
		req: Request<ParamsDictionary & { id: number }, any, CreateGovernmentInnovationRequest, ParsedQs, Record<string, any>>,
		res: Response<any, Record<string, any>>,
		next: NextFunction
	): Promise<void> => {
		const uploads = req.files as {
			[fieldname: string]: Express.Multer.File[];
		};
		try {
			const {
				bentuk_inovasi,
				urusan_pemerintah,
				hasil_inovasi,
				inisiator_inovasi,
				jenis_inovasi,
				manfaat,
				nama_inovasi,
				nama_pemda,
				rancang_bangun,
				tahapan_inovasi,
				tematik,
				tujuan,
				urusan_lain,
				urusan_pertama,
				waktu_penerapan,
				waktu_uji_coba,
			} = req.body;
			let anggaran_file: Express.Multer.File | undefined = undefined;
			let profile_file: Express.Multer.File | undefined = undefined;
			let foto: Express.Multer.File | undefined = undefined;
			if (uploads && uploads.anggaran_file) anggaran_file = uploads.anggaran_file[0];
			if (uploads && uploads.profile_file) profile_file = uploads.profile_file[0];
			if (uploads && uploads.foto) foto = uploads.foto[0];
			const result = await this.service.update(
				req.params.id,
				nama_pemda,
				urusan_pemerintah,
				nama_inovasi,
				tahapan_inovasi,
				inisiator_inovasi,
				jenis_inovasi,
				bentuk_inovasi,
				tematik,
				urusan_pertama,
				urusan_lain,
				waktu_uji_coba,
				waktu_penerapan,
				rancang_bangun,
				tujuan,
				manfaat,
				hasil_inovasi,
				anggaran_file,
				profile_file,
				foto,
				res.locals.user.username
			);
			response.success(result).create(res);
		} catch (error) {
			next(error);
		}
	};

	delete = async (
		req: Request<ParamsDictionary & { id: number }, any, any, ParsedQs, Record<string, any>>,
		res: Response<any, Record<string, any>>,
		next: NextFunction
	): Promise<void> => {
		try {
			const result = await this.service.delete(req.params.id);
			if (!result) {
				next(
					new ApiError({
						httpCode: HttpCode.BAD_REQUEST,
						message: 'Gagal hapus data',
					})
				);
			} else {
				response.success('Berhasil hapus data').create(res);
			}
		} catch (error) {
			next(error);
		}
	};
}

export default GovernmentInnovationController;
