import { NextFunction, Request, Response } from 'express';
import { ParamsDictionary } from 'express-serve-static-core';
import { ParsedQs } from 'qs';
import PemdaIndikatorService from 'src/service/pemda_indikator.service';
import BaseController from './base.controller';
import { IBaseController } from './interface/ibase.controller';
import ApiError from 'src/providers/exceptions/api.error';
import { HttpCode } from 'src/types/http_code.enum';
import {
  CreatePemdaIndikatorRequest,
  ListPemdaIndikator,
  PemdaIndikatorParams
} from 'src/schema/pemda_indikator.schema';
import { PaginationData } from 'src/schema/base.schema';
import response from 'src/providers/response';

class PemdaIndikatorController
  extends BaseController<PemdaIndikatorService>
  implements IBaseController
{
  constructor() {
    super(new PemdaIndikatorService());
  }

  create = async (
    req: Request<
      ParamsDictionary & { pemda_id: string },
      any,
      CreatePemdaIndikatorRequest,
      ParsedQs,
      Record<string, any>
    >,
    res: Response<any, Record<string, any>>,
    next: NextFunction
  ): Promise<void> => {
    try {
      const result = await this.service.create(
        req.params.pemda_id,
        req.body,
        res.locals.user.username
      );
      response.success(result).create(res);
    } catch (error) {
      next(error);
    }
  };

  get = async (
    req: Request<
      ParamsDictionary & { pemda_id: string },
      any,
      any,
      ParsedQs & PemdaIndikatorParams,
      Record<string, any>
    >,
    res: Response<any, Record<string, any>>,
    next: NextFunction
  ): Promise<void> => {
    try {
      const { username } = res.locals.user;
      const url = req.protocol + '://' + req.get('host') + req.originalUrl;
      const result = (await this.service.get(
        req.params.pemda_id,
        url,
        username,
        req.query
      )) as PaginationData<ListPemdaIndikator>;
      response.pagination(result.res, result.paging).create(res);
    } catch (error) {
      next(error);
    }
  };

  detail = async (
    req: Request<
      ParamsDictionary & { pemda_id: number; indikator_id: number },
      any,
      any,
      ParsedQs,
      Record<string, any>
    >,
    res: Response<any, Record<string, any>>,
    next: NextFunction
  ): Promise<void> => {
    try {
      const { pemda_id, indikator_id } = req.params;

      const result = await this.service.detail(pemda_id, indikator_id);
      response.success(result).create(res);
    } catch (error) {
      next(error);
    }
  };

  update = async (
    req: Request<
      ParamsDictionary & { pemda_id: string; pemda_indikator_id: string },
      any,
      any,
      ParsedQs,
      Record<string, any>
    >,
    res: Response<any, Record<string, any>>,
    next: NextFunction
  ): Promise<void> => {
    try {
      const { pemda_id, pemda_indikator_id } = req.params;
      const request = req.body;

      const result = await this.service.update(
        pemda_id,
        pemda_indikator_id,
        res.locals.user.username,
        request
      );
      if (!result) {
        throw new ApiError({
          httpCode: HttpCode.BAD_REQUEST,
          message: 'Gagal ubah data'
        });
      }
      response.success('Berhasil ubah data').create(res);
    } catch (error) {
      next(error);
    }
  };

  delete = async (
    req: Request<
      ParamsDictionary & { pemda_id: string; pemda_indikator_id: string },
      any,
      any,
      ParsedQs,
      Record<string, any>
    >,
    res: Response<any, Record<string, any>>,
    next: NextFunction
  ): Promise<void> => {
    try {
      const { pemda_id, pemda_indikator_id } = req.params;

      const result = await this.service.delete(pemda_id, pemda_indikator_id);
      if (!result) {
        throw new ApiError({
          httpCode: HttpCode.BAD_REQUEST,
          message: 'Gagal hapus data'
        });
      }
      response.success('Berhasil hapus data').create(res);
    } catch (error) {
      next(error);
    }
  };
}

export default PemdaIndikatorController;
