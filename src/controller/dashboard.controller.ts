import BaseController from "./base.controller";
import { Request, Response, NextFunction } from "express";
import { ParamsDictionary } from "express-serve-static-core";
import { ParsedQs } from "qs";
import {
	DashboardArsipParams,
	DashboardOPDResponse,
	DashboardResponse,
	StatistikIndikatorInovasiParams,
	StatistikIndikatorInovasiResponse,
} from "src/schema/dashboard.schema";
import DashboardService from "src/service/dashboard.service";
import { PaginationData } from "src/schema/base.schema";
import { DashboardArsip } from "src/entity/DashboardArsip.entity";
import response from "src/providers/response";
import { SuccessResponse } from "src/types/response.type";
import { DownloadType } from "src/types/lib";
import { unlinkSync } from "fs";

export class DashboardController extends BaseController<DashboardService> {
	constructor() {
		super(new DashboardService());
	}

	arsip = async (
		req: Request<
			ParamsDictionary,
			any,
			any,
			ParsedQs & DashboardArsipParams,
			Record<string, any>
		>,
		res: Response<any, Record<string, any>>,
		next: NextFunction
	): Promise<void> => {
		try {
			const { username } = res.locals?.user ?? { username: "" };
			const url = req.protocol + "://" + req.get("host") + req.originalUrl;
			const result = (await this.service.get(
				url,
				username,
				req.query
			)) as PaginationData<DashboardArsip[]>;
			response.pagination(result.res, result.paging).create(res);
		} catch (error) {
			next(error);
		}
	};

	arsip_download = async (
		req: Request<
			ParamsDictionary & { type: DownloadType },
			any,
			any,
			ParsedQs & DashboardArsipParams,
			Record<string, any>
		>,
		res: Response<any, Record<string, any>>,
		next: NextFunction
	): Promise<void> => {
		try {
			const { username } = res.locals?.user ?? { username: "" };
			const result = await this.service.arsip_download(
				req.params.type,
				username,
				req.query
			);
			res.download(result, err => !err && unlinkSync(result));
		} catch (error) {
			next(error);
		}
	};

	statistik_indikator = async (
		req: Request<
			ParamsDictionary,
			any,
			any,
			ParsedQs & StatistikIndikatorInovasiParams,
			Record<string, any>
		>,
		res: Response<any, Record<string, any>>,
		next: NextFunction
	): Promise<void> => {
		try {
			const result = (await this.service.statistik_indikator(
				req.query
			)) as SuccessResponse<StatistikIndikatorInovasiResponse[]>;
			response.success(result).create(res);
		} catch (error) {
			next(error);
		}
	};

	statistik_inovasi = async (
		req: Request<ParamsDictionary, any, any, ParsedQs, Record<string, any>>,
		res: Response<any, Record<string, any>>,
		next: NextFunction
	): Promise<void> => {
		try {
			const result =
				(await this.service.statistik_inovasi()) as SuccessResponse<DashboardResponse>;
			response.success(result).create(res);
		} catch (error) {
			next(error);
		}
	};

	statistik_opd = async (
		req: Request<ParamsDictionary, any, any, ParsedQs, Record<string, any>>,
		res: Response<any, Record<string, any>>,
		next: NextFunction
	): Promise<void> => {
		try {
			const result =
				(await this.service.opd_menangani()) as SuccessResponse<DashboardOPDResponse>;
			response.success(result).create(res);
		} catch (error) {
			next(error);
		}
	};
}
