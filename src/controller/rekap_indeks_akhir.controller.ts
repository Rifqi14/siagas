import BaseController from './base.controller';
import { Request, Response, NextFunction } from 'express';
import { ParamsDictionary } from 'express-serve-static-core';
import { unlinkSync } from 'fs';
import { ParsedQs } from 'qs';
import { RekapIndeksAkhir } from 'src/entity/RekapIndeksAkhir.entity';
import ApiError from 'src/providers/exceptions/api.error';
import response from 'src/providers/response';
import { PaginationData } from 'src/schema/base.schema';
import { RekapIndeksAkhirParams, UpdateNominatorRequest } from 'src/schema/rekap_indeks_akhir.schema';
import RekapIndeksAkhirService from 'src/service/rekap_indeks_akhir.service';
import { HttpCode } from 'src/types/http_code.enum';
import { DownloadType } from 'src/types/lib';

export class RekapIndeksAkhirController extends BaseController<RekapIndeksAkhirService> {
	constructor() {
		super(new RekapIndeksAkhirService());
	}

	get = async (
		req: Request<ParamsDictionary, any, any, ParsedQs & RekapIndeksAkhirParams, Record<string, any>>,
		res: Response<any, Record<string, any>>,
		next: NextFunction
	): Promise<void> => {
		try {
			const { username } = res.locals.user;
			const url = req.protocol + '://' + req.get('host') + req.originalUrl;
			const result = (await this.service.get(url, username, req.query)) as PaginationData<RekapIndeksAkhir[]>;
			response.pagination(result.res, result.paging).create(res);
		} catch (error) {
			next(error);
		}
	};

	update = async (
		req: Request<ParamsDictionary & { id: number }, UpdateNominatorRequest, any, ParsedQs, Record<string, any>>,
		res: Response<any, Record<string, any>>,
		next: NextFunction
	): Promise<void> => {
		try {
			const { id } = req.params;

			const result = await this.service.nominator(id, req.body);
			if (!result) {
				throw new ApiError({
					httpCode: HttpCode.BAD_REQUEST,
					message: 'Gagal ubah data',
				});
			}
			response.success('Berhasil ubah data').create(res);
		} catch (error) {
			next(error);
		}
	};

	getDownload = async (
		req: Request<ParamsDictionary & { type: DownloadType }, any, any, ParsedQs & RekapIndeksAkhirParams, Record<string, any>>,
		res: Response<any, Record<string, any>>,
		next: NextFunction
	): Promise<void> => {
		try {
			const { username } = res.locals.user;
			const result = await this.service.download(req.params.type, username, req.query);
			res.download(result, err => !err && unlinkSync(result));
		} catch (error) {
			next(error);
		}
	};
}
