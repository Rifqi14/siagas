import { NextFunction, Request, Response } from 'express';
import { ParamsDictionary } from 'express-serve-static-core';
import { ParsedQs } from 'qs';
import InovasiIndikatorFileService from 'src/service/inovasi_indikator_file.service';
import BaseController from './base.controller';
import { IBaseController } from './interface/ibase.controller';
import {
  InovasiIndikatorFileParams,
  ListInovasiIndikatorFiles,
  UploadInovasiIndikatorFileRequest
} from 'src/schema/inovasi_indikator_file.schema';
import { InovasiIndikatorFile } from 'src/entity/InovasiIndikatorFile.entity';
import { PaginationResponse, SuccessResponse } from 'src/types/response.type';
import ApiError from 'src/providers/exceptions/api.error';
import { HttpCode } from 'src/types/http_code.enum';
import response from 'src/providers/response';
import { PaginationResponse as PaginationResponseInterface } from '../interface/pagination.interface';
import { PaginationData } from 'src/schema/base.schema';

export class InovasiIndikatorFileController
  extends BaseController<InovasiIndikatorFileService>
  implements IBaseController
{
  constructor() {
    super(new InovasiIndikatorFileService());
  }

  create = async (
    req: Request<
      ParamsDictionary,
      any,
      UploadInovasiIndikatorFileRequest,
      ParsedQs,
      Record<string, any>
    >,
    res: Response<any, Record<string, any>>,
    next: NextFunction
  ): Promise<void> => {
    throw new Error('Method not implemented');
  };

  upload = async (
    req: Request<
      ParamsDictionary & { inovasi_id: string; indikator_id: string },
      any,
      UploadInovasiIndikatorFileRequest,
      ParsedQs,
      Record<string, any>
    >,
    res: Response<SuccessResponse<InovasiIndikatorFile>, Record<string, any>>,
    next: NextFunction
  ): Promise<void> => {
    const document = req.file;
    if (!document)
      throw new ApiError({
        httpCode: HttpCode.BAD_REQUEST,
        message: 'Invalid request'
      });
    try {
      const result = await this.service.upload(
        req.params.inovasi_id,
        req.params.indikator_id,
        req.body.nomor_surat,
        req.body.tanggal_surat,
        req.body.tentang,
        document,
        req.body.name,
        res.locals.user.username
      );
      response.success(result).create(res);
    } catch (error) {
      next(error);
    }
  };

  get = async (
    req: Request<
      ParamsDictionary & { inovasi_id: string; indikator_id: string },
      any,
      any,
      ParsedQs & InovasiIndikatorFileParams,
      Record<string, any>
    >,
    res: Response<
      PaginationResponse<PaginationResponseInterface, Document>,
      Record<string, any>
    >,
    next: NextFunction
  ): Promise<void> => {
    try {
      const host = req.protocol + '://' + req.get('host');
      const url = host + req.originalUrl;
      const result = (await this.service.get(
        req.params.inovasi_id,
        req.params.indikator_id,
        url,
        res.locals.user.username,
        req.query
      )) as PaginationData<ListInovasiIndikatorFiles>;
      response.pagination(result.res, result.paging).create(res);
    } catch (error) {
      next(error);
    }
  };

  detail = async (
    req: Request<
      ParamsDictionary & {
        inovasi_id: number;
        indikator_id: number;
        file_id: number;
      },
      any,
      any,
      ParsedQs,
      Record<string, any>
    >,
    res: Response<any, Record<string, any>>,
    next: NextFunction
  ): Promise<void> => {
    try {
      const result = await this.service.detail(
        req.params.inovasi_id,
        req.params.indikator_id,
        req.params.file_id
      );
      response.success(result).create(res);
    } catch (error) {
      next(error);
    }
  };

  update = async (
    req: Request<
      ParamsDictionary & { id: number },
      any,
      any,
      ParsedQs,
      Record<string, any>
    >,
    res: Response<SuccessResponse<string>, Record<string, any>>,
    next: NextFunction
  ): Promise<void> => {
    throw new Error('Method not implemented');
  };

  delete = async (
    req: Request<
      ParamsDictionary & {
        inovasi_id: number;
        indikator_id: number;
        file_id: number;
      },
      any,
      any,
      ParsedQs,
      Record<string, any>
    >,
    res: Response<SuccessResponse<string>, Record<string, any>>,
    next: NextFunction
  ): Promise<void> => {
    try {
      const { inovasi_id, indikator_id, file_id } = req.params;

      const result = await this.service.delete(
        inovasi_id,
        indikator_id,
        file_id
      );
      if (!result) {
        next(
          new ApiError({
            httpCode: HttpCode.BAD_REQUEST,
            message: 'Gagal hapus data'
          })
        );
      }
      response.success('Berhasil hapus data').create(res);
    } catch (error) {
      next(error);
    }
  };
}
