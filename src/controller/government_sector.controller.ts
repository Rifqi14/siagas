import { Request, Response, NextFunction } from 'express';
import { ParamsDictionary } from 'express-serve-static-core';
import { ParsedQs } from 'qs';
import {
  CreateGovernmentSectorRequest,
  GetGovernmentSectorRequest
} from '../schema/government_sector.schema';
import { SuccessResponse } from '../types/response.type';
import GovernmentSectorService from '../service/government_sector.service';
import BaseController from './base.controller';
import { IGovernmentSector } from './interface/igovernment_sector.controller';
import GovernmentSector from '../entity/GovernmentSector';
import response from '../providers/response';
import ApiError from '../providers/exceptions/api.error';
import { HttpCode } from '../types/http_code.enum';
import { PaginationResponse } from '../interface/pagination.interface';

class GovernmentSectorController
  extends BaseController<GovernmentSectorService>
  implements IGovernmentSector
{
  constructor() {
    const service = new GovernmentSectorService();
    super(service);
  }

  detail = async (
    req: Request<
      ParamsDictionary & { id: number },
      any,
      any,
      ParsedQs,
      Record<string, any>
    >,
    res: Response<any, Record<string, any>>,
    next: NextFunction
  ): Promise<void> => {
    try {
      const { id } = req.params;

      const result = await this.service.detail(id);
      response.success(result).create(res);
    } catch (error) {
      next(error);
    }
  };

  update = async (
    req: Request<
      ParamsDictionary & { id: number },
      SuccessResponse<GovernmentSector>,
      CreateGovernmentSectorRequest,
      ParsedQs,
      Record<string, any>
    >,
    res: Response<any, Record<string, any>>,
    next: NextFunction
  ): Promise<void> => {
    try {
      const { id } = req.params;
      const request = req.body;

      const result = await this.service.update(id, request);
      if (!result) {
        throw new ApiError({
          httpCode: HttpCode.BAD_REQUEST,
          message: 'Gagal ubah data'
        });
      }
      response.success('Berhasil ubah data').create(res);
    } catch (error) {
      next(error);
    }
  };

  delete = async (
    req: Request<
      ParamsDictionary & { id: number },
      any,
      any,
      ParsedQs,
      Record<string, any>
    >,
    res: Response<any, Record<string, any>>,
    next: NextFunction
  ): Promise<void> => {
    try {
      const { id } = req.params;

      const result = await this.service.delete(id);
      if (!result) {
        throw new ApiError({
          httpCode: HttpCode.BAD_REQUEST,
          message: 'Gagal hapus data'
        });
      }
      response.success('Berhasil hapus data').create(res);
    } catch (error) {
      next(error);
    }
  };

  get = async (
    req: Request<
      ParamsDictionary,
      any,
      any,
      ParsedQs & GetGovernmentSectorRequest,
      Record<string, any>
    >,
    res: Response<any, Record<string, any>>,
    next: NextFunction
  ): Promise<void> => {
    try {
      const url = req.protocol + '://' + req.get('host') + req.originalUrl;
      const result = (await this.service.get(req.query, url)) as {
        paging: PaginationResponse;
        res: GovernmentSector[];
      };
      response.pagination(result.res, result.paging).create(res);
    } catch (error) {
      next(error);
    }
  };

  create = async (
    req: Request<
      ParamsDictionary,
      SuccessResponse<GovernmentSector>,
      CreateGovernmentSectorRequest,
      ParsedQs,
      Record<string, any>
    >,
    res: Response<any, Record<string, any>>,
    next: NextFunction
  ): Promise<void> => {
    try {
      const request = req.body;
      const result = await this.service.create(request);
      response.success(result).create(res);
    } catch (error) {
      next(error);
    }
  };
}

export default GovernmentSectorController;
