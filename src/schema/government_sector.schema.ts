import { object, string, z } from 'zod';
import { BasePaginationRequest } from './base.schema';

export const createGovernmentSectorSchema = object({
  body: object({
    name: string({ required_error: 'Name is requierd' }),
    deadline: string({ required_error: 'Deadline is requierd' })
  })
});

export type CreateGovernmentSectorRequest = z.infer<
  typeof createGovernmentSectorSchema
>['body'];

export interface GetGovernmentSectorRequest extends BasePaginationRequest {
  name?: string;
}
