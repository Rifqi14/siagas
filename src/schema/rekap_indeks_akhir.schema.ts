import { RekapIndeksAkhir } from 'src/entity/RekapIndeksAkhir.entity';
import { BasePaginationRequest } from './base.schema';

export interface RekapIndeksAkhirParams extends Partial<BasePaginationRequest> {
	q?: string;
}

export interface UpdateNominatorRequest {
	nominator: 'Ya' | 'Tidak';
}

export interface DownloadRekapIndeksAkhirResponse {
	no: number;
	daerah: string;
	jumlah_inovasi: string;
	nilai_indeks_mandiri: string;
	nilai_indeks_verifikasi: string;
	predikat: string;
	nominator: string;
}

export const toDownloadResponse = (data: RekapIndeksAkhir, no: number): DownloadRekapIndeksAkhirResponse => {
	return {
		no,
		daerah: data.nama_daerah ?? '',
		jumlah_inovasi: data.jumlah_inovasi?.toString() ?? '',
		nilai_indeks_mandiri: data.nilai_indeks?.toString() ?? '',
		nilai_indeks_verifikasi: data.nilai_indeks_verifikasi?.toString() ?? '',
		predikat: data.predikat ?? '',
		nominator: data.nominator ?? '',
	};
};
