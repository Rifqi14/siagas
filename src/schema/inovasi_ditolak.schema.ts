import { ReviewInovasiDaerah } from 'src/entity/ReviewInovasiDaerah.entity';
import { BasePaginationRequest } from './base.schema';

export interface InovasiDitolakParams extends Partial<BasePaginationRequest> {
	q?: string;
	pemda_id?: number;
}

export interface InovasiDitolakResponse {
	review_inovasi_id: string | null;
	nomor: string | null;
	judul: string | null;
	pemda: {
		pemda_id: string | null;
		pemda_name: string | null;
	} | null;
	waktu_penerapan: string | null;
	kematangan: number | null;
	skor_verifikasi: number | null;
	qc: string | null;
}

export interface DownloadInovasiDitolakResponse {
	no: number;
	nomor: string;
	judul: string;
	nama_pemda: string;
	waktu_penerapan: string;
	kematangan: string;
	skor_verifikasi: string;
	QC: string;
}

export const toDownloadResponse = (data: ReviewInovasiDaerah, no: number): DownloadInovasiDitolakResponse => {
	let skor = 0;
	let kematangan = 0;
	switch (data.inovasi?.innovation_phase?.toLowerCase()) {
		case 'inisiatif':
			skor = 50;
			kematangan = 3;
			break;
		case 'uji coba':
			skor = 102;
			kematangan = 6;
			break;
		case 'penerapan':
			skor = 105;
			kematangan = 9;
			break;

		default:
			skor = 0;
			break;
	}

	return {
		no,
		nomor: data.random_number?.toString() ?? '',
		judul: data.inovasi?.innovation_name ?? '',
		nama_pemda: data.inovasi?.profilPemda?.nama_daerah ?? '',
		waktu_penerapan: data.inovasi?.implementation_time?.toString() ?? '',
		kematangan: kematangan?.toString() ?? '',
		skor_verifikasi: skor?.toString() ?? '',
		QC: data.status ?? '',
	};
};
