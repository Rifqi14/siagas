import { RangkingIndex } from "src/entity/RangkingIndex.entity";
import { BasePaginationRequest } from "./base.schema";

export interface RangkingIndexParams extends Partial<BasePaginationRequest> {
	q?: string;
}

export interface RangkingIndexParamsDownload {
	q?: string;
}

export interface UpdateNominatorRequest {
	nominator: "Ya" | "Tidak";
}

export interface RangkingIndexDownloadResponse {
	no: number;
	nama_daerah: string;
	jumlah_inovasi: string;
	total_skor: string;
	nilai_indeks: string;
	total_file: string;
	predikat: string;
	indeks: string;
	nominator: string;
}

export const toDownload = (data: RangkingIndex, no: number): RangkingIndexDownloadResponse => {
	return {
		no,
		nama_daerah: data.nama_daerah ?? "",
		jumlah_inovasi: data.jumlah_inovasi?.toString() ?? "",
		total_skor: data.total_skor_mandiri?.toString() ?? "",
		nilai_indeks: data.nilai_indeks?.toString() ?? "",
		total_file: data.total_file?.toString() ?? "",
		predikat: data.predikat?.toString() ?? "",
		indeks: data.indeks?.toString() ?? "",
		nominator: data.nominator?.toString() ?? "",
	};
};
