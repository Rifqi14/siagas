import { object, string, z } from 'zod';
import { BasePaginationRequest } from './base.schema';

export const createTuxedoSchema = object({
  body: object({
    title: string(),
    slug: string(),
    section: string(),
    content: string()
  })
});

export type CreateTuxedoRequest = z.infer<typeof createTuxedoSchema>['body'];

export interface GetTuxedoRequest extends BasePaginationRequest {
  q?: string;
}
