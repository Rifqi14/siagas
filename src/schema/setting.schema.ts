import { object, string, z } from 'zod';
import { BasePaginationRequest } from './base.schema';

export const createSettingSchema = object({
  body: object({
    key: string({ required_error: 'Key is required' })
      .refine(data => !/[A-Z]/g.test(data), {
        message: 'Key cannot contain upper case',
        path: ['body', 'key']
      })
      .refine(data => !data.includes(' '), {
        message: 'Key cannot contain space or blank space',
        path: ['body', 'key']
      }),
    value: string({ required_error: 'Value is required' }),
    helper: string()
  })
});

export type CreateSettingRequest = z.infer<typeof createSettingSchema>['body'];

export interface GetSettingRequest extends BasePaginationRequest {
  q?: string;
}
