import { z } from 'zod';
import { BasePaginationRequest } from './base.schema';
import { InovasiIndikatorFile } from 'src/entity/InovasiIndikatorFile.entity';

export const uploadInovasiIndikatorFileSchema = z.object({
  body: z.object({
    nomor_surat: z.string(),
    tanggal_surat: z.string(),
    tentang: z.string(),
    name: z.string().optional()
  })
});

export type UploadInovasiIndikatorFileRequest = z.infer<
  typeof uploadInovasiIndikatorFileSchema
>['body'] & {
  dokumen?: Express.Multer.File;
};

export interface InovasiIndikatorFileParams extends BasePaginationRequest {
  q?: string;
}

export type ListInovasiIndikatorFiles = InovasiIndikatorFile[];
