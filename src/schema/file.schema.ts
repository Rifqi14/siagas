import { any, object, string, z } from 'zod';

export const uploadFileSchema = object({
  body: object({
    name: string().optional(),
    file: any({ required_error: 'File is required' })
  })
});

export type UploadFileRequest = {
  name?: string;
  file: Express.Multer.File;
};
