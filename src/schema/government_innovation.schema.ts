import { object, string, z } from "zod";
import { BasePaginationRequest } from "./base.schema";
import GovernmentInnovation from "src/entity/GovernmentInnovation.entity";

export const createGovernmentInnovationSchema = object({
	body: object({
		nama_pemda: string(),
		dibuat_oleh: string(),
		nama_inovasi: string(),
		tahapan_inovasi: string(),
		inisiator_inovasi: string(),
		jenis_inovasi: string(),
		bentuk_inovasi: string(),
		tematik: string(),
		urusan_pemerintah: string(),
		urusan_pertama: string(),
		urusan_lain: string(),
		waktu_uji_coba: string(),
		waktu_penerapan: string(),
		rancang_bangun: string(),
		tujuan: string(),
		manfaat: string(),
		hasil_inovasi: string(),
	}),
});

export type CreateGovernmentInnovationRequest = z.infer<
	typeof createGovernmentInnovationSchema
>["body"] & {
	anggaran_file?: Express.Multer.File;
	profile_file?: Express.Multer.File;
};

export interface GetGovernmentInnovationRequest extends BasePaginationRequest {
	tahap?: string;
	q?: string;
}

export interface GetGovernmentInnovationDownloadRequest {
	tahap?: string;
	q?: string;
}

export class GovernmentInnovationExportSchema {
	no: number;
	nama_pemda: string;
	nama_inovasi: string;
	tahapan_inovasi: string;
	urusan_pemerintahan: string;
	waktu_uji_coba_inovasi: string;
	waktu_penerapan: string;
	estimasi_skor_kematangan: number;
}

export const toGovernmentInnovationExport = (
	data: GovernmentInnovation,
	no: number
): GovernmentInnovationExportSchema => {
	let estimasi = 0;
	switch (data.innovation_phase.toLowerCase()) {
		case "inisiatif":
			estimasi = 3;
			break;
		case "uji coba":
			estimasi = 6;
			break;
		case "penerapan":
			estimasi = 9;
			break;
	}

	return {
		no,
		nama_pemda: data.profilPemda?.nama_daerah ?? "",
		nama_inovasi: data.innovation_name ?? "",
		tahapan_inovasi: data.innovation_phase ?? "",
		urusan_pemerintahan: data.urusan?.name ?? "",
		waktu_uji_coba_inovasi: data.trial_time ?? "",
		waktu_penerapan: data.implementation_time ?? "",
		estimasi_skor_kematangan: estimasi,
	};
};

export class GovernmentInnovationProfilExportSchema {
	nama_pemda: string;
	nama_inovasi: string;
	tahapan_inovasi: string;
	inisiator_daerah: string;
	jenis_inovasi: string;
	bentuk_inovasi_daerah: string;
	tematik: string;
	detail_tematik: string;
	urusan_pemerintahan: string;
	tingkatan: string;
	waktu_uji_coba_inovasi: string;
	waktu_penerapan: string;
	opd_yang_menangani: string;
	alamat_pemda: string;
	email: string;
	no_telp: string;
	nama_admin: string;
}

export const toGovernmentInnovationProfilExportSchema = (
	data: GovernmentInnovation
): GovernmentInnovationProfilExportSchema => {
	return {
		nama_pemda: data.profilPemda?.nama_daerah ?? "",
		nama_inovasi: data.innovation_name ?? "",
		tahapan_inovasi: data.innovation_phase ?? "",
		inisiator_daerah: data.innovation_initiator ?? "",
		jenis_inovasi: data.innovation_type ?? "",
		bentuk_inovasi_daerah: data.innovation_form ?? "",
		tematik: data.thematic ?? "",
		detail_tematik: data.thematic_detail ?? "",
		urusan_pemerintahan: data.urusan?.name ?? "",
		tingkatan: "",
		waktu_uji_coba_inovasi: data.trial_time ?? "",
		waktu_penerapan: data.implementation_time ?? "",
		opd_yang_menangani: data.userCreator?.nama_pemda ?? "",
		alamat_pemda: data.profilPemda?.alamat_pemda ?? "",
		email: data.userCreator?.email ?? "",
		no_telp: data.profilPemda?.no_telpon ?? "",
		nama_admin: data.profilPemda?.nama_admin ?? "",
	};
};
