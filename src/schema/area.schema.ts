import { number, object, string, z } from 'zod';
import { BasePaginationRequest } from './base.schema';

export const createAreaSchema = object({
  body: object({
    name: string({ required_error: 'Name is required' }),
    region_id: number({ required_error: 'Region is required' })
  })
});

export type CreateAreaRequest = z.infer<typeof createAreaSchema>['body'];

export interface GetAreaRequest extends BasePaginationRequest {
  q?: string;
}
