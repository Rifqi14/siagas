import { any, object, string, z } from 'zod';
import { BasePaginationRequest } from './base.schema';

export const createAnnouncementSchema = object({
  body: object({
    title: string(),
    slug: string(),
    content: string(),
    document: any({ required_error: 'Document is required' })
  })
});

export type CreateAnnouncementRequest = Omit<
  z.infer<typeof createAnnouncementSchema>['body'],
  'document'
>;

export interface GetAnnouncementRequest extends BasePaginationRequest {
  q?: string;
  category?: string;
}
