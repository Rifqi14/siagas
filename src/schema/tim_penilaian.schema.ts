import { z } from 'zod';
import { BasePaginationRequest } from './base.schema';
import { TimPenilaian } from 'src/entity/TimPenilaian.entity';

export const createTimPenilaianSchema = z.object({
  body: z.object({
    asn_username: z.string().optional(),
    nama: z.string().optional(),
    instansi: z.string().optional()
  })
});

export type CreateTimPenilaianRequest = z.infer<
  typeof createTimPenilaianSchema
>['body'];

export interface ListTimPenilaianParams extends BasePaginationRequest {
  q?: string;
}

export type ListTimPenilaian = TimPenilaian[];
