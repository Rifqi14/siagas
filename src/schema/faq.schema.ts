import { object, string, z } from 'zod';
import { BasePaginationRequest } from './base.schema';

export const createFaqSchema = object({
  body: object({
    question: string({ required_error: 'Question is required' }),
    answer: string({ required_error: 'Answer is required' })
  })
});

export type CreateFaqRequest = z.infer<typeof createFaqSchema>['body'];

export interface GetFaqRequest extends BasePaginationRequest {
  q?: string;
}
