import { boolean, object, string, z } from 'zod';
import Role from '../entity/Role.entity';
import User from 'src/entity/User.entity';

export const loginSchema = object({
	body: object({
		username: string({ required_error: 'Username is required' }),
		password: string({ required_error: 'Password is required' }),
		remember_me: boolean().default(false),
	}),
});

export type LoginRequest = z.infer<typeof loginSchema>['body'];

export interface TokenPayload {
	user_id: number;
	username: string;
	role: Role;
}

export interface TokenResponse {
	token: string;
	role: Role;
	user: User;
	token_expired_at: number | null;
}
