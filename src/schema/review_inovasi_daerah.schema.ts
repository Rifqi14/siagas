import { ReviewInovasiDaerah } from 'src/entity/ReviewInovasiDaerah.entity';
import { BasePaginationRequest } from './base.schema';

export interface CreateReviewInovasiDaerahRequest extends Pick<ReviewInovasiDaerah, 'skor' | 'status'> {}

export interface GetReviewInovasiRequest extends Partial<BasePaginationRequest> {
	q?: string;
	pemda_id?: number;
}

export interface UpdateEvaluasiRequest {
	evaluasi_id?: string;
	data_saat_ini?: string;
	kategori?: string;
	keterangan?: string;
}

export interface ReviewInovasiDaerahResponse {
	id: string;
	nomor: string;
	judul: string;
	pemda: {
		id: string;
		nama_pemda: string;
	} | null;
	waktu_penerapan: string;
	skor: string;
	skor_verifikasi: string;
	waktu_pengiriman: string;
	qc: string | null;
	inovasi_id?: string;
	preview: {
		id: string;
		pemda: {
			id: string;
			nama_pemda: string;
		} | null;
		judul: string;
		status: string;
		alasan_ditolak: string;
	} | null;
}

export interface ReviewInovasiDaerahDownload {
	no: number;
	nomor: string;
	judul: string;
	pemda: string;
	waktu_penerapan: string;
	skor: string;
	skor_verifikasi: string;
	qc: string;
}

export interface ProfilInovasiResponse {
	id: string | null;
	nama_pemda: string | null;
	nama_inovasi: string | null;
	tahapan_inovasi: string | null;
	inisiator_daerah: string | null;
	jenis_inovasi: string | null;
	bentuk_inovasi_daerah: string | null;
	tematik: string | null;
	detail_tematik: string | null;
	urusan_pemerintah: string | null;
	tingkatan: string | null;
	waktu_uji_coba: string | null;
	waktu_penerapan: string | null;
	opd: string | null;
	pemda: {
		id: string | null;
		name: string | null;
		email: string | null;
		no_telp: string | null;
		nama_admin: string | null;
	} | null;
	dokumen: {
		id: string | null;
		name: string | null;
		url: string | null;
	} | null;
}

export interface IndikatorResponse {
	indikator_id: string | null;
	name: string | null;
	informasi: string | null;
	skor: number | null;
	skor_evaluasi: number | null;
}

export interface EvaluasiResponse {
	review_id: string | null;
	inovasi_id: string | null;
	nama_inovasi: string | null;
	evaluasi_id: string | null;
	data_saat_ini: string | null;
	kategori: string | null;
	judul: string | null;
	keterangan: string | null;
}

export const toReviewInovasiDaerahDownload = (data: ReviewInovasiDaerah, no: number): ReviewInovasiDaerahDownload => {
	let skor = 0;
	switch (data.inovasi.innovation_phase) {
		case 'inisiatif':
			skor = 50;
			break;
		case 'uji coba':
			skor = 102;
			break;
		case 'penerapan':
			skor = 105;
			break;

		default:
			skor = 0;
			break;
	}
	let skor_verifikasi = skor;
	if (data.status == 'Rejected') skor_verifikasi = 10;
	if (data.status == 'Accept') skor_verifikasi += 50;
	return {
		no,
		nomor: data.random_number?.toString() ?? '',
		judul: data.inovasi?.innovation_name ?? '',
		pemda: data.inovasi?.profilPemda?.nama_daerah ?? '',
		waktu_penerapan: data.inovasi?.implementation_time ?? '',
		skor: skor.toString() ?? '',
		skor_verifikasi: skor_verifikasi.toString() ?? '',
		qc: data.status ?? '',
	};
};
