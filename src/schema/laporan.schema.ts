import { LaporanIndeks } from 'src/entity/LaporanIndeks.entity';
import { BasePaginationRequest } from './base.schema';

// Laporan Indeks
export interface LaporanIndeksParams extends Partial<BasePaginationRequest> {
	q?: string;
	predikat?: 'semua' | 'belum mengisi data' | 'kurang inovatif' | 'inovatif' | 'terinovatif';
	pemda_id?: number;
	opd_yang_menangani?: 'Badan Litbang' | 'Bappeda Litbang' | 'Diluar';
}

export interface LaporanIndeksResponse {
	pemda_id: string;
	nama_pemda: string;
	opd_yang_menangani: string;
	skor: string;
	rangking: string;
	predikat: string;
}

export const toLaporanIndexResponse = (indeks: LaporanIndeks): LaporanIndeksResponse => {
	return {
		pemda_id: indeks.pemda_id?.toString() ?? '',
		nama_pemda: indeks.nama_daerah ?? '',
		opd_yang_menangani: indeks.opd_yang_menangani ?? '',
		skor: indeks.nilai_indeks?.toString() ?? '',
		rangking: indeks.indeks?.toString() ?? '',
		predikat: indeks.predikat ?? '',
	};
};

// Laporan Jenis Inovasi
export interface LaporanJenisInovasiParams extends Partial<BasePaginationRequest> {
	pemda_id?: number;
	q?: string;
}

// Laporan Bentuk Inovasi
export interface LaporanBentukInovasiParams extends Partial<BasePaginationRequest> {
	pemda_id?: number;
	q?: string;
}

// Laporan Inisiator Inovasi
export interface LaporanInisiatorInovasiParams extends Partial<BasePaginationRequest> {
	pemda_id?: number;
	q?: string;
}

// Laporan Urusan Inovasi
export interface LaporanUrusanInovasiParams extends Partial<BasePaginationRequest> {
	pemda_id?: number;
	q?: string;
}
