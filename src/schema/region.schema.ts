import { object, string, z } from 'zod';
import { BasePaginationRequest } from './base.schema';

export const createRegionsSchema = object({
  body: object({
    name: string({ required_error: 'Name is required' })
  })
});

export type CreateRegionRequest = z.infer<typeof createRegionsSchema>['body'];

export interface GetRegionRequest extends BasePaginationRequest {
  name?: string;
}
