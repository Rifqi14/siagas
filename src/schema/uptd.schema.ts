import { number, object, string, z } from 'zod';
import { BasePaginationRequest } from './base.schema';

export const createUptdSchema = object({
  body: object({
    name: string({ required_error: 'Name is required' }),
    regional_apparatus_id: number({
      required_error: 'Regional apparatus id required'
    })
  })
});

export type CreateUptdRequest = z.infer<typeof createUptdSchema>['body'];

export interface GetUptdRequest extends BasePaginationRequest {
  q?: string;
}
