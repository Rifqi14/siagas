import { LaporanBentukInovasi } from 'src/entity/LaporanBentukInovasi.entity';
import { LaporanInisiatorInovasi } from 'src/entity/LaporanInisatorInovasi.entity';
import { LaporanJenisInovasi } from 'src/entity/LaporanJenisInovasi.entity';
import { LaporanUrusanInovasi } from 'src/entity/LaporanUrusanInovasi.entity';

// Rawling Params
export interface RawlingParams {
  pemda_id: number;
}

export interface RawlingResponse {
  jenis_inovasi: LaporanJenisInovasi[];
  urusan_pemerintah: LaporanUrusanInovasi[];
  inisiator: LaporanInisiatorInovasi[];
  bentuk_inovasi: LaporanBentukInovasi[];
}
