import { Entity, ViewColumn, ViewEntity } from 'typeorm';

@ViewEntity({
  expression: `select
                (select count(id) from profil_pemda) as total_pemda,
                (select count(id) from government_innovations) as total_inovasi,
                (select count(id) from government_innovations gi where lower(gi.innovation_phase) = 'uji coba') as total_uji_coba,
                (select count(id) from government_innovations gi where lower(gi.innovation_phase) = 'penerapan') as total_penerapan,
                (select count(id) from government_innovations gi where lower(gi.innovation_phase) = 'inisiatif') as total_inisiatif,
                (select nama_daerah from (select pp.nama_daerah, count(gi.id) total_innovation
                    from government_innovations gi
                    left join profil_pemda pp on pp.id = gi.pemda_id
                    group by pp.nama_daerah
                    order by count(gi.id) desc
                    limit 1) as lowest) as daerah_tertinggi,
                (select nama_daerah from (select pp.nama_daerah, count(gi.id) total_innovation
                    from government_innovations gi
                    left join profil_pemda pp on pp.id = gi.pemda_id
                    group by pp.nama_daerah
                    order by count(gi.id) asc
                    limit 1) as lowest) as daerah_terendah`
})
export class Dashboard {
  @ViewColumn()
  total_pemda: number;

  @ViewColumn()
  total_inovasi: number;

  @ViewColumn()
  total_uji_coba: number;

  @ViewColumn()
  total_penerapan: number;

  @ViewColumn()
  total_inisiatif: number;

  @ViewColumn()
  daerah_tertinggi: string;

  @ViewColumn()
  daerah_terendah: string;
}
