import {
  AfterInsert,
  AfterLoad,
  AfterUpdate,
  BaseEntity,
  Column,
  CreateDateColumn,
  Entity,
  OneToMany,
  PrimaryGeneratedColumn,
  UpdateDateColumn
} from 'typeorm';
import Config from '../providers/config';
import Announcement from './Announcement.entity';

@Entity({ name: 'files' })
export default class File extends BaseEntity {
  @PrimaryGeneratedColumn()
  id: number;
  @Column()
  name: string;
  @Column()
  path: string;
  @Column()
  extension: string;
  @Column()
  size: string;
  @Column()
  uploaded_by: string;
  @CreateDateColumn()
  created_at: Date;
  @UpdateDateColumn()
  updated_at: Date;

  @Column({ select: false, nullable: true, insert: false, update: false })
  full_path: string;

  @OneToMany(() => Announcement, announcement => announcement.file)
  announcements: Announcement[];

  @AfterInsert()
  @AfterLoad()
  @AfterUpdate()
  setFullPath() {
    this.full_path = Config.load().app_url + '/' + this.path;
  }
}
