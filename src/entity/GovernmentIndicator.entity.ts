import {
  BaseEntity,
  Column,
  CreateDateColumn,
  DeleteDateColumn,
  Entity,
  JoinColumn,
  ManyToOne,
  PrimaryGeneratedColumn,
  UpdateDateColumn
} from 'typeorm';
import Indicator from './Indicator.entity';
import GovernmentProfile from './GovernmentProfile.entity';
import IndicatorScale from './IndicatorScale.entity';

@Entity({ name: 'government_indicators', orderBy: { created_at: 'DESC' } })
export class GovernmentIndicators extends BaseEntity {
  @PrimaryGeneratedColumn()
  id: number;

  @Column()
  value: string;

  @Column()
  value_before: string;

  @Column()
  value_after: string;

  @Column()
  other_value: string;

  @Column()
  score: string;

  @CreateDateColumn()
  created_at: Date;

  @UpdateDateColumn()
  updated_at: Date;

  @DeleteDateColumn()
  deleted_at: Date;

  @ManyToOne(() => IndicatorScale)
  @JoinColumn({ name: 'scale_id' })
  scale: IndicatorScale;

  @ManyToOne(() => Indicator)
  @JoinColumn({ name: 'indicator_id' })
  indicator: Indicator;

  @ManyToOne(() => GovernmentProfile)
  @JoinColumn({ name: 'gov_profile_id' })
  profile: GovernmentProfile;
}
