import {
  BaseEntity,
  Column,
  CreateDateColumn,
  Entity,
  JoinColumn,
  ManyToOne,
  PrimaryGeneratedColumn,
  UpdateDateColumn
} from 'typeorm';
import File from './File.entity';
import Regions from './Region.entity';

@Entity({ name: 'government_profiles', orderBy: { created_at: 'DESC' } })
export default class GovernmentProfile extends BaseEntity {
  @PrimaryGeneratedColumn()
  id: number;

  @Column()
  region_id: number;

  @Column()
  name: string;

  @Column()
  pic: string;

  @Column()
  address: string;

  @Column()
  email: string;

  @Column()
  phone: string;

  @Column()
  admin_name: string;

  @Column({ nullable: true })
  file_id: number;

  @CreateDateColumn()
  created_at: Date;

  @UpdateDateColumn()
  updated_at: Date;

  @ManyToOne(() => Regions, { eager: true })
  @JoinColumn({ name: 'region_id' })
  region: Omit<Regions, 'areas' | 'clusters'>;

  @ManyToOne(() => File, { eager: true })
  @JoinColumn({ name: 'file_id' })
  file: Omit<File, 'announcement'>;
}
