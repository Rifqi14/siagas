import { AppBaseEntity } from 'src/types/lib';
import {
  Column,
  Entity,
  JoinColumn,
  ManyToOne,
  PrimaryGeneratedColumn
} from 'typeorm';
import { InovasiIndikator } from './InovasiIndikator.entity';
import File from './File.entity';
import GovernmentInnovation from './GovernmentInnovation.entity';
import Indicator from './Indicator.entity';

@Entity({ name: 'inovasi_indikator_file' })
export class InovasiIndikatorFile extends AppBaseEntity {
  @PrimaryGeneratedColumn()
  id: number;

  @Column()
  inovasi_indikator_id: number;

  @Column()
  inovasi_id: number;

  @Column()
  indikator_id: number;

  @Column()
  file_id: number;

  @Column()
  nomor_surat: string;

  @Column()
  tanggal_surat: string;

  @Column()
  tentang: string;

  @ManyToOne(() => GovernmentInnovation)
  @JoinColumn({ name: 'inovasi_id' })
  inovasi: GovernmentInnovation;

  @ManyToOne(() => Indicator, { eager: true })
  @JoinColumn({ name: 'indikator_id' })
  indikator: Indicator;

  @ManyToOne(() => InovasiIndikator)
  @JoinColumn({ name: 'inovasi_indikator_id' })
  inovasi_indikator: InovasiIndikator;

  @ManyToOne(() => File, { eager: true })
  @JoinColumn({ name: 'file_id' })
  file: File;
}
