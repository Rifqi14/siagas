import { AppBaseEntity } from 'src/types/lib';
import {
  Column,
  Entity,
  JoinColumn,
  ManyToOne,
  OneToMany,
  PrimaryGeneratedColumn
} from 'typeorm';
import GovernmentInnovation from './GovernmentInnovation.entity';
import Indicator from './Indicator.entity';
import { InovasiIndikatorFile } from './InovasiIndikatorFile.entity';
import { EvaluasiInovasiDaerah } from './EvaluasiInovasiDaerah.entity';

@Entity({ name: 'inovasi_indikator' })
export class InovasiIndikator extends AppBaseEntity {
  @PrimaryGeneratedColumn()
  id: number;

  @Column()
  inovasi_id: number;

  @Column()
  indikator_id: number;

  @Column()
  informasi: string;

  @Column()
  nilai: number;

  @Column()
  nilai_sebelum: number;

  @Column()
  nilai_sesudah: number;

  @ManyToOne(() => GovernmentInnovation)
  @JoinColumn({ name: 'inovasi_id' })
  inovasi: GovernmentInnovation;

  @ManyToOne(() => Indicator, { eager: true })
  @JoinColumn({ name: 'indikator_id' })
  indikator: Indicator;

  @OneToMany(
    () => EvaluasiInovasiDaerah,
    evaluasi => evaluasi.inovasi_indikator
  )
  evaluasi: EvaluasiInovasiDaerah[];

  @OneToMany(() => InovasiIndikatorFile, file => file.inovasi_indikator, {
    eager: true
  })
  files: InovasiIndikatorFile[];
}
