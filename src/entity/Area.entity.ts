import {
  BaseEntity,
  Column,
  CreateDateColumn,
  Entity,
  JoinColumn,
  ManyToOne,
  PrimaryGeneratedColumn,
  UpdateDateColumn
} from 'typeorm';
import Regions from './Region.entity';

@Entity({ name: 'areas' })
export default class Area extends BaseEntity {
  @PrimaryGeneratedColumn()
  id: number;

  @Column()
  region_id: number;

  @Column()
  name: string;

  @CreateDateColumn()
  created_at: Date;

  @UpdateDateColumn()
  updated_at: Date;

  @ManyToOne(() => Regions, region => region.areas, { eager: true })
  @JoinColumn({ name: 'region_id' })
  region: Regions;
}
