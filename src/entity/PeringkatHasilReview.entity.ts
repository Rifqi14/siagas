import { Entity, ViewColumn, ViewEntity } from 'typeorm';

@ViewEntity({
	expression: `SELECT
  pp.id,
  pp.nama_daerah,
  count(gi.id) AS jumlah_inovasi,
  CASE
    WHEN lower(pp.nominator::text) = 'ya'::text THEN '1.00'::text
    ELSE '0.00'::text
  END AS isp,
  (
    SELECT
      count(pp2.id) AS count
    FROM
      profil_pemda pp2
  ) AS total_pemda,
  CASE
    WHEN (
      (
        SELECT
          sum(pi.skor_verifikasi) AS sum
        FROM
          pemda_indikator pi
        WHERE
          pi.pemda_id = pp.id
      )
    ) > 0::numeric THEN (
      SELECT
        sum(pi.skor_verifikasi) AS sum
      FROM
        pemda_indikator pi
      WHERE
        pi.pemda_id = pp.id
    )
    ELSE 0::numeric
  END AS total_skor_verifikasi,
  (
    SELECT
      ri.predikat
    FROM
      rangking_index ri
    WHERE
      ri.id = pp.id
  ) AS predikat,
  pp.nominator,
  (
    SELECT
      sum(pi.skor) AS sum
    FROM
      pemda_indikator pi
    WHERE
      pi.pemda_id = pp.id
  ) AS skor,
  (
    SELECT
      sum(pi.skor_verifikasi) AS sum
    FROM
      pemda_indikator pi
    WHERE
      pi.pemda_id = pp.id
  ) AS skor_evaluasi,
  sum(
    CASE
      WHEN lower(gi.innovation_phase::text) = 'inisiatif'::text THEN 50
      WHEN lower(gi.innovation_phase::text) = 'uji coba'::text THEN 102
      WHEN lower(gi.innovation_phase::text) = 'penerapan'::text THEN 105
      ELSE 0
    END
  ) AS total_skor_mandiri,
  pp.created_by
FROM
  profil_pemda pp
  LEFT JOIN government_innovations gi ON gi.pemda_id = pp.id
GROUP BY
  pp.id,
  pp.nama_daerah`,
})
export class PeringkatHasilReview {
	@ViewColumn()
	id: number;

	@ViewColumn()
	nama_daerah: string;

	@ViewColumn()
	jumlah_inovasi: number;

	@ViewColumn()
	isp: number;

	@ViewColumn()
	total_pemda: number;

	@ViewColumn()
	total_skor_verifikasi: number;

	@ViewColumn()
	predikat: string;

	@ViewColumn()
	nominator: string;

	@ViewColumn()
	skor: number;

	@ViewColumn()
	skor_evaluasi: number;

	@ViewColumn()
	total_skor_mandiri: number;

	@ViewColumn()
	created_by: string;
}
