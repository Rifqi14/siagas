import { AppBaseEntity } from 'src/types/lib';
import {
  Column,
  CreateDateColumn,
  Entity,
  JoinColumn,
  ManyToOne,
  OneToMany,
  PrimaryGeneratedColumn,
  UpdateDateColumn
} from 'typeorm';
import Indicator from './Indicator.entity';
import { PemdaIndikatorFile } from './PemdaIndikatorFile.entity';
import { ProfilPemda } from './ProfilePemda.entity';

@Entity({ name: 'pemda_indikator' })
export class PemdaIndikator extends AppBaseEntity {
  @PrimaryGeneratedColumn()
  id: number;

  @Column()
  pemda_id: number;

  @Column()
  indikator_id: number;

  @Column()
  informasi: string;

  @Column()
  keterangan: string;

  @Column()
  catatan: string;

  @Column()
  skor: number;

  @Column()
  nilai_sebelum: number;

  @Column()
  nilai_sesudah: number;

  @Column()
  skor_verifikasi: string;

  @Column()
  data_saat_ini: string;

  @CreateDateColumn()
  created_at: Date;

  @UpdateDateColumn()
  updated_at: Date;

  @ManyToOne(() => ProfilPemda)
  @JoinColumn({ name: 'pemda_id' })
  pemda: ProfilPemda;

  @ManyToOne(() => Indicator, { eager: true })
  @JoinColumn({ name: 'indikator_id' })
  indicator: Indicator;

  @OneToMany(() => PemdaIndikatorFile, file => file.pemda_indikator, {
    eager: true
  })
  files: PemdaIndikatorFile[];
}
