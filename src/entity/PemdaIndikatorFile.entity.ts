import { AppBaseEntity } from 'src/types/lib';
import {
  Column,
  Entity,
  JoinColumn,
  ManyToOne,
  PrimaryGeneratedColumn
} from 'typeorm';
import { PemdaIndikator } from './PemdaIndikator.entity';
import File from './File.entity';
import { ProfilPemda } from './ProfilePemda.entity';
import Indicator from './Indicator.entity';

@Entity({ name: 'pemda_indikator_file' })
export class PemdaIndikatorFile extends AppBaseEntity {
  @PrimaryGeneratedColumn()
  id: number;

  @Column()
  pemda_indikator_id: number;

  @Column()
  pemda_id: number;

  @Column()
  indikator_id: number;

  @Column()
  file_id: number;

  @Column()
  nomor_surat: string;

  @Column()
  tanggal_surat: string;

  @Column()
  nama_surat: string;

  @ManyToOne(() => ProfilPemda)
  @JoinColumn({ name: 'pemda_id' })
  pemda: ProfilPemda;

  @ManyToOne(() => Indicator, { eager: true })
  @JoinColumn({ name: 'indikator_id' })
  indicator: Indicator;

  @ManyToOne(() => PemdaIndikator)
  @JoinColumn({ name: 'pemda_indikator_id' })
  pemda_indikator: PemdaIndikator;

  @ManyToOne(() => File, { eager: true })
  @JoinColumn({ name: 'file_id' })
  file: File;
}
