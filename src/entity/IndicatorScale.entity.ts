import {
  BaseEntity,
  Column,
  CreateDateColumn,
  Entity,
  JoinColumn,
  ManyToOne,
  PrimaryGeneratedColumn,
  UpdateDateColumn
} from 'typeorm';
import Indicator from './Indicator.entity';

@Entity({ name: 'indicator_scales' })
export default class IndicatorScale extends BaseEntity {
  @PrimaryGeneratedColumn()
  id: number;

  @Column({ name: 'indicator_id' })
  indicator_id: number;

  @Column()
  value: number;

  @Column()
  start_at: number;

  @Column()
  finish_at: number;

  @Column()
  description: string;

  @CreateDateColumn()
  created_at: Date;

  @UpdateDateColumn()
  updated_at: Date;
}
