import {
  BaseEntity,
  Column,
  CreateDateColumn,
  Entity,
  JoinColumn,
  ManyToOne,
  PrimaryGeneratedColumn,
  UpdateDateColumn
} from 'typeorm';
import RegionalApparatus from './RegionApparatus.entity';

@Entity({ name: 'uptd' })
export default class Uptd extends BaseEntity {
  @PrimaryGeneratedColumn()
  id: number;

  @Column()
  regional_apparatus_id: number;

  @Column()
  name: string;

  @CreateDateColumn()
  created_at: Date;

  @Column()
  created_by: string;

  @UpdateDateColumn()
  updated_at: Date;

  @Column()
  updated_by: string;

  @ManyToOne(
    () => RegionalApparatus,
    regionalApparatus => regionalApparatus.uptds,
    { eager: true }
  )
  @JoinColumn({ name: 'regional_apparatus_id' })
  regionalApparatus: RegionalApparatus;
}
