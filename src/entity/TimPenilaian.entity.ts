import { CustomBaseEntity } from 'src/types/lib';
import { Column, Entity, PrimaryGeneratedColumn } from 'typeorm';

@Entity({ name: 'tim_penilaian' })
export class TimPenilaian extends CustomBaseEntity {
  @PrimaryGeneratedColumn()
  id: number;

  @Column()
  asn_username: string;

  @Column()
  nama: string;

  @Column()
  instansi: string;
}
