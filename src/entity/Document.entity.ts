import {
  BaseEntity,
  Column,
  CreateDateColumn,
  Entity,
  JoinColumn,
  ManyToOne,
  PrimaryGeneratedColumn,
  UpdateDateColumn
} from 'typeorm';
import DocumentCategory from './DocumentCategory.entity';
import File from './File.entity';

@Entity({ name: 'documents', orderBy: { created_at: 'DESC' } })
export default class Document extends BaseEntity {
  @PrimaryGeneratedColumn()
  id: number;

  @Column({ nullable: true })
  category_id: number | null;

  @Column()
  title: string;

  @Column()
  content: string;

  @Column({ nullable: true })
  document_id: number | null;

  @CreateDateColumn()
  created_at: number;

  @UpdateDateColumn()
  updated_at: number;

  @ManyToOne(() => DocumentCategory, { eager: true })
  @JoinColumn({ name: 'category_id' })
  category: DocumentCategory;

  @ManyToOne(() => File, { eager: true })
  @JoinColumn({ name: 'document_id' })
  document: File;
}
