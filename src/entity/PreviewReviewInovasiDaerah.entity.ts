import { AppBaseEntity } from 'src/types/lib';
import {
  Column,
  Entity,
  JoinColumn,
  ManyToOne,
  OneToMany,
  PrimaryGeneratedColumn
} from 'typeorm';
import { ReviewInovasiDaerah } from './ReviewInovasiDaerah.entity';
import { EvaluasiReviewInovasiDaerah } from './EvaluasiReviewInovasiDaerah.entity';

@Entity({ name: 'preview_review_inovasi_daerah' })
export class PreviewReviewInovasiDaerah extends AppBaseEntity {
  @PrimaryGeneratedColumn()
  id: number;

  @Column('varchar')
  komentar?: string;

  @Column('bigint')
  review_inovasi_daerah_id: number;

  @ManyToOne(() => ReviewInovasiDaerah)
  @JoinColumn({ name: 'review_inovasi_daerah_id' })
  review_inovasi_daerah: ReviewInovasiDaerah;

  @OneToMany(() => EvaluasiReviewInovasiDaerah, evaluasi => evaluasi.preview)
  evaluasi: EvaluasiReviewInovasiDaerah[];
}
