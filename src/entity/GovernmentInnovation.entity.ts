import { CustomBaseEntity, OmitType } from "src/types/lib";
import { Column, CreateDateColumn, Entity, JoinColumn, ManyToOne, OneToMany, PrimaryGeneratedColumn, UpdateDateColumn } from "typeorm";
import File from "./File.entity";
import GovernmentSector from "./GovernmentSector";
import User from "./User.entity";
import { InovasiIndikator } from "./InovasiIndikator.entity";
import { ProfilPemda } from "./ProfilePemda.entity";

@Entity({ name: "government_innovations" })
export default class GovernmentInnovation extends CustomBaseEntity {
	@PrimaryGeneratedColumn()
	id: number;

	@Column()
	government_name: string;

	@Column()
	innovation_name: string;

	@Column()
	innovation_phase: string;

	@Column()
	innovation_initiator: string;

	@Column()
	innovation_type: string;

	@Column()
	innovation_form: string;

	@Column()
	thematic: string;

	@Column()
	first_field: string;

	@Column()
	other_fields: string;

	@Column({ type: "date" })
	trial_time: string;

	@Column({ type: "date" })
	implementation_time: string;

	@Column()
	design: string;

	@Column()
	purpose: string;

	@Column()
	benefit: string;

	@Column()
	result: string;

	@Column()
	budget_file_id: number;

	@Column()
	profile_file_id: number;

	@Column()
	foto_id?: number;

	@Column()
	thematic_detail: string;

	@Column()
	government_sector_id: number;

	@Column()
	pemda_id: number;

	@CreateDateColumn()
	created_at: Date;

	@UpdateDateColumn()
	updated_at: Date;

	@ManyToOne(() => GovernmentSector, { cascade: true })
	@JoinColumn({ name: "government_sector_id" })
	urusan: GovernmentSector;

	@ManyToOne(() => User, { eager: true })
	@JoinColumn({ name: "government_name", referencedColumnName: "username" })
	pemda: OmitType<User, "role">;

	@OneToMany(() => InovasiIndikator, indikator => indikator.inovasi, {
		cascade: true,
		eager: true,
	})
	indikator: InovasiIndikator[];

	@ManyToOne(() => File)
	@JoinColumn({ name: "budget_file_id" })
	budgetFile: Omit<File, "announcements">;

	@ManyToOne(() => File)
	@JoinColumn({ name: "profile_file_id" })
	profileFile: Omit<File, "announcements">;

	@ManyToOne(() => File)
	@JoinColumn({ name: "foto_id" })
	fotoFile: Omit<File, "announcements">;

	@ManyToOne(() => ProfilPemda)
	@JoinColumn({ name: "pemda_id" })
	profilPemda: ProfilPemda;
}
