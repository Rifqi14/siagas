import { Application, Router } from 'express';
import DocumentCategoryController from '../controller/document_category.controller';
import { authenticated } from '../middleware/jwt.middleware';
import { validate } from '../middleware/validation.middleware';
import { createDocumentCategorySchema } from '../schema/document_category.schema';
import BaseRoutes from './base.routes';

class DocumentCategoryRoutes extends BaseRoutes<DocumentCategoryController> {
  constructor(express: Application) {
    super(express, new DocumentCategoryController(), '/kategori_dokumen');
  }

  routes(): Application {
    let express = this.express;
    const route = Router();

    route.use(authenticated);
    route.get('', this.controller.get);
    route.get('/:id', this.controller.detail);
    route.post(
      '',
      validate(createDocumentCategorySchema),
      this.controller.create
    );
    route.patch(
      '/:id',
      validate(createDocumentCategorySchema),
      this.controller.update
    );
    route.delete('/:id', this.controller.delete);

    express.use(this.prefixRoutes, route);
    return express;
  }
}

export default DocumentCategoryRoutes;
