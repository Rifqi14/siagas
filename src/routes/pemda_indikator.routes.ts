import PemdaIndikatorController from 'src/controller/pemda_indikator.controller';
import BaseRoutes from './base.routes';
import { Application, Router } from 'express';
import { authenticated } from 'src/middleware/jwt.middleware';

class PemdaIndikatorRoutes extends BaseRoutes<PemdaIndikatorController> {
  constructor(express: Application) {
    super(express, new PemdaIndikatorController(), '/profil_pemda');
  }

  routes(): Application {
    let express = this.express;
    const route = Router();

    route.use(authenticated);
    route.post('/:pemda_id/indikator', this.controller.create);
    route.get('/:pemda_id/indikator', this.controller.get);
    route.get('/:pemda_id/indikator/:indikator_id', this.controller.detail);
    route.patch(
      '/:pemda_id/indikator/:pemda_indikator_id',
      this.controller.update
    );
    route.delete(
      '/:pemda_id/indikator/:pemda_indikator_id',
      this.controller.delete
    );

    express.use(this.prefixRoutes, route);
    return express;
  }
}

export default PemdaIndikatorRoutes;
