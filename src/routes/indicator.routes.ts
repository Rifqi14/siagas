import { Application, Router } from 'express';
import IndicatorController from '../controller/indicator.controller';
import { authenticated } from '../middleware/jwt.middleware';
import BaseRoutes from './base.routes';

class IndicatorRoutes extends BaseRoutes<IndicatorController> {
  constructor(express: Application) {
    super(express, new IndicatorController(), '/indikator');
  }

  routes(): Application {
    let express = this.express;
    const route = Router();

    route.use(authenticated);
    route.get('', this.controller.get);
    route.get('/:id', this.controller.detail);
    route.post('', this.controller.create);
    route.patch('/:id', this.controller.update);
    route.delete('/:id', this.controller.delete);

    express.use(this.prefixRoutes, route);

    return express;
  }
}

export default IndicatorRoutes;
