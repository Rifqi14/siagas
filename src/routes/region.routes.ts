import { Application } from 'express';
import RegionController from '../controller/region.controller';
import { authenticated } from '../middleware/jwt.middleware';
import BaseRoutes from './base.routes';

class RegionRoutes extends BaseRoutes<RegionController> {
  constructor(express: Application) {
    super(express, new RegionController(), '/daerah');
  }

  routes(): Application {
    let express = this.express;
    express.get(this.prefixRoutes + '', this.controller.get);
    express.get(this.prefixRoutes + '/:id', this.controller.detail);
    express.post(this.prefixRoutes + '', this.controller.create);
    express.patch(this.prefixRoutes + '/:id', this.controller.update);
    express.delete(this.prefixRoutes + '/:id', this.controller.delete);

    return express;
  }
}

export default RegionRoutes;
