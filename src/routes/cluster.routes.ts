import { Application, Router } from 'express';
import ClusterController from '../controller/cluster.controller';
import { authenticated } from '../middleware/jwt.middleware';
import { validate } from '../middleware/validation.middleware';
import { createClusterSchema } from '../schema/cluster.schema';
import { createClusterDetailSchema } from '../schema/clusterDetail.schema';
import BaseRoutes from './base.routes';

class ClusterRoutes extends BaseRoutes<ClusterController> {
  constructor(express: Application) {
    super(express, new ClusterController(), '/cluster');
  }

  routes(): Application {
    let express = this.express;
    const route = Router();

    route.use(authenticated);
    route.get('', this.controller.get);
    route.get('/:id', this.controller.detail);
    route.post('', validate(createClusterSchema), this.controller.create);
    route.patch('/:id', validate(createClusterSchema), this.controller.update);
    route.delete('/:id', this.controller.delete);
    // Detail route
    route.get('/:id/detail', this.controller.getDetails);
    route.get('/:id/detail/:region_id', this.controller.getDetail);
    route.post(
      '/:id/detail',
      validate(createClusterDetailSchema),
      this.controller.createDetail
    );
    route.patch(
      '/:id/detail/:region_id',
      validate(createClusterDetailSchema),
      this.controller.updateDetail
    );
    route.delete('/:id/detail/:region_id', this.controller.deleteDetail);

    express.use(this.prefixRoutes, route);
    return express;
  }
}

export default ClusterRoutes;
