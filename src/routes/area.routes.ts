import { Application, Router } from 'express';
import AreaController from '../controller/area.controller';
import { authenticated } from '../middleware/jwt.middleware';
import BaseRoutes from './base.routes';

class AreaRoutes extends BaseRoutes<AreaController> {
  constructor(express: Application) {
    super(express, new AreaController(), '/wilayah');
  }

  routes(): Application {
    let express = this.express;
    const route = Router();

    route.use(authenticated);
    route.get('', this.controller.get);
    route.get('/:id', this.controller.detail);
    route.post('', this.controller.create);
    route.patch('/:id', this.controller.update);
    route.delete('/:id', this.controller.delete);

    express.use(this.prefixRoutes, route);
    return express;
  }
}

export default AreaRoutes;
