import PaktaIntegritasController from 'src/controller/pakta-integritas.controller';
import BaseRoutes from './base.routes';
import { Application, Router } from 'express';
import { authenticated } from 'src/middleware/jwt.middleware';
import { storage, upload } from 'src/middleware/file.middleware';

class PaktaIntegritasRoutes extends BaseRoutes<PaktaIntegritasController> {
  constructor(express: Application) {
    super(express, new PaktaIntegritasController(), '/pakta_integritas');
  }

  routes(): Application {
    let express = this.express;
    const route = Router();

    route.use(authenticated);
    route.get('', this.controller.get);
    route.get('/latest', this.controller.getLatest);
    route.get('/:id', this.controller.detail);
    route.post(
      '',
      upload(storage('public/upload/pakta_integritas/')).single('file'),
      this.controller.create
    );
    route.patch(
      '/:id',
      upload(storage('public/upload/pakta_integritas/')).single('file'),
      this.controller.update
    );
    route.delete('/:id', this.controller.delete);

    express.use(this.prefixRoutes, route);
    return express;
  }
}

export default PaktaIntegritasRoutes;
