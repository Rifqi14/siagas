import { InovasiDitolakController } from 'src/controller/inovasi_ditolak.controller';
import BaseRoutes from './base.routes';
import { Application, Router } from 'express';
import { authenticated } from 'src/middleware/jwt.middleware';

export class InovasiDitolakRoute extends BaseRoutes<InovasiDitolakController> {
	constructor(express: Application) {
		super(express, new InovasiDitolakController(), '/inovasi_ditolak');
	}

	routes(): Application {
		let express = this.express;
		const route = Router();

		route.use(authenticated);
		route.get('', this.controller.get);
		route.get('/download/:type', this.controller.getDownload);
		route.get('/:review_inovasi_id', this.controller.detail);
		route.patch('/:review_inovasi_id', this.controller.update);

		express.use(this.prefixRoutes, route);
		return express;
	}
}
