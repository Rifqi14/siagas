import { DashboardController } from "src/controller/dashboard.controller";
import BaseRoutes from "./base.routes";
import { Application, Router } from "express";
import { authenticated } from "src/middleware/jwt.middleware";

export class DashboardRoute extends BaseRoutes<DashboardController> {
	constructor(express: Application) {
		super(express, new DashboardController(), "/dashboard");
	}

	routes(): Application {
		let express = this.express;
		const route = Router();

		route.get("/arsip", this.controller.arsip);
		route.get("/arsip/download/:type", this.controller.arsip_download);
		route.get("/statistik_indikator", this.controller.statistik_indikator);
		route.get("/statistik_inovasi", this.controller.statistik_inovasi);
		route.get("/statistik_opd", this.controller.statistik_opd);

		express.use(this.prefixRoutes, route);
		return express;
	}
}
