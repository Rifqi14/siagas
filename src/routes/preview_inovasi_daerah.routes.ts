import { PreviewReviewInovasiDaerahController } from 'src/controller/preview_inovasi_daerah.controller';
import BaseRoutes from './base.routes';
import { Application, Router } from 'express';
import { authenticated } from 'src/middleware/jwt.middleware';

export class PreviewReviewInovasiDaerahRoutes extends BaseRoutes<PreviewReviewInovasiDaerahController> {
  constructor(express: Application) {
    super(
      express,
      new PreviewReviewInovasiDaerahController(),
      '/review_inovasi_daerah/:review_id/preview'
    );
  }

  routes(): Application {
    let express = this.express;
    const route = Router();

    route.use(authenticated);
    route.get('', this.controller.get);
    route.get('/:id', this.controller.detail);
    route.post('', this.controller.create);
    route.patch('/:id', this.controller.update);
    route.delete('/:id', this.controller.delete);

    express.use(this.prefixRoutes, route);
    return express;
  }
}
