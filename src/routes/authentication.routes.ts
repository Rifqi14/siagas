import { Application, Router } from 'express';
import AuthenticationController from '../controller/authentication.controller';
import BaseRoutes from './base.routes';
import { authenticated } from '../middleware/jwt.middleware';

class AuthenticationRoutes extends BaseRoutes<AuthenticationController> {
  constructor(express: Application) {
    super(express, new AuthenticationController(), '/authentication');
  }

  routes(): Application {
    let express = this.express;
    const route = Router();

    route.get('/check_email', this.controller.checkEmail);
    route.get('/me', authenticated, this.controller.me);
    route.post('/login', this.controller.login);

    express.use(this.prefixRoutes, route);

    return express;
  }
}

export default AuthenticationRoutes;
