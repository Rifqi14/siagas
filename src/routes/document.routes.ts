import { Application, Router } from 'express';
import DocumentController from '../controller/document.controller';
import { storage, upload } from '../middleware/file.middleware';
import { authenticated } from '../middleware/jwt.middleware';
import { validate } from '../middleware/validation.middleware';
import { createDocumentSchema } from '../schema/document.schema';
import BaseRoutes from './base.routes';

class DocumentRoutes extends BaseRoutes<DocumentController> {
  constructor(express: Application) {
    super(express, new DocumentController(), '/dokumen');
  }

  routes(): Application {
    let express = this.express;
    const route = Router();

    route.use(authenticated);
    route.get('', this.controller.get);
    route.get('/:id', this.controller.detail);
    route.post(
      '',
      upload(storage('public/upload/dokumen/')).single('document'),
      validate(createDocumentSchema),
      this.controller.create
    );
    route.patch(
      '/:id',
      upload(storage('public/upload/dokumen/')).single('document'),
      validate(createDocumentSchema),
      this.controller.update
    );
    route.delete('/:id', this.controller.delete);

    express.use(this.prefixRoutes, route);
    return express;
  }
}

export default DocumentRoutes;
