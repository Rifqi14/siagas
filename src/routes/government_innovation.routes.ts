import { Application, Router } from "express";
import GovernmentInnovationController from "../controller/government_innovation.controller";
import { storage, upload } from "../middleware/file.middleware";
import { authenticated } from "../middleware/jwt.middleware";
import { validate } from "../middleware/validation.middleware";
import { createGovernmentInnovationSchema } from "../schema/government_innovation.schema";
import BaseRoutes from "./base.routes";

class GovernmentInnovationRoutes extends BaseRoutes<GovernmentInnovationController> {
	constructor(express: Application) {
		super(
			express,
			new GovernmentInnovationController(),
			"/inovasi_pemerintah_daerah"
		);
	}

	routes(): Application {
		let express = this.express;
		const route = Router();

		route.use(authenticated);
		route.get("", this.controller.get);
		route.get("/download/:type", this.controller.download);
		route.get("/:id", this.controller.detail);
		route.get("/:id/download/:type", this.controller.downloadDetail);
		route.post(
			"",
			upload(storage("public/upload/inovasi_pemda/")).fields([
				{ name: "anggaran_file", maxCount: 1 },
				{ name: "profile_file", maxCount: 1 },
				{ name: "foto", maxCount: 1 },
			]),
			this.controller.create
		);
		route.patch(
			"/:id",
			upload(storage("public/upload/inovasi_pemda/")).fields([
				{ name: "anggaran_file", maxCount: 1 },
				{ name: "profile_file", maxCount: 1 },
				{ name: "foto", maxCount: 1 },
			]),
			this.controller.update
		);
		route.delete("/:id", this.controller.delete);

		express.use(this.prefixRoutes, route);
		return express;
	}
}

export default GovernmentInnovationRoutes;
