import { RangkingIndexController } from "src/controller/rangking_index.controller";
import BaseRoutes from "./base.routes";
import { Application, Router } from "express";
import { authenticated } from "src/middleware/jwt.middleware";

export class RangkingIndexRoute extends BaseRoutes<RangkingIndexController> {
	constructor(express: Application) {
		super(express, new RangkingIndexController(), "/ranking_index");
	}

	routes(): Application {
		let express = this.express;
		const route = Router();

		route.use(authenticated);
		route.get("", this.controller.get);
		route.get("/download/:type", this.controller.getDownload);
		route.patch("/:id", this.controller.update);

		express.use(this.prefixRoutes, route);
		return express;
	}
}
