import { Application, Router } from 'express';
import SettingController from '../controller/setting.controller';
import { authenticated } from '../middleware/jwt.middleware';
import { validate } from '../middleware/validation.middleware';
import { createSettingSchema } from '../schema/setting.schema';
import BaseRoutes from './base.routes';

class SettingRoutes extends BaseRoutes<SettingController> {
  constructor(express: Application) {
    super(express, new SettingController(), '/setting');
  }

  routes(): Application {
    let express = this.express;
    const route = Router();

    route.use(authenticated);
    route.get('', this.controller.get);
    route.get('/:id', this.controller.detail);
    route.post('', validate(createSettingSchema), this.controller.create);
    route.patch('/:id', validate(createSettingSchema), this.controller.update);
    route.delete('/:id', this.controller.delete);

    express.use(this.prefixRoutes, route);
    return express;
  }
}

export default SettingRoutes;
