import { Application } from 'express';
import HealthController from '../controller/health.controller';
import BaseRoutes from './base.routes';

class HealthRoutes extends BaseRoutes<HealthController> {
	constructor(express: Application) {
		const controller: HealthController = new HealthController();
		const prefix: string = '/health';
		super(express, controller, prefix);
	}

	routes(): Application {
		let express: Application = this.express;

		express.get(this.prefixRoutes, this.controller.health);
		express.get('/template', this.controller.template);

		return express;
	}
}

export default HealthRoutes;
