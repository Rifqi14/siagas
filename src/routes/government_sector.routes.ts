import { Application, Router } from 'express';
import { validate } from '../middleware/validation.middleware';
import GovernmentSectorController from '../controller/government_sector.controller';
import BaseRoutes from './base.routes';
import { createGovernmentSectorSchema } from '../schema/government_sector.schema';
import { authenticated } from '../middleware/jwt.middleware';

class GovSectorRoutes extends BaseRoutes<GovernmentSectorController> {
  constructor(express: Application) {
    super(express, new GovernmentSectorController(), '/urusan_pemerintahan');
  }

  routes(): Application {
    let express = this.express;
    const route = Router();

    route.use(authenticated);
    route.get('', this.controller.get);
    route.get('/:id', this.controller.detail);
    route.post(
      '',
      validate(createGovernmentSectorSchema),
      this.controller.create
    );
    route.patch(
      '/:id',
      validate(createGovernmentSectorSchema),
      this.controller.update
    );
    route.delete('/:id', this.controller.delete);

    express.use(this.prefixRoutes, route);

    return express;
  }
}

export default GovSectorRoutes;
