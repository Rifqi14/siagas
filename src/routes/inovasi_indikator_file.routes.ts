import { InovasiIndikatorFileController } from 'src/controller/inovasi_indikator_file.controller';
import BaseRoutes from './base.routes';
import { Application, Router } from 'express';
import { authenticated } from 'src/middleware/jwt.middleware';
import { storage, upload } from 'src/middleware/file.middleware';
import { validate } from 'src/middleware/validation.middleware';
import { uploadInovasiIndikatorFileSchema } from 'src/schema/inovasi_indikator_file.schema';

export class InovasiIndikatorFileRoutes extends BaseRoutes<InovasiIndikatorFileController> {
  constructor(express: Application) {
    super(
      express,
      new InovasiIndikatorFileController(),
      '/inovasi_pemerintah_daerah/indikator'
    );
  }

  routes(): Application {
    let express = this.express;
    const route = Router();

    route.use(authenticated);
    route.get('/:inovasi_id/:indikator_id/files', this.controller.get);
    route.get(
      '/:inovasi_id/:indikator_id/files/:file_id',
      this.controller.detail
    );
    route.post(
      '/:inovasi_id/:indikator_id/upload',
      upload(storage('public/upload/inovasi/dokumen/')).single('dokumen'),
      validate(uploadInovasiIndikatorFileSchema),
      this.controller.upload
    );
    route.delete(
      '/:inovasi_id/:indikator_id/delete/:file_id',
      this.controller.delete
    );

    express.use(this.prefixRoutes, route);
    return express;
  }
}
