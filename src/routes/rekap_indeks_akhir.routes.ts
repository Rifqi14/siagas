import { Application, Router } from 'express';
import { RekapIndeksAkhirController } from 'src/controller/rekap_indeks_akhir.controller';
import { authenticated } from 'src/middleware/jwt.middleware';
import BaseRoutes from './base.routes';

export class RekapIndeksAkhirRoute extends BaseRoutes<RekapIndeksAkhirController> {
	constructor(express: Application) {
		super(express, new RekapIndeksAkhirController(), '/rekap_indeks_akhir');
	}

	routes(): Application {
		let express = this.express;
		const route = Router();

		route.use(authenticated);
		route.get('', this.controller.get);
		route.get('/download/:type', this.controller.getDownload);
		route.patch('/:id', this.controller.update);

		express.use(this.prefixRoutes, route);
		return express;
	}
}
