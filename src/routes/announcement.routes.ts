import { Application, Router } from 'express';
import AnnouncementController from '../controller/announcement.controller';
import { storage, upload } from '../middleware/file.middleware';
import { authenticated } from '../middleware/jwt.middleware';
import { validate } from '../middleware/validation.middleware';
import { createAnnouncementSchema } from '../schema/announcement.schema';
import BaseRoutes from './base.routes';

class AnnouncementRoutes extends BaseRoutes<AnnouncementController> {
  constructor(express: Application) {
    super(express, new AnnouncementController(), '/pengumuman');
  }

  routes(): Application {
    let express = this.express;
    const route = Router();

    route.get('', this.controller.get);
    route.get('/:id', this.controller.detail);
    route.use(authenticated);
    route.post(
      '',
      upload(storage('public/upload/pengumuman/')).single('document'),
      validate(createAnnouncementSchema),
      this.controller.create
    );
    route.patch(
      '/:id',
      upload(storage('public/upload/pengumuman/')).single('document'),
      validate(createAnnouncementSchema),
      this.controller.update
    );
    route.delete('/:id', this.controller.delete);

    express.use(this.prefixRoutes, route);
    return express;
  }
}

export default AnnouncementRoutes;
