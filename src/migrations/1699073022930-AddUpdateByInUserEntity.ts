import { MigrationInterface, QueryRunner, TableColumn, TableForeignKey } from 'typeorm';

export class AddUpdateByInUserEntity1699073022930 implements MigrationInterface {
	public async up(queryRunner: QueryRunner): Promise<void> {
		await queryRunner.addColumn(
			'users',
			new TableColumn({
				name: 'created_by',
				type: 'bigint',
				isNullable: true,
			})
		);

		await queryRunner.createForeignKey(
			'users',
			new TableForeignKey({
				columnNames: ['created_by'],
				referencedColumnNames: ['id'],
				referencedTableName: 'users',
				onDelete: 'set null',
				name: 'fk_created_by_users',
			})
		);
	}

	public async down(queryRunner: QueryRunner): Promise<void> {
		await queryRunner.dropForeignKey('users', 'fk_created_by_users');
		await queryRunner.dropColumn('users', 'created_by');
	}
}
