import {
  MigrationInterface,
  QueryRunner,
  TableColumn,
  TableForeignKey
} from 'typeorm';

export class AddDocumentCategoryToEvaluasiInovasi1691572448206
  implements MigrationInterface
{
  public async up(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.addColumns('evaluasi_inovasi_daerah', [
      new TableColumn({
        name: 'judul',
        type: 'varchar',
        isNullable: true
      }),
      new TableColumn({
        name: 'document_category_id',
        type: 'bigint',
        isNullable: true
      })
    ]);

    await queryRunner.createForeignKey(
      'evaluasi_inovasi_daerah',
      new TableForeignKey({
        columnNames: ['document_category_id'],
        referencedColumnNames: ['id'],
        referencedTableName: 'document_categories'
      })
    );
  }

  public async down(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.dropForeignKey(
      'evaluasi_inovasi_daerah',
      new TableForeignKey({
        columnNames: ['document_category_id'],
        referencedColumnNames: ['id'],
        referencedTableName: 'document_categories'
      })
    );

    await queryRunner.dropColumns('evaluasi_inovasi_daerah', [
      new TableColumn({
        name: 'judul',
        type: 'varchar',
        isNullable: true
      }),
      new TableColumn({
        name: 'document_category_id',
        type: 'bigint',
        isNullable: true
      })
    ]);
  }
}
