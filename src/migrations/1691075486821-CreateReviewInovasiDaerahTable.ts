import { MigrationInterface, QueryRunner, Table } from 'typeorm';

export class CreateReviewInovasiDaerahTable1691075486821
  implements MigrationInterface
{
  public async up(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.createTable(
      new Table({
        name: 'review_inovasi_daerah',
        columns: [
          {
            name: 'id',
            type: 'serial',
            isPrimary: true,
            primaryKeyConstraintName: 'pk_review_inovasi_daerah'
          },
          {
            name: 'random_number',
            type: 'bigint',
            isNullable: true
          },
          {
            name: 'skor',
            type: 'float',
            default: 0
          },
          {
            name: 'status',
            type: 'varchar',
            isNullable: true
          },
          { name: 'inovasi_id', type: 'bigint', isNullable: true },
          { name: 'created_by', type: 'varchar' },
          { name: 'updated_by', type: 'varchar' },
          { name: 'created_at', type: 'timestamp', default: `now()` },
          { name: 'updated_at', type: 'timestamp', default: `now()` }
        ],
        foreignKeys: [
          {
            columnNames: ['inovasi_id'],
            referencedColumnNames: ['id'],
            referencedTableName: 'government_innovations',
            onDelete: 'cascade'
          },
          {
            columnNames: ['created_by'],
            referencedColumnNames: ['username'],
            referencedTableName: 'users'
          },
          {
            columnNames: ['updated_by'],
            referencedColumnNames: ['username'],
            referencedTableName: 'users'
          }
        ]
      }),
      true
    );
  }

  public async down(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.dropTable(
      new Table({ name: 'review_inovasi_daerah' }),
      true
    );
  }
}
