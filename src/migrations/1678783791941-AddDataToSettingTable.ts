import { MigrationInterface, QueryRunner } from 'typeorm';
import settings from '../data/seeder/setting.json';
import Setting from '../entity/Setting.entity';

export class AddDataToSettingTable1678783791941 implements MigrationInterface {
  public async up(queryRunner: QueryRunner): Promise<void> {
    const repo = queryRunner.manager.getRepository(Setting);
    const entites: Setting[] = [];

    for (let i = 0; i < settings.length; i++) {
      const exists = await repo.exist({ where: { key: settings[i].key } });
      if (exists) {
        return;
      }
      const data = repo.create({
        key: settings[i].key,
        value: settings[i].value,
        helper: settings[i].helper
      });
      entites.push(data);
    }

    await repo.insert(entites);
  }

  public async down(queryRunner: QueryRunner): Promise<void> {}
}
