import {
  MigrationInterface,
  QueryRunner,
  TableColumn,
  TableForeignKey,
  TableUnique
} from 'typeorm';

export class UpdateGovernmentInnovationTable1688833567130
  implements MigrationInterface
{
  public async up(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.addColumns('government_innovations', [
      new TableColumn({
        name: 'thematic_detail',
        type: 'text',
        isNullable: true
      }),
      new TableColumn({
        name: 'government_sector_id',
        type: 'bigint',
        isNullable: true
      })
    ]);

    await queryRunner.createUniqueConstraint(
      'users',
      new TableUnique({ columnNames: ['nama_pemda'] })
    );

    await queryRunner.createForeignKeys('government_innovations', [
      new TableForeignKey({
        columnNames: ['government_name'],
        referencedColumnNames: ['nama_pemda'],
        referencedTableName: 'users'
      }),
      new TableForeignKey({
        columnNames: ['government_sector_id'],
        referencedColumnNames: ['id'],
        referencedTableName: 'government_sectors'
      })
    ]);
  }

  public async down(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.dropForeignKeys('government_innovations', [
      new TableForeignKey({
        columnNames: ['government_name'],
        referencedColumnNames: ['nama_pemda'],
        referencedTableName: 'users'
      }),
      new TableForeignKey({
        columnNames: ['government_sector_id'],
        referencedColumnNames: ['id'],
        referencedTableName: 'government_sectors'
      })
    ]);

    await queryRunner.dropUniqueConstraint(
      'users',
      new TableUnique({ columnNames: ['nama_pemda'] })
    );

    await queryRunner.dropColumns('government_innovations', [
      new TableColumn({
        name: 'thematic_detail',
        type: 'text',
        isNullable: true
      }),
      new TableColumn({
        name: 'government_sector_id',
        type: 'bigint',
        isNullable: true
      })
    ]);
  }
}
