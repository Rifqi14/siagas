import { MigrationInterface, QueryRunner, Table } from "typeorm";

export class CreateDistrikTable1697789648894 implements MigrationInterface {
	public async up(queryRunner: QueryRunner): Promise<void> {
		await queryRunner.createTable(
			new Table({
				name: "distrik",
				columns: [
					{ name: "id", type: "serial8", isPrimary: true },
					{ name: "nama_distrik", type: "varchar" },
					{ name: "created_at", type: "timestamp", default: "now()" },
					{ name: "updated_at", type: "timestamp", default: "now()" },
				],
			})
		);
	}

	public async down(queryRunner: QueryRunner): Promise<void> {
		await queryRunner.dropTable(
			new Table({
				name: "distrik",
				columns: [
					{ name: "id", type: "serial8", isPrimary: true },
					{ name: "nama_distrik", type: "varchar" },
					{ name: "created_at", type: "timestamp", default: "now()" },
					{ name: "updated_at", type: "timestamp", default: "now()" },
				],
			})
		);
	}
}
