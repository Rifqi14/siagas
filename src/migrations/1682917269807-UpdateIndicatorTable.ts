import {
  MigrationInterface,
  QueryRunner,
  TableColumn,
  TableForeignKey
} from 'typeorm';

export class UpdateIndicatorTable1682917269807 implements MigrationInterface {
  public async up(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.changeColumns('indicators', [
      {
        oldColumn: new TableColumn({
          name: 'parent',
          type: 'varchar',
          length: '255',
          isNullable: true
        }),
        newColumn: new TableColumn({
          name: 'parent_id',
          type: 'bigint',
          isNullable: true
        })
      },
      {
        oldColumn: new TableColumn({
          name: 'mandatory',
          type: 'varchar',
          length: '255',
          isNullable: true
        }),
        newColumn: new TableColumn({
          name: 'mandatory',
          type: 'boolean',
          default: false
        })
      }
    ]);

    await queryRunner.createForeignKey(
      'indicators',
      new TableForeignKey({
        columnNames: ['parent_id'],
        referencedColumnNames: ['id'],
        referencedTableName: 'indicators',
        name: 'fk_parent_id_indicators'
      })
    );

    await queryRunner.dropColumn(
      'indicators',
      new TableColumn({
        name: 'sub',
        type: 'varchar',
        length: '255',
        isNullable: true
      })
    );
  }

  public async down(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.addColumn(
      'indicators',
      new TableColumn({
        name: 'sub',
        type: 'varchar',
        length: '255',
        isNullable: true
      })
    );

    await queryRunner.dropForeignKey(
      'indicators',
      new TableForeignKey({
        columnNames: ['parent_id'],
        referencedColumnNames: ['id'],
        referencedTableName: 'indicators',
        name: 'fk_parent_id_indicators'
      })
    );

    await queryRunner.changeColumns('indicators', [
      {
        newColumn: new TableColumn({
          name: 'parent',
          type: 'varchar',
          length: '255',
          isNullable: true
        }),
        oldColumn: new TableColumn({
          name: 'parent_id',
          type: 'bigint',
          isNullable: true
        })
      },
      {
        newColumn: new TableColumn({
          name: 'mandatory',
          type: 'varchar',
          length: '255',
          isNullable: true
        }),
        oldColumn: new TableColumn({
          name: 'mandatory',
          type: 'boolean',
          default: false
        })
      }
    ]);
  }
}
