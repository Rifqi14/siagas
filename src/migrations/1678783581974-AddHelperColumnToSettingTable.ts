import { MigrationInterface, QueryRunner, TableColumn } from 'typeorm';

export class AddHelperColumnToSettingTable1678783581974
  implements MigrationInterface
{
  public async up(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.addColumn(
      'settings',
      new TableColumn({
        name: 'helper',
        type: 'varchar',
        isNullable: true,
        comment:
          'Helper digunakan untuk menambahkan text bantuan ketika mengisi value'
      })
    );
  }

  public async down(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.dropColumn('settings', 'helper');
  }
}
