import {
  MigrationInterface,
  QueryRunner,
  Table,
  TableForeignKey
} from 'typeorm';

export class CreateClusterDetailTable1678763517930
  implements MigrationInterface
{
  public async up(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.createTable(
      new Table({
        name: 'cluster_details',
        columns: [
          {
            name: 'cluster_id',
            type: 'bigint'
          },
          {
            name: 'region_id',
            type: 'bigint'
          },
          {
            name: 'created_at',
            type: 'timestamp',
            default: 'now()'
          },
          {
            name: 'updated_at',
            type: 'timestamp',
            default: 'now()'
          }
        ]
      }),
      true
    );

    await queryRunner.createPrimaryKey(
      'cluster_details',
      ['cluster_id', 'region_id'],
      'pk_cluster_details'
    );

    await queryRunner.createForeignKeys('cluster_details', [
      new TableForeignKey({
        columnNames: ['cluster_id'],
        referencedColumnNames: ['id'],
        referencedTableName: 'clusters',
        name: 'fk_cluster_details_cluster_id',
        onDelete: 'cascade'
      }),
      new TableForeignKey({
        columnNames: ['region_id'],
        referencedColumnNames: ['id'],
        referencedTableName: 'regions',
        name: 'fk_cluster_details_region_id',
        onDelete: 'cascade'
      })
    ]);
  }

  public async down(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.dropTable('cluster_details', true);
  }
}
