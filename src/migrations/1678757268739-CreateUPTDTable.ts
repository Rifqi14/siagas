import {
  MigrationInterface,
  QueryRunner,
  Table,
  TableForeignKey,
  TableIndex
} from 'typeorm';

export class CreateUPTDTable1678757268739 implements MigrationInterface {
  public async up(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.createTable(
      new Table({
        name: 'uptd',
        columns: [
          {
            name: 'id',
            type: 'serial8'
          },
          {
            name: 'regional_apparatus_id',
            type: 'bigint'
          },
          {
            name: 'name',
            type: 'varchar',
            length: '255'
          },
          {
            name: 'created_at',
            type: 'timestamp',
            default: 'now()'
          },
          {
            name: 'created_by',
            type: 'varchar',
            length: '255',
            isNullable: true
          },
          {
            name: 'updated_at',
            type: 'timestamp',
            default: 'now()'
          },
          {
            name: 'updated_by',
            type: 'varchar',
            length: '255',
            isNullable: true
          }
        ]
      }),
      true
    );

    await queryRunner.createPrimaryKey('uptd', ['id'], 'pk_uptd');

    await queryRunner.createForeignKey(
      'uptd',
      new TableForeignKey({
        columnNames: ['regional_apparatus_id'],
        referencedColumnNames: ['id'],
        referencedTableName: 'regional_apparatus',
        name: 'fk_uptd_regional_apparatus',
        onDelete: 'cascade'
      })
    );

    await queryRunner.createForeignKeys('regional_apparatus', [
      new TableForeignKey({
        columnNames: ['created_by'],
        referencedColumnNames: ['username'],
        referencedTableName: 'users',
        name: 'fk_uptd_created_by',
        onDelete: 'set null'
      }),
      new TableForeignKey({
        columnNames: ['updated_by'],
        referencedColumnNames: ['username'],
        referencedTableName: 'users',
        name: 'fk_uptd_updated_by',
        onDelete: 'set null'
      })
    ]);

    await queryRunner.createIndex(
      'uptd',
      new TableIndex({
        columnNames: ['name'],
        name: 'idx_uptd_name'
      })
    );
  }

  public async down(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.dropTable('uptd', true);
  }
}
