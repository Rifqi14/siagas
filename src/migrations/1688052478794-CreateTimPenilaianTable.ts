import { MigrationInterface, QueryRunner, Table } from 'typeorm';

export class CreateTimPenilaianTable1688052478794
  implements MigrationInterface
{
  public async up(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.createTable(
      new Table({
        name: 'tim_penilaian',
        columns: [
          { name: 'id', type: 'bigserial', isPrimary: true },
          { name: 'asn_username', type: 'varchar', isNullable: true },
          { name: 'nama', type: 'varchar', isNullable: true },
          { name: 'instansi', type: 'varchar', isNullable: true },
          { name: 'created_by', type: 'varchar', isNullable: true },
          { name: 'updated_by', type: 'varchar', isNullable: true },
          { name: 'created_at', type: 'timestamp', default: `now()` },
          { name: 'updated_at', type: 'timestamp', default: `now()` }
        ],
        foreignKeys: [
          {
            columnNames: ['created_by'],
            referencedColumnNames: ['username'],
            referencedTableName: 'users'
          },
          {
            columnNames: ['updated_by'],
            referencedColumnNames: ['username'],
            referencedTableName: 'users'
          }
        ]
      }),
      true
    );
  }

  public async down(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.dropTable(new Table({ name: 'tim_penilaian' }), true);
  }
}
