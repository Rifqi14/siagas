import { MigrationInterface, QueryRunner, Table } from 'typeorm';

export class CreateFaqTable1678958051471 implements MigrationInterface {
  public async up(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.createTable(
      new Table({
        name: 'faq',
        columns: [
          {
            name: 'id',
            type: 'serial8',
            isPrimary: true,
            primaryKeyConstraintName: 'pk_faq'
          },
          {
            name: 'question',
            type: 'text'
          },
          {
            name: 'answer',
            type: 'text'
          },
          {
            name: 'created_at',
            type: 'timestamp',
            default: 'now()'
          },
          {
            name: 'updated_at',
            type: 'timestamp',
            default: 'now()'
          }
        ]
      }),
      true
    );
  }

  public async down(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.dropTable('faq', true);
  }
}
