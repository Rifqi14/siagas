import { MigrationInterface, QueryRunner, Table } from 'typeorm';

export class CreateFileTable1678783021467 implements MigrationInterface {
  public async up(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.createTable(
      new Table({
        name: 'files',
        columns: [
          {
            name: 'id',
            type: 'serial8',
            primaryKeyConstraintName: 'pk_files',
            isPrimary: true
          },
          { name: 'name', type: 'varchar', length: '255' },
          { name: 'path', type: 'varchar', length: '255' },
          { name: 'extension', type: 'varchar', length: '255' },
          { name: 'size', type: 'varchar', length: '255' },
          { name: 'uploaded_by', type: 'varchar', length: '255' },
          { name: 'created_at', type: 'timestamp', default: 'now()' },
          { name: 'updated_at', type: 'timestamp', default: 'now()' }
        ],
        indices: [{ name: 'fk_files_name', columnNames: ['name'] }]
      }),
      true
    );
  }

  public async down(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.dropTable(new Table({ name: 'files' }), true);
  }
}
