import {
  MigrationInterface,
  QueryRunner,
  Table,
  TableForeignKey
} from 'typeorm';

export class CreateIndicatorScaleTable1678345361293
  implements MigrationInterface
{
  public async up(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.createTable(
      new Table({
        name: 'indicator_scales',
        columns: [
          {
            name: 'id',
            type: 'serial4'
          },
          {
            name: 'indicator_id',
            type: 'int4'
          },
          {
            name: 'value',
            type: 'float8',
            default: 0
          },
          {
            name: 'start_at',
            type: 'float8',
            default: 0
          },
          {
            name: 'finish_at',
            type: 'float8',
            default: 0
          },
          {
            name: 'description',
            type: 'varchar',
            length: '255'
          },
          {
            name: 'created_at',
            type: 'timestamp',
            default: 'now()'
          },
          {
            name: 'updated_at',
            type: 'timestamp',
            default: 'now()'
          }
        ]
      }),
      true
    );

    await queryRunner.createPrimaryKey(
      'indicator_scales',
      ['id'],
      'pk_indicator_scales'
    );

    await queryRunner.createForeignKey(
      'indicator_scales',
      new TableForeignKey({
        columnNames: ['indicator_id'],
        referencedColumnNames: ['id'],
        referencedTableName: 'indicators',
        onDelete: 'cascade',
        name: 'fk_indicator_scales_indicator_id'
      })
    );
  }

  public async down(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.dropTable('indicator_scales', true);
  }
}
