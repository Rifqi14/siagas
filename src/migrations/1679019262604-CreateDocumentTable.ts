import { MigrationInterface, QueryRunner, Table } from 'typeorm';

export class CreateDocumentTable1679019262604 implements MigrationInterface {
  public async up(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.createTable(
      new Table({
        name: 'documents',
        columns: [
          {
            name: 'id',
            type: 'serial8',
            isPrimary: true,
            primaryKeyConstraintName: 'pk_document'
          },
          {
            name: 'category_id',
            type: 'bigint',
            isNullable: true
          },
          {
            name: 'title',
            type: 'varchar',
            length: '255'
          },
          {
            name: 'content',
            type: 'text'
          },
          {
            name: 'document_id',
            type: 'bigint',
            isNullable: true
          },
          {
            name: 'created_at',
            type: 'timestamp',
            default: 'now()'
          },
          {
            name: 'updated_at',
            type: 'timestamp',
            default: 'now()'
          }
        ],
        foreignKeys: [
          {
            columnNames: ['category_id'],
            referencedColumnNames: ['id'],
            referencedTableName: 'document_categories',
            name: 'fk_category_id',
            onDelete: 'set null'
          },
          {
            columnNames: ['document_id'],
            referencedColumnNames: ['id'],
            referencedTableName: 'files',
            name: 'fk_document_id',
            onDelete: 'set null'
          }
        ]
      }),
      true
    );
  }

  public async down(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.dropTable('documents', true);
  }
}
