import { MigrationInterface, QueryRunner, Table } from 'typeorm';

export class CreateTuxedoTable1678830821842 implements MigrationInterface {
  public async up(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.createTable(
      new Table({
        name: 'tuxedos',
        columns: [
          {
            name: 'id',
            type: 'serial8',
            primaryKeyConstraintName: 'pk_tuxedos',
            isPrimary: true
          },
          {
            name: 'title',
            type: 'varchar',
            length: '255'
          },
          {
            name: 'slug',
            type: 'varchar',
            length: '255'
          },
          {
            name: 'section',
            type: 'varchar',
            length: '255'
          },
          {
            name: 'content',
            type: 'text'
          },
          {
            name: 'created_at',
            type: 'timestamp',
            default: 'now()'
          },
          {
            name: 'updated_at',
            type: 'timestamp',
            default: 'now()'
          }
        ],
        indices: [
          {
            columnNames: ['title'],
            name: 'fk_tuxedos_title'
          },
          {
            columnNames: ['slug'],
            name: 'fk_tuxedos_slug'
          }
        ]
      }),
      true
    );
  }

  public async down(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.dropTable('tuxedos', true);
  }
}
