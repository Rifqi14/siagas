import {
	MigrationInterface,
	QueryRunner,
	TableColumn,
	TableForeignKey,
} from "typeorm";

export class AddFotoColumnInGovernmentInnovationTable1697387399831
	implements MigrationInterface
{
	public async up(queryRunner: QueryRunner): Promise<void> {
		await queryRunner.addColumn(
			"government_innovations",
			new TableColumn({
				name: "foto_id",
				type: "bigint",
				isNullable: true,
			})
		);

		await queryRunner.createForeignKey(
			"government_innovations",
			new TableForeignKey({
				name: "fk_foto",
				columnNames: ["foto_id"],
				referencedColumnNames: ["id"],
				referencedTableName: "files",
			})
		);
	}

	public async down(queryRunner: QueryRunner): Promise<void> {
		await queryRunner.dropForeignKey(
			"government_innovations",
			new TableForeignKey({
				name: "fk_foto",
				columnNames: ["foto_id"],
				referencedColumnNames: ["id"],
				referencedTableName: "files",
			})
		);

		await queryRunner.dropColumn(
			"government_innovations",
			new TableColumn({
				name: "foto_id",
				type: "bigint",
				isNullable: true,
			})
		);
	}
}
