import { MigrationInterface, QueryRunner, Table } from 'typeorm';

export class CreateAnnouncementTable1678868516337
  implements MigrationInterface
{
  public async up(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.createTable(
      new Table({
        name: 'announcements',
        columns: [
          {
            name: 'id',
            type: 'serial8',
            primaryKeyConstraintName: 'pk_announcement',
            isPrimary: true
          },
          {
            name: 'title',
            type: 'varchar',
            length: '255'
          },
          {
            name: 'slug',
            type: 'varchar',
            length: '255'
          },
          {
            name: 'file_id',
            type: 'bigint'
          },
          {
            name: 'content',
            type: 'text'
          },
          {
            name: 'created_at',
            type: 'timestamp',
            default: 'now()'
          },
          {
            name: 'updated_at',
            type: 'timestamp',
            default: 'now()'
          }
        ],
        foreignKeys: [
          {
            columnNames: ['file_id'],
            referencedColumnNames: ['id'],
            referencedTableName: 'files',
            name: 'fk_files'
          }
        ],
        indices: [
          {
            columnNames: ['title'],
            name: 'fk_announcement_title'
          },
          {
            columnNames: ['slug'],
            name: 'fk_announcement_slug'
          }
        ]
      }),
      true
    );
  }

  public async down(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.dropTable('announcements', true);
  }
}
