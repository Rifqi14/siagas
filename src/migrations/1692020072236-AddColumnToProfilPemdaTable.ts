import { MigrationInterface, QueryRunner, TableColumn } from 'typeorm';

export class AddColumnToProfilPemdaTable1692020072236
  implements MigrationInterface
{
  public async up(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.addColumns('profil_pemda', [
      new TableColumn({
        name: 'nama_pemda',
        type: 'varchar',
        isNullable: true
      }),
      new TableColumn({ name: 'nominator', type: 'varchar', isNullable: true })
    ]);
  }

  public async down(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.dropColumns('profil_pemda', [
      new TableColumn({
        name: 'nama_pemda',
        type: 'varchar',
        isNullable: true
      }),
      new TableColumn({ name: 'nominator', type: 'varchar', isNullable: true })
    ]);
  }
}
