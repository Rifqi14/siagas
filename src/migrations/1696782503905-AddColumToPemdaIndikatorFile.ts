import {
  MigrationInterface,
  QueryRunner,
  TableColumn,
  TableForeignKey
} from 'typeorm';

export class AddColumToPemdaIndikatorFile1696782503905
  implements MigrationInterface
{
  public async up(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.addColumns('pemda_indikator_file', [
      new TableColumn({ name: 'pemda_id', type: 'bigint', isNullable: true }),
      new TableColumn({
        name: 'indikator_id',
        type: 'bigint',
        isNullable: true
      })
    ]);

    await queryRunner.createForeignKeys('pemda_indikator_file', [
      new TableForeignKey({
        columnNames: ['pemda_id'],
        referencedColumnNames: ['id'],
        referencedTableName: 'profil_pemda'
      }),
      new TableForeignKey({
        columnNames: ['indikator_id'],
        referencedColumnNames: ['id'],
        referencedTableName: 'indicators'
      })
    ]);
  }

  public async down(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.dropColumns('pemda_indikator_file', [
      new TableColumn({ name: 'pemda_id', type: 'bigint', isNullable: true }),
      new TableColumn({
        name: 'indikator_id',
        type: 'bigint',
        isNullable: true
      })
    ]);

    await queryRunner.dropForeignKeys('pemda_indikator_file', [
      new TableForeignKey({
        columnNames: ['pemda_id'],
        referencedColumnNames: ['id'],
        referencedTableName: 'profil_pemda'
      }),
      new TableForeignKey({
        columnNames: ['indikator_id'],
        referencedColumnNames: ['id'],
        referencedTableName: 'indicators'
      })
    ]);
  }
}
