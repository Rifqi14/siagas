import { MigrationInterface, QueryRunner, TableColumn } from 'typeorm';

export class AddTentangToInovasiIndikatorFile1696777532075
  implements MigrationInterface
{
  public async up(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.dropColumn('inovasi_indikator_file', 'nama_surat');

    await queryRunner.addColumn(
      'inovasi_indikator_file',
      new TableColumn({ name: 'tentang', type: 'varchar', isNullable: true })
    );
  }

  public async down(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.addColumn(
      'inovasi_indikator_file',
      new TableColumn({ name: 'nama_surat', type: 'varchar', isNullable: true })
    );

    await queryRunner.dropColumn('inovasi_indikator_file', 'tentang');
  }
}
