import { MigrationInterface, QueryRunner } from 'typeorm';

export class CreateViewArsip1692545438422 implements MigrationInterface {
  public async up(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.query(`create or replace view dashboard_arsip as
            select 
                gi.id,
                pp.id as pemda_id,
                pp.nama_daerah,
                gi.innovation_name,
                gi.innovation_phase,
                gi.trial_time,
                gi.implementation_time,
                case
                    when lower(gi.innovation_phase) = 'inisiatif' then 3
                    when lower(gi.innovation_phase) = 'uji coba' then 6
                    when lower(gi.innovation_phase) = 'penerapan' then 9
                    else 0
                end as skor,
                gi.created_by
            from government_innovations gi
            left join profil_pemda pp on gi.pemda_id = pp.id`);
  }

  public async down(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.query(`drop view dashboard_arsip;`);
  }
}
