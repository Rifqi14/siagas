import { MigrationInterface, QueryRunner, TableColumn } from 'typeorm';

export class AddIsCreatorColumnTable1678519684438
  implements MigrationInterface
{
  public async up(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.addColumn(
      'roles',
      new TableColumn({
        name: 'is_creator',
        type: 'bpchar',
        enum: ['y', 't'],
        default: `'t'`
      })
    );
  }

  public async down(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.dropColumn('roles', 'is_creator');
  }
}
