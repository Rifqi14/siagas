import { MigrationInterface, QueryRunner, Table } from 'typeorm';

export class CreateClusterTable1678762165098 implements MigrationInterface {
  public async up(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.createTable(
      new Table({
        name: 'clusters',
        columns: [
          {
            name: 'id',
            type: 'serial8'
          },
          {
            name: 'name',
            type: 'varchar',
            length: '255'
          },
          {
            name: 'created_at',
            type: 'timestamp',
            default: 'now()'
          },
          {
            name: 'updated_at',
            type: 'timestamp',
            default: 'now()'
          }
        ]
      }),
      true
    );

    await queryRunner.createPrimaryKey('clusters', ['id'], 'pk_clusters');
  }

  public async down(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.dropTable('clusters', true);
  }
}
