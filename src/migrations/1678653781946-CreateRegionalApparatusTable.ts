import {
  MigrationInterface,
  QueryRunner,
  Table,
  TableForeignKey,
  TableIndex
} from 'typeorm';

export class CreateRegionalApparatusTable1678653781946
  implements MigrationInterface
{
  public async up(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.createTable(
      new Table({
        name: 'regional_apparatus',
        columns: [
          {
            name: 'id',
            type: 'serial4'
          },
          {
            name: 'name',
            type: 'varchar',
            length: '255'
          },
          {
            name: 'created_at',
            type: 'timestamp',
            default: 'now()'
          },
          {
            name: 'created_by',
            type: 'varchar',
            length: '255',
            isNullable: true
          },
          {
            name: 'updated_at',
            type: 'timestamp',
            default: 'now()'
          },
          {
            name: 'updated_by',
            type: 'varchar',
            length: '255',
            isNullable: true
          }
        ]
      }),
      true
    );

    await queryRunner.createPrimaryKey(
      'regional_apparatus',
      ['id'],
      'pk_regional_apparatus'
    );

    await queryRunner.createForeignKeys('regional_apparatus', [
      new TableForeignKey({
        columnNames: ['created_by'],
        referencedColumnNames: ['username'],
        referencedTableName: 'users',
        name: 'fk_regional_apparatus_created_by',
        onDelete: 'set null'
      }),
      new TableForeignKey({
        columnNames: ['updated_by'],
        referencedColumnNames: ['username'],
        referencedTableName: 'users',
        name: 'fk_regional_apparatus_updated_by',
        onDelete: 'set null'
      })
    ]);

    await queryRunner.createIndex(
      'regional_apparatus',
      new TableIndex({
        columnNames: ['name'],
        name: 'fk_regional_apparatus_name'
      })
    );
  }

  public async down(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.dropTable('regional_apparatus', true);
  }
}
