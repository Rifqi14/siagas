import app from './app';
import { seedRole } from './providers/seeder';

async function bootstrap(): Promise<void> {
  app.loadServer();
}

bootstrap();
