import { AppDataSource } from '../data-source';
import Role from '../entity/Role.entity';
import roles from '../data/seeder/role.json';

export const seedRole = () => {
  const repo = AppDataSource.getRepository(Role);

  roles.forEach(async value => {
    const role = await repo.findOneBy({ name: value.name });

    if (role) {
      return;
    }

    await repo.insert({ is_creator: value.is_creator, name: value.name }).then(
      val => {
        console.log(`Success seeder ${value.name} data`);
      },
      () => {
        console.log(`Error seeder ${value.name} data`);
      }
    );
  });
};
