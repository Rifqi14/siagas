import {
  AddWorksheetOptions,
  Border,
  Borders,
  Column,
  Style,
  Workbook,
  Worksheet
} from 'exceljs';
import { Stream } from 'stream';
import Config from './config';

interface WriteArgs {
  filename: string;
  stream: Stream;
  sheetname: string;
  dateformat: string;
}

type AtLeast<T, K extends keyof T> = Partial<T> & Pick<T, K>;

export class Excel {
  private workbook: Workbook = new Workbook();
  private activeWs: Worksheet;
  private rowHeader: number;

  public setProperties = (creator: string = Config.load().app_name): this => {
    this.workbook.creator = creator;
    this.workbook.created = new Date();

    return this;
  };

  public setWorksheet = (
    worksheet?: string,
    opts?: Partial<AddWorksheetOptions>
  ): this => {
    this.activeWs = this.workbook.addWorksheet(worksheet, opts);

    return this;
  };

  public setHeader = (header: Array<Partial<Column>>): this => {
    this.activeWs.columns = header;
    this.rowHeader = this.activeWs.rowCount;
    return this;
  };

  public styleSelectedCell = (start: number, end: number): this => {
    this.activeWs.getCell(start, end).style;

    return this;
  };

  public styleHeaderCell = (style: Partial<Style>): this => {
    this.activeWs
      .getRows(this.rowHeader, this.activeWs.columnCount)
      ?.forEach((row, index, rows) => {
        row.eachCell((cell, colNumber) => {
          cell.style = style;
        });
      });
    return this;
  };

  public setStyleAll = (style: Partial<Style>): this => {
    this.activeWs.eachRow({ includeEmpty: false }, (row, rowNumber) => {
      row.eachCell({ includeEmpty: false }, (cell, colNumber) => {
        cell.style = style;
      });
    });

    return this;
  };

  public addRow = (data: unknown[]): this => {
    this.activeWs.addRows(data);
    return this;
  };

  private writeXlsx = async (
    args: AtLeast<WriteArgs, 'stream' | 'filename'>
  ): Promise<void> => {
    await this.workbook.xlsx.write(args.stream, { filename: args.filename });
  };

  public write = async (
    format: 'xlsx' | 'csv',
    args: AtLeast<WriteArgs, 'stream' | 'filename'>
  ) => {
    switch (format) {
      case 'xlsx':
        await this.writeXlsx(args);
        break;

      default:
        await this.writeXlsx(args);
        break;
    }
  };
}
