import * as bcrypt from 'bcrypt';

class Hash {
  private salt: number = 10;

  generate(value: string): string {
    return bcrypt.hashSync(value, this.salt);
  }

  compare(plain: string, hash: string): boolean {
    return bcrypt.compareSync(plain, hash);
  }
}

export default Hash;
