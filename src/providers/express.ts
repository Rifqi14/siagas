import * as bodyParser from 'body-parser';
import cors from 'cors';
import express, { Application, NextFunction, Request, Response } from 'express';
import { engine } from 'express-handlebars';
import { join } from 'path';
import swaggerUi from 'swagger-ui-express';
import { AppDataSource } from '../data-source';
import JSDoc from '../docs/swagger.json';
import { HttpCode } from '../types/http_code.enum';
import Config from './config';
import ApiError from './exceptions/api.error';
import errorHandler from './exceptions/handler.error';
import response from './response';
import Route from './routes';

class Express {
	public express: Application;

	constructor() {
		this.express = express();
		this.express.use(bodyParser.urlencoded({ extended: false }));

		this.express.use(bodyParser.json());
		this.express.use(cors());
	}

	private mountRoutes(): void {
		this.express = Route.init(this.express);
	}

	private mountMiddleware(): void {
		this.express.use(express.static('public'));

		JSDoc.components.securitySchemes = {
			bearer: {
				type: 'http',
				scheme: 'bearer',
				bearerFormat: 'JWT',
			},
		};
		JSDoc.servers.unshift({
			url: Config.load().app_url,
			description: 'Development environment',
		});
		this.express.use(
			'/docs',
			swaggerUi.serve,
			swaggerUi.setup(JSDoc, {
				swaggerOptions: {
					docExpansion: 'none',
					persistAuthorization: true,
					tagsSorter: 'alpha',
				},
			})
		);
	}

	public init(): void {
		const port: string = Config.load().app_port;
		const host: string = Config.load().app_url;

		this.mountMiddleware();
		this.mountRoutes();
		AppDataSource.initialize();

		this.express.use((err: Error, req: Request, res: Response, next: NextFunction) => {
			errorHandler.handleError(err, res);
		});

		this.express.engine('hbs', engine({ extname: 'hbs' }));
		this.express.set('view engine', 'hbs');
		this.express.set('views', join(__dirname, '..', 'views'));

		this.express.use((req: Request, res: Response, next: NextFunction) => {
			response
				.error(
					new ApiError({
						httpCode: HttpCode.NOT_FOUND,
						message: 'Route not found',
					}),
					HttpCode.NOT_FOUND,
					'Route not found!'
				)
				.create(res);
		});

		this.express.listen(parseInt(port), () => {
			console.log(`App started on ${host}`);
		});
	}
}

export default new Express();
