import * as dotenv from 'dotenv';
import { Application } from 'express';
import * as path from 'path';
import { ConfigType } from 'src/types/config.types';

class Config {
	public static load(): ConfigType {
		dotenv.config({ path: path.join(__dirname, '../../.env') });

		const res: ConfigType = {
			app_name: process.env.APP_NAME || 'SIAGAS',
			app_url: process.env.APP_URL || 'http://localhost',
			app_port: process.env.APP_PORT || '3000',
			database_host: process.env.DATABASE_HOST || 'localhost',
			database_name: process.env.DATABASE_NAME || 'siagas',
			database_user: process.env.DATABASE_USER || 'siagas',
			database_password: process.env.DATABASE_PASSWORD || 'siagaspass',
			database_port: process.env.DATABASE_PORT || '5432',
			token_expired_in_day: parseInt(process.env.TOKEN_EXPIRED_IN_DAY || '1'),
			private_key_location: process.env.PRIVATE_KEY_LOCATION || '',
			public_key_location: process.env.PUBLIC_KEY_LOCATION || '',
		};

		return res;
	}

	public static init(_express: Application): Application {
		_express.locals.app = this.load();
		return _express;
	}
}

export default Config;
