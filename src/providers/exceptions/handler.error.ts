import { Response } from 'express';
import { HttpCode } from '../../types/http_code.enum';
import { ZodError } from 'zod';
import responseAdaptor from '../response';
import ApiError from './api.error';

class ErrorHandler {
  public handleError(
    error: Error | ApiError | ZodError,
    response?: Response
  ): void {
    if (response && (error instanceof ZodError || error instanceof ApiError)) {
      this.handleResponseError(error, response);
    } else {
      this.handleUnresponseError(error, response);
    }
  }

  private handleResponseError(
    error: ApiError | ZodError,
    response: Response
  ): void {
    if (error instanceof ZodError) {
      responseAdaptor.validation(error).create(response);
    } else {
      responseAdaptor
        .error(error, error.httpCode, error.message)
        .create(response);
    }
  }

  private handleUnresponseError(error: Error, response?: Response): void {
    // console.log('Response : ', response);
    if (response) {
      responseAdaptor
        .error(error, HttpCode.INTERNAL_SERVER_ERROR, error.message)
        .create(response);
    }

    console.log('Application encountered an untrusted error.');
    console.log(error);
  }
}

export default new ErrorHandler();
