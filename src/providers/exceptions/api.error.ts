interface ApiErrorArgs {
  name?: string;
  httpCode: number;
  message: string;
}

export default class ApiError extends Error {
  public readonly name: string;
  public readonly httpCode: number;
  public readonly message: string;

  constructor(args: ApiErrorArgs) {
    super(args.message);

    Object.setPrototypeOf(this, new.target.prototype);

    this.name = args.name || 'Error';
    this.message = args.message;
    this.httpCode = args.httpCode;

    Error.captureStackTrace(this);
  }
}
