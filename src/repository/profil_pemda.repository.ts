import { ProfilPemda } from 'src/entity/ProfilePemda.entity';
import BaseRepository from './interface/base.repository';

export default class ProfilPemdaRepository extends BaseRepository<ProfilPemda> {
  constructor() {
    super(ProfilPemda);
  }
}
