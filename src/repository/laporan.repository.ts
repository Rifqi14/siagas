import { AppDataSource } from 'src/data-source';
import { EntityManager } from 'typeorm';

export default class LaporanRepository {
  manager: EntityManager;
  constructor() {
    this.manager = AppDataSource.manager;
  }
}
