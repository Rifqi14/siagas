import { InovasiIndikatorFile } from 'src/entity/InovasiIndikatorFile.entity';
import BaseRepository from './interface/base.repository';

export default class InovasiIndikatorFileRepository extends BaseRepository<InovasiIndikatorFile> {
  constructor() {
    super(InovasiIndikatorFile);
  }
}
