export interface Write<T> {
  create(item: Partial<T>): Promise<T>;
  update(item: Partial<T>, id: number): Promise<T>;
  delete(id: number): Promise<boolean>;
}
