import { AppDataSource } from 'src/data-source';
import { DashboardArsip } from 'src/entity/DashboardArsip.entity';
import { EntityManager, Repository } from 'typeorm';

export default class DashboardRepository {
  viewArsip: Repository<DashboardArsip>;
  manager: EntityManager;
  constructor() {
    this.viewArsip = AppDataSource.getRepository(DashboardArsip);
    this.manager = AppDataSource.manager;
  }
}
