import File from '../entity/File.entity';
import BaseRepository from './interface/base.repository';

export default class FileRepository extends BaseRepository<File> {
  constructor() {
    super(File);
  }
}
