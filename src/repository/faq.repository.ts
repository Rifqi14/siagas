import Faq from '../entity/Faq.entity';
import BaseRepository from './interface/base.repository';

export default class FaqRepository extends BaseRepository<Faq> {
  constructor() {
    super(Faq);
  }
}
