import { PemdaIndikatorFile } from 'src/entity/PemdaIndikatorFile.entity';
import BaseRepository from './interface/base.repository';

export default class PemdaIndikatorFileRepository extends BaseRepository<PemdaIndikatorFile> {
  constructor() {
    super(PemdaIndikatorFile);
  }
}
