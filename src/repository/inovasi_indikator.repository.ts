import { InovasiIndikator } from 'src/entity/InovasiIndikator.entity';
import BaseRepository from './interface/base.repository';

export default class InovasiIndikatorRepository extends BaseRepository<InovasiIndikator> {
  constructor() {
    super(InovasiIndikator);
  }
}
