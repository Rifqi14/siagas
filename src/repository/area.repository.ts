import Area from '../entity/Area.entity';
import BaseRepository from './interface/base.repository';

export default class AreaRepository extends BaseRepository<Area> {
  constructor() {
    super(Area);
  }
}
