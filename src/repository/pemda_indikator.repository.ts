import { PemdaIndikator } from 'src/entity/PemdaIndikator.entity';
import BaseRepository from './interface/base.repository';

export default class PemdaIndikatorRepository extends BaseRepository<PemdaIndikator> {
  constructor() {
    super(PemdaIndikator);
  }
}
