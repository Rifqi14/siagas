import { PaktaIntegritas } from 'src/entity/PaktaIntegritas.entity';
import BaseRepository from './interface/base.repository';

export default class PaktaIntegritasRepository extends BaseRepository<PaktaIntegritas> {
  constructor() {
    super(PaktaIntegritas);
  }
}
