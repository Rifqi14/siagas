import { ReviewInovasiDaerah } from 'src/entity/ReviewInovasiDaerah.entity';
import BaseRepository from './interface/base.repository';

export default class ReviewInovasiDaerahRepository extends BaseRepository<ReviewInovasiDaerah> {
  constructor() {
    super(ReviewInovasiDaerah);
  }
}
