import { TimPenilaian } from 'src/entity/TimPenilaian.entity';
import BaseRepository from './interface/base.repository';

export default class TimPenilaianRepository extends BaseRepository<TimPenilaian> {
  constructor() {
    super(TimPenilaian);
  }
}
