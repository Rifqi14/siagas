import { AppDataSource } from 'src/data-source';
import { RekapIndeksAkhir } from 'src/entity/RekapIndeksAkhir.entity';
import { Repository } from 'typeorm';

export default class RekapIndeksAkhirRepository {
  repository: Repository<RekapIndeksAkhir>;
  constructor() {
    this.repository = AppDataSource.getRepository(RekapIndeksAkhir);
  }
}
