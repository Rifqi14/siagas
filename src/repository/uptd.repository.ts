import Uptd from '../entity/Uptd.entity';
import BaseRepository from './interface/base.repository';

export default class UptdRepository extends BaseRepository<Uptd> {
  constructor() {
    super(Uptd);
  }
}
