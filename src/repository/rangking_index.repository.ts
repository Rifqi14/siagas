import { AppDataSource } from 'src/data-source';
import { RangkingIndex } from 'src/entity/RangkingIndex.entity';
import { Repository } from 'typeorm';

export default class RangkingIndexRepository {
  repository: Repository<RangkingIndex>;
  constructor() {
    this.repository = AppDataSource.getRepository(RangkingIndex);
  }
}
