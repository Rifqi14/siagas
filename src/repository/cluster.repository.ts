import Cluster from '../entity/Cluster.entity';
import BaseRepository from './interface/base.repository';

export default class ClusterRepository extends BaseRepository<Cluster> {
  constructor() {
    super(Cluster);
  }
}
