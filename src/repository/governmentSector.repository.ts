import GovernmentSector from '../entity/GovernmentSector';
import BaseRepository from './interface/base.repository';

export default class GovernmentSectorRepository extends BaseRepository<GovernmentSector> {
  constructor() {
    super(GovernmentSector);
  }
}
