import Express from './providers/express';

class App {
  public loadServer(): void {
    Express.init();
  }
}

export default new App();
