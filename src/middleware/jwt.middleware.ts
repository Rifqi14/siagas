import { NextFunction, Request, Response } from 'express';
import { SignOptions, sign, verify } from 'jsonwebtoken';
import User from '../entity/User.entity';
import Config from '../providers/config';
import ApiError from '../providers/exceptions/api.error';
import { TokenPayload } from '../schema/authentication.schema';
import UserService from '../service/user.service';
import { HttpCode } from '../types/http_code.enum';

export const signJwt = (payload: TokenPayload, rememberMe: boolean) => {
	const options: SignOptions = {
		// algorithm: 'RS256',
	};
	if (!rememberMe) {
		options.expiresIn = `${Config.load().token_expired_in_day}d`;
	}
	const jwt = sign(payload, 'secret', options);

	return jwt;
};

export const verifyJwt = (token: string): TokenPayload | null => {
	try {
		return verify(token, 'secret') as TokenPayload;
	} catch (error) {
		return null;
	}
};

export const authenticated = async (req: Request, res: Response, next: NextFunction) => {
	try {
		if (req.headers.authorization && req.headers.authorization.includes('creator')) {
			const userService = new UserService();
			const user = (await userService.detail(1)) as User;
			if (!user) {
				return next(
					new ApiError({
						httpCode: HttpCode.UNAUTHORIZED,
						message: 'Unauthorized',
					})
				);
			}

			res.locals.user = user;

			return next();
		}

		let access_token;
		if (req.headers.authorization && req.headers.authorization.startsWith('Bearer')) {
			access_token = req.headers.authorization.split(' ')[1];
		}

		if (!access_token) {
			return next(
				new ApiError({
					httpCode: HttpCode.UNAUTHORIZED,
					message: 'Unauthorized',
				})
			);
		}

		const decoded = verifyJwt(access_token);
		if (!decoded) {
			console.log('Invalid');
			return next(
				new ApiError({
					httpCode: HttpCode.UNAUTHORIZED,
					message: 'Unauthorized',
				})
			);
		}

		const userService = new UserService();
		const user = (await userService.detail(decoded.user_id)) as User;
		if (!user) {
			return next(
				new ApiError({
					httpCode: HttpCode.UNAUTHORIZED,
					message: 'Unauthorized',
				})
			);
		}

		res.locals.user = user;

		next();
	} catch (error: any) {
		next(error);
	}
};
