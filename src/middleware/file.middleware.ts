import { NextFunction, Request, Response } from 'express';
import { ParamsDictionary } from 'express-serve-static-core';
import { ParsedQs } from 'qs';
import multer from 'multer';
import ApiError from '../providers/exceptions/api.error';
import SettingRepository from '../repository/setting.repository';
import { HttpCode } from '../types/http_code.enum';
import * as path from 'path';
import { UploadFileRequest } from '../schema/file.schema';
import { extname } from 'path';

export const validateFile = async (
  req: Request,
  res: Response,
  next: NextFunction
) => {
  try {
    const file = req.file;
    if (!file) {
      throw new ApiError({
        httpCode: HttpCode.BAD_REQUEST,
        message: 'File not valid'
      });
    }
    const repo = new SettingRepository();
    const ext = await repo.findOneByOrFail({ key: 'whitelisted_file' });
    const size = await repo.findOneByOrFail({ key: 'file_max_size' });
    const array_of_allowed_ext = ext.value.split(',');
    const file_extension = file.originalname.slice(
      ((file.originalname.lastIndexOf('.') - 1) >>> 0) + 2
    );

    if (!array_of_allowed_ext.includes(file_extension)) {
      throw new ApiError({
        httpCode: HttpCode.BAD_REQUEST,
        message: 'Extension not valid'
      });
    }

    if (file.size / (1024 * 1024) > parseInt(size.value)) {
      throw new ApiError({
        httpCode: HttpCode.BAD_REQUEST,
        message: 'File too large'
      });
    }

    next();
  } catch (error) {
    next(error);
  }
};

export const storage = (dest: string = 'public/tmp/') => {
  return multer.diskStorage({
    destination: dest,
    filename(
      req: Request<
        ParamsDictionary,
        any,
        UploadFileRequest,
        ParsedQs,
        Record<string, any>
      >,
      file,
      callback
    ) {
      callback(
        null,
        `${new Date().getTime().toString()}_${
          req.body.name
            ? `${req.body.name}${extname(file.originalname)}`
            : file.originalname.toLowerCase().replace(' ', '_')
        }`
      );
    }
  });
};

export const upload = (dest: multer.StorageEngine) => {
  return multer({
    storage: dest,
    fileFilter: async (req, file, callback) => {
      const repo = new SettingRepository();
      const ext = await repo.findOneByOrFail({ key: 'whitelisted_file' });
      const size = await repo.findOneByOrFail({ key: 'file_max_size' });
      const array_of_allowed_ext = ext.value.split(',');
      const file_extension = file.originalname.slice(
        ((file.originalname.lastIndexOf('.') - 1) >>> 0) + 2
      );

      if (!array_of_allowed_ext.includes(`.${file_extension}`)) {
        return callback(
          new ApiError({
            httpCode: HttpCode.BAD_REQUEST,
            message: 'Extension not valid'
          })
        );
      }

      callback(null, true);
    }
  });
};
