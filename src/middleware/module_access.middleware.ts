import { NextFunction, Request, Response } from 'express';
import ApiError from '../providers/exceptions/api.error';
import { TokenPayload } from '../schema/authentication.schema';
import { HttpCode } from '../types/http_code.enum';

export const moduleAccessUser = async (
  req: Request,
  res: Response,
  next: NextFunction
) => {
  try {
    const { role } = res.locals.user as TokenPayload;
    if (role.is_super_admin != 't') {
      return next(
        new ApiError({
          httpCode: HttpCode.UNAUTHORIZED,
          message: "Your role don't have permission to access this endpoint."
        })
      );
    }
    next();
  } catch (error: any) {
    next(error);
  }
};

export const moduleAccessSuperAdmin = async (
  req: Request,
  res: Response,
  next: NextFunction
) => {
  try {
    const { role } = res.locals.user as TokenPayload;
    if (role.is_super_admin != 'y') {
      return next(
        new ApiError({
          httpCode: HttpCode.UNAUTHORIZED,
          message: "Your role don't have permission to access this endpoint."
        })
      );
    }
    next();
  } catch (error: any) {
    next(error);
  }
};
