import {
  Body,
  Delete,
  Get,
  Hidden,
  Patch,
  Post,
  Queries,
  Query,
  Route,
  Security,
  Tags
} from 'tsoa';
import RegionalApparatus from '../entity/RegionApparatus.entity';
import ApiError from '../providers/exceptions/api.error';
import RegionApparatusRepository from '../repository/region_apparatus.repository';
import { PaginationData } from '../schema/base.schema';
import {
  CreateRegionApparatusRequest,
  GetRegionApparatusRequest
} from '../schema/region_apparatus.scheme';
import { HttpCode } from '../types/http_code.enum';
import { PaginationResponse, SuccessResponse } from '../types/response.type';
import { PaginationResponse as PaginationResponseInterface } from '../interface/pagination.interface';
import pagination from '../providers/pagination';

@Route('/opd')
@Tags('Konfigurasi | OPD')
@Security('bearer')
export default class RegionalApparatusService {
  private repo: RegionApparatusRepository;
  constructor() {
    this.repo = new RegionApparatusRepository();
  }

  @Post('')
  async create(
    @Body() req: CreateRegionApparatusRequest,
    @Query() @Hidden() username: string = ''
  ): Promise<SuccessResponse<RegionalApparatus> | RegionalApparatus> {
    const create = this.repo.create({
      name: req.name,
      created_by: username,
      updated_by: username
    });

    await this.repo.insert(create);

    return create;
  }

  @Get('/{id}')
  async detail(
    id: number
  ): Promise<SuccessResponse<RegionalApparatus> | RegionalApparatus> {
    const res = await this.repo.findOneBy({ id: id });

    if (!res) {
      throw new ApiError({
        httpCode: HttpCode.NOT_FOUND,
        message: 'Data not found'
      });
    }

    return res;
  }

  @Get('')
  async get(
    @Query('url') @Hidden() url: string = '',
    @Queries() req: GetRegionApparatusRequest
  ): Promise<
    | PaginationResponse<PaginationResponseInterface, RegionalApparatus[]>
    | PaginationData<RegionalApparatus[]>
  > {
    const query = this.repo
      .createQueryBuilder()
      .where(`lower(name) like lower(:q)`, { q: `%${req.q ?? ''}%` });

    const total = await query.getCount();
    const { limit, offset, page, order } = pagination.options(
      req.page,
      req.limit
    );

    query.limit(limit).offset(offset);

    for (const key in order) {
      query.addOrderBy(key, order[key]);
    }

    const res = await query.getMany();
    let paging = pagination.build(url, page, limit, total);

    return { paging, res };
  }

  @Delete('/{id}')
  async delete(id: number): Promise<SuccessResponse<string> | boolean> {
    const { affected } = await this.repo.delete({ id: id });

    if (affected && affected > 0) {
      return true;
    }
    return false;
  }

  @Patch('/{id}')
  async update(
    id: number,
    @Body() req: CreateRegionApparatusRequest,
    @Query() @Hidden() username: string = ''
  ): Promise<SuccessResponse<string> | boolean> {
    const update = this.repo.create({
      name: req.name,
      updated_by: username
    });
    const { affected } = await this.repo.update(id, update);

    if (!affected) {
      return false;
    }
    return true;
  }
}
