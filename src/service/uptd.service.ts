import {
  Body,
  Delete,
  Get,
  Hidden,
  Patch,
  Post,
  Queries,
  Query,
  Route,
  Security,
  Tags
} from 'tsoa';
import Uptd from '../entity/Uptd.entity';
import ApiError from '../providers/exceptions/api.error';
import UptdRepository from '../repository/uptd.repository';
import { PaginationData } from '../schema/base.schema';
import { CreateUptdRequest, GetUptdRequest } from '../schema/uptd.schema';
import { HttpCode } from '../types/http_code.enum';
import { PaginationResponse, SuccessResponse } from '../types/response.type';
import { PaginationResponse as PaginationResponseInterface } from '../interface/pagination.interface';
import pagination from '../providers/pagination';

@Route('/uptd')
@Tags('Konfigurasi | UPTD')
@Security('bearer')
export default class UptdService {
  private repo: UptdRepository;
  constructor() {
    this.repo = new UptdRepository();
  }

  @Post('')
  async create(
    @Body() req: CreateUptdRequest,
    @Query() @Hidden() username: string = ''
  ): Promise<SuccessResponse<Uptd> | Uptd> {
    const create = this.repo.create({
      name: req.name,
      regional_apparatus_id: req.regional_apparatus_id,
      created_by: username,
      updated_by: username
    });

    await this.repo.insert(create);

    return create;
  }

  @Get('/{id}')
  async detail(id: number): Promise<SuccessResponse<Uptd> | Uptd> {
    const res = await this.repo.findOneBy({ id: id });

    if (!res) {
      throw new ApiError({
        httpCode: HttpCode.NOT_FOUND,
        message: 'Data not found'
      });
    }

    return res;
  }

  @Get('')
  async get(
    @Query('url') @Hidden() url: string = '',
    @Queries() req: GetUptdRequest
  ): Promise<
    | PaginationResponse<PaginationResponseInterface, Uptd[]>
    | PaginationData<Uptd[]>
  > {
    const query = this.repo
      .createQueryBuilder('u')
      .leftJoinAndSelect('u.regionalApparatus', 'regionalApparatus')
      .where(`lower(u.name) like lower(:q)`, { q: `%${req.q ?? ''}%` })
      .orWhere(`lower(regionalApparatus.name) like lower(:q)`, {
        q: `%${req.q ?? ''}%`
      });

    const total = await query.getCount();
    const { limit, offset, page, order } = pagination.options(
      req.page,
      req.limit
    );

    query.limit(limit).offset(offset);

    for (const key in order) {
      query.addOrderBy(`u.${key}`, order[key]);
    }

    const res = await query.getMany();
    let paging = pagination.build(url, page, limit, total);

    return { paging, res };
  }

  @Delete('/{id}')
  async delete(id: number): Promise<SuccessResponse<string> | boolean> {
    const { affected } = await this.repo.delete({ id: id });

    if (affected && affected > 0) {
      return true;
    }
    return false;
  }

  @Patch('/{id}')
  async update(
    id: number,
    @Body() req: CreateUptdRequest,
    @Query() @Hidden() username: string = ''
  ): Promise<SuccessResponse<string> | boolean> {
    const update = this.repo.create({
      name: req.name,
      regional_apparatus_id: req.regional_apparatus_id,
      updated_by: username
    });
    const { affected } = await this.repo.update(id, update);

    if (!affected) {
      return false;
    }
    return true;
  }
}
