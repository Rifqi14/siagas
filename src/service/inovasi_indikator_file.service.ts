import InovasiIndikatorFileRepository from 'src/repository/inovasi_indikator_file.repository';
import {
  InovasiIndikatorFileParams,
  ListInovasiIndikatorFiles
} from 'src/schema/inovasi_indikator_file.schema';
import {
  Delete,
  FormField,
  Get,
  Hidden,
  Post,
  Queries,
  Query,
  Route,
  Security,
  Tags,
  UploadedFile
} from 'tsoa';
import { PaginationResponse as PaginationResponseInterface } from '../interface/pagination.interface';
import { PaginationResponse, SuccessResponse } from 'src/types/response.type';
import { PaginationData } from 'src/schema/base.schema';
import pagination from 'src/providers/pagination';
import { InovasiIndikatorFile } from 'src/entity/InovasiIndikatorFile.entity';
import ApiError from 'src/providers/exceptions/api.error';
import { HttpCode } from 'src/types/http_code.enum';
import FileService from './file.service';
import File from 'src/entity/File.entity';

@Route('/inovasi_pemerintah_daerah/indikator')
@Tags(
  'Database Inovasi Daerah | Inovasi Daerah | Inovasi Indikator | Inovasi Indikator File'
)
@Security('bearer')
export default class InovasiIndikatorFileService {
  private repo = new InovasiIndikatorFileRepository();

  @Get('/{inovasi_id}/{indikator_id}/files')
  async get(
    inovasi_id: string,
    indikator_id: string,
    @Query('url') @Hidden() url = '',
    @Query() @Hidden() username = '',
    @Queries() req: InovasiIndikatorFileParams
  ): Promise<
    | PaginationResponse<PaginationResponseInterface, ListInovasiIndikatorFiles>
    | PaginationData<ListInovasiIndikatorFiles>
  > {
    const query = this.repo
      .createQueryBuilder(`iif`)
      .leftJoinAndSelect(`iif.file`, `f`)
      .where(
        `iif.inovasi_id = :inovasi_id and iif.indikator_id = :indikator_id`,
        {
          inovasi_id,
          indikator_id
        }
      );

    // if (username) {
    //   query.where(`iif.created_by = :username`, { username });
    // }

    const total = await query.getCount();
    const { limit, offset, page, order } = pagination.options(
      req.page,
      req.limit
    );

    query.limit(limit).offset(offset);

    for (const key in order) {
      query.addOrderBy(`iif.${key}`, order[key]);
    }

    const res = await query.getMany();
    let paging = pagination.build(url, page, limit, total);

    return { paging, res };
  }

  @Get('/{inovasi_id}/{indikator_id}/files/{file_id}')
  async detail(
    inovasi_id: number,
    indikator_id: number,
    file_id: number
  ): Promise<SuccessResponse<InovasiIndikatorFile> | InovasiIndikatorFile> {
    const res = await this.repo.findOneBy({
      inovasi_id,
      indikator_id,
      file_id
    });

    if (!res) {
      throw new ApiError({
        httpCode: HttpCode.NOT_FOUND,
        message: 'Data not found'
      });
    }

    return res;
  }

  @Post('/{inovasi_id}/{indikator_id}/upload')
  async upload(
    inovasi_id: string,
    indikator_id: string,
    @FormField() nomor_surat: string,
    @FormField() tanggal_surat: string,
    @FormField() tentang: string,
    @UploadedFile() dokumen: Express.Multer.File,
    @FormField() nama_dokumen?: string,
    @Query() @Hidden() username = ''
  ): Promise<SuccessResponse<InovasiIndikatorFile> | InovasiIndikatorFile> {
    const create = this.repo.create({
      nomor_surat: nomor_surat,
      tentang: tentang,
      tanggal_surat: tanggal_surat,
      created_by: username,
      updated_by: username,
      inovasi_id: +inovasi_id,
      indikator_id: +indikator_id
    });

    if (dokumen) {
      const file = await new FileService().create(
        dokumen,
        nama_dokumen,
        username
      );
      create.file = file as File;
    }

    await this.repo.insert(create);

    return create;
  }

  @Delete('/{inovasi_id}/{indikator_id}/delete/{file_id}')
  async delete(
    inovasi_id: number,
    indikator_id: number,
    file_id: number
  ): Promise<SuccessResponse<string> | boolean> {
    const { affected } = await this.repo.delete({
      inovasi_id,
      indikator_id,
      file_id
    });

    if (affected && affected > 0) {
      return true;
    }
    return false;
  }
}
