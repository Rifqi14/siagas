import { Column, Workbook } from "exceljs";
import { createWriteStream } from "fs";
import jsPDF from "jspdf";
import { join } from "path";
import { cwd } from "process";
import { Body, Hidden, Post, Query, Route, Tags } from "tsoa";
import { Excel } from "../providers/excel";
import { CreateExportRequest } from "../schema/export.schema";
import axios from "axios";

type Props = {
	data: Array<Record<string, any>>;
	name: string;
};

@Route("/export")
@Tags("Export")
export default class ExportService {
	@Post("")
	async export(
		@Body() req: CreateExportRequest,
		@Query()
		@Hidden()
		stream: any = createWriteStream(join(__dirname, "..", "..", "public", "tmp", "file.xlsx"))
	): Promise<void> {
		const excel = new Excel();
		await excel
			.setWorksheet(req.name)
			.setHeader(req.headers)
			.addRow(req.data)
			.setStyleAll({
				border: {
					bottom: { style: "thin", color: { argb: "FF000000" } },
					top: { style: "thin", color: { argb: "FF000000" } },
					left: { style: "thin", color: { argb: "FF000000" } },
					right: { style: "thin", color: { argb: "FF000000" } },
				},
			})
			.styleHeaderCell({
				font: { bold: true },
				fill: {
					bgColor: { argb: "FFFF740F" },
					fgColor: { argb: "FFFF740F" },
					type: "pattern",
					pattern: "solid",
				},
				border: {
					bottom: { style: "double", color: { argb: "FF000000" } },
					top: { style: "thick", color: { argb: "FF000000" } },
					left: { style: "thick", color: { argb: "FF000000" } },
					right: { style: "thick", color: { argb: "FF000000" } },
				},
			})
			.write(req.format, { filename: req.name, stream });
	}

	async download({ data, name }: Props): Promise<string> {
		try {
			const headers: string[] = Array.from(Object.keys(data[0]));
			// const rows: any[][] = [];
			// let i = 0;

			// while (i < data.length) {
			//   rows.push(Object.values(data[i]));
			//   i++;
			// }

			// Creating a workbook
			const book = new Workbook();

			// Create worksheet
			const sheet = book.addWorksheet();

			sheet.columns = headers
				.filter(v => !v.includes("_id"))
				.map<Partial<Column>>(key => ({
					key,
					header: key
						.split("_")
						.map(column => column.charAt(0).toUpperCase() + column.slice(1))
						.join(" "),
				}));

			data.forEach(item => sheet.addRow(item));

			const exportPath = join(cwd(), "public", "tmp", `${new Date().getTime().toString()}-${name}.xlsx`);

			await book.xlsx.writeFile(exportPath);

			return exportPath;
		} catch (error) {
			console.log(error);
			throw error;
		}
	}

	async generatePdfFromUrl(url: string, outputName: string): Promise<string> {
		try {
			const response = await axios.get(url);
			const content = response.data;
			const doc = new jsPDF();
			doc.text(content, 10, 10);
			const exportPath = join(cwd(), "public", "tmp", `${new Date().getTime().toString()}-${outputName}.pdf`);
			doc.save(exportPath);
			return exportPath;
		} catch (error) {
			console.log(error);
			throw error;
		}
	}
}
