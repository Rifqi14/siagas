import { Dashboard } from "src/entity/Dashboard.entity";
import { DashboardArsip } from "src/entity/DashboardArsip.entity";
import { PemdaIndikator } from "src/entity/PemdaIndikator.entity";
import { ProfilPemda } from "src/entity/ProfilePemda.entity";
import User from "src/entity/User.entity";
import { PaginationResponse as PaginationResponseInterface } from "src/interface/pagination.interface";
import ApiError from "src/providers/exceptions/api.error";
import pagination from "src/providers/pagination";
import DashboardRepository from "src/repository/dashboard.repository";
import { PaginationData } from "src/schema/base.schema";
import {
	DashboardArsipDownload,
	DashboardArsipDownloadRequest,
	DashboardArsipParams,
	DashboardOPDResponse,
	DashboardResponse,
	StatistikIndikatorInovasiParams,
	StatistikIndikatorInovasiResponse,
	toDashboardArsipDownload,
} from "src/schema/dashboard.schema";
import { DownloadType } from "src/types/lib";
import { PaginationResponse, SuccessResponse } from "src/types/response.type";
import { Get, Hidden, Queries, Query, Route, Security, Tags } from "tsoa";
import { FindOptionsWhere, ILike } from "typeorm";
import ExportService from "./export.service";

@Route("/dashboard")
@Tags("Dashboard")
export default class DashboardService {
	private repo = new DashboardRepository();
	private exportService = new ExportService();

	@Get("/arsip")
	async get(
		@Query() @Hidden() url = "",
		@Query() @Hidden() username = "",
		@Queries() req: DashboardArsipParams
	): Promise<
		| PaginationResponse<PaginationResponseInterface, DashboardArsip[]>
		| PaginationData<DashboardArsip[]>
	> {
		const query = this.repo.viewArsip
			.createQueryBuilder("a")
			.where(`lower(innovation_name) ilike lower(:q)`, {
				q: `%${req.q ?? ""}%`,
			});

		if (req.tahapan && req.tahapan !== "semua") {
			query.andWhere(`lower(innovation_phase) ilike lower(:tahapan)`, {
				tahapan: req.tahapan.toLowerCase(),
			});
		}

		if (req.pemda_id) {
			query.andWhere(`pemda_id = :pemda_id`, { pemda_id: req.pemda_id });
		}

		const total = await query.getCount();
		const { limit, offset, page, order } = pagination.options(
			req.page,
			req.limit
		);

		if (username != "") {
			const user = await this.repo.viewArsip.manager
				.getRepository(User)
				.findOneByOrFail({ username });
			if (user && user.role && user.role.is_super_admin === "t") {
				query.andWhere(`a.created_by = (:created_by)`, {
					created_by: username,
				});
			}
		}

		query.limit(limit).offset(offset);

		const res = await query.getMany();
		let paging = pagination.build(url, page, limit, total);

		return { paging, res };
	}

	@Tags("Export")
	@Get("/arsip/download/{type}")
	async arsip_download(
		type: DownloadType,
		@Query() @Hidden() username = "",
		@Queries() req: DashboardArsipDownloadRequest
	): Promise<string> {
		try {
			if (type === "pdf")
				throw new ApiError({
					httpCode: 403,
					message: "Download PDF under maintenance",
				});

			let where:
				| FindOptionsWhere<DashboardArsip>
				| FindOptionsWhere<DashboardArsip>[] = {
				innovation_name: ILike(`%${req.q ?? ""}%`),
			};

			if (req.tahapan && req.tahapan !== "semua")
				where = { ...where, innovation_phase: req.tahapan };

			if (req.pemda_id) where = { ...where, pemda_id: +req.pemda_id };

			if (username != "") {
				const user = await this.repo.manager
					.getRepository(User)
					.findOneByOrFail({ username });
				if (user && user.role && user.role.is_super_admin === "t")
					where = { ...where, created_by: username };
			}

			const data = await this.repo.viewArsip.find({ where });

			const path = await this.exportService.download({
				data: data.map<DashboardArsipDownload>((value, idx) =>
					toDashboardArsipDownload(value, idx + 1)
				),
				name: "arsip",
			});

			return path;
		} catch (error) {
			throw error;
		}
	}

	@Get("/statistik_indikator")
	async statistik_indikator(
		@Queries() req: StatistikIndikatorInovasiParams
	): Promise<
		| SuccessResponse<StatistikIndikatorInovasiResponse[]>
		| StatistikIndikatorInovasiResponse[]
	> {
		const manager = this.repo.manager;
		const query = manager
			.createQueryBuilder(PemdaIndikator, "pi")
			.leftJoinAndSelect("pi.indicator", "pii")
			.leftJoinAndSelect("pi.files", "pif")
			.where(`pi.pemda_id = :pemda_id`, { pemda_id: req.pemda_id });

		const total_indikators = await query.getCount();

		const res = (await query.getMany()).map<StatistikIndikatorInovasiResponse>(
			value => {
				return {
					id: value.id.toString(),
					nama_indikator: value.indicator.nama_indikator,
					total_indikator: total_indikators,
					jumlah_upload: value.files ? value.files.length : 0,
				};
			}
		);

		return res;
	}

	@Get("/statistik_inovasi")
	async statistik_inovasi(): Promise<
		SuccessResponse<DashboardResponse> | DashboardResponse
	> {
		const manager = this.repo.manager;
		const res = await manager.find(Dashboard, {});

		const response: DashboardResponse = {
			indeks_rata_rata:
				res &&
				res.length > 0 &&
				res[0].total_inovasi > 0 &&
				res[0].total_pemda > 0
					? res[0].total_inovasi / res[0].total_pemda
					: 0,
			total_inovasi: res && res.length > 0 ? +res[0].total_inovasi : 0,
			total_uji_coba: res && res.length > 0 ? +res[0].total_uji_coba : 0,
			total_penerapan: res && res.length > 0 ? +res[0].total_penerapan : 0,
			total_inisiatif: res && res.length > 0 ? +res[0].total_inisiatif : 0,
			daerah_inovasi_tertinggi:
				res && res.length > 0 ? res[0].daerah_tertinggi : "",
			daerah_inovasi_terendah:
				res && res.length > 0 ? res[0].daerah_terendah : "",
		};
		return response;
	}

	@Get("/statistik_opd")
	async opd_menangani(): Promise<
		SuccessResponse<DashboardOPDResponse> | DashboardOPDResponse
	> {
		const manager = this.repo.manager;

		const response: DashboardOPDResponse = {
			total_badan_litbang: await manager.countBy(ProfilPemda, {
				opd_yang_menangani: ILike("Badan Litbang"),
			}),
			total_bappeda_litbang: await manager.countBy(ProfilPemda, {
				opd_yang_menangani: ILike("Bappeda Litbang"),
			}),
			total_diluar: await manager.countBy(ProfilPemda, {
				opd_yang_menangani: ILike("diluar"),
			}),
		};

		return response;
	}
}
