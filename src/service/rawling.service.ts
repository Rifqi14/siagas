import { LaporanBentukInovasi } from 'src/entity/LaporanBentukInovasi.entity';
import { LaporanInisiatorInovasi } from 'src/entity/LaporanInisatorInovasi.entity';
import { LaporanJenisInovasi } from 'src/entity/LaporanJenisInovasi.entity';
import { LaporanUrusanInovasi } from 'src/entity/LaporanUrusanInovasi.entity';
import { ProfilPemda } from 'src/entity/ProfilePemda.entity';
import ApiError from 'src/providers/exceptions/api.error';
import RawlingRepository from 'src/repository/rawling.repository';
import { RawlingParams, RawlingResponse } from 'src/schema/rawling.schema';
import { HttpCode } from 'src/types/http_code.enum';
import { SuccessResponse } from 'src/types/response.type';
import { Get, Queries, Route, Security, Tags } from 'tsoa';

@Route('/rawlog')
@Tags('Rawlog')
@Security('bearer')
export default class RawlingService {
  private repo = new RawlingRepository();

  @Get('')
  async rawling(
    @Queries() req: RawlingParams
  ): Promise<SuccessResponse<RawlingResponse> | RawlingResponse> {
    if (!req.pemda_id)
      throw new ApiError({
        httpCode: HttpCode.BAD_REQUEST,
        message: 'Silahkan pilih pemda terlebih dahulu'
      });

    const pemda = await this.repo.manager.findOneBy(ProfilPemda, {
      id: req.pemda_id
    });

    if (!pemda)
      throw new ApiError({
        httpCode: HttpCode.NOT_FOUND,
        message: 'Pemda tidak ditemukan'
      });

    const jenis_inovasi = await this.repo.manager.find(LaporanJenisInovasi, {
      where: { pemda_id: pemda.id }
    });
    const urusan_pemerintah = await this.repo.manager.find(
      LaporanUrusanInovasi,
      {
        where: { pemda_id: pemda.id }
      }
    );
    const inisiator = await this.repo.manager.find(LaporanInisiatorInovasi, {
      where: { pemda_id: pemda.id }
    });
    const bentuk_inovasi = await this.repo.manager.find(LaporanBentukInovasi, {
      where: { pemda_id: pemda.id }
    });

    return {
      jenis_inovasi,
      urusan_pemerintah,
      inisiator,
      bentuk_inovasi
    };
  }
}
