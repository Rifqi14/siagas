import { unlink } from 'fs';
import { join } from 'path';
import { AppDataSource } from 'src/data-source';
import GovernmentSector from 'src/entity/GovernmentSector';
import Indicator from 'src/entity/Indicator.entity';
import { InovasiIndikator } from 'src/entity/InovasiIndikator.entity';
import { ProfilPemda } from 'src/entity/ProfilePemda.entity';
import { ReviewInovasiDaerah } from 'src/entity/ReviewInovasiDaerah.entity';
import User from 'src/entity/User.entity';
import { DownloadType } from 'src/types/lib';
import { Delete, FormField, Get, Hidden, Patch, Post, Queries, Query, Route, Security, Tags, UploadedFile } from 'tsoa';
import { FindOptionsWhere, ILike } from 'typeorm';
import File from '../entity/File.entity';
import GovernmentInnovation from '../entity/GovernmentInnovation.entity';
import { PaginationResponse as PaginationResponseInterface } from '../interface/pagination.interface';
import ApiError from '../providers/exceptions/api.error';
import pagination from '../providers/pagination';
import GovernmentInnovationRepository from '../repository/government_innovation.repository';
import {
	GetGovernmentInnovationDownloadRequest,
	GetGovernmentInnovationRequest,
	GovernmentInnovationExportSchema,
	GovernmentInnovationProfilExportSchema,
	toGovernmentInnovationExport,
	toGovernmentInnovationProfilExportSchema,
} from '../schema/government_innovation.schema';
import { HttpCode } from '../types/http_code.enum';
import { PaginationResponse, SuccessResponse } from '../types/response.type';
import FileService from './file.service';
import ExportService from './export.service';

@Route('/inovasi_pemerintah_daerah')
@Tags('Database Inovasi Daerah | Inovasi Daerah')
@Security('bearer')
export default class GovernmentInnovationService {
	private repo: GovernmentInnovationRepository;
	private fileService: FileService = new FileService();
	private exportService = new ExportService();

	constructor() {
		this.repo = new GovernmentInnovationRepository();
	}

	@Post('')
	async create(
		@FormField() nama_pemda: string,
		@FormField() nama_inovasi?: string,
		@FormField() tahapan_inovasi?: string,
		@FormField() inisiator_inovasi?: string,
		@FormField() jenis_inovasi?: string,
		@FormField() bentuk_inovasi?: string,
		@FormField() tematik?: string,
		@FormField() urusan_pertama?: string,
		@FormField() urusan_lain?: string,
		@FormField() waktu_uji_coba?: string,
		@FormField() waktu_penerapan?: string,
		@FormField() rancang_bangun?: string,
		@FormField() tujuan?: string,
		@FormField() manfaat?: string,
		@FormField() hasil_inovasi?: string,
		@FormField() urusan_pemerintah?: string,
		@UploadedFile('anggaran_file')
		anggaran_file?: Express.Multer.File,
		@UploadedFile('profile_file') profile_file?: Express.Multer.File,
		@UploadedFile('foto') foto?: Express.Multer.File,
		@Query() @Hidden() username: string = ''
	): Promise<GovernmentInnovation> {
		const queryRunner = AppDataSource.createQueryRunner();

		await queryRunner.startTransaction();
		try {
			const pemda = await queryRunner.manager.findOne(ProfilPemda, {
				where: [{ nama_daerah: nama_pemda }, { nama_pemda: nama_pemda }],
				relations: { user: true },
			});

			const government_sector = await queryRunner.manager.findOne(GovernmentSector, { where: { name: urusan_pemerintah } });
			const entity: GovernmentInnovation = this.repo.create({
				government_name: pemda && pemda.user && pemda.user.nama_pemda ? pemda.user.nama_pemda : undefined,
				created_by: username,
				innovation_name: nama_inovasi,
				innovation_phase: tahapan_inovasi,
				innovation_initiator: inisiator_inovasi,
				innovation_type: jenis_inovasi,
				innovation_form: bentuk_inovasi,
				thematic: tematik,
				first_field: urusan_pertama,
				other_fields: urusan_lain,
				trial_time: waktu_uji_coba ? waktu_uji_coba : undefined,
				urusan: government_sector
					? government_sector
					: queryRunner.manager.create(GovernmentSector, {
							name: urusan_pemerintah,
							deadline: new Date().toISOString(),
					  }),
				implementation_time: waktu_penerapan ? waktu_penerapan : undefined,
				design: rancang_bangun,
				purpose: tujuan,
				benefit: manfaat,
				result: hasil_inovasi,
				profilPemda: pemda ? pemda : undefined,
			});

			if (anggaran_file) {
				const file = await this.fileService.create(anggaran_file, undefined, username);
				entity.budgetFile = file as File;
			}

			if (profile_file) {
				const file = await this.fileService.create(profile_file, undefined, username);
				entity.profileFile = file as File;
			}

			if (foto) {
				const file = await this.fileService.create(foto, undefined, username);
				entity.fotoFile = file as File;
			}

			const inovasi = await this.repo.save(entity);
			const indikator = await this.repo.manager.findBy(Indicator, {
				jenis_indikator: 'si',
			});

			inovasi.indikator = indikator.map<InovasiIndikator>(indikator => {
				return this.repo.manager.create(InovasiIndikator, {
					indikator,
					inovasi,
					created_by: username,
					updated_by: username,
				});
			});

			await this.repo.save(inovasi);

			let skor = 0;

			switch (tahapan_inovasi?.toLowerCase()) {
				case 'inisiatif':
					skor = 50;
					break;
				case 'uji coba':
					skor = 102;
					break;
				case 'penerapan':
					skor = 105;
					break;

				default:
					skor = 0;
					break;
			}

			await this.repo.manager.insert(ReviewInovasiDaerah, {
				random_number: Math.floor(Math.random() * (1000000000000 - 100000000000)) + 100000000000,
				skor: skor,
				status: 'Pending',
				inovasi: inovasi,
				created_by: username,
				updated_by: username,
			});

			await queryRunner.commitTransaction();
			return entity;
		} catch (error) {
			await queryRunner.rollbackTransaction();
			throw new ApiError({
				httpCode: HttpCode.INTERNAL_SERVER_ERROR,
				message: JSON.stringify(error),
			});
		}
	}

	@Get('')
	async get(
		@Queries() req: GetGovernmentInnovationRequest,
		@Query('url') @Hidden() url: string = 'http://localhost',
		@Query() @Hidden() username: string = ''
	): Promise<
		| PaginationResponse<PaginationResponseInterface, Array<GovernmentInnovation>>
		| { paging: PaginationResponseInterface; res: Array<GovernmentInnovation> }
	> {
		const query = this.repo
			.createQueryBuilder('gi')
			.leftJoinAndSelect('gi.budgetFile', 'budgetFile')
			.leftJoinAndSelect('gi.profileFile', 'profileFile')
			.leftJoinAndSelect('gi.fotoFile', 'fotoFile');
		if (req.q) {
			query.andWhere(`lower(innovation_name) like lower(:name)`, {
				name: `%${req.q ?? ''}%`,
			});
		}
		if (req.tahap) {
			query.andWhere(`innovation_phase = :phase`, { phase: req.tahap });
		}

		const user = await this.repo.manager.getRepository(User).findOneByOrFail({ username });
		if (user && user.role && user.role.name.toLowerCase() === 'user') {
			query.andWhere('gi.created_by = (:created_by)', { created_by: username });
		}

		const total = await query.getCount();
		const { limit, offset, page, order } = pagination.options(req.page, req.limit);

		query.limit(limit).offset(offset);

		const res = await query.getMany();
		let paging = pagination.build(url, page, limit, total);

		return { paging, res };
	}

	@Tags('Export')
	@Get('/download/{type}')
	async download(type: DownloadType, @Query() @Hidden() username = '', @Queries() req: GetGovernmentInnovationDownloadRequest): Promise<string> {
		try {
			if (type === 'pdf')
				throw new ApiError({
					httpCode: 403,
					message: 'Download PDF under maintenance',
				});

			let where: FindOptionsWhere<GovernmentInnovation> | FindOptionsWhere<GovernmentInnovation>[] = {
				innovation_name: ILike(`%${req.q ?? ''}%`),
			};

			if (req.tahap) where = { ...where, innovation_phase: req.tahap };

			const user = await this.repo.manager.getRepository(User).findOneByOrFail({ username });
			if (user && user.role && user.role.is_super_admin === 't') where = { ...where, created_by: username };

			const data = await this.repo.find({ where });

			const path = await this.exportService.download({
				data: data.map<GovernmentInnovationExportSchema>((value, idx) => toGovernmentInnovationExport(value, idx + 1)),
				name: 'inovasi-daerah',
			});

			return path;
		} catch (error) {
			throw error;
		}
	}

	@Delete('/{id}')
	async delete(id: number): Promise<SuccessResponse<string> | boolean> {
		const entity = await this.repo.findOneBy({ id: id });
		const { affected } = await this.repo.delete({ id: id });

		if (affected && affected > 0) {
			if (entity && entity.budgetFile) {
				unlink(join(__dirname, '..', '..', 'public', 'upload', 'inovasi_pemda', entity.budgetFile.name), err => {
					if (err) {
						console.log(`${entity.budgetFile.name} was deleted`);
					}
				});
			}
			if (entity && entity.profileFile) {
				unlink(join(__dirname, '..', '..', 'public', 'upload', 'inovasi_pemda', entity.profileFile.name), err => {
					if (err) {
						console.log(`${entity.profileFile.name} was deleted`);
					}
				});
			}
			return true;
		}
		return false;
	}

	@Get('/{id}')
	async detail(id: number): Promise<SuccessResponse<GovernmentInnovation> | GovernmentInnovation> {
		const res = await this.repo.findOne({
			where: { id: id },
			relations: { budgetFile: true, profileFile: true, fotoFile: true },
		});

		if (!res) {
			throw new ApiError({
				httpCode: HttpCode.NOT_FOUND,
				message: 'Data not found',
			});
		}

		return res;
	}

	@Tags('Export')
	@Get('/{id}/download/{type}')
	async downloadProfil(id: number, type: DownloadType): Promise<string> {
		try {
			if (type === 'pdf')
				throw new ApiError({
					httpCode: 403,
					message: 'Download PDF under maintenance',
				});

			const data = await this.repo.find({ where: { id } });

			const path = await this.exportService.download({
				data: data.map<GovernmentInnovationProfilExportSchema>((value, idx) => toGovernmentInnovationProfilExportSchema(value)),
				name: 'inovasi-daerah',
			});

			return path;
		} catch (error) {
			throw error;
		}
	}

	@Patch('/{id}')
	async update(
		id: number,
		@FormField() nama_pemda: string,
		@FormField() urusan_pemerintah: string,
		@FormField() nama_inovasi: string,
		@FormField() tahapan_inovasi: string,
		@FormField() inisiator_inovasi: string,
		@FormField() jenis_inovasi: string,
		@FormField() bentuk_inovasi: string,
		@FormField() tematik: string,
		@FormField() urusan_pertama: string,
		@FormField() urusan_lain: string,
		@FormField() waktu_uji_coba: string,
		@FormField() waktu_penerapan: string,
		@FormField() rancang_bangun: string,
		@FormField() tujuan: string,
		@FormField() manfaat: string,
		@FormField() hasil_inovasi: string,
		@UploadedFile('anggaran_file')
		anggaran_file?: Express.Multer.File,
		@UploadedFile('profile_file') profile_file?: Express.Multer.File,
		@UploadedFile('foto') foto?: Express.Multer.File,
		@Query() @Hidden() username: string = ''
	): Promise<SuccessResponse<string> | boolean> {
		const queryRunner = AppDataSource.createQueryRunner();
		const pemda = await queryRunner.manager.findOne(ProfilPemda, {
			where: [{ nama_daerah: nama_pemda }, { nama_pemda: nama_pemda }],
			relations: { user: true },
		});

		const government_sector = await queryRunner.manager.findOne(GovernmentSector, { where: { name: ILike(`%${urusan_pemerintah}%`) } });

		const entity: GovernmentInnovation = this.repo.create({
			government_name: pemda && pemda.user && pemda.user.nama_pemda ? pemda.user.nama_pemda : undefined,
			created_by: username,
			innovation_name: nama_inovasi,
			innovation_phase: tahapan_inovasi,
			innovation_initiator: inisiator_inovasi,
			innovation_type: jenis_inovasi,
			innovation_form: bentuk_inovasi,
			thematic: tematik,
			first_field: urusan_pertama,
			other_fields: urusan_lain,
			trial_time: waktu_uji_coba,
			urusan: government_sector
				? government_sector
				: queryRunner.manager.create(GovernmentSector, {
						name: urusan_pemerintah,
						deadline: new Date().toISOString(),
				  }),
			implementation_time: waktu_penerapan,
			design: rancang_bangun,
			purpose: tujuan,
			benefit: manfaat,
			result: hasil_inovasi,
		});

		if (anggaran_file) {
			const file = await this.fileService.create(anggaran_file, undefined, username);
			entity.budgetFile = file as File;
		}

		if (profile_file) {
			const file = await this.fileService.create(profile_file, undefined, username);
			entity.profileFile = file as File;
		}

		if (foto) {
			const file = await this.fileService.create(foto, undefined, username);
			entity.profileFile = file as File;
		}
		const { affected } = await this.repo.update(id, entity);

		if (!affected) {
			return false;
		}
		return true;
	}
}
