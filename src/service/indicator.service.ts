import {
  Body,
  Delete,
  Get,
  Hidden,
  Patch,
  Post,
  Queries,
  Query,
  Route,
  Security,
  Tags
} from 'tsoa';
import Indicator from '../entity/Indicator.entity';
import { PaginationResponse as PaginationResponseInterface } from '../interface/pagination.interface';
import ApiError from '../providers/exceptions/api.error';
import pagination from '../providers/pagination';
import IndicatorRepository from '../repository/indicator.repository';
import { PaginationData } from '../schema/base.schema';
import {
  CreateIndicatorRequest,
  GetIndicatorRequest
} from '../schema/indicator.schema';
import { HttpCode } from '../types/http_code.enum';
import { PaginationResponse, SuccessResponse } from '../types/response.type';

@Route('/indikator')
@Tags('Master | Indikator')
@Security('bearer')
export default class IndicatorService {
  private repo: IndicatorRepository;
  constructor() {
    this.repo = new IndicatorRepository();
  }

  /**
   *
   * jenis_indikator `ENUM [spd: Satuan Pemerintah Daerah, si: Satuan Inovasi, iv: Indikator Validasi, ipkd: Indikator Presentasi Kepala Daerah]`
   */
  @Post('')
  async create(
    @Body() req: CreateIndicatorRequest
  ): Promise<SuccessResponse<Indicator> | Indicator> {
    const indicator = this.repo.create({
      ...req
    });

    await this.repo.insert(indicator);

    return indicator;
  }

  @Get('/{id}')
  async detail(id: number): Promise<SuccessResponse<Indicator> | Indicator> {
    const res = await this.repo.findOneBy({ id: id });

    if (!res) {
      throw new ApiError({
        httpCode: HttpCode.NOT_FOUND,
        message: 'Data not found'
      });
    }

    return res;
  }

  @Get()
  async get(
    @Query('url') @Hidden() url: string = '',
    @Queries() req: GetIndicatorRequest
  ): Promise<
    | PaginationResponse<PaginationResponseInterface, Indicator[]>
    | PaginationData<Indicator[]>
  > {
    const query = this.repo
      .createQueryBuilder()
      .where(`lower(nama_indikator) like lower(:q)`, { q: `%${req.q ?? ''}%` });

    if (req.jenis_indikator) {
      query.andWhere(`jenis_indikator = :jenis_indikator`, {
        jenis_indikator: req.jenis_indikator
      });
    }

    const total = await query.getCount();
    const { limit, offset, page, order } = pagination.options(
      req.page,
      req.limit
    );

    query.limit(limit).offset(offset);

    for (const key in order) {
      query.addOrderBy(key, order[key]);
    }

    const res = await query.getMany();
    let paging = pagination.build(url, page, limit, total);

    return { paging, res };
  }

  @Delete('/{id}')
  async delete(id: number): Promise<SuccessResponse<string> | boolean> {
    const { affected } = await this.repo.delete({ id: id });

    if (affected && affected > 0) {
      return true;
    }
    return false;
  }

  /**
   *
   * @param jenis_indikator `ENUM [spd: Satuan Pemerintah Daerah, si: Satuan Inovasi, iv: Indikator Validasi, ipkd: Indikator Presentasi Kepala Daerah]`
   */
  @Patch('/{id}')
  async update(
    id: number,
    @Body() req: CreateIndicatorRequest
  ): Promise<SuccessResponse<string> | boolean> {
    const update = this.repo.create({
      ...req
    });
    const { affected } = await this.repo.update(id, update);

    if (!affected) {
      return false;
    }

    return true;
  }
}
