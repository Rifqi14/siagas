import {
  Delete,
  FormField,
  Get,
  Hidden,
  Patch,
  Post,
  Queries,
  Query,
  Route,
  Security,
  Tags,
  UploadedFile
} from 'tsoa';
import File from '../entity/File.entity';
import GovernmentProfile from '../entity/GovernmentProfile.entity';
import GovernmentProfileRepository from '../repository/government_profile.repository';
import { GetGovernmentProfileRequest } from '../schema/government_profile.schema';
import FileService from './file.service';
import { PaginationResponse as PaginationResponseInterface } from '../interface/pagination.interface';
import ApiError from '../providers/exceptions/api.error';
import pagination from '../providers/pagination';
import { PaginationResponse, SuccessResponse } from '../types/response.type';
import { unlink } from 'fs';
import { join } from 'path';
import { HttpCode } from '../types/http_code.enum';

@Hidden()
@Route('/profile_pemda')
@Tags('Profil Pemda')
@Security('bearer')
export default class GovernmentProfileService {
  private repo: GovernmentProfileRepository = new GovernmentProfileRepository();
  private fileService: FileService = new FileService();

  @Post('')
  async create(
    @FormField() daerah: string,
    @FormField() name: string,
    @FormField() opd_menangani: string,
    @FormField() alamat: string,
    @FormField() email: string,
    @FormField() nomor_telepon: string,
    @FormField() nama_admin: string,
    @UploadedFile() file?: Express.Multer.File,
    @Query() @Hidden() username: string = ''
  ): Promise<GovernmentProfile> {
    const entity = this.repo.create({
      region_id: parseInt(daerah),
      name,
      pic: opd_menangani,
      address: alamat,
      email,
      phone: nomor_telepon,
      admin_name: nama_admin
    });

    if (file) {
      const uploaded = await this.fileService.create(file, undefined, username);
      entity.file = uploaded as File;
    }

    await this.repo.insert(entity);

    return entity;
  }

  @Get('')
  async get(
    @Queries() req: GetGovernmentProfileRequest,
    @Query('url') @Hidden() url: string = 'http://localhost'
  ): Promise<
    | PaginationResponse<PaginationResponseInterface, Array<GovernmentProfile>>
    | { paging: PaginationResponseInterface; res: Array<GovernmentProfile> }
  > {
    const query = this.repo
      .createQueryBuilder('gp')
      .leftJoinAndSelect('gp.region', 'r')
      .leftJoinAndSelect('gp.file', 'f')
      .where(`lower(gp.name) like lower(:name)`, {
        name: `%${req.q ?? ''}%`
      });
    if (req.daerah) {
      query.andWhere(`r.name = :daerah`, { daerah: req.daerah });
    }

    const total = await query.getCount();
    const { limit, offset, page, order } = pagination.options(
      req.page,
      req.limit
    );

    query.limit(limit).offset(offset);

    const res = await query.getMany();
    let paging = pagination.build(url, page, limit, total);

    return { paging, res };
  }

  @Delete('/{id}')
  async delete(id: number): Promise<SuccessResponse<string> | boolean> {
    const entity = await this.repo.findOneBy({ id: id });
    const { affected } = await this.repo.delete({ id: id });

    if (affected && affected > 0) {
      if (entity && entity.file) {
        unlink(
          join(
            __dirname,
            '..',
            '..',
            'public',
            'upload',
            'inovasi_pemda',
            entity.file.name
          ),
          err => {
            if (err) {
              console.log(`${entity.file.name} was deleted`);
            }
          }
        );
      }
      return true;
    }
    return false;
  }

  @Get('/{id}')
  async detail(
    id: number
  ): Promise<SuccessResponse<GovernmentProfile> | GovernmentProfile> {
    const res = await this.repo.findOneBy({ id: id });

    if (!res) {
      throw new ApiError({
        httpCode: HttpCode.NOT_FOUND,
        message: 'Data not found'
      });
    }

    return res;
  }

  @Patch('/{id}')
  async update(
    id: number,
    @FormField() daerah: string,
    @FormField() name: string,
    @FormField() opd_menangani: string,
    @FormField() alamat: string,
    @FormField() email: string,
    @FormField() nomor_telepon: string,
    @FormField() nama_admin: string,
    @UploadedFile() file?: Express.Multer.File,
    @Query() @Hidden() username: string = ''
  ): Promise<GovernmentProfile> {
    const entity = this.repo.create({
      region_id: parseInt(daerah),
      name,
      pic: opd_menangani,
      address: alamat,
      email,
      phone: nomor_telepon,
      admin_name: nama_admin
    });

    if (file) {
      const uploaded = await this.fileService.create(file, undefined, username);
      entity.file = uploaded as File;
    }

    await this.repo.update(id, entity);

    return entity;
  }
}
