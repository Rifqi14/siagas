import {
  Body,
  Delete,
  Get,
  Hidden,
  Patch,
  Post,
  Queries,
  Query,
  Route,
  Security,
  Tags
} from 'tsoa';
import SettingRepository from '../repository/setting.repository';
import { PaginationResponse, SuccessResponse } from '../types/response.type';
import { PaginationResponse as PaginationResponseInterface } from '../interface/pagination.interface';
import Setting from '../entity/Setting.entity';
import {
  CreateSettingRequest,
  GetSettingRequest
} from '../schema/setting.schema';
import ApiError from '../providers/exceptions/api.error';
import { HttpCode } from '../types/http_code.enum';
import { PaginationData } from '../schema/base.schema';
import pagination from '../providers/pagination';

@Route('/setting')
@Tags('Konfigurasi | Setting')
@Security('bearer')
export default class SettingService {
  private repo: SettingRepository = new SettingRepository();

  @Post('')
  async create(
    @Body() req: CreateSettingRequest
  ): Promise<SuccessResponse<Omit<Setting, ''>> | Setting> {
    const create = this.repo.create({
      key: req.key,
      value: req.value,
      helper: req.helper
    });

    await this.repo.insert(create);

    return create;
  }

  @Get('/{id}')
  async detail(
    id: number
  ): Promise<SuccessResponse<Omit<Setting, ''>> | Setting> {
    const res = await this.repo.findOneBy({ id: id });

    if (!res) {
      throw new ApiError({
        httpCode: HttpCode.NOT_FOUND,
        message: 'Data not found'
      });
    }

    return res;
  }

  @Get('')
  async get(
    @Query('url') @Hidden() url: string = '',
    @Queries() req: GetSettingRequest
  ): Promise<
    | PaginationResponse<
        PaginationResponseInterface,
        Omit<Setting, 'details'>[]
      >
    | PaginationData<Omit<Setting, 'details'>[]>
  > {
    const query = this.repo
      .createQueryBuilder()
      .where(`lower(key) like lower(:q)`, {
        q: `%${req.q?.toLowerCase().replace(' ', '_') ?? ''}%`
      })
      .orWhere(`lower(value) like lower(:q)`, {
        q: `%${req.q ?? ''}%`
      });

    const total = await query.getCount();
    const { limit, offset, page } = pagination.options(req.page, req.limit);

    query.limit(limit).offset(offset);

    const res = await query.getMany();
    let paging = pagination.build(url, page, limit, total);

    return { paging, res };
  }

  @Delete('/{id}')
  async delete(id: number): Promise<SuccessResponse<string> | boolean> {
    const { affected } = await this.repo.delete({ id: id });

    if (affected && affected > 0) {
      return true;
    }
    return false;
  }

  @Patch('/{id}')
  async update(
    id: number,
    @Body() req: CreateSettingRequest
  ): Promise<SuccessResponse<string> | boolean> {
    const update = this.repo.create({
      key: req.key,
      value: req.value,
      helper: req.helper
    });
    const { affected } = await this.repo.update(id, update);

    if (!affected) {
      return false;
    }
    return true;
  }
}
