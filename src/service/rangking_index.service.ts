import RangkingIndexRepository from "src/repository/rangking_index.repository";
import {
	RangkingIndexDownloadResponse,
	RangkingIndexParams,
	RangkingIndexParamsDownload,
	UpdateNominatorRequest,
	toDownload,
} from "src/schema/rangking_index.schema";
import { Body, Get, Hidden, Patch, Queries, Query, Route, Security, Tags } from "tsoa";
import { PaginationResponse, SuccessResponse } from "src/types/response.type";
import { PaginationResponse as PaginationResponseInterface } from "src/interface/pagination.interface";
import { RangkingIndex } from "src/entity/RangkingIndex.entity";
import pagination from "src/providers/pagination";
import User from "src/entity/User.entity";
import { PaginationData } from "src/schema/base.schema";
import { ProfilPemda } from "src/entity/ProfilePemda.entity";
import { DownloadType } from "src/types/lib";
import ApiError from "src/providers/exceptions/api.error";
import { FindOptionsWhere, Raw } from "typeorm";
import ExportService from "./export.service";

@Route("/ranking_index")
@Tags("Verifikasi Index", "Verifikasi Index | Ranking Index")
@Security("bearer")
export default class RangkingIndexService {
	private repo = new RangkingIndexRepository();
	private exportService = new ExportService();

	@Get("")
	async get(
		@Query() @Hidden() url = "",
		@Query() @Hidden() username = "",
		@Queries() req: RangkingIndexParams
	): Promise<PaginationResponse<PaginationResponseInterface, RangkingIndex[]> | PaginationData<RangkingIndex[]>> {
		const query = this.repo.repository.createQueryBuilder("ri").where(`lower(nama_daerah) ilike lower(:q)`, { q: `%${req.q ?? ""}%` });

		const total = await query.getCount();
		const { limit, offset, page, order } = pagination.options(req.page, req.limit);

		const user = await this.repo.repository.manager.getRepository(User).findOneByOrFail({ username });
		if (user && user.role && user.role.is_super_admin === "t") {
			query.andWhere(`ri.created_by = (:created_by)`, {
				created_by: username,
			});
		}

		query.limit(limit).offset(offset);

		query.orderBy(`ri.indeks`, "ASC");

		const res = await query.getMany();
		let paging = pagination.build(url, page, limit, total);

		return { paging, res };
	}

	@Patch("/{id}")
	async nominator(id: number, @Body() req: UpdateNominatorRequest): Promise<SuccessResponse<string> | boolean> {
		const { affected } = await this.repo.repository.manager.getRepository(ProfilPemda).update({ id }, { nominator: req.nominator });

		if (affected && affected > 0) {
			return true;
		}
		return false;
	}

	@Tags("Export")
	@Get("/download/{type}")
	async download(type: DownloadType, @Query() @Hidden() username = "", @Queries() req: RangkingIndexParamsDownload): Promise<string> {
		try {
			if (type === "pdf")
				throw new ApiError({
					httpCode: 403,
					message: "Download PDF under maintenance",
				});

			let where: FindOptionsWhere<RangkingIndex> | FindOptionsWhere<RangkingIndex>[] = {
				nama_daerah: Raw(alias => `lower(${alias}) ilike lower('%${req.q ?? ""}%')`),
			};

			if (username) {
				const user = await this.repo.repository.manager.getRepository(User).findOneByOrFail({ username });
				if (user && user.role && user.role.is_super_admin === "t") where = { ...where, created_by: username };
			}

			const data = await this.repo.repository.find({
				where,
			});

			const path = await this.exportService.download({
				data: data.map<RangkingIndexDownloadResponse>((v, idx) => toDownload(v, idx + 1)),
				name: "rangking-index",
			});

			return path;
		} catch (error) {
			throw error;
		}
	}
}
