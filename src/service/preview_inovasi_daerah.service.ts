import PreviewInovasiDaerahRepository from 'src/repository/preview_inovasi_daerah.repository';
import {
  CreatePreviewInovasiDaerahRequest,
  GetPreviewInovasiRequest
} from 'src/schema/preview_inovasi_daerah.schema';
import {
  Body,
  Delete,
  Get,
  Hidden,
  Patch,
  Post,
  Queries,
  Query,
  Route,
  Security,
  Tags
} from 'tsoa';
import { PaginationResponse, SuccessResponse } from 'src/types/response.type';
import { PaginationResponse as PaginationResponseInterface } from 'src/interface/pagination.interface';
import { PreviewReviewInovasiDaerah } from 'src/entity/PreviewReviewInovasiDaerah.entity';
import ApiError from 'src/providers/exceptions/api.error';
import { HttpCode } from 'src/types/http_code.enum';
import { PaginationData } from 'src/schema/base.schema';
import pagination from 'src/providers/pagination';
import User from 'src/entity/User.entity';

@Route('/review_inovasi_daerah')
@Tags('Varifikasi Index | Preview Review Inovasi Daerah')
@Security('bearer')
export default class PreviewReviewInovasiDaerahService {
  private repo = new PreviewInovasiDaerahRepository();

  @Post('/{review_id}/preview')
  async create(
    review_id: number,
    @Query() @Hidden() username: string = '',
    @Body() req: CreatePreviewInovasiDaerahRequest
  ): Promise<
    SuccessResponse<PreviewReviewInovasiDaerah> | PreviewReviewInovasiDaerah
  > {
    const indicator = this.repo.create({
      ...req,
      review_inovasi_daerah_id: review_id,
      created_by: username,
      updated_by: username
    });

    await this.repo.insert(indicator);

    return indicator;
  }

  @Get('/{review_id}/preview/{id}')
  async detail(
    review_id: number,
    id: number
  ): Promise<
    SuccessResponse<PreviewReviewInovasiDaerah> | PreviewReviewInovasiDaerah
  > {
    const res = await this.repo.findOneBy({
      id: id,
      review_inovasi_daerah_id: review_id
    });

    if (!res) {
      throw new ApiError({
        httpCode: HttpCode.NOT_FOUND,
        message: 'Data not found'
      });
    }

    return res;
  }

  @Get('/{review_id}/preview')
  async get(
    review_id: number,
    @Query('url') @Hidden() url: string = '',
    @Query() @Hidden() username: string = '',
    @Queries() req: GetPreviewInovasiRequest
  ): Promise<
    | PaginationResponse<
        PaginationResponseInterface,
        PreviewReviewInovasiDaerah[]
      >
    | PaginationData<PreviewReviewInovasiDaerah[]>
  > {
    const query = this.repo
      .createQueryBuilder(`e`)
      .leftJoinAndSelect('e.review_inovasi_daerah', 'i');

    const total = await query.getCount();
    const { limit, offset, page, order } = pagination.options(
      req.page,
      req.limit
    );

    const user = await this.repo.manager
      .getRepository(User)
      .findOneByOrFail({ username });
    if (user && user.role && user.role.is_super_admin === 't') {
      query.andWhere(`e.created_by = (:created_by)`, { created_by: username });
    }

    query.limit(limit).offset(offset);

    for (const key in order) {
      query.addOrderBy(`e.${key}`, order[key]);
    }

    const res = await query.getMany();
    let paging = pagination.build(url, page, limit, total);

    return { paging, res };
  }

  @Delete('/{review_id}/preview/{id}')
  async delete(
    review_id: number,
    id: number
  ): Promise<SuccessResponse<string> | boolean> {
    const { affected } = await this.repo.delete({
      id: id
    });

    if (affected && affected > 0) {
      return true;
    }
    return false;
  }

  @Patch('/{review_id}/preview/{id}')
  async update(
    review_id: number,
    id: number,
    @Query() @Hidden() username: string = '',
    @Body() req: CreatePreviewInovasiDaerahRequest
  ): Promise<SuccessResponse<string> | boolean> {
    const update = this.repo.create({
      ...req,
      updated_by: username
    });
    const { affected } = await this.repo.update({ id: id }, update);

    if (!affected) {
      return false;
    }

    return true;
  }
}
