import GovernmentInnovation from 'src/entity/GovernmentInnovation.entity';
import GovernmentSector from 'src/entity/GovernmentSector';
import { LaporanIndeks } from 'src/entity/LaporanIndeks.entity';
import { LaporanInisiatorInovasi } from 'src/entity/LaporanInisatorInovasi.entity';
import { LaporanUrusanInovasi } from 'src/entity/LaporanUrusanInovasi.entity';
import { ProfilPemda } from 'src/entity/ProfilePemda.entity';
import { ReviewInovasiDaerah } from 'src/entity/ReviewInovasiDaerah.entity';
import { PaginationResponse as PaginationResponseInterface } from 'src/interface/pagination.interface';
import ApiError from 'src/providers/exceptions/api.error';
import pagination from 'src/providers/pagination';
import LaporanRepository from 'src/repository/laporan.repository';
import { PaginationData } from 'src/schema/base.schema';
import {
	LaporanBentukInovasiParams,
	LaporanIndeksParams,
	LaporanIndeksResponse,
	LaporanInisiatorInovasiParams,
	LaporanJenisInovasiParams,
	LaporanUrusanInovasiParams,
	toLaporanIndexResponse,
} from 'src/schema/laporan.schema';
import { DownloadType } from 'src/types/lib';
import { PaginationResponse } from 'src/types/response.type';
import { Get, Hidden, Queries, Query, Route, Security, Tags } from 'tsoa';
import { FindOptionsWhere, ILike } from 'typeorm';
import ExportService from './export.service';

@Route('/laporan')
@Tags('Laporan')
@Security('bearer')
export default class LaporanService {
	private repo = new LaporanRepository();
	private exportService = new ExportService();

	@Get('/urusan_inovasi')
	async laporanUrusanInovasi(
		@Query() @Hidden() url = '',
		@Queries() req: LaporanUrusanInovasiParams
	): Promise<PaginationResponse<PaginationResponseInterface, any[]> | PaginationData<any[]>> {
		// const query = this.repo.manager
		// 	.getRepository(LaporanUrusanInovasi)
		// 	.createQueryBuilder()
		// 	.where(`lower(urusan_pemerintahan) ilike lower(:q)`, {
		// 		q: `%${req.q ?? ""}%`,
		// 	});

		// if (req.pemda_id) {
		// 	query.andWhere(`pemda_id = :pemda_id`, { pemda_id: req.pemda_id });
		// }

		const query = this.repo.manager
			.getRepository(GovernmentSector)
			.createQueryBuilder('gs')
			.select('gs.name', 'urusan_pemerintah')
			.addSelect(qb => {
				const subQ1 = qb
					.select(`count(rid.id)`, 'count')
					.from(ReviewInovasiDaerah, 'rid')
					.leftJoin(GovernmentInnovation, 'gi2', 'gi2.id = rid.inovasi_id')
					.where('gi2.government_sector_id = gi.government_sector_id')
					.andWhere(`lower(rid.status) = 'accept'`);
				if (req.pemda_id) subQ1.andWhere(`gi2.pemda_id = :pemda_id`, { pemda_id: req.pemda_id });
				return subQ1;
			}, 'total_disetujui')
			.addSelect(qb => {
				const subQ2 = qb
					.select(`count(rid.id)`, 'count')
					.from(ReviewInovasiDaerah, 'rid')
					.leftJoin(GovernmentInnovation, 'gi2', 'gi2.id = rid.inovasi_id')
					.where('gi2.government_sector_id = gi.government_sector_id')
					.andWhere(`lower(rid.status) = 'rejected'`);
				if (req.pemda_id) subQ2.andWhere(`gi2.pemda_id = :pemda_id`, { pemda_id: req.pemda_id });
				return subQ2;
			}, 'total_ditolak')
			// .addSelect("count(gi.id)", "total_keseluruhan")
			.addSelect(qb => {
				const subQ3 = qb
					.select(`count(gi2.id)`, 'count')
					.from(GovernmentInnovation, 'gi2')
					.where('gi2.government_sector_id = gi.government_sector_id');
				if (req.pemda_id) subQ3.andWhere(`gi2.pemda_id = :pemda_id`, { pemda_id: req.pemda_id });
				return subQ3;
			}, 'total_keseluruhan')
			.leftJoin('gs.innovations', 'gi')
			.where(`gs.name is not null and gi.government_sector_id is not null`)
			.groupBy(`gs.name, gi.government_sector_id, gs.id`);

		const total = await query.getCount();
		const { limit, offset, page } = pagination.options(req.page, req.limit);

		query.limit(limit).offset(offset);

		const res = await query.printSql().getRawMany();
		let paging = pagination.build(url, page, limit, total);

		return { paging, res };
	}

	@Get('/inisiator_inovasi')
	async laporanInisiatorInovasi(
		@Query() @Hidden() url = '',
		@Queries() req: LaporanInisiatorInovasiParams
	): Promise<PaginationResponse<PaginationResponseInterface, any[]> | PaginationData<any[]>> {
		// const query = this.repo.manager
		// 	.getRepository(LaporanInisiatorInovasi)
		// 	.createQueryBuilder()
		// 	.where(`lower(inisiator_inovasi) ilike lower(:q)`, {
		// 		q: `%${req.q ?? ""}%`,
		// 	});

		// if (req.pemda_id) {
		// 	query.andWhere(`pemda_id = :pemda_id`, { pemda_id: req.pemda_id });
		// }

		const query = this.repo.manager
			.getRepository(ProfilPemda)
			.createQueryBuilder('pp')
			.select('gi.innovation_initiator', 'inisiator_inovasi')
			.addSelect(qb => {
				const subQ1 = qb
					.select(`count(rid.id)`, 'count')
					.from(ReviewInovasiDaerah, 'rid')
					.leftJoin(GovernmentInnovation, 'gi2', 'gi2.id = rid.inovasi_id')
					.where('gi2.innovation_initiator = gi.innovation_initiator')
					.andWhere(`lower(rid.status) = 'accept'`);
				if (req.pemda_id) subQ1.andWhere(`gi2.pemda_id = :pemda_id`, { pemda_id: req.pemda_id });
				return subQ1;
			}, 'total_disetujui')
			.addSelect(qb => {
				const subQ2 = qb
					.select(`count(rid.id)`, 'count')
					.from(ReviewInovasiDaerah, 'rid')
					.leftJoin(GovernmentInnovation, 'gi2', 'gi2.id = rid.inovasi_id')
					.where('gi2.innovation_initiator = gi.innovation_initiator')
					.andWhere(`lower(rid.status) = 'rejected'`);
				if (req.pemda_id) subQ2.andWhere(`gi2.pemda_id = :pemda_id`, { pemda_id: req.pemda_id });
				return subQ2;
			}, 'total_ditolak')
			// .addSelect("count(gi.id)", "total_keseluruhan")
			.addSelect(qb => {
				const subQ3 = qb
					.select(`count(gi2.id)`, 'count')
					.from(GovernmentInnovation, 'gi2')
					.where('gi2.innovation_initiator = gi.innovation_initiator');
				if (req.pemda_id) subQ3.andWhere(`gi2.pemda_id = :pemda_id`, { pemda_id: req.pemda_id });
				return subQ3;
			}, 'total_keseluruhan')
			.leftJoin('pp.innovations', 'gi')
			.where(`gi.innovation_initiator is not null`)
			.groupBy(`gi.innovation_initiator`);

		const total = await query.getCount();
		const { limit, offset, page } = pagination.options(req.page, req.limit);

		query.limit(limit).offset(offset);

		const res = await query.getRawMany();
		let paging = pagination.build(url, page, limit, total);

		return { paging, res };
	}

	@Get('/bentuk_inovasi')
	async laporanBentukInovasi(
		@Query() @Hidden() url = '',
		@Queries() req: LaporanBentukInovasiParams
	): Promise<PaginationResponse<PaginationResponseInterface, any[]> | PaginationData<any[]>> {
		// const query = this.repo.manager
		// 	.getRepository(LaporanBentukInovasi)
		// 	.createQueryBuilder()
		// 	.where(`lower(bentuk_inovasi) ilike lower(:q)`, {
		// 		q: `%${req.q ?? ""}%`,
		// 	});

		// if (req.pemda_id) {
		// 	query.andWhere(`pemda_id = :pemda_id`, { pemda_id: req.pemda_id });
		// }

		const query = this.repo.manager
			.getRepository(ProfilPemda)
			.createQueryBuilder('pp')
			.select('gi.innovation_form', 'bentuk_inovasi')
			.addSelect(qb => {
				const subQ1 = qb
					.select(`count(rid.id)`, 'count')
					.from(ReviewInovasiDaerah, 'rid')
					.leftJoin(GovernmentInnovation, 'gi2', 'gi2.id = rid.inovasi_id')
					.where('gi2.innovation_form = gi.innovation_form')
					.andWhere(`lower(rid.status) = 'accept'`);
				if (req.pemda_id) subQ1.andWhere(`gi2.pemda_id = :pemda_id`, { pemda_id: req.pemda_id });
				return subQ1;
			}, 'total_disetujui')
			.addSelect(qb => {
				const subQ2 = qb
					.select(`count(rid.id)`, 'count')
					.from(ReviewInovasiDaerah, 'rid')
					.leftJoin(GovernmentInnovation, 'gi2', 'gi2.id = rid.inovasi_id')
					.where('gi2.innovation_form = gi.innovation_form')
					.andWhere(`lower(rid.status) = 'rejected'`);
				if (req.pemda_id) subQ2.andWhere(`gi2.pemda_id = :pemda_id`, { pemda_id: req.pemda_id });
				return subQ2;
			}, 'total_ditolak')
			.addSelect(qb => {
				const subQ3 = qb.select(`count(gi2.id)`, 'count').from(GovernmentInnovation, 'gi2').where('gi2.innovation_form = gi.innovation_form');
				if (req.pemda_id) subQ3.andWhere(`gi2.pemda_id = :pemda_id`, { pemda_id: req.pemda_id });
				return subQ3;
			}, 'total_keseluruhan')
			// .addSelect("count(gi.id)", "total_keseluruhan")
			.leftJoin('pp.innovations', 'gi')
			.where(`gi.innovation_form is not null`)
			.groupBy(`gi.innovation_form`);

		const total = await query.getCount();
		const { limit, offset, page } = pagination.options(req.page, req.limit);

		query.limit(limit).offset(offset);

		const res = await query.getRawMany();
		let paging = pagination.build(url, page, limit, total);

		return { paging, res };
	}

	@Get('/jenis_inovasi')
	async laporanJenisInovasi(
		@Query() @Hidden() url = '',
		@Queries() req: LaporanJenisInovasiParams
	): Promise<PaginationResponse<PaginationResponseInterface, any[]> | PaginationData<any[]>> {
		// const query = this.repo.manager
		// 	.getRepository(LaporanJenisInovasi)
		// 	.createQueryBuilder()
		// 	.where(`lower(jenis_inovasi) ilike lower(:q)`, {
		// 		q: `%${req.q ?? ""}%`,
		// 	});

		// if (req.pemda_id) {
		// 	query.andWhere(`pemda_id = :pemda_id`, { pemda_id: req.pemda_id });
		// }

		const query = this.repo.manager
			.getRepository(ProfilPemda)
			.createQueryBuilder('pp')
			.select('gi.innovation_type', 'jenis_inovasi')
			// .addSelect("count(gi.id)", "total_keseluruhan")
			.addSelect(qb => {
				const subQ1 = qb
					.select(`count(rid.id)`, 'count')
					.from(ReviewInovasiDaerah, 'rid')
					.leftJoin(GovernmentInnovation, 'gi2', 'gi2.id = rid.inovasi_id')
					.where('gi2.innovation_type = gi.innovation_type')
					.andWhere(`lower(rid.status) = 'accept'`);
				if (req.pemda_id) subQ1.andWhere(`gi2.pemda_id = :pemda_id`, { pemda_id: req.pemda_id });
				return subQ1;
			}, 'total_disetujui')
			.addSelect(qb => {
				const subQ2 = qb
					.select(`count(rid.id)`, 'count')
					.from(ReviewInovasiDaerah, 'rid')
					.leftJoin(GovernmentInnovation, 'gi2', 'gi2.id = rid.inovasi_id')
					.where('gi2.innovation_type = gi.innovation_type')
					.andWhere(`lower(rid.status) = 'rejected'`);
				if (req.pemda_id) subQ2.andWhere(`gi2.pemda_id = :pemda_id`, { pemda_id: req.pemda_id });
				return subQ2;
			}, 'total_ditolak')
			.addSelect(qb => {
				const subQ3 = qb.select(`count(gi2.id)`, 'count').from(GovernmentInnovation, 'gi2').where('gi2.innovation_type = gi.innovation_type');
				if (req.pemda_id) subQ3.andWhere(`gi2.pemda_id = :pemda_id`, { pemda_id: req.pemda_id });
				return subQ3;
			}, 'total_keseluruhan')
			.leftJoin('pp.innovations', 'gi')
			.where(`gi.innovation_type is not null`)
			.groupBy(`gi.innovation_type`);

		const total = await query.getCount();
		const { limit, offset, page } = pagination.options(req.page, req.limit);

		query.limit(limit).offset(offset);

		const res = await query.getRawMany();
		let paging = pagination.build(url, page, limit, total);

		return { paging, res };
	}

	@Get('/indeks')
	async laporanIndeks(
		@Query() @Hidden() url = '',
		@Queries() req: LaporanIndeksParams
	): Promise<PaginationResponse<PaginationResponseInterface, LaporanIndeksResponse[]> | PaginationData<LaporanIndeksResponse[]>> {
		const query = this.repo.manager
			.getRepository(LaporanIndeks)
			.createQueryBuilder()
			.where(`lower(nama_daerah) ilike lower(:q)`, {
				q: `%${req.q ?? ''}%`,
			});

		if (req.pemda_id) {
			query.andWhere(`pemda_id = :pemda_id`, { pemda_id: req.pemda_id });
		}

		if (req.opd_yang_menangani) {
			query.andWhere('lower(opd_yang_menangani) = lower(:opd_yang_menangani)', { opd_yang_menangani: req.opd_yang_menangani });
		}

		if (req.predikat && req.predikat !== 'semua') {
			if (req.predikat === 'belum mengisi data') {
				query.andWhere(`jumlah_inovasi = 0`);
			} else {
				let predikat = '';
				switch (req.predikat) {
					case 'terinovatif':
						predikat = 'sangat invovatif';
						break;

					default:
						predikat = req.predikat as string;
						break;
				}

				query.andWhere(`lower(predikat) = lower(:predikat)`, {
					predikat,
				});
			}
		}

		const total = await query.getCount();
		const { limit, offset, page } = pagination.options(req.page, req.limit);

		query.limit(limit).offset(offset);

		const res = (await query.getMany()).map<LaporanIndeksResponse>(response => toLaporanIndexResponse(response));
		let paging = pagination.build(url, page, limit, total);

		return { paging, res };
	}

	@Tags('Export')
	@Get('/indeks/download/{type}')
	async laporanIndeksdownload(type: DownloadType, @Queries() req: LaporanIndeksParams): Promise<string> {
		try {
			let where: FindOptionsWhere<LaporanIndeks> | FindOptionsWhere<LaporanIndeks>[] = {
				nama_daerah: ILike(`%${req.q ?? ''}%`),
			};

			if (req.pemda_id) where = { ...where, pemda_id: req.pemda_id };

			if (req.predikat && req.predikat !== 'semua') {
				if (req.predikat === 'belum mengisi data') {
					where = { ...where, jumlah_inovasi: 0 };
				} else {
					let predikat = '';
					switch (req.predikat) {
						case 'terinovatif':
							predikat = 'sangat invovatif';
							break;

						default:
							predikat = req.predikat as string;
							break;
					}
					where = { ...where, predikat: ILike(req.predikat) };
				}
			}
			const data = (
				await this.repo.manager.find(LaporanIndeks, {
					where,
				})
			).map<LaporanIndeksResponse>(laporan => toLaporanIndexResponse(laporan));

			if (type === 'pdf') {
				return await this.exportService.generatePdfFromUrl('http://localhost:3002/docs', 'test');
				throw new ApiError({
					httpCode: 403,
					message: 'Download PDF under maintenance',
				});
			}

			const path = await this.exportService.download({
				data,
				name: 'indeks-rata-rata-opd',
			});

			return path;
		} catch (error) {
			throw error;
		}
	}

	@Tags('Export')
	@Get('/urusan_inovasi/download/{type}')
	async laporanUrusanInovasidownload(type: DownloadType, @Queries() req: LaporanUrusanInovasiParams): Promise<string> {
		try {
			if (type === 'pdf')
				throw new ApiError({
					httpCode: 403,
					message: 'Download PDF under maintenance',
				});

			let where: FindOptionsWhere<LaporanUrusanInovasi> | FindOptionsWhere<LaporanUrusanInovasi>[] = {
				urusan_pemerintahan: ILike(`%${req.q ?? ''}%`),
			};

			if (req.pemda_id) where = { ...where, pemda_id: req.pemda_id };

			const data = await this.repo.manager.find(LaporanUrusanInovasi, {
				where,
			});

			const path = await this.exportService.download({
				data,
				name: 'indeks-urusan-inovasi',
			});

			return path;
		} catch (error) {
			throw error;
		}
	}

	@Tags('Export')
	@Get('/inisiator_inovasi/download/{type}')
	async laporanInisiatorInovasidownload(type: DownloadType, @Queries() req: LaporanInisiatorInovasiParams): Promise<string> {
		try {
			if (type === 'pdf')
				throw new ApiError({
					httpCode: 403,
					message: 'Download PDF under maintenance',
				});

			let where: FindOptionsWhere<LaporanInisiatorInovasi> | FindOptionsWhere<LaporanInisiatorInovasi>[] = {
				inisiator_inovasi: ILike(`%${req.q ?? ''}%`),
			};

			if (req.pemda_id) where = { ...where, pemda_id: req.pemda_id };

			const data = await this.repo.manager.find(LaporanInisiatorInovasi, {
				where,
			});

			const path = await this.exportService.download({
				data,
				name: 'inisiator-inovasi',
			});

			return path;
		} catch (error) {
			throw error;
		}
	}
}
