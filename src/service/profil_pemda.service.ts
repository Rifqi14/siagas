import { ProfilPemda } from 'src/entity/ProfilePemda.entity';
import User from 'src/entity/User.entity';
import ProfilPemdaRepository from 'src/repository/profil_pemda.repository';
import { PaginationResponse, SuccessResponse } from 'src/types/response.type';
import { Delete, FormField, Get, Hidden, Patch, Post, Queries, Query, Route, Security, Tags, UploadedFile } from 'tsoa';
import FileService from './file.service';
import File from 'src/entity/File.entity';
import ApiError from 'src/providers/exceptions/api.error';
import { HttpCode } from 'src/types/http_code.enum';
import {
	ListProfilPemda,
	ProfilPemdaDownloadResponse,
	ProfilPemdaFilterDownload,
	ProfilPemdaParams,
	toDownload,
} from 'src/schema/profil_pemda.schema';
import { PaginationResponse as PaginationResponseInterface } from '../interface/pagination.interface';
import { PaginationData } from 'src/schema/base.schema';
import pagination from 'src/providers/pagination';
import { AppDataSource } from 'src/data-source';
import Indicator from 'src/entity/Indicator.entity';
import { PemdaIndikator } from 'src/entity/PemdaIndikator.entity';
import { DownloadType } from 'src/types/lib';
import { FindOptionsWhere, ILike } from 'typeorm';
import ExportService from './export.service';

@Route('/profil_pemda')
@Tags('Database Inovasi Daerah | Profil Pemda')
@Security('bearer')
export default class ProfilPemdaService {
	private repo = new ProfilPemdaRepository();
	private fileService: FileService = new FileService();
	private exportService: ExportService = new ExportService();

	@Tags('Export')
	@Get('/download/{type}')
	async download(type: DownloadType, @Query() @Hidden() username = '', @Queries() req: ProfilPemdaFilterDownload): Promise<string> {
		try {
			if (type === 'pdf')
				throw new ApiError({
					httpCode: 403,
					message: 'Download PDF under maintenance',
				});

			let where: FindOptionsWhere<ProfilPemda> | FindOptionsWhere<ProfilPemda>[] = {};

			if (req.q) {
				where = {
					nama_daerah: ILike(`%${req.q}%`),
				};
			}

			const data = await this.repo.find({
				where,
			});

			const path = await this.exportService.download({
				data: data.map<ProfilPemdaDownloadResponse>((value, idx) => toDownload(value, idx + 1)),
				name: 'profil-pemda',
			});

			return path;
		} catch (error) {
			throw error;
		}
	}

	@Tags('Export')
	@Get('/{id}/download/{type}')
	async downloadProfil(id: number, type: DownloadType): Promise<string> {
		try {
			if (type === 'pdf')
				throw new ApiError({
					httpCode: 403,
					message: 'Download PDF under maintenance',
				});

			const data = await this.repo.findOneOrFail({ where: { id } });

			const path = await this.exportService.download({
				data: [toDownload(data, 1)],
				name: 'profil-pemda',
			});

			return path;
		} catch (error) {
			throw error;
		}
	}

	@Post('')
	async create(
		@FormField() nama_pemda: string,
		@FormField() nama_daerah?: string,
		@FormField() opd_yang_menangani?: string,
		@FormField() alamat_pemda?: string,
		@FormField() email?: string,
		@FormField() no_telpon?: string,
		@FormField() nama_admin?: string,
		@UploadedFile() file?: Express.Multer.File,
		@Query() @Hidden() username: string = ''
	): Promise<SuccessResponse<ProfilPemda> | ProfilPemda> {
		const queryRunner = AppDataSource.createQueryRunner();
		await queryRunner.startTransaction();
		try {
			const create = this.repo.create({
				nama_daerah,
				user: await this.repo.manager.getRepository(User).findOneByOrFail({ nama_pemda }),
				opd_yang_menangani,
				alamat_pemda,
				email,
				no_telpon,
				nama_admin,
				created_by: username,
				updated_by: username,
			});

			if (file) {
				const uploaded = await this.fileService.create(file, undefined, username);
				create.document = uploaded as File;
			}

			const pemda = await this.repo.save(create);

			const indicators = await this.repo.manager.find(Indicator, {
				where: { jenis_indikator: 'spd' },
			});
			pemda.indicators = indicators.map<PemdaIndikator>(indicator => {
				return this.repo.manager.create(PemdaIndikator, {
					pemda,
					indicator,
					created_by: username,
					updated_by: username,
				});
			});

			await this.repo.save(pemda);

			await queryRunner.commitTransaction();

			return create;
		} catch (error) {
			await queryRunner.rollbackTransaction();
			throw new ApiError({
				httpCode: HttpCode.INTERNAL_SERVER_ERROR,
				message: JSON.stringify(error),
			});
		}
	}

	@Get('/{id}')
	async detail(id: number): Promise<SuccessResponse<ProfilPemda> | ProfilPemda> {
		const res = await this.repo.findOne({
			where: { id: id },
			relations: { innovations: true },
		});

		if (!res) {
			throw new ApiError({
				httpCode: HttpCode.NOT_FOUND,
				message: 'Data not found',
			});
		}

		return res;
	}

	@Get('')
	async get(
		@Query('url') @Hidden() url: string = '',
		@Query() @Hidden() username: string = '',
		@Queries() req: ProfilPemdaParams
	): Promise<PaginationResponse<PaginationResponseInterface, ListProfilPemda> | PaginationData<ListProfilPemda>> {
		const query = this.repo
			.createQueryBuilder('e')
			.leftJoinAndSelect('e.user', 'u')
			.leftJoinAndSelect('e.document', 'd')
			.where(`lower(e.nama_daerah) like lower(:q)`, { q: `%${req.q ?? ''}%` });

		const total = await query.getCount();
		const { limit, offset, page, order } = pagination.options(req.page, req.limit);

		const user = await this.repo.manager.getRepository(User).findOneByOrFail({ username });
		if (user && user.role && user.role.is_super_admin === 't') {
			query.andWhere(`e.created_by = (:created_by)`, { created_by: username });
		}

		query.limit(limit).offset(offset);

		for (const key in order) {
			query.addOrderBy(`e.${key}`, order[key]);
		}

		const res = await query.getMany();
		let paging = pagination.build(url, page, limit, total);

		return { paging, res };
	}

	@Delete('/{id}')
	async delete(id: number): Promise<SuccessResponse<string> | boolean> {
		const { affected } = await this.repo.delete({ id });

		if (affected && affected > 0) {
			return true;
		}
		return false;
	}

	@Patch('/{id}')
	async update(
		id: number,
		@FormField() nama_pemda: string,
		@FormField() nama_daerah?: string,
		@FormField() opd_yang_menangani?: string,
		@FormField() alamat_pemda?: string,
		@FormField() email?: string,
		@FormField() no_telpon?: string,
		@FormField() nama_admin?: string,
		@UploadedFile() file?: Express.Multer.File,
		@Query() @Hidden() username: string = ''
	): Promise<SuccessResponse<string> | boolean> {
		const update = this.repo.create({
			nama_daerah,
			user: await this.repo.manager.getRepository(User).findOneByOrFail({ nama_pemda }),
			opd_yang_menangani,
			alamat_pemda,
			email,
			no_telpon,
			nama_admin,
			updated_by: username,
		});

		if (file) {
			const uploaded = await this.fileService.create(file, undefined, username);
			update.document = uploaded as File;
		}

		const { affected } = await this.repo.update(id, update);

		if (!affected) {
			return false;
		}
		return true;
	}
}
