import {
  Delete,
  FormField,
  Get,
  Hidden,
  Patch,
  Post,
  Queries,
  Query,
  Route,
  Security,
  Tags,
  UploadedFile
} from 'tsoa';
import Announcement from '../entity/Announcement.entity';
import File from '../entity/File.entity';
import { PaginationResponse as PaginationResponseInterface } from '../interface/pagination.interface';
import ApiError from '../providers/exceptions/api.error';
import pagination from '../providers/pagination';
import AnnouncementRepository from '../repository/announcement.repository';
import { GetAnnouncementRequest } from '../schema/announcement.schema';
import { PaginationData } from '../schema/base.schema';
import { HttpCode } from '../types/http_code.enum';
import { PaginationResponse, SuccessResponse } from '../types/response.type';
import FileService from './file.service';

@Route('/pengumuman')
@Tags('Master | Pengumuman')
// @Security('bearer')
export default class AnnouncementService {
  private repo: AnnouncementRepository = new AnnouncementRepository();
  private fileService: FileService = new FileService();

  @Security('bearer')
  @Post('')
  async create(
    @FormField() content: string,
    @FormField() title: string,
    @FormField() slug: string,
    @UploadedFile() document?: Express.Multer.File,
    @Query() @Hidden() username: string = ''
  ): Promise<SuccessResponse<Omit<Announcement, ''>> | Announcement> {
    const create = this.repo.create({
      title: title,
      slug: slug,
      content: content
    });

    if (document) {
      const file = await this.fileService.create(document, undefined, username);
      create.file = file as File;
    }

    await this.repo.insert(create);

    return create;
  }

  @Get('/{id}')
  async detail(
    id: number,
    @Query() @Hidden() host: string = ''
  ): Promise<SuccessResponse<Omit<Announcement, ''>> | Announcement> {
    const res = await this.repo.findOneBy({ id: id });

    if (!res) {
      throw new ApiError({
        httpCode: HttpCode.NOT_FOUND,
        message: 'Data not found'
      });
    }

    if (res.file) {
      res.file.full_path = host + '/' + res.file.path;
    }

    return res;
  }

  @Get('')
  async get(
    @Query('url') @Hidden() url: string = '',
    @Query('host') @Hidden() host: string = '',
    @Queries() req: GetAnnouncementRequest
  ): Promise<
    | PaginationResponse<PaginationResponseInterface, Omit<Announcement, ''>[]>
    | PaginationData<Omit<Announcement, ''>[]>
  > {
    const query = this.repo
      .createQueryBuilder('a')
      .leftJoinAndSelect('a.file', 'f')
      .addSelect(`'${host}/' || f.path`, 'f_full_path')
      .where(`lower(title) like lower(:q)`, {
        q: `%${req.q ?? ''}%`
      })
      .orWhere(`lower(slug) like lower(:q)`, {
        q: `%${req.q ?? ''}%`
      });

    const total = await query.getCount();
    const { limit, offset, page } = pagination.options(req.page, req.limit);

    query.limit(limit).offset(offset);

    const res = await query.getMany();
    let paging = pagination.build(url, page, limit, total);

    return { paging, res };
  }

  @Security('bearer')
  @Delete('/{id}')
  async delete(id: number): Promise<SuccessResponse<string> | boolean> {
    const { affected } = await this.repo.delete({ id: id });

    if (affected && affected > 0) {
      return true;
    }
    return false;
  }

  @Security('bearer')
  @Patch('/{id}')
  async update(
    id: number,
    @FormField() content: string,
    @FormField() title: string,
    @FormField() slug: string,
    @UploadedFile() document?: Express.Multer.File,
    @Query() @Hidden() username: string = ''
  ): Promise<SuccessResponse<string> | boolean> {
    let file_id: number = 0;
    if (document) {
      const oldData = (await this.detail(id)) as Announcement;

      if (
        !oldData.file ||
        (oldData.file && oldData.file.name != document.filename)
      ) {
        const file = (await this.fileService.create(
          document,
          undefined,
          username
        )) as File;
        file_id = file.id;
      }
    }

    const update = this.repo.create({
      title: title,
      slug: slug,
      content: content
    });
    if (file_id > 0) {
      update.file_id = file_id;
    }
    const { affected } = await this.repo.update(id, update);

    if (!affected) {
      return false;
    }
    return true;
  }
}
