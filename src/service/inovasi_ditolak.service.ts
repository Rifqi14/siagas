import ApiError from 'src/providers/exceptions/api.error';
import InovasiDitolakRepository from 'src/repository/inovasi_ditolak.repository';
import { DownloadInovasiDitolakResponse, InovasiDitolakParams, InovasiDitolakResponse, toDownloadResponse } from 'src/schema/inovasi_ditolak.schema';
import { HttpCode } from 'src/types/http_code.enum';
import { PaginationResponse, SuccessResponse } from 'src/types/response.type';
import { PaginationResponse as PaginationResponseInterface } from 'src/interface/pagination.interface';
import { Get, Hidden, Patch, Queries, Query, Route, Security, Tags } from 'tsoa';
import { FindOptionsWhere, ILike, Raw } from 'typeorm';
import { PaginationData } from 'src/schema/base.schema';
import pagination from 'src/providers/pagination';
import User from 'src/entity/User.entity';
import { ReviewInovasiDaerah } from 'src/entity/ReviewInovasiDaerah.entity';
import { DownloadType } from 'src/types/lib';
import ExportService from './export.service';

@Route('/inovasi_ditolak')
@Tags('Verifikasi Index', 'Verifikasi Index | Inovasi Ditolak')
@Security('bearer')
export default class InovasiDitolakService {
	private repo = new InovasiDitolakRepository();
	private exportService = new ExportService();

	@Patch('/{review_inovasi_id}')
	async setujui(review_inovasi_id: number, @Query() @Hidden() username = ''): Promise<SuccessResponse<string> | boolean> {
		const { affected } = await this.repo.update(
			{
				id: review_inovasi_id,
				status: ILike('Rejected'),
			},
			{ status: 'Accept', updated_by: username }
		);

		if (affected && affected > 0) {
			return true;
		}
		return false;
	}

	@Get('/{review_inovasi_id}')
	async detail(review_inovasi_id: number): Promise<SuccessResponse<InovasiDitolakResponse> | InovasiDitolakResponse> {
		const res = await this.repo.findOne({
			where: { id: review_inovasi_id },
			relations: { inovasi: { pemda: true }, previews: { evaluasi: true } },
		});

		if (!res) {
			throw new ApiError({
				httpCode: HttpCode.NOT_FOUND,
				message: 'Data not found',
			});
		}

		return this.toResponse(res);
	}

	@Get()
	async get(
		@Query() @Hidden() url = '',
		@Query() @Hidden() username = '',
		@Queries() req: InovasiDitolakParams
	): Promise<PaginationResponse<PaginationResponseInterface, InovasiDitolakResponse[]> | PaginationData<InovasiDitolakResponse[]>> {
		const query = this.repo
			.createQueryBuilder('hri')
			.leftJoinAndSelect('hri.inovasi', 'i')
			.leftJoinAndSelect('i.profilPemda', 'ip')
			.leftJoinAndSelect('hri.previews', 'ep')
			.leftJoinAndSelect('ep.evaluasi', 'epv')
			.where(`(lower(i.innovation_name) ilike lower(:q) or lower(ip.nama_daerah) ilike lower(:q))`, {
				q: `%${req.q ?? ''}%`,
			})
			.andWhere(`lower(hri.status) = lower(:status)`, { status: 'rejected' });

		if (req.pemda_id) [query.andWhere(`ip.id = :pemda_id`, { pemda_id: req.pemda_id })];

		const total = await query.getCount();
		const { limit, offset, page, order } = pagination.options(req.page, req.limit);

		const user = await this.repo.manager.getRepository(User).findOneByOrFail({ username });
		if (user && user.role && user.role.is_super_admin === 't') {
			query.andWhere(`hri.created_by = (:created_by)`, {
				created_by: username,
			});
		}

		query.limit(limit).offset(offset);

		for (const key in order) {
			query.addOrderBy(`hri.${key}`, order[key]);
		}

		const res = (await query.getMany()).map<InovasiDitolakResponse>(response => this.toResponse(response));
		let paging = pagination.build(url, page, limit, total);

		return { paging, res };
	}

	@Tags('Export')
	@Get('/download/{type}')
	async download(type: DownloadType, @Query() @Hidden() username = '', @Queries() req: InovasiDitolakParams): Promise<string> {
		try {
			if (type === 'pdf')
				throw new ApiError({
					httpCode: 403,
					message: 'Download PDF under maintenance',
				});

			let where: FindOptionsWhere<ReviewInovasiDaerah> | FindOptionsWhere<ReviewInovasiDaerah>[] = {
				status: Raw(alias => `lower(${alias}) = 'rejected'`),
			};

			if (req.pemda_id) where = { ...where, inovasi: { pemda_id: +req.pemda_id } };

			if (username) {
				const user = await this.repo.manager.getRepository(User).findOneByOrFail({ username });
				if (user && user.role && user.role.is_super_admin === 't') where = { ...where, created_by: username };
			}

			const data = await this.repo.find({
				where,
				relations: { inovasi: { profilPemda: true } },
			});

			const path = await this.exportService.download({
				data: data.map<DownloadInovasiDitolakResponse>((value, idx) => toDownloadResponse(value, idx + 1)),
				name: 'inovasi-ditolak',
			});

			return path;
		} catch (error) {
			throw error;
		}
	}

	toResponse(data: ReviewInovasiDaerah): InovasiDitolakResponse {
		let skor = 0;
		let kematangan = 0;
		switch (data.inovasi.innovation_phase.toLowerCase()) {
			case 'inisiatif':
				skor = 50;
				kematangan = 3;
				break;
			case 'uji coba':
				skor = 102;
				kematangan = 6;
				break;
			case 'penerapan':
				skor = 105;
				kematangan = 9;
				break;

			default:
				skor = 0;
				break;
		}

		let res: InovasiDitolakResponse = {
			review_inovasi_id: data.id.toString() ?? null,
			nomor: data.random_number.toString() ?? null,
			judul: data.inovasi.innovation_name ?? null,
			pemda:
				{
					pemda_id: data.inovasi.profilPemda?.id.toString() ?? null,
					pemda_name: data.inovasi.profilPemda?.nama_daerah ?? null,
				} ?? null,
			waktu_penerapan: data.inovasi.implementation_time ?? null,
			kematangan: kematangan ?? null,
			skor_verifikasi: skor ?? null,
			qc: data.status ?? null,
		};

		return res;
	}
}
