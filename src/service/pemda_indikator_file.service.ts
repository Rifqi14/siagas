import PemdaIndikatorFileRepository from 'src/repository/pemda_indikator_file.repository';
import {
  ListPemdaIndikatorFiles,
  PemdaIndikatorFileParams
} from 'src/schema/pemda_indikator_file.schema';
import { PaginationResponse, SuccessResponse } from 'src/types/response.type';
import {
  Delete,
  FormField,
  Get,
  Hidden,
  Post,
  Queries,
  Query,
  Route,
  Security,
  Tags,
  UploadedFile
} from 'tsoa';
import { PaginationResponse as PaginationResponseInterface } from '../interface/pagination.interface';
import { PaginationData } from 'src/schema/base.schema';
import pagination from 'src/providers/pagination';
import { PemdaIndikatorFile } from 'src/entity/PemdaIndikatorFile.entity';
import ApiError from 'src/providers/exceptions/api.error';
import { HttpCode } from 'src/types/http_code.enum';
import FileService from './file.service';
import File from 'src/entity/File.entity';

@Route('/profil_pemda/indikator')
@Tags(
  'Database Inovasi Daerah | Profil Pemda | Pemda Indikator | Pemda Indikator File'
)
@Security('bearer')
export default class PemdaIndikatorFileService {
  private repo = new PemdaIndikatorFileRepository();

  @Get('/{pemda_id}/{indikator_id}/files')
  async get(
    pemda_id: string,
    indikator_id: string,
    @Query('url') @Hidden() url = '',
    @Query() @Hidden() username = '',
    @Queries() req: PemdaIndikatorFileParams
  ): Promise<
    | PaginationResponse<PaginationResponseInterface, ListPemdaIndikatorFiles>
    | PaginationData<ListPemdaIndikatorFiles>
  > {
    const query = this.repo
      .createQueryBuilder(`pif`)
      .leftJoinAndSelect(`pif.file`, `f`)
      .where(`pif.pemda_id = :pemda_id and pif.indikator_id = :indikator_id`, {
        pemda_id,
        indikator_id
      });

    // if (username) {
    //   query.where(`pif.created_by = :username`, { username });
    // }

    const total = await query.getCount();
    const { limit, offset, page, order } = pagination.options(
      req.page,
      req.limit
    );

    query.limit(limit).offset(offset);

    for (const key in order) {
      query.addOrderBy(`pif.${key}`, order[key]);
    }

    const res = await query.getMany();
    let paging = pagination.build(url, page, limit, total);

    return { paging, res };
  }

  @Get('/{pemda_id}/{indikator_id}/files/{file_id}')
  async detail(
    pemda_id: number,
    indikator_id: number,
    file_id: number
  ): Promise<SuccessResponse<PemdaIndikatorFile> | PemdaIndikatorFile> {
    const res = await this.repo.findOneBy({ pemda_id, indikator_id, file_id });

    if (!res) {
      throw new ApiError({
        httpCode: HttpCode.NOT_FOUND,
        message: 'Data not found'
      });
    }

    return res;
  }

  @Post('/{pemda_id}/{indikator_id}/upload')
  async upload(
    pemda_id: string,
    indikator_id: string,
    @FormField() nomor_dokumen: string,
    @FormField() tanggal_dokumen: string,
    @FormField() tentang: string,
    @UploadedFile() dokumen: Express.Multer.File,
    @FormField() nama_dokumen?: string,
    @Query() @Hidden() username = ''
  ): Promise<SuccessResponse<PemdaIndikatorFile> | PemdaIndikatorFile> {
    const create = this.repo.create({
      nomor_surat: nomor_dokumen,
      nama_surat: tentang,
      tanggal_surat: tanggal_dokumen,
      created_by: username,
      updated_by: username,
      pemda_id: +pemda_id,
      indikator_id: +indikator_id
    });

    if (dokumen) {
      const file = await new FileService().create(
        dokumen,
        nama_dokumen,
        username
      );
      create.file = file as File;
    }

    await this.repo.insert(create);

    return create;
  }

  @Delete('/{pemda_id}/{indikator_id}/delete/{file_id}')
  async delete(
    pemda_id: number,
    indikator_id: number,
    file_id: number
  ): Promise<SuccessResponse<string> | boolean> {
    const { affected } = await this.repo.delete({
      pemda_id,
      indikator_id,
      file_id
    });

    if (affected && affected > 0) {
      return true;
    }
    return false;
  }
}
