import {
  Body,
  Delete,
  Get,
  Hidden,
  Patch,
  Post,
  Queries,
  Query,
  Route,
  Security,
  Tags
} from 'tsoa';
import { PaginationResponse, SuccessResponse } from '../types/response.type';
import { PaginationResponse as PaginationResponseInterface } from '../interface/pagination.interface';
import ApiError from '../providers/exceptions/api.error';
import { HttpCode } from '../types/http_code.enum';
import { PaginationData } from '../schema/base.schema';
import pagination from '../providers/pagination';
import DocumentCategoryRepository from '../repository/document_category.repository';
import {
  CreateDocumentCategoryRequest,
  GetDocumentCategoryRequest
} from '../schema/document_category.schema';
import DocumentCategory from '../entity/DocumentCategory.entity';

@Route('/kategori_dokumen')
@Tags('Master | Kategori Dokumen')
@Security('bearer')
export default class DocumentCategoryService {
  private repo: DocumentCategoryRepository = new DocumentCategoryRepository();

  @Post('')
  async create(
    @Body() req: CreateDocumentCategoryRequest
  ): Promise<SuccessResponse<Omit<DocumentCategory, ''>> | DocumentCategory> {
    const create = this.repo.create({
      name: req.name,
      slug: req.slug
    });

    await this.repo.insert(create);

    return create;
  }

  @Get('/{id}')
  async detail(
    id: number
  ): Promise<SuccessResponse<Omit<DocumentCategory, ''>> | DocumentCategory> {
    const res = await this.repo.findOneBy({ id: id });

    if (!res) {
      throw new ApiError({
        httpCode: HttpCode.NOT_FOUND,
        message: 'Data not found'
      });
    }

    return res;
  }

  @Get('')
  async get(
    @Query('url') @Hidden() url: string = '',
    @Queries() req: GetDocumentCategoryRequest
  ): Promise<
    | PaginationResponse<
        PaginationResponseInterface,
        Omit<DocumentCategory, 'details'>[]
      >
    | PaginationData<Omit<DocumentCategory, 'details'>[]>
  > {
    const query = this.repo
      .createQueryBuilder()
      .where(`lower(name) like lower(:q)`, {
        q: `%${req.q ?? ''}%`
      })
      .orWhere(`lower(slug) like lower(:q)`, {
        q: `%${req.q ?? ''}%`
      });

    const total = await query.getCount();
    const { limit, offset, page } = pagination.options(req.page, req.limit);

    query.limit(limit).offset(offset);

    const res = await query.getMany();
    let paging = pagination.build(url, page, limit, total);

    return { paging, res };
  }

  @Delete('/{id}')
  async delete(id: number): Promise<SuccessResponse<string> | boolean> {
    const { affected } = await this.repo.delete({ id: id });

    if (affected && affected > 0) {
      return true;
    }
    return false;
  }

  @Patch('/{id}')
  async update(
    id: number,
    @Body() req: CreateDocumentCategoryRequest
  ): Promise<SuccessResponse<string> | boolean> {
    const update = this.repo.create({
      name: req.name,
      slug: req.slug
    });
    const { affected } = await this.repo.update(id, update);

    if (!affected) {
      return false;
    }
    return true;
  }
}
