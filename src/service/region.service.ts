import Regions from '../entity/Region.entity';
import {
  Body,
  Delete,
  Get,
  Patch,
  Post,
  Queries,
  Route,
  Tags,
  Hidden,
  Query
} from 'tsoa';
import RegionRepository from '../repository/region.repository';
import { CreateRegionRequest, GetRegionRequest } from '../schema/region.schema';
import { PaginationResponse, SuccessResponse } from '../types/response.type';
import ApiError from '../providers/exceptions/api.error';
import { HttpCode } from '../types/http_code.enum';
import { PaginationResponse as PaginationResponseInterface } from '../interface/pagination.interface';
import pagination from '../providers/pagination';

@Route('/daerah')
@Tags('Master | Daerah')
export default class RegionService {
  private repo: RegionRepository;
  constructor() {
    this.repo = new RegionRepository();
  }

  @Post('')
  async create(
    @Body() req: CreateRegionRequest
  ): Promise<SuccessResponse<Regions> | Regions> {
    const regions = this.repo.create({ name: req.name });

    await this.repo.insert(regions);

    return regions;
  }

  @Get('/{id}')
  async detail(id: number): Promise<SuccessResponse<Regions> | Regions> {
    const res = await this.repo.findOneBy({ id: id });

    if (!res) {
      throw new ApiError({
        httpCode: HttpCode.NOT_FOUND,
        message: 'Data not found'
      });
    }

    return res;
  }

  @Get('')
  async get(
    @Query('url') @Hidden() url: string = 'http://localhost',
    @Queries() req: GetRegionRequest
  ): Promise<
    | PaginationResponse<PaginationResponseInterface, Array<Regions>>
    | { paging: PaginationResponseInterface; res: Array<Regions> }
  > {
    const query = this.repo
      .createQueryBuilder()
      .where(`lower(name) like lower(:name)`, { name: `%${req.name ?? ''}%` });

    const total = await query.getCount();
    const { limit, offset, page, order } = pagination.options(
      req.page,
      req.limit
    );

    query.limit(limit).offset(offset);

    for (const key in order) {
      query.addOrderBy(key, order[key]);
    }

    const res = await query.getMany();
    let paging = pagination.build(url, page, limit, total);

    return { paging, res };
  }

  @Delete('/{id}')
  async delete(id: number): Promise<SuccessResponse<string> | boolean> {
    const { affected } = await this.repo.delete({ id: id });

    if (affected && affected > 0) {
      return true;
    }
    return false;
  }

  @Patch('/{id}')
  async update(
    id: number,
    @Body() req: CreateRegionRequest
  ): Promise<SuccessResponse<string> | boolean> {
    const update = this.repo.create({ name: req.name });
    const { affected } = await this.repo.update(id, update);

    if (!affected) {
      return false;
    }
    return true;
  }
}
