export interface IHealthService {
  health(): Promise<string>;
}
